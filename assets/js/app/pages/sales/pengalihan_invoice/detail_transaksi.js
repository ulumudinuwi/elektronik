var VM_DETAIL = new Vue({
    el: '#detail_transaksi',
    data : {
        detail_member : '',
        detail_transaksi : '',
        detail_pembayaran : '',
        detail_pembelian : '',
        detail_cicilan : '',
        no_kuitansi : '',
        master_cara_bayar: [
            {id: 'kartu_kredit', text: 'KARTU KREDIT'},
            {id: 'kartu_debit', text: 'KARTU DEBIT'},
            {id: 'transfer', text: 'TRANSFER'},
            {id: 'tunai', text: 'TUNAI'},
        ],
        master_ekspedisi: [
            {id: 'JNE', text: 'JNE'},
            {id: 'POS Indonesia', text: 'POS Indonesia'},
            {id: 'J&T', text: 'J&T'},
            {id: 'SiCepat', text: 'SiCepat'},
            {id: 'Ninja Express', text: 'Ninja Express'},
            {id: 'TiKi', text: 'TiKi'},
            {id: 'First Logistics', text: 'First Logistics'},
            {id: 'Indah Logistik', text: 'Indah Logistik'},
            {id: 'Wahana Logistik', text: 'Wahana Logistik'},
            {id: 'Pandu Logistik', text: 'Pandu Logistik'},
            {id: 'RPX', text: 'RPX'},
            {id: 'Cahaya Logistik', text: 'Cahaya Logistik'},
            {id: 'GO-SEND', text: 'GO-SEND'},
            {id: 'Grab Express', text: 'Grab Express'},
        ],
        master_bank : [],
        master_edc : [],  
        invoice_url: '',
        tanda_terima_url: '',
        kuitansi_url: '',
        disabled_button: false,
        batas_pembayaran : [],
        pembayaran : {
            total : '',
            type_disc: 'persen',
            disc  : 0,
            piutang : false,
            point_didapat : '',
            grand_total : '',
            metode_pembayarans : [],
            is_kurir : false, 
            is_free : false,
            ekspedisi: '',
            ongkir: 0,
            total_pembayaran : 0,
            sisa_pembayaran : 0,
            mustApprove : '',
        },
        uang_diterima: 0,
    },
    methods: {
        find_cara_bayar(find){
            let found = this.master_cara_bayar.find(function(value, key) {
                // console.log('cara_bayar', value.id);
                return value.id == find;
            });

            return found ? found : false;
        },
        find_bank(find){
            let found = this.master_bank.find(function(value, key) {
                return value.id == find;
            });

            return found ? found.nama : '-';
        },
        find_edc(find){
            let found = this.master_edc.find(function(value, key) {
                return value.id == find;
            });

            return found ? found.nama : '-';
        },
        getDetailPos(){
            let urlParams = new URLSearchParams(window.location.search);
            let uid = urlParams.get('uid');
            let dis = urlParams.get('dis');
            if(dis) this.disabled_button = true;

            axios.post(`${this.global_url.api.sales.pos.get_detail_pos}?uid=${uid}`, )
            .then(response => {
                let data = response.data;
                this.detail_member = data.detail_member;
                this.detail_transaksi = data.detail_transaksi;
                this.detail_pembayaran = data.detail_pembayaran;
                this.detail_pembelian = data.detail_pembelian;
                this.detail_cicilan = data.detail_cicilan;
                this.no_kuitansi = data.no_kuitansi;
                this.invoice_url = `${this.global_url.api.sales.pos.print_invoice}?uid=${this.detail_transaksi.uid}`;
                this.tanda_terima_url = `${this.global_url.api.sales.pos.print_tanda_terima}?uid=${this.detail_transaksi.uid}`;
                this.kuitansi_url = `${this.global_url.api.sales.pos.print_kuitansi}?uid=${this.detail_transaksi.uid}`;
                if (data.detail_transaksi.status_pembayaran == 2) {
                    $('.belum_lunas').addClass('hide');
                    $('.lunas').removeClass('hide');
                }else{
                    $('.belum_lunas').removeClass('hide');
                    $('.lunas').addClass('hide');
                }
                VM_PAGE_HEADER.title = 'Detail Transaksi '+this.detail_transaksi.no_invoice;
            }).catch(xhr => {
               alert('Server Bermasalah')
            })     
        },
        get_master_bank() {
            axios.post(this.global_url.api.master.bank.list_data)
            .then(response => {
                //console.log('bank',response);
                this.master_bank = this.mapOption(response.data.list);
            }).catch(xhr => {
                //console.log(xhr)
            })
        },
        get_master_edc() {
            axios.post(this.global_url.api.master.mesin_edc.list_data)
            .then(response => {
                //console.log('edc',response);
                this.master_edc = this.mapOption(response.data.list);
            }).catch(xhr => {
                //console.log(xhr)
            })
        },
        total_pembayaran_per_cicilan(cicilan){
            let total = 0;
            //console.log('cicilan ke',cicilan)
            if(this.detail_cicilan[cicilan].length > 0){
                for(item of this.detail_cicilan[cicilan]){
                    //console.log(item)
                    total += parseFloat(item.nominal_pembayaran);
                }
            }            
            return total;
        },
        cicilanKe(title){
            return title.replace('_',' ').replace('_','-').toUpperCase();
        },
        text_cicilan(str){
            return str.replace('_',' ').replace('_','-');
        },
        getKetCicilan(cicilan){
            let text = 'Pembayaran '+this.cicilanKe(cicilan)+' No.Invoice - '+this.detail_transaksi.no_invoice;
            return text;
        },
        getKetTotal(){
            let text = 'Pembayaran Kontan No.Invoice - '+this.detail_transaksi.no_invoice;
            return text;
        },
        kuitansi_proc(nominal, ket, no_kuitansi, sisa_pembayaran){
            return `${this.kuitansi_url}&nominal=${nominal}&ket=${ket}&no_kuitansi=${no_kuitansi}&sisa_pembayaran=${sisa_pembayaran}`;
        },
        setKurir(){
            this.pembayaran.ongkir = 0;
        },
        addPembayaran(){
            let n_p = {
                'cara_bayar' :'',
                'receipt_no' :'',
                'bank_id': '',
                'mesin_edc_id': '',
                'nominal': 0,
            };

            let batas_baru = {
                currencySymbol: 'Rp. ',
                minimumValue: 0,
                emptyInputBehavior: 'min',
                currencySymbolPlacement: 'p',
                decimalCharacter: ',',
                decimalPlacesShownOnFocus: 2,
                digitGroupSeparator: '.',
                minimumValue: '0',
                maximumValue: this.sisapembayaran,
            };
            this.batas_pembayaran.push(batas_baru);
            this.pembayaran.metode_pembayarans.push(n_p);
        },
        setPiutang(){
            let batas_baru = {
                currencySymbol: 'Rp. ',
                minimumValue: 0,
                emptyInputBehavior: 'min',
                currencySymbolPlacement: 'p',
                decimalCharacter: ',',
                decimalPlacesShownOnFocus: 2,
                digitGroupSeparator: '.',
                minimumValue: '0',
                maximumValue: this.sisapembayaran,
            };
            this.batas_pembayaran.push(batas_baru);
            this.pembayaran.metode_pembayarans.push();
        },
        removePembayaran(i){
            if(this.pembayaran.metode_pembayarans.length == 1)return false;
            this.pembayaran.metode_pembayarans.splice(i,1);
            this.batas_pembayaran.splice(i,1);
        },
        mapOption(array){
            if(typeof array == 'object'){
                let change = array.map(function(item){
                    return {
                        id: item.id,
                        text: item.nama,
                    }
                });

                return change;
            }
        },
        validasiPembayaran(){
            let filter = this.pembayaran.metode_pembayarans.filter((item)=>{
                if(item.cara_bayar == 'tunai'){
                    if(item.cara_bayar == 'tunai' && item.nominal == 0){
                        return false;
                    }else{
                        return true;
                    } 
                // }else if(item.cara_bayar != 'tunai' || item.cara_bayar == ''){
                //     if(item.nominal == 0 || !item.receipt_no || !item.bank_id || !item.mesin_edc_id){
                //         return true;
                //     }else{
                //         return false;
                //     } 
                // }
                }else if(item.cara_bayar != 'tunai' || item.cara_bayar == ''){
                    if(item.nominal == 0 || !item.bank_id ){
                        return false;
                    }else{
                        return true;
                    } 
                }
            });
            if(!this.pembayaran.piutang && filter.length == 0){
                return false;
            }else{
                return true;
            }
        },
        validasiKurir(){
            if(this.pembayaran.is_kurir){
                if(!this.pembayaran.ekspedisi) return false;
            }
            return true;
        },
        onProcess(){         
                Sweet.fire({
                    title: 'Transaksi akan dilanjut?',
                    text: "Pastikan informasi yang ada masukan sudah benar !",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '<b><i class="icon-checkmark4"></i></b> Ya',
                    cancelButtonText: '<b><i class="icon-cancel-square"></i></b> Tidak',
                    // reverseButtons: true
                }).then((result) => {
                    if (result.value) {
                        global.method.vueBlockUI();
                        let formData = new FormData();
                        formData.append('status', '1');
                        formData.append('id', this.detail_transaksi.id);
                        formData.append('uid', this.detail_transaksi.uid);
                        formData.append('new_sales_id', $('#id_sales').val());
                        axios.post(this.global_url.api.sales.pengalihan_invoice.proccess_order, formData)
                        .then(response => {
                            //console.log(response);
                            $.unblockUI();
                            Swal.fire({
                                type: 'success',
                                title: response.data.message,
                            });
                            setTimeout(()=>{
                                window.location.href = this.global_url.page.sales.pengalihan_invoice.index;
                            },2000)
                        }).catch(xhr => {
                            //console.log(xhr);
                            Swal.fire({
                                type: 'error',
                                title: 'Terjadi kesalahan',
                            });
                            $.unblockUI();
                        })     
                    }
                });
        },
    },
    computed: {
        total_harga_barang() {
            let total = 0;
            if(this.detail_pembelian.length > 0){
                for(item of this.detail_pembelian){
                    total += parseFloat(item.grand_total);
                }
            }      
            return total;
        },
        /*total_pembayaran() {
            let total = 0;
            if(this.detail_pembayaran.length > 0){
                for(item of this.detail_pembayaran){
                    total += parseFloat(item.nominal_pembayaran);
                }
            }            
            return total;
        },*/
        total_pembayaran() {
            let total = 0;
            if(this.pembayaran.metode_pembayarans.length > 0){
                for(item of this.pembayaran.metode_pembayarans){
                    total += parseFloat(item.nominal);
                }
            }            
            this.pembayaran.total_pembayaran = total;
            return total;
        },
        total_pembayaran_cicilan() {
            let total = 0;
            if(this.detail_pembayaran.length > 0){
                for(item of this.detail_pembayaran){
                    total += parseFloat(item.nominal_pembayaran);
                }
            }            
            return total;
        },
        grand_total(){
            let grand_total = (parseFloat(this.detail_transaksi.total_harga)) + parseFloat(this.pembayaran.ongkir);

            this.detail_transaksi.grand_total = grand_total;
            // console.log('test',grand_total)
            if(grand_total > 0) {
                this.pembayaran.is_free = false;
                return grand_total;
            }    
            if(grand_total == 0) {
                this.pembayaran.is_free = true;
                return 0;
            }
        },
       /* sisapembayaran(){
            let sisa_pembayaran = this.detail_transaksi.grand_total - this.detail_transaksi.total_pembayaran;
            this.detail_transaksi.sisa_pembayaran = sisa_pembayaran;
            // this.batas_pembayaran.maximumValue = sisa_pembayaran;
            return sisa_pembayaran;
        },*/
         sisapembayaran(){
            let sisa_pembayaran = this.detail_transaksi.grand_total - this.pembayaran.total_pembayaran;
            this.pembayaran.sisa_pembayaran = sisa_pembayaran;
            // this.batas_pembayaran.maximumValue = sisa_pembayaran;
            return sisa_pembayaran;
        },
        bayar_tunai_only(){
            let total = 0;
            if(this.pembayaran.metode_pembayarans.length > 0){
                for(item of this.pembayaran.metode_pembayarans){
                    if(item.cara_bayar == "tunai")total += parseFloat(item.nominal);
                }
            } 
            return total;           
        },
        kembalian(){
            let total = this.bayar_tunai_only;
            if(this.uang_diterima && this.uang_diterima > total) return this.uang_diterima - total;
            if(this.uang_diterima && this.uang_diterima < total) return 'Uang diterima tidak cukup';
            if(!this.uang_diterima) return 0;
        },
    },
    watch: {
        'pembayaran.grand_total': function(newValue, oldValue){
            //console.log('Watch > grand total :','new ',newValue, 'old ',oldValue)
            
            // let sisa_pembayaran = this.pembayaran.grand_total - this.total_pembayaran;

            if(newValue != oldValue){
                this.pembayaran.metode_pembayarans = [];
                this.batas_pembayaran = [];
                this.batas_pembayaran.push({
                    currencySymbol: 'Rp. ',
                    minimumValue: 0,
                    emptyInputBehavior: 'min',
                    currencySymbolPlacement: 'p',
                    decimalCharacter: ',',
                    decimalPlacesShownOnFocus: 2,
                    digitGroupSeparator: '.',
                    minimumValue: '0',
                    maximumValue: this.sisapembayaran,
                });
                this.pembayaran.metode_pembayarans.push({
                    'cara_bayar' : '',
                    'receipt_no' : '',
                    'bank_id': '',
                    'mesin_edc_id': '',
                    'nominal': 0,
                });
            }
        },
    },
    created() {
        this.getDetailPos();
        this.get_master_bank();
        this.get_master_edc();
        this.batas_ongkir = {
            currencySymbol: 'Rp. ',
            minimumValue: 0,
            emptyInputBehavior: 'min',
            currencySymbolPlacement: 'p',
            decimalCharacter: ',',
            decimalPlacesShownOnFocus: 2,
            digitGroupSeparator: '.',
            minimumValue: '0',
        };
    },
});

let status = '1';

$('body').find(".change-sales").on('click', function(){
    $.getJSON(loadSales.replace(':STATUS', status), function(res, status) {
        if(status === 'success') {
            let data = res.data;
            if(data.length != '') {
                $('body').find('#table-sales tbody').empty();
                for (var i = 0; i < data.length; i++) {
                    fillSales(data[i], (i+1));
                }
                $('body').find('.div-data').show();
            } else $('body').find('.div-no_data').show();
        }
    });
    $('#modal_sales').modal({backdrop: 'static', keyboard: false},'show');
});

let fillSales = (obj, no) => {
    let tbody = $('#table-sales tbody'),
        button = '<a class="click" data-id="' + obj.id + '" data-uid="' + obj.uid + '" data-nama="' + obj.nama + '">'+ obj.nama +'</a>';

    if(tbody.children('.tr-data').length <= 0) 
        tbody.empty();

    let tr = $("<tr/>")
        .addClass('tr-data')
        .appendTo(tbody);

    let tdTanggal = $("<td/>")
        .html(button)
        .appendTo(tr);

    let tdKode = $("<td/>")
        .html(obj.no_hp ? obj.no_hp : "&mdash;")
        .appendTo(tr);

    let tdDokterperujuk = $("<td/>")
        .html(obj.alamat ? obj.alamat : "&mdash;")
        .appendTo(tr);
}

$('body').find('#table-sales').on("click", ".click", function () {
    var id = $(this).data('id');
    var uid = $(this).data('uid');
    var nama = $(this).data('nama');

    $('body').find('#nama_sales').html(nama);
    $('body').find('#id_sales').val(id);
    $('body').find('.simpan').attr('disabled', false);
    
    $('#modal_sales').modal('hide');
});

if ($('body').find('#id_sales').val() == "") {
    $('body').find('.simpan').attr('disabled', true);
}