var FORM_MANAGER_SALES = new function(){
    this.el = {
        'form' : '#myForm',
        'uid' : '[name="uid"]',
    };
    this.data = {

    };
    this.method = {
        store(formData) {
            provider.service.api.master.manager_sales.store(formData).done((res) => {
                let message = res.message;
                console.log(message)
                successMessage('Berhasil',message);
                window.location.href = provider.url.page.master.manager_sales.index;
            }).fail((xhr) => {
                $('.input-rupiah').each(function(i, obj){
                    $(obj).autoNumeric('set', $(obj).val());
                });
                console.log(xhr);
                $.unblockUI();
                global.method._throw_error.form_error(xhr);
                $('button').prop('disabled',false);
            })
        },
        update(formData) {
            provider.service.api.master.manager_sales.update(formData).done((res) => {
                let message = res.message;
                console.log(message)
                successMessage('Berhasil',message);
                window.location.href = provider.url.page.master.manager_sales.index;
            }).fail((xhr) => {
                $('.input-rupiah').each(function(i, obj){
                    $(obj).autoNumeric('set', $(obj).val());
                });
                console.log(xhr);
                $.unblockUI();
                global.method._throw_error.form_error(xhr);
                $('button').prop('disabled',false);
            })
        },
    };
    this.onCreate = new function(){
        this.setup = () => {
            $(FORM_MANAGER_SALES.el.form).on('submit', (e) => {
                e.preventDefault();
                $('.input-rupiah').each(function(i, obj){
                    $(obj).val($(obj).autoNumeric('get'));
                });
                $('button').prop('disabled',true);
                global.method.blockUI();
                global.method.clear_error(e.target);
                let el_form = $(FORM_MANAGER_SALES.el.form);
                let formData = new FormData(el_form[0]);
                let uid = $('[name="uid"]').val();
                //unmask
                if(!uid) {
                    FORM_MANAGER_SALES.method.store(formData);
                }else{
                    FORM_MANAGER_SALES.method.update(formData);
                }
            })
        };
    };
    this.onCreated = () => {
        FORM_MANAGER_SALES.onCreate.setup();
    };
}

$(() => {
    FORM_MANAGER_SALES.onCreated();
    $(".input-rupiah").autoNumeric('init', {
        aSep: '.',
        aDec: ',',
        // aSign: 'Rp. ',
        pSign: 'p',
    });
})