var the_table;
$unitusaha      = $('#unit_usaha');
$bulan          = $('#bulan');
$tahun          = $('#tahun');
var page_url    = '/api/hrd/payroll';
$(function(){

    $unitusaha.change(function(){
        unitusaha   = $unitusaha.val();
        bulan  = $bulan.val();
        tahun   = $tahun.val();
        get_data(bulan,tahun,unitusaha);
    });

    $bulan.change(function(){
        unitusaha   = $unitusaha.val();
        bulan  = $bulan.val();
        tahun   = $tahun.val();
        get_data(bulan,tahun,unitusaha);
    });

    $tahun.change(function(){
        unitusaha   = $unitusaha.val();
        bulan  = $bulan.val();
        tahun   = $tahun.val();
        get_data(bulan,tahun,unitusaha);
    });

    get_data();
});

function view_detail(element){
    console.log($(element).attr('href'));
    var url = $(element).attr('href');
    $.ajax({
        'url':url,
        'type':'POST',
        'dataType':'html',
        'success': function(data){
            console.log(data);
            $('#detail_modal .modal-dialog').html(data);
            $('#detail_modal').modal('show');
        }
    });
    return false;
}

function nama_bulan(bln)
{
    bulan = [];
    bulan['01'] = 'Januari';
    bulan['02'] = 'Februari';
    bulan['03'] = 'Maret';
    bulan['04'] = 'April';
    bulan['05'] = 'Mei';
    bulan['06'] = 'Juni';
    bulan['07'] = 'Juli';
    bulan['08'] = 'Agustus';
    bulan['09'] = 'September';
    bulan['10'] = 'Oktober';
    bulan['11'] = 'November';
    bulan['12'] = 'Desember';
    return bulan[bln];
}

function get_data(bulan,tahun,unitusaha){
    console.log('run');
    if(typeof the_table != 'undefined'){
        the_table.destroy();
    }
    the_table = $("#dataTable_lowongan").DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+page_url,
            "type": "POST",
            "dataType":"json",
            "data": {bulan:bulan,tahun:tahun,unit_usaha:unitusaha}
        },
        "columns": [
            {
                "data": "unit_usaha_nama",
                "searchable": false
            },
            {
                "data": "bulan",
                "render": function(data){
                    return nama_bulan(data);
                }
            },
            {
                "data": "tahun",
            },
            {
                "data": "jumlah_personil"
            },
            {
                "data": "status_kelengkapan"
            },
            {
                "data": "aksi",
                "render": function(data, type, row, meta) {
                    return '<a class="btn btn-xs btn-default" href="'+base_url+'/hrd/payroll/view/' + row.uid + '" title="Lihat Daftar Slip"><i class="icon-list3"></i></a> '
                    + '<a target="_blank" class="btn btn-xs btn-primary" href="'+base_url+'/hrd/payroll/print_slip_gaji_all/' + row.uid + '" title="Cetak Semua Slip Gaji"><i class="icon-printer4"></i></a>';
                }
            }

        ]
    });
    /*the_table.on( 'order.dt search.dt stateLoaded.dt', function () {
        the_table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();*/
}
