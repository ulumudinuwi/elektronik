$formSkill     = $('#form-skill');
$notifKualifikasi   = $('#notif-kualifikasi');
$modalSkill    = $('#add_skill_modal');

$(function(){
    $formSkill.validate({
        submitHandler: function(form) {
            var dataForm    = $(form).serializeArray();
            var target_url  = page_url+'save_skill/'+pelamar_id;
            var data        = convert_array_to_object(dataForm);
            console.log(data);
            // save_skill(target_url,data);
            MyModel.insert(target_url,data,function(data){
                console.log(data);
                Skill.getSkill();
                $modalSkill.modal('hide');
                $notifKualifikasi.text(data.info);
                $notifKualifikasi.fadeIn('slow').delay('3500').fadeOut('slow');
                $formSkill[0].reset();
            });
            return false;
        }
    });
});

var Skill = {
    getSkill: function() {
        var page_url    = '/api/hrd/rekrutment/pelamar/get_skill/'+pelamar_id;
        var columns     = [
            {
                "data": "skill",
            },
            {
                "data": "level",
            },
            {
                "data": "pengalaman",
            },
            {
                "data": "action",
                "render": function(data, type, row, meta) {
                    return '<span style="cursor:pointer" class="text-danger text-center" href="'+base_url+'/api/hrd/rekrutment/pelamar/hapus_skill/' + row.uid + '" onClick="Skill.deleteSkill(this)"><i class="fa fa-trash"></i></span>';
                },
                "sortable":false
            }
        ];
        MyModel.page_url = page_url;
        MyModel.get('tabel-skill',columns);
    },
    deleteSkill: function(element){
        console.log($(element).attr('href'));
        var url = $(element).attr('href');
        MyModel.delete(url,function(data){
            console.log(data);
            Skill.getSkill();
            $notifKualifikasi.text(data.info);
            $notifKualifikasi.fadeIn('slow').delay('3500').fadeOut('slow');
        });
        return false;
    },
    tambahSkill: function(){
        $modalSkill.modal('show');
    }
}
