app.controller('ProfilePegawaiIndexController', ['$scope', 'api', '$http', 'DTOptionsBuilder', 'DTColumnBuilder', 'DTColumnDefBuilder', '$compile', 'toastr', '$q', '$uibModal',
    function($scope, api, $http, DTOptionsBuilder, DTColumnBuilder, DTColumnDefBuilder, $compile, toastr, $q, $uibModal, $location) {


        $scope.data = {};
        $scope.umum = {};
        $scope.alamat = {};
        $scope.dataPendidikan = {};
        $scope.dataPekerjaan = {};
        $scope.dataRujukan = {};

        $scope.dataHubunganPasien = {};
        $scope.profile = {};
        $scope.dataPegawai = {};


        $scope.data = {
            tanggalDari: moment().format('DD/MM/YYYY'),
            tanggalSampai: moment().format('DD/MM/YYYY')
        };



        function initData() {
            $q.all([
               
                $http.get(base_url + "/api/master/identitas"),
                $http.get(base_url + "/api/master/status_kawin"),
                $http.get(base_url + "/api/master/agama/hrd"),
                $http.get(base_url + "/api/master/hubungan_pasien"),
                $http.get(base_url + "/api/hrd/daftar_pegawai/profile/" + uid.trim())


            ]).then(function(results) {
             
                $scope.dataIdentitas = angular.copy(results[0].data);
                $scope.data.identitasId = angular.copy($scope.dataIdentitas)[_.findIndex(angular.copy($scope.dataIdentitas), {
                    id: "1"
                })]; //default id kepulauan bangka belitun

                $scope.dataStatusKawin = angular.copy(results[1].data);
                $scope.dataAgamaUmum = angular.copy(results[2].data);
                $scope.dataHubunganPasien = angular.copy(results[3].data);
                $scope.profile = angular.copy(results[4].data);
                $scope.dataPegawai = [];
             
                mappingData($scope.profile);

            }).catch(function(err) {
                console.log(err);
            });
        }

        function resetData() {
            $scope.data.pekerjaanId = {};
            $scope.data.pegawaiId = {};
            $scope.data.pendidikanId = {};
        }

        function mappingData($dataPegawai) {
            console.log("masuk mapping");

            // Scope UMUM
            $scope.umum.nama = $dataPegawai.nama;
            $scope.umum.panggilan = $dataPegawai.nama_panggilan;
            $scope.umum.jenisKelamin=$dataPegawai.jenis_kelamin;
            $scope.umum.tempatLahir=$dataPegawai.tempat_lahir;
            $scope.umum.tanggalLahir=$dataPegawai.tgl_lahir;
            $scope.umum.tanggalLahir=moment().format('DD/MM/YYYY');
            $scope.umum.statusKawin=$scope.dataStatusKawin[$dataPegawai.status_kawin];

            $scope.umum.kelamin = $dataPegawai.jenis_kelamin;
           // $scope.umum.kewarganegaraan= ;
           // $scope.umum.jenisIdentitas;
            $scope.umum.noIdentitas=$dataPegawai.no_ktp;
           // $scope.umum.suku;
            $scope.umum.agama=  $scope.dataAgamaUmum[$dataPegawai.agama];
            $scope.umum.noNPWP=$dataPegawai.npwp;
            $scope.umum.kewarganegaraan = 1;
            $scope.umum.jabatan = $dataPegawai.jabatan;

            // END SCOPE UMUM
        }

        $scope.iniDataUmum = function() {
          initData();
        };

        $scope.initDataAlamat = function() {

            $q.all([
                $http.get(base_url + "/api/master/wilayah/provinsi"),
                $http.get(base_url + "/api/master/wilayah/kota/", {
                    params: {
                        'provinsiId': "9" // default kepulauan bangka belitung
                    }
                }),             
                $http.get(base_url + "/api/hrd/daftar_pegawai/profileAlamat/" + uid.trim())


            ]).then(function(results) {
                $scope.alamatProvinsi = angular.copy(results[0].data);
                $scope.alamatKota = angular.copy(results[1].data);
               
                $scope.alamat.provinsi = angular.copy($scope.alamatProvinsi)[_.findIndex(angular.copy($scope.alamatProvinsi), {
                    id: "9"
                })]; //default id kepulauan bangka belitun 
               
                $scope.alamat.kota = angular.copy($scope.alamatKota)[_.findIndex(angular.copy($scope.alamatKota), {
                    id: "179"
                })]; //default id kepulauan bangka belitun
                $scope.dataAlamat = angular.copy(results[2].data);
                  $scope.alamat.alamat=  $scope.dataAlamat.alamat ;
                   $scope.alamat.kodepos =  $scope.dataAlamat.kodepos;
                   $scope.alamat.telepon = $scope.dataAlamat.home_phone;
                   $scope.alamat.nohp =  $scope.dataAlamat.mobile_phone;
                   $scope.alamat.email =  $scope.dataAlamat.email;
            
            }).catch(function(err) {
                console.log(err);
            });
          
            // $http.get(base_url + '/api/hrd/daftar_pegawai/profileAlamat/' + uid.trim())
            //     .success(function(data, status, headers, config) {
            //         console.log(data);
            //        $scope.alamat.alamat= data.alamat ;
            //        $scope.alamat.kodepos = data.kodepos;
            //        $scope.alamat.telepon = data.home_phone;
            //        $scope.alamat.nohp = data.mobile_phone;
            //        $scope.alamat.email = data.email;
            //     })
            //     .error(function(data, status, headers, config) {
            //         console.log("Errrrrororororr");
            //         console.log(data);
            //     });
        };


        $scope.initDataPekerjaan = function() {
            // console.log(angular.copy($scope.data));
          // console.log("ini init  data pekerjaan");
           
            // var vm = this;
            // vm.rtrw = {};
            // console.log("ini uid");
            // console.log(uid.trim());
            // $http.get(base_url + '/api/hrd/daftar_pegawai/profile/' + uid.trim())
            //     .success(function(data, status, headers, config) {
            //         console.log("ininininininini dadtatatdatatdatdtadata");
            //         console.log(data);
            //         console.log("tttttttttttttt dadtatatdatatdatdtadata");
            //     })
            //     .error(function(data, status, headers, config) {
            //         console.log("Errrrrororororr");
            //         console.log(data);
            //     });
        };


        $scope.initDataPassport = function() {
            // console.log(angular.copy($scope.data));
         //   alert ("ini data passport");
            // var vm = this;
            // vm.rtrw = {};
            // console.log("ini uid");
            // console.log(uid.trim());
            // $http.get(base_url + '/api/hrd/daftar_pegawai/profile/' + uid.trim())
            //     .success(function(data, status, headers, config) {
            //         console.log("ininininininini dadtatatdatatdatdtadata");
            //         console.log(data);
            //         console.log("tttttttttttttt dadtatatdatatdatdtadata");
            //     })
            //     .error(function(data, status, headers, config) {
            //         console.log("Errrrrororororr");
            //         console.log(data);
            //     });
        };


        $scope.initDataKontak = function() {
            // console.log(angular.copy($scope.data));
            var vm = this;
            vm.rtrw = {};
            console.log("ini uid");
            console.log(uid.trim());
            $http.get(base_url + '/api/hrd/daftar_pegawai/profile/' + uid.trim())
                .success(function(data, status, headers, config) {
                    console.log("ininininininini dadtatatdatatdatdtadata");
                    console.log(data);
                    console.log("tttttttttttttt dadtatatdatatdatdtadata");
                })
                .error(function(data, status, headers, config) {
                    console.log("Errrrrororororr");
                    console.log(data);
                });
        };


        $scope.initDataTanggungan = function() {
            // console.log(angular.copy($scope.data));
            var vm = this;
            vm.rtrw = {};
            console.log("ini uid");
            console.log(uid.trim());
            $http.get(base_url + '/api/hrd/daftar_pegawai/profile/' + uid.trim())
                .success(function(data, status, headers, config) {
                    console.log("ininininininini dadtatatdatatdatdtadata");
                    console.log(data);
                    console.log("tttttttttttttt dadtatatdatatdatdtadata");
                })
                .error(function(data, status, headers, config) {
                    console.log("Errrrrororororr");
                    console.log(data);
                });
        };


        $scope.initDataKeanggotaan = function() {
            // console.log(angular.copy($scope.data));
            var vm = this;
            vm.rtrw = {};
            console.log("ini uid");
            console.log(uid.trim());
            $http.get(base_url + '/api/hrd/daftar_pegawai/profile/' + uid.trim())
                .success(function(data, status, headers, config) {
                    console.log("ininininininini dadtatatdatatdatdtadata");
                    console.log(data);
                    console.log("tttttttttttttt dadtatatdatatdatdtadata");
                })
                .error(function(data, status, headers, config) {
                    console.log("Errrrrororororr");
                    console.log(data);
                });
        };

        initData();


        $scope.save = function() {
            console.log(angular.copy($scope.data));
        };

        // $scope.saveUmum = function () {
        //     console.log($scope.umum);
        //    // console.log(umum1.nama);
        // };
        // $scope.selected = function (item) {
        //     console.log("masuk selected");
        //     if (item) {
        //         var obj = angular.copy(item.originalObject);
        //         $scope.data = obj;
        //         $scope.data.tanggal = new Date($scope.data.tanggal);
        //         $scope.$emit('selectedPasien', obj);
        //         console.log(obj);
        //         // $scope.reload();
        //         buildTable(item.originalObject.no_rekam_medis);
        //     }

        // };
        //  $scope.selected = function (item) {
        //     console.log("masuk");
        //     if (item) {
        //         resetKelompokPasien();
        //         $scope.$emit(EVENT.OLD_PATIENT);
        //         $scope.isBiodataDisabled = true;
        //         // $scope.isTambah = false;
        //         var obj = item.originalObject;
        //         $scope.data = angular.copy(obj);
        //         $scope.data.tanggal = new Date($scope.data.tanggal);
        //         $scope.alamat.provinsi = angular.copy($scope.dataProvinsi)[_.findIndex(angular.copy($scope.dataProvinsi), {id: obj.provinsi_id})];
        //         $scope.data.identitasId = angular.copy($scope.dataIdentitas)[_.findIndex(angular.copy($scope.dataIdentitas), {id: obj.identitas_id})];
        //         $scope.data.noIdentitas = obj.no_identitas;
        //         $scope.data.telepon1 = obj.no_telepon_1;
        //         $scope.data.tempatlahir = obj.tempat_lahir;
        //         $scope.data.tanggallahir = moment(obj.tgl_lahir, "YYYY-MM-DD").format("DD/MM/YYYY");
        //         $scope.data.umur = moment().diff(moment(obj.tgl_lahir, "YYYY-MM-DD").format(), 'years');
        //         $scope.data.golonganDarah = obj.gol_darah_id;
        //         $scope.data.agamaId = angular.copy($scope.dataAgama)[_.findIndex(angular.copy($scope.dataAgama), {id: obj.agama_id})];
        //         $scope.data.statusKawin = angular.copy($scope.dataStatusKawin)[_.findIndex(angular.copy($scope.dataStatusKawin), {id: obj.status_kawin_id})];
        //         $scope.data.pendidikanId = angular.copy($scope.dataPendidikan)[_.findIndex(angular.copy($scope.dataPendidikan), {id: obj.pendidikan_id})];
        //         $scope.data.pekerjaanId = angular.copy($scope.dataPekerjaan)[_.findIndex(angular.copy($scope.dataPekerjaan), {id: obj.pekerjaan_id})];

        //         $scope.data.titleId = angular.copy($scope.dataTitle)[_.findIndex(angular.copy($scope.dataTitle), {id: obj.title_id})];
        //         $scope.data.kelompokpasienId = angular.copy($scope.dataKelompokPasien)[_.findIndex(angular.copy($scope.dataKelompokPasien), {id: "2"})]; //default id kepulauan bangka belitun
        //         $scope.data.jenisKelamin = obj.jenis_kelamin;
        //         $scope.data.kewarganegaraan = obj.kewarganegaraan;
        //         $scope.data.golonganDarah = obj.gol_darah_id;
        //         $scope.data.isOldPasien = true;

        //         $scope.data.rujukanId = angular.copy($scope.dataRujukan)[2];


        //         $q.all([
        //             $http.get(base_url + "/api/master/wilayah/search/", {
        //                 params: {
        //                     'id': obj.kabupaten_id
        //                 }
        //             }),
        //             $http.get(base_url + "/api/master/wilayah/search/", {
        //                 params: {
        //                     'id': obj.kecamatan_id
        //                 }
        //             }),
        //             $http.get(base_url + "/api/master/wilayah/search/", {
        //                 params: {
        //                     'id': obj.kelurahan_id
        //                 }
        //             }),
        //             $http.get(base_url + "/api/master/tindakan/init", {
        //                 params: {
        //                     status_pasien: 'lama'
        //                 }
        //             })
        //         ]).then(function (results) {
        //             console.log(results);

        //             $scope.alamatKota = results[0].data;
        //             $scope.alamatKecamatan = results[1].data;
        //             $scope.alamatDesa = results[2].data;

        //             $scope.alamat.kota = angular.copy($scope.dataKota)[0];
        //             $scope.alamat.kecamatan = angular.copy($scope.dataKecamatan)[0];
        //             $scope.alamat.desa = angular.copy($scope.dataDesa)[0];
        //             $scope.dataTindakan = results[3].data.tarif;
        //             $scope.tarifTotal = results[3].data.total;

        //         });

        //     } else {
        //         $scope.isBiodataDisabled = false;
        //     }
        // };
        $scope.onChangeProvinsi = function(provinsi) {

            $http.get(base_url + "/api/master/wilayah/kota/", {
                params: {
                    'provinsiId': provinsi.id
                }
            }).then(
                function success(result) {
                    $scope.alamatKota = _.map(result.data, function(data) {
                        return {
                            id: data.id,
                            nama: data.nama
                        }
                    });
                    $scope.$broadcast('change::select2');

                },
                function error(err) {
                    //console.log(err);
                })
        };

        $scope.onChangeKota = function(kota) {
            var defer = $q.defer();

            if (kota) {
                $http.get(base_url + "/api/master/wilayah/kecamatan/", {
                    params: {
                        'kotaId': kota.id
                    }
                }).then(
                    function success(result) {
                        $scope.alamatKecamatan = _.map(result.data, function(data) {
                            return {
                                id: data.id,
                                nama: data.nama
                            }
                        });
                    },
                    function error(err) {
                        defer.reject(err);
                    });

            }


        };

        $scope.onChangeKecamatan = function(kecamatan) {
            var defer = $q.defer();
            if (kecamatan) {


                $http.get(base_url + "/api/master/wilayah/kelurahan/", {
                    params: {
                        'kecamatanId': kecamatan.id
                    }
                }).then(
                    function success(result) {
                        //console.log("desa");
                        //console.log(result);
                        $scope.alamatDesa = _.map(result.data, function(data) {
                            return {
                                id: data.id,
                                nama: data.nama
                            }
                        });
                    },
                    function error(err) {
                        //console.log(err);
                        defer.reject(err);
                    })
            }
        };

        $scope.onChangeDesa = function(desa) {
            var defer = $q.defer();
            if (desa) {
                $http.get(base_url + "/api/master/wilayah/search/", {
                    params: {
                        'id': desa.id
                    }
                }).then(
                    function success(result) {
                        //console.log("desa");
                        console.log(result);
                        $scope.alamat.kodepos = angular.copy(result.data[0].kodepos);
                        // $scope.dataDesa = angular.copy(result.data);
                    },
                    function error(err) {
                        //console.log(err);
                        defer.reject(err);
                    })
            }
        };




        $scope.onChangeRangeWaktu = function(range) {
            var tanggalDari = moment(range.substr(0, 10)).format('YYYY-MM-DD'),
                tanggalSampai = moment(range.substr(13, 10)).format('YYYY-MM-DD');
            buildTable(null, {
                tanggalDari: tanggalDari,
                tanggalSampai: tanggalSampai
            });
        };

        // open modal in tab general

        $scope.tambahKontak = function(size) {

            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modalTambahKontak.html',
                controller: 'ModalTambahKontakController',
                size: size,

            });

            modalInstance.result.then(function(selectedItem) {
                // $scope.selected = selectedItem;
                $scope.dataTindakan.push(selectedItem);
            }, function() {

            });

        };

        $scope.tambahFoto = function(size) {

            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modalTambahFoto.html',
                controller: 'ModalTambahFotoController',
                size: size,

            });

            modalInstance.result.then(function(selectedItem) {
                // $scope.selected = selectedItem;
                $scope.dataTindakan.push(selectedItem);
            }, function() {

            });

        };

        $scope.tambahTanggungan = function(size) {

            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modalTambahTanggungan.html',
                controller: 'ModalTambahTanggunganController',
                size: size,

            });

            modalInstance.result.then(function(selectedItem) {
                // $scope.selected = selectedItem;
                $scope.dataTindakan.push(selectedItem);
            }, function() {

            });

        };

        $scope.tambahKeanggotaan = function(size) {

            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modalTambahKeanggotaan.html',
                controller: 'ModalTambahKeanggotaanController',
                size: size,

            });

            modalInstance.result.then(function(selectedItem) {
                // $scope.selected = selectedItem;
                $scope.dataTindakan.push(selectedItem);
            }, function() {

            });

        };

        // END TAB GENERAL

        //START TAB KUALIFIKASI 

         $scope.tambahPendidikan= function(size) {

            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modalTambahPendidikan.html',
                controller: 'ModalTambahPendidikanController',
                size: size,

            });

            modalInstance.result.then(function(selectedItem) {
                // $scope.selected = selectedItem;
                $scope.dataTindakan.push(selectedItem);
            }, function() {

            });

        };

         $scope.tambahBahasa= function(size) {

            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modalTambahBahasa.html',
                controller: 'ModalTambahBahasaController',
                size: size,

            });

            modalInstance.result.then(function(selectedItem) {
                // $scope.selected = selectedItem;
                $scope.dataTindakan.push(selectedItem);
            }, function() {

            });

        };


         $scope.tambahKemampuan= function(size) {

            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modalTambahKemampuan.html',
                controller: 'ModalTambahKemampuanController',
                size: size,

            });

            modalInstance.result.then(function(selectedItem) {
                // $scope.selected = selectedItem;
                $scope.dataTindakan.push(selectedItem);
            }, function() {

            });

        };

         $scope.tambahLisensi= function(size) {

            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modalTambahLisensi.html',
                controller: 'ModalTambahLisensiController',
                size: size,

            });

            modalInstance.result.then(function(selectedItem) {
                // $scope.selected = selectedItem;
                $scope.dataTindakan.push(selectedItem);
            }, function() {

            });

        };

         $scope.tambahOrganisasi= function(size) {

            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modalTambahOrganisasi.html',
                controller: 'ModalTambahOrganisasiController',
                size: size,

            });

            modalInstance.result.then(function(selectedItem) {
                // $scope.selected = selectedItem;
                $scope.dataTindakan.push(selectedItem);
            }, function() {

            });

        };

         $scope.tambahPengalaman= function(size) {

            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modalTambahPengalaman.html',
                controller: 'ModalTambahPengalamanController',
                size: size,

            });

            modalInstance.result.then(function(selectedItem) {
                // $scope.selected = selectedItem;
                $scope.dataTindakan.push(selectedItem);
            }, function() {

            });

        };

         $scope.tambahSertifikat= function(size) {

            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'modalTambahSertifikat.html',
                controller: 'ModalTambahSertifikatController',
                size: size,

            });

            modalInstance.result.then(function(selectedItem) {
                // $scope.selected = selectedItem;
                $scope.dataTindakan.push(selectedItem);
            }, function() {

            });

        };

        // END TAB KUALIDIKASI


        $scope.wrongDate = false;
        $scope.onChangeTanggalLahir = function(tanggalLahir) {
            console.log(tanggalLahir);
            var tanggal = parseInt(tanggalLahir.toString().substr(0, 2));
            var bulan = parseInt(tanggalLahir.toString().substr(2, 2));

            if (tanggal > 31 || bulan > 12) {
                $scope.wrongDate = true;
            } else {
                $scope.wrongDate = false;
                var date = moment(tanggalLahir, "DD-MM-YYYY").format();
                var umur = {
                    tahun: moment().diff(date, 'years'),
                    bulan: moment().diff(date, 'months'),
                    hari: moment().diff(date, 'days'),
                };
                $scope.data.umur = umur.tahun;
                console.log(umur);
            }

        };


        $scope.openProfile = function(id) {
            window.location.href = base_url + '/master/pegawai/profile/' + id;
            //  console.log( $location.path());
        };

        $scope.dateFilter = function() {
            console.log('filter', {
                tanggalDari: moment(angular.copy($scope.data.tanggalDari)).format('YYYY-MM-DD'),
                tanggalSampai: moment(angular.copy($scope.data.tanggalSampai)).format('YYYY-MM-DD')
            });

            buildTable(null, {
                tanggalDari: moment(angular.copy($scope.data.tanggalDari)).format('YYYY-MM-DD'),
                tanggalSampai: moment(angular.copy($scope.data.tanggalSampai)).format('YYYY-MM-DD')
            });
        };

        //init datatable
        console.log(base_url + '/api/rawat_jalan/daftar_kunjungan/data_daftar_kunjungan');

        function buildTable(noRekamMedis, dateFilter) {
            //var url = (noRekamMedis) ? base_url + '/api/rawat_jalan/daftar_kunjungan/data_daftar_kunjungan?no_rm=' + noRekamMedis : base_url + '/api/hrd/daftar_pegawai/data_daftar_pegawai';
            var url = base_url + '/api/hrd/daftar_pegawai/data_daftar_pegawai';

            // if (dateFilter) {
            //     url += "?tanggalDari=" + dateFilter.tanggalDari + "&tanggalSampai=" + dateFilter.tanggalSampai
            // }
            $scope.dtOptions = DTOptionsBuilder.newOptions()
                .withOption('ajax', {
                    url: url,
                    type: 'POST',
                })
                // or here
                .withDataProp('data')
                .withOption('processing', true)
                .withOption('serverSide', true)
                .withOption('order', [
                    [0, 'desc']
                ])
                .withOption('createdRow', function(row) {
                    // Recompiling so we can bind Angular directive to the DT
                    $compile(angular.element(row).contents())($scope);
                })
                .withPaginationType('full_numbers');

            $scope.dtInstance = {};
            $scope.dtColumns = [

                DTColumnBuilder.newColumn('nik').withTitle('NIP').withClass('text-center').renderWith(function(data, type, row, meta) {
                    return '<a ng-click="openProfile(\'' + row.uid + '\')" ">' + row.nik + '</a>';

                }).withOption('searchable', true),

                DTColumnBuilder.newColumn('nama').withTitle('NAMA'),
                DTColumnBuilder.newColumn('tgl_kerja').withTitle('TGL GABUNG').withClass('text-center').renderWith(function(data, type, row, meta) {

                    return moment(row.tgl_kerja).format('DD-MMM-YYYY ');
                }).withOption('searchable', false),

                DTColumnBuilder.newColumn('unit_kerja').withTitle('UNIT KERJA'),
                DTColumnBuilder.newColumn('departemen').withTitle('DEPARTEMEN'),
                DTColumnBuilder.newColumn('jabatan').withTitle('JABATAN').withOption('searchable', true)

            ];
            console.log($scope.dtInstance);
        }

        buildTable();


        $scope.reload = function() {
            // $scope.dtInstance._renderer.rerender();

        }

        $scope.saveUmum = function() {
            console.log("TTTTTTTTTTTTTT");
            console.log(angular.copy($scope.umum));
           // $rootScope.$emit('LOADING:EVENT:PROCESSING');

            // var req = {
            //     method: 'POST',
            //     url: base_url + '/api/rawat_jalan/pemeriksaan_dokter/save',
            //     data: 'data=' + JSON.stringify(angular.copy($scope.data)),
            //     headers: {
            //         'Content-type': 'application/x-www-form-urlencoded'
            //     }
            // };
            // $http(req).then(function (result) {
            //     console.log("result save, ", result);
            //     $rootScope.$emit('LOADING:EVENT:FINISH');
            //     toastr.success('Simpan Rawat Jalan Berhasil !', 'Sukses');
            // }, function (err) {
            //     console.log("err", err);
            //     toastr.error(err.toString(), 'Error');
            // });

        };

        function saveUmum1() {

            //  $rootScope.$emit('LOADING:EVENT:PROCESSING');


            // var req = {
            //     method: 'POST',
            //     url: base_url + '/api/rawat_jalan/pemeriksaan_dokter/save',
            //     data: 'data=' + JSON.stringify(angular.copy($scope.data)),
            //     headers: {
            //         'Content-type': 'application/x-www-form-urlencoded'
            //     }
            // };
            // $http(req).then(function (result) {
            //     console.log("result save, ", result);
            //     $rootScope.$emit('LOADING:EVENT:FINISH');
            //     toastr.success('Simpan Rawat Jalan Berhasil !', 'Sukses');
            // }, function (err) {
            //     console.log("err", err);
            //     toastr.error(err.toString(), 'Error');
            // });

        }

        //  $scope.saveUmum = function (result) {

        //     $rootScope.isProcessingUmum = true;
        //      var _data = angular.copy($scope.data);
        //     $rootScope.$emit('LOADING:EVENT:PROCESSING');
        //     var req = {
        //         method: 'POST',
        //         url: url,
        //         data: 'data=' + JSON.stringify(_data),
        //         headers: {
        //             'Content-type': 'application/x-www-form-urlencoded'
        //         }
        //     };
        //     console.log("req data , ", _data);
        //     $http(req).then(function (result) {
        //         console.log("result save, ", result);
        //         $rootScope.$emit('LOADING:EVENT:FINISH');
        //         toastr.success('Simpan Rawat Jalan Berhasil !', 'Sukses');

        //         buildTable();
        //     }, function (err) {
        //         console.log("err", err);
        //         toastr.error(err.data, 'Error');
        //     });

        // };



        $scope.reset = function() {
            window.location.href = base_url + '/rawat_jalan/pemeriksaan_awal';
        };


    }
]).controller('ModalCancelKunjunganController', ['$scope', 'api', '$http', '$uibModalInstance', '$log', '$compile',
    function($scope, api, $http, $uibModalInstance, $log, $compile) {


        $scope.close = function() {
            console.log("data : ", angular.copy($scope.choose));
            $uibModalInstance.close(angular.copy($scope.choose));
        };

        $scope.dismiss = function() {
            $uibModalInstance.dismiss();
        };

    }
]).controller('ModalTambahKontakController', ['$scope', 'api', '$http', '$uibModalInstance', '$log', '$q', '$compile',
    function($scope, api, $http, $uibModalInstance, $log, $q, $compile) {
        console.log("ttttttttttttttttttttttttttttttttttttttt");

        function initData() {
            $q.all([
                $http.get(base_url + "/api/master/hubungan_pasien")

            ]).then(function(results) {
                $scope.dataHubunganKontak = angular.copy(results[0].data);
            }).catch(function(err) {
                console.log(err);
            });
        }
        console.log("ttssssssssssssssssssss");
        initData();

        function resetData() {
            $scope.data.pekerjaanId = {};
            $scope.data.pegawaiId = {};
            $scope.data.pendidikanId = {};
        }


        // $scope.wrongDateHubunganKontak = false;
        $scope.onChangeHubunganKontak = function(tanggalLahir) {


            console.log(tanggalLahir);
            var tanggal = parseInt(tanggalLahir.toString().substr(0, 2));
            var bulan = parseInt(tanggalLahir.toString().substr(2, 2));

            if (tanggal > 31 || bulan > 12) {
                $scope.wrongDateHubungan = true;
            } else {
                $scope.wrongDateHubungan = false;
                var date = moment(tanggalLahir, "DD-MM-YYYY").format();
                var umur = {
                    tahun: moment().diff(date, 'years'),
                    bulan: moment().diff(date, 'months'),
                    hari: moment().diff(date, 'days'),
                };
                $scope.data.umur = umur.tahun;
                console.log(umur);
            }

        };

        function resetData() {
            $scope.data.pekerjaanId = {};
            $scope.data.pegawaiId = {};
            $scope.data.pendidikanId = {};
        }

        // initData();

        $scope.close = function() {
            console.log("data : ", angular.copy($scope.choose));
            $uibModalInstance.close(angular.copy($scope.choose));
        };

        $scope.dismiss = function() {
            $uibModalInstance.dismiss();
        };

    }
]).controller('ModalTambahFotoController', ['$scope', 'api', '$http', '$uibModalInstance', '$log', '$q', '$compile',
    function($scope, api, $http, $uibModalInstance, $log, $q, $compile) {

        function initData() {
            $q.all([
                $http.get(base_url + "/api/master/hubungan_pasien")

            ]).then(function(results) {
                $scope.dataHubunganKontak = angular.copy(results[0].data);
            }).catch(function(err) {
                console.log(err);
            });
        }

        initData();

        function resetData() {
            $scope.data.pekerjaanId = {};

        }


        $scope.close = function() {
            console.log("data : ", angular.copy($scope.choose));
            $uibModalInstance.close(angular.copy($scope.choose));
        };

        $scope.dismiss = function() {
            $uibModalInstance.dismiss();
        };

    }
]).controller('ModalTambahTanggunganController', ['$scope', 'api', '$http', '$uibModalInstance', '$log', '$q', '$compile',
    function($scope, api, $http, $uibModalInstance, $log, $q, $compile) {

        function initData() {
            $q.all([
                $http.get(base_url + "/api/master/hubungan_pasien")

            ]).then(function(results) {
                $scope.dataHubunganPasien = angular.copy(results[0].data);
            }).catch(function(err) {
                console.log(err);
            });
        }
        initData();

        function resetData() {
            $scope.data.pekerjaanId = {};
            $scope.data.pegawaiId = {};
            $scope.data.pendidikanId = {};
        }

        $scope.wrongDateHubungan = false;
        $scope.onChangeTanggalLahirTanggungan = function(tanggalLahir) {

            console.log(tanggalLahir);
            var tanggal = parseInt(tanggalLahir.toString().substr(0, 2));
            var bulan = parseInt(tanggalLahir.toString().substr(2, 2));

            if (tanggal > 31 || bulan > 12) {
                $scope.wrongDateHubungan = true;
            } else {
                $scope.wrongDateHubungan = false;
                var date = moment(tanggalLahir, "DD-MM-YYYY").format();
                var umur = {
                    tahun: moment().diff(date, 'years'),
                    bulan: moment().diff(date, 'months'),
                    hari: moment().diff(date, 'days'),
                };
                $scope.data.umur = umur.tahun;
                console.log(umur);
            }

        };

        $scope.close = function() {
            console.log("data : ", angular.copy($scope.choose));
            $uibModalInstance.close(angular.copy($scope.choose));
        };

        $scope.dismiss = function() {
            $uibModalInstance.dismiss();
        };

    }
]).controller('ModalTambahKeanggotaanController', ['$scope', 'api', '$http', '$uibModalInstance', '$log', '$compile',
    function($scope, api, $http, $uibModalInstance, $log, $compile) {
        $scope.wrongDateKeanggotaan = false;
        $scope.wrongDateKadaluarsa = false;

        $scope.onChangeTglMasuk = function(tanggalLahir) {


            console.log(tanggalLahir);
            var tanggal = parseInt(tanggalLahir.toString().substr(0, 2));
            var bulan = parseInt(tanggalLahir.toString().substr(2, 2));

            if (tanggal > 31 || bulan > 12) {
                $scope.wrongDateKeanggotaan = true;
            } else {
                $scope.wrongDateKeanggotaan = false;
                var date = moment(tanggalLahir, "DD-MM-YYYY").format();
                var umur = {
                    tahun: moment().diff(date, 'years'),
                    bulan: moment().diff(date, 'months'),
                    hari: moment().diff(date, 'days'),
                };
                $scope.data.umur = umur.tahun;
                console.log(umur);
            }

        };

        $scope.onChangeTglKadaluarsa = function(tanggalLahir) {


            console.log(tanggalLahir);
            var tanggal = parseInt(tanggalLahir.toString().substr(0, 2));
            var bulan = parseInt(tanggalLahir.toString().substr(2, 2));

            if (tanggal > 31 || bulan > 12) {
                $scope.wrongDateKadaluarsa = true;
            } else {
                $scope.wrongDateKadaluarsa = false;
                var date = moment(tanggalLahir, "DD-MM-YYYY").format();
                var umur = {
                    tahun: moment().diff(date, 'years'),
                    bulan: moment().diff(date, 'months'),
                    hari: moment().diff(date, 'days'),
                };
                $scope.data.umur = umur.tahun;
                console.log(umur);
            }

        };

        $scope.close = function() {
            console.log("data : ", angular.copy($scope.choose));
            $uibModalInstance.close(angular.copy($scope.choose));
        };

        $scope.dismiss = function() {
            $uibModalInstance.dismiss();
        };

    }
]).controller('ModalTambahPendidikanController', ['$scope', 'api', '$http', '$uibModalInstance', '$log', '$q', '$compile',
    function($scope, api, $http, $uibModalInstance, $log, $q, $compile) { 


        function initData() {
            $q.all([
                $http.get(base_url + "/api/master/hubungan_pasien")

            ]).then(function(results) {
                $scope.dataHubunganPasien = angular.copy(results[0].data);
            }).catch(function(err) {
                console.log(err);
            });
        }
        initData();

        function resetData() {
            $scope.data.pekerjaanId = {};
            $scope.data.pegawaiId = {};
            $scope.data.pendidikanId = {};
        }

        $scope.wrongDateHubungan = false;
        $scope.onChangeTanggalLahirTanggungan = function(tanggalLahir) {

            console.log(tanggalLahir);
            var tanggal = parseInt(tanggalLahir.toString().substr(0, 2));
            var bulan = parseInt(tanggalLahir.toString().substr(2, 2));

            if (tanggal > 31 || bulan > 12) {
                $scope.wrongDateHubungan = true;
            } else {
                $scope.wrongDateHubungan = false;
                var date = moment(tanggalLahir, "DD-MM-YYYY").format();
                var umur = {
                    tahun: moment().diff(date, 'years'),
                    bulan: moment().diff(date, 'months'),
                    hari: moment().diff(date, 'days'),
                };
                $scope.data.umur = umur.tahun;
                console.log(umur);
            }

        };

        $scope.close = function() {
            console.log("data : ", angular.copy($scope.choose));
            $uibModalInstance.close(angular.copy($scope.choose));
        };

        $scope.dismiss = function() {
            $uibModalInstance.dismiss();
        };

    }

    ]).controller('ModalTambahBahasaController', ['$scope', 'api', '$http', '$uibModalInstance', '$log', '$q', '$compile',
    function($scope, api, $http, $uibModalInstance, $log, $q, $compile) { 


        function initData() {
            $q.all([
                $http.get(base_url + "/api/master/hubungan_pasien")

            ]).then(function(results) {
                $scope.dataHubunganPasien = angular.copy(results[0].data);
            }).catch(function(err) {
                console.log(err);
            });
        }
        initData();

        function resetData() {
            $scope.data.pekerjaanId = {};
            $scope.data.pegawaiId = {};
            $scope.data.pendidikanId = {};
        }

        $scope.wrongDateHubungan = false;
        $scope.onChangeTanggalLahirTanggungan = function(tanggalLahir) {

            console.log(tanggalLahir);
            var tanggal = parseInt(tanggalLahir.toString().substr(0, 2));
            var bulan = parseInt(tanggalLahir.toString().substr(2, 2));

            if (tanggal > 31 || bulan > 12) {
                $scope.wrongDateHubungan = true;
            } else {
                $scope.wrongDateHubungan = false;
                var date = moment(tanggalLahir, "DD-MM-YYYY").format();
                var umur = {
                    tahun: moment().diff(date, 'years'),
                    bulan: moment().diff(date, 'months'),
                    hari: moment().diff(date, 'days'),
                };
                $scope.data.umur = umur.tahun;
                console.log(umur);
            }

        };

        $scope.close = function() {
            console.log("data : ", angular.copy($scope.choose));
            $uibModalInstance.close(angular.copy($scope.choose));
        };

        $scope.dismiss = function() {
            $uibModalInstance.dismiss();
        };

    }

    ]).controller('ModalTambahLisensiController', ['$scope', 'api', '$http', '$uibModalInstance', '$log', '$q', '$compile',
    function($scope, api, $http, $uibModalInstance, $log, $q, $compile) { 


        function initData() {
            $q.all([
                $http.get(base_url + "/api/master/hubungan_pasien")

            ]).then(function(results) {
                $scope.dataHubunganPasien = angular.copy(results[0].data);
            }).catch(function(err) {
                console.log(err);
            });
        }
        initData();

        function resetData() {
            $scope.data.pekerjaanId = {};
            $scope.data.pegawaiId = {};
            $scope.data.pendidikanId = {};
        }

        $scope.wrongDateHubungan = false;
        $scope.onChangeTanggalLahirTanggungan = function(tanggalLahir) {

            console.log(tanggalLahir);
            var tanggal = parseInt(tanggalLahir.toString().substr(0, 2));
            var bulan = parseInt(tanggalLahir.toString().substr(2, 2));

            if (tanggal > 31 || bulan > 12) {
                $scope.wrongDateHubungan = true;
            } else {
                $scope.wrongDateHubungan = false;
                var date = moment(tanggalLahir, "DD-MM-YYYY").format();
                var umur = {
                    tahun: moment().diff(date, 'years'),
                    bulan: moment().diff(date, 'months'),
                    hari: moment().diff(date, 'days'),
                };
                $scope.data.umur = umur.tahun;
                console.log(umur);
            }

        };

        $scope.close = function() {
            console.log("data : ", angular.copy($scope.choose));
            $uibModalInstance.close(angular.copy($scope.choose));
        };

        $scope.dismiss = function() {
            $uibModalInstance.dismiss();
        };

    }

    ]).controller('ModalTambahOrganisasiController', ['$scope', 'api', '$http', '$uibModalInstance', '$log', '$q', '$compile',
    function($scope, api, $http, $uibModalInstance, $log, $q, $compile) { 


        function initData() {
            $q.all([
                $http.get(base_url + "/api/master/hubungan_pasien")

            ]).then(function(results) {
                $scope.dataHubunganPasien = angular.copy(results[0].data);
            }).catch(function(err) {
                console.log(err);
            });
        }
        initData();

        function resetData() {
            $scope.data.pekerjaanId = {};
            $scope.data.pegawaiId = {};
            $scope.data.pendidikanId = {};
        }

        $scope.wrongDateHubungan = false;
        $scope.onChangeTanggalLahirTanggungan = function(tanggalLahir) {

            console.log(tanggalLahir);
            var tanggal = parseInt(tanggalLahir.toString().substr(0, 2));
            var bulan = parseInt(tanggalLahir.toString().substr(2, 2));

            if (tanggal > 31 || bulan > 12) {
                $scope.wrongDateHubungan = true;
            } else {
                $scope.wrongDateHubungan = false;
                var date = moment(tanggalLahir, "DD-MM-YYYY").format();
                var umur = {
                    tahun: moment().diff(date, 'years'),
                    bulan: moment().diff(date, 'months'),
                    hari: moment().diff(date, 'days'),
                };
                $scope.data.umur = umur.tahun;
                console.log(umur);
            }

        };

        $scope.close = function() {
            console.log("data : ", angular.copy($scope.choose));
            $uibModalInstance.close(angular.copy($scope.choose));
        };

        $scope.dismiss = function() {
            $uibModalInstance.dismiss();
        };

    }

    ]).controller('ModalTambahPengalamanController', ['$scope', 'api', '$http', '$uibModalInstance', '$log', '$q', '$compile',
    function($scope, api, $http, $uibModalInstance, $log, $q, $compile) { 


        function initData() {
            $q.all([
                $http.get(base_url + "/api/master/hubungan_pasien")

            ]).then(function(results) {
                $scope.dataHubunganPasien = angular.copy(results[0].data);
            }).catch(function(err) {
                console.log(err);
            });
        }
        initData();

        function resetData() {
            $scope.data.pekerjaanId = {};
            $scope.data.pegawaiId = {};
            $scope.data.pendidikanId = {};
        }

        $scope.wrongDateHubungan = false;
        $scope.onChangeTanggalLahirTanggungan = function(tanggalLahir) {

            console.log(tanggalLahir);
            var tanggal = parseInt(tanggalLahir.toString().substr(0, 2));
            var bulan = parseInt(tanggalLahir.toString().substr(2, 2));

            if (tanggal > 31 || bulan > 12) {
                $scope.wrongDateHubungan = true;
            } else {
                $scope.wrongDateHubungan = false;
                var date = moment(tanggalLahir, "DD-MM-YYYY").format();
                var umur = {
                    tahun: moment().diff(date, 'years'),
                    bulan: moment().diff(date, 'months'),
                    hari: moment().diff(date, 'days'),
                };
                $scope.data.umur = umur.tahun;
                console.log(umur);
            }

        };

        $scope.close = function() {
            console.log("data : ", angular.copy($scope.choose));
            $uibModalInstance.close(angular.copy($scope.choose));
        };

        $scope.dismiss = function() {
            $uibModalInstance.dismiss();
        };

    }

    ]).controller('ModalTambahSertifikatController', ['$scope', 'api', '$http', '$uibModalInstance', '$log', '$q', '$compile',
    function($scope, api, $http, $uibModalInstance, $log, $q, $compile) { 


        function initData() {
            $q.all([
                $http.get(base_url + "/api/master/hubungan_pasien")

            ]).then(function(results) {
                $scope.dataHubunganPasien = angular.copy(results[0].data);
            }).catch(function(err) {
                console.log(err);
            });
        }
        initData();

        function resetData() {
            $scope.data.pekerjaanId = {};
            $scope.data.pegawaiId = {};
            $scope.data.pendidikanId = {};
        }

        $scope.wrongDateHubungan = false;
        $scope.onChangeTanggalLahirTanggungan = function(tanggalLahir) {

            console.log(tanggalLahir);
            var tanggal = parseInt(tanggalLahir.toString().substr(0, 2));
            var bulan = parseInt(tanggalLahir.toString().substr(2, 2));

            if (tanggal > 31 || bulan > 12) {
                $scope.wrongDateHubungan = true;
            } else {
                $scope.wrongDateHubungan = false;
                var date = moment(tanggalLahir, "DD-MM-YYYY").format();
                var umur = {
                    tahun: moment().diff(date, 'years'),
                    bulan: moment().diff(date, 'months'),
                    hari: moment().diff(date, 'days'),
                };
                $scope.data.umur = umur.tahun;
                console.log(umur);
            }

        };

        $scope.close = function() {
            console.log("data : ", angular.copy($scope.choose));
            $uibModalInstance.close(angular.copy($scope.choose));
        };

        $scope.dismiss = function() {
            $uibModalInstance.dismiss();
        };

    }

    ]).controller('ModalTambahKemampuanController', ['$scope', 'api', '$http', '$uibModalInstance', '$log', '$q', '$compile',
    function($scope, api, $http, $uibModalInstance, $log, $q, $compile) { 


        function initData() {
            $q.all([
                $http.get(base_url + "/api/master/hubungan_pasien")

            ]).then(function(results) {
                $scope.dataHubunganPasien = angular.copy(results[0].data);
            }).catch(function(err) {
                console.log(err);
            });
        }
        initData();

        function resetData() {
            $scope.data.pekerjaanId = {};
            $scope.data.pegawaiId = {};
            $scope.data.pendidikanId = {};
        }

        $scope.wrongDateHubungan = false;
        $scope.onChangeTanggalLahirTanggungan = function(tanggalLahir) {

            console.log(tanggalLahir);
            var tanggal = parseInt(tanggalLahir.toString().substr(0, 2));
            var bulan = parseInt(tanggalLahir.toString().substr(2, 2));

            if (tanggal > 31 || bulan > 12) {
                $scope.wrongDateHubungan = true;
            } else {
                $scope.wrongDateHubungan = false;
                var date = moment(tanggalLahir, "DD-MM-YYYY").format();
                var umur = {
                    tahun: moment().diff(date, 'years'),
                    bulan: moment().diff(date, 'months'),
                    hari: moment().diff(date, 'days'),
                };
                $scope.data.umur = umur.tahun;
                console.log(umur);
            }

        };

        $scope.close = function() {
            console.log("data : ", angular.copy($scope.choose));
            $uibModalInstance.close(angular.copy($scope.choose));
        };

        $scope.dismiss = function() {
            $uibModalInstance.dismiss();
        };

    }

    ]);