var page_url    = '/api/hrd/pegawai/';
$notif          = $('#notif');
$formResign     = $('#form-pengajuan');
$btnSubmitResign= $('#save-button');
$detailPegawai  = $('#detail-pegawai');
$jenisPengajuan = $('select[name=jenis_pengajuan]');
$jabatanLama    = $('select[name=jabatan_lama]');
$jabatanBaru    = $('select[name=jabatan_baru]');
$unitusaha_id   = $('select[name=unitusaha_id]');
$tahunAwal      = $('#tahun_awal');
$tahunAkhir     = $('#tahun_akhir');
$fromSearch     = $('#form-search');
$unitusaha      = $('#unitusaha');
$unitkerja      = $('#unitkerja');
$cari           = $('#cari');
$btncari        = $('#btn-cari');
$departemen     = $('#departemen');

$(function(){
    var the_table;

    $unitusaha.change(function(){
        unitusaha   = $unitusaha.val();
        unitkerja   = $unitkerja.val();
        departemen  = $departemen.val();
        cari        = $cari.val();
        get_data(cari,unitusaha,unitkerja,departemen);
    });

    $unitkerja.change(function(){
        unitusaha   = $unitusaha.val();
        unitkerja   = $unitkerja.val();
        departemen  = $departemen.val();
        cari        = $cari.val();
        get_data(cari,unitusaha,unitkerja,departemen);
    });

    $btncari.click(function(){
        unitusaha   = $unitusaha.val();
        unitkerja   = $unitkerja.val();
        departemen  = $departemen.val();
        cari        = $cari.val();
        get_data(cari,unitusaha,unitkerja,departemen);
    });

    $departemen.change(function(){
        unitusaha   = $unitusaha.val();
        unitkerja   = $unitkerja.val();
        departemen  = $departemen.val();
        cari        = $cari.val();
        get_data(cari,unitusaha,unitkerja,departemen);
    });

    $( "#cari" ).autocomplete({
      source: base_url+"/api/hrd/jabatan/pengajuan/search",
      minLength: 2,
      select: function( event, ui ) {
        search();
        // log( "Selected: " + ui.item.value + " aka " + ui.item.id );
      }
    });
});

function search()
{
    unitusaha   = $unitusaha.val();
    unitkerja   = $unitkerja.val();
    departemen  = $departemen.val();
    cari        = $fromSearch.find('#cari').val();
    console.log(cari);
    get_data(cari,unitusaha,unitkerja,departemen);
}

function view_detail(element){
    console.log($(element).attr('href'));
    var url = $(element).attr('href');
    $.ajax({
        'url':url,
        'type':'POST',
        'dataType':'html',
        'success': function(data){
            console.log(data);
            if(data == '0')
                alert('maaf data yang anda pilih sudah ada, silahkan pilih data yang lain !');
            else
            {
                $detailPegawai.html(data);
                $detailPegawai.fadeIn('slow');
                var unit_usaha = $detailPegawai.find('input[name=unitusaha_id]').val();
                $unitusaha_id.val(unit_usaha);
                var jabatan_lama = $detailPegawai.find('input[name=jabatan]').val();
                $jabatanLama.val(jabatan_lama);
                console.log(jabatan_lama);
            }
        }
    });
    return false;
}

function get_data(cari,unitusaha,unitkerja,departemen){
    console.log('run');
    if(typeof the_table != 'undefined'){
        the_table.destroy();
    }
    the_table = $("#dataTable-pegawai").DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+'/api/hrd/pegawai',
            "type": "POST",
            "dataType":"json",
            "data": {cari:cari,unitusaha:unitusaha,unitkerja:unitkerja,departemen:departemen}
        },
        "columns": [
            {
                "data": "nik",
                "render": function(data, type, row, meta) {
                    return '<span class="btn-link text-info" onClick="view_detail(this)" style="cursor:pointer" href="'+base_url+'/api/hrd/jabatan/pengajuan/view/' + row.uid + '">' + data + '</span>';
                }
            },
            {
                "data": "nama",
                "render": function(data, type, row, meta) {
                    return '<span class="btn-link text-info" onClick="view_detail(this)" style="cursor:pointer" href="'+base_url+'/api/hrd/jabatan/pengajuan/view/' + row.uid + '">' + data + '</span>';
                }
            },
            {
                "data": "unitusaha_nama"
            },
            {
                "data": "unitkerja_nama"
            },
            {
                "data": "departemen_nama"
            },
            {
                "data": "jabatan_nama"
            }

        ]
    });
}

var rules_default = {
        "jabatan_lama": {
            required: true,
        },
        "jabatan_baru": {
            required: true,
        },
        "jenis_pengajuan": {
            required: true,
        },
        "unitusaha_id": {
            required: true,
        },
        "tahun_awal": {
            required: true,
        },
        "tahun_akhir": {
            required: true,
        }
    };

$(function(){
    $('input.form-control').keypress(function (e) {
      if (e.which == 13) {
        return false;    //<---- Add this line
      }
    });

    $('.pickadate').pickadate({
        // options
        format: 'dd-mm-yyyy'
    });

    set_validation(rules_default);
    $jenisPengajuan.change(function(){
        rules = rules_default;
        if($(this).val() == 'resign')
        {
            delete(rules.jabatan_baru);
            delete(rules.jabatan_lama);
            delete(rules.tahun_awal);
            delete(rules.tahun_akhir);
            $jabatanLama.prop('disabled',true);
            $jabatanBaru.prop('disabled',true);
            $tahunAwal.prop('disabled',true);
            $tahunAkhir.prop('disabled',true);
        }
        else{
            $jabatanLama.prop('disabled',false);
            $jabatanBaru.prop('disabled',false);
            $tahunAwal.prop('disabled',false);
            $tahunAkhir.prop('disabled',false);
        }
        set_validation(rules);
    });

});

function set_validation(rules){
    console.log('set validation');
    if(typeof formValidator != 'undefined')
        formValidator.destroy();

    formValidator = $formResign.validate({
        rules:rules
        ,
        errorPlacement: function (error, element) {
            error.insertAfter(element);
            element.focus();  // <- this line is causing your whole problem
        },
        submitHandler: function(form) {
            var pegawai_id = $(document).find('input[name=pegawai_id]').val();
            if(!pegawai_id)
            {
                alert('Silahkan Pilih Dulu Pegawai!');
                return false;
            }

            var target_url  = base_url+'/api/hrd/jabatan/pengajuan/save/'+pegawai_id;
            var captionBtn  = $btnSubmitResign.html();
            var data = new FormData($(form)[0]);
            console.log(data);
            $.ajax({
                url         : target_url,
                type        : "POST",
                data        : data,
                dataType    : 'json',
                mimeType    : "multipart/form-data",
                contentType : false,
                cache       : false,
                processData : false,
                beforeSend: function() {
                        $btnSubmitResign.html('Please wait....');
                },
                success:function(data)
                {
                    console.log(data);
                    if(data.status == 1)
                    {
                        $formResign[0].reset();
                        $detailPegawai.html('');
                        $btncari.click();
                    }
                    $btnSubmitResign.html(captionBtn);
                    $notif.text(data.info);
                    $notif.fadeIn('slow').delay('3500').fadeOut('slow');
                }
            });
            return false;
        }
    });
    return false;
}

function delete_kontak(element){
    console.log($(element).attr('href'));
    var url = $(element).attr('href');
    MyModel.delete(url,function(data){
        console.log(data);
        get_kontak();
        $notif.text(data.info);
        $notif.fadeIn('slow').delay('3500').fadeOut('slow');
    });
    return false;
}

function tambah_kontak(){
    $('#add_kontak_modal').modal('show');
}

function convert_array_to_object(dataArray){
    var result = {};
    dataArray.forEach(function(item,index){
        result[item.name] = item.value;
    });
    return result;
}

var MyModel = {
    page_url : '/api/hrd/pegawai/',
    insert : function(page_url,data,callback){
        $.ajax({
            'url':base_url+page_url,
            'type':'POST',
            'dataType':'json',
            'data':data,
            'success': function(data){
                return callback(data);
            }
        });
    },
    delete : function(url,callback){
        $.ajax({
            'url':url,
            'type':'POST',
            'dataType':'json',
            'success': function(data){
                return callback(data);
            }
        });
    },
    get : function(tabel,columns,data){
        console.log('run');
        var page_url = this.page_url;
        if(typeof the_table != 'undefined'){
            the_table.destroy();
        }
        the_table = $("#"+tabel).DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": base_url+page_url,
                "type": "POST",
                "dataType":"json",
                "data": data
            },
            "columns": columns
        });
    }
}
