$(function(){
    var tanggal = false;
    var the_table;
    $tgl        = $('#tgl_penyelenggaraan');
    $unitusaha  = $('#unitusaha');
    $cari       = $('#cari');
    $btncari       = $('#btn-cari');
    $departemen = $('#departemen');
    $jenisPengajuan = $('#jenis_pengajuan');

    $tgl.daterangepicker({
        applyClass: 'bg-slate-600',
        cancelClass: 'btn-default',
        locale: {
          format: 'DD/MM/YYYY'
        },
        startDate: {
            value: moment().subtract(29, 'days')
        },
        endDate: {
            value: moment()
        }
    });

    $tgl.on('apply.daterangepicker', function(ev, picker) {
        tanggal = true;
        console.log('jalan');
        tgl_mulai   = picker.startDate.format('YYYY-MM-DD');
        tgl_akhir   = picker.endDate.format('YYYY-MM-DD');
        unitusaha   = $unitusaha.val();
        departemen  = $departemen.val();
        cari        = $cari.val();
        get_data(cari,tgl_mulai,tgl_akhir,unitusaha,departemen);
    });

    $unitusaha.change(function(){
        var tgl_akhir;
        var tgl_mulai;
        if(tanggal){
            tgl_mulai   = $tgl.data('daterangepicker').startDate.format('YYYY-MM-DD');
            tgl_akhir   = $tgl.data('daterangepicker').endDate.format('YYYY-MM-DD');
        }
        unitusaha   = $unitusaha.val();
        departemen  = $departemen.val();
        cari        = $cari.val();
        get_data(cari,tgl_mulai,tgl_akhir,unitusaha,departemen);
    });

    $btncari.click(function(){
        var tgl_akhir;
        var tgl_mulai;
        if(tanggal){
            tgl_mulai   = $tgl.data('daterangepicker').startDate.format('YYYY-MM-DD');
            tgl_akhir   = $tgl.data('daterangepicker').endDate.format('YYYY-MM-DD');
        }
        unitusaha   = $unitusaha.val();
        departemen  = $departemen.val();
        cari        = $cari.val();
        get_data(cari,tgl_mulai,tgl_akhir,unitusaha,departemen);
        // $(document).find('#dataTable_pengajuan_filter input[type=search]').val(cari);
    });
    $jenisPengajuan.change(function(){
        var tgl_akhir;
        var tgl_mulai;
        if(tanggal){
            tgl_mulai   = $tgl.data('daterangepicker').startDate.format('YYYY-MM-DD');
            tgl_akhir   = $tgl.data('daterangepicker').endDate.format('YYYY-MM-DD');
        }
        unitusaha   = $unitusaha.val();
        jenis_pengajuan  = $jenisPengajuan.val();
        cari        = $cari.val();
        get_data(cari,tgl_mulai,tgl_akhir,unitusaha,jenis_pengajuan);
    });

    get_data();
});

function view_detail(element){
    console.log($(element).attr('href'));
    var url = $(element).attr('href');
    $.ajax({
        'url':url,
        'type':'POST',
        'dataType':'html',
        'success': function(data){
            console.log(data);
            $('#detail_modal .modal-dialog').html(data);
            $('#detail_modal').modal('show');
        }
    });
    return false;
}

function get_data(cari,tgl_mulai,tgl_akhir,unitusaha,jenis_pengajuan){
    console.log('run');
    if(typeof the_table != 'undefined'){
        the_table.destroy();
    }
    the_table = $("#dataTable_pengajuan").DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+'/api/hrd/jabatan/verifikasi_hrd',
            "type": "POST",
            "dataType":"json",
            "data": {cari:cari,tgl_mulai:tgl_mulai,tgl_akhir:tgl_akhir,unitusaha:unitusaha,jenis_pengajuan:jenis_pengajuan}
        },
        "columns": [
            {
                "data": "created_at",
                "render": function(data){
                    return moment(data).format('DD/MM/YYYY');
                }
            },
            {
                "data": "nik",
            },
            {
                "data": "nama",
                "render": function(data, type, row, meta) {
                    return '<span class="text-primary" style="cursor:pointer" href="'+base_url+'/api/hrd/jabatan/verifikasi_hrd/view_profil/' + row.uid +'" onClick="view_detail(this)">' + data + '</span>';
                }
            },
            {
                "data": "unitusaha_nama"
            },
            {
                "data": "unitkerja_nama"
            },
            {
                "data": "jabatan_nama"
            },
            {
                "data": "jenis_pengajuan",
                "render": function(data){
                    return '<span class="label label-info">'+data+'</span>';
                }
            }

        ]
    });
    /*the_table.on( 'order.dt search.dt stateLoaded.dt', function () {

    } ).draw();*/
}
