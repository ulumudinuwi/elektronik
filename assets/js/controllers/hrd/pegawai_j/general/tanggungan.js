$formTanggungan     = $('#form-tanggungan');
$notifGeneral       = $('#notif');
$modalTanggungan    = $('#add_tanggungan_modal');

$(function(){
    $formTanggungan.validate({
        submitHandler: function(form) {
            var dataForm    = $(form).serializeArray();
            var target_url  = page_url+'save_tanggungan/'+pegawai_id;
            var data        = convert_array_to_object(dataForm);
            console.log(data);
            MyModel.insert(target_url,data,function(data){
                console.log(data);
                Tanggungan.getTanggungan();
                $modalTanggungan.modal('hide');
                $notifGeneral.text(data.info);
                $notifGeneral.fadeIn('slow').delay('3500').fadeOut('slow');
                $formTanggungan[0].reset();
            });
            return false;
        }
    });
});

var Tanggungan = {
    getTanggungan: function() {
        var page_url    = '/api/hrd/pegawai/get_tanggungan/'+pegawai_id;
        var columns     = [
            {
                "data": "tanggungan_nama",
            },
            {
                "data": "jenis_kelamin",
            },
            {
                "data": "tanggal_lahir",
                "render":function(data){
                    if(data)
                    return moment(data).format('DD/MM/YYYY');
                    else
                    return '';
                }
            },
            {
                "data": "hubungan",
            },
            {
                "data": "action",
                "render": function(data, type, row, meta) {
                    return '<span style="cursor:pointer" class="text-danger text-center" href="'+base_url+'/api/hrd/pegawai/hapus_tanggungan/' + row.uid + '" onClick="Tanggungan.deleteTanggungan(this)"><i class="fa fa-trash"></i></span>';
                },
                "sortable":false
            }
        ];
        MyModel.page_url = page_url;
        MyModel.get('tabel-tanggungan',columns);
    },
    deleteTanggungan: function(element){
        console.log($(element).attr('href'));
        var url = $(element).attr('href');
        MyModel.delete(url,function(data){
            console.log(data);
            Tanggungan.getTanggungan();
            $notifGeneral.text(data.info);
            $notifGeneral.fadeIn('slow').delay('3500').fadeOut('slow');
        });
        return false;
    },
    tambahTanggungan: function(){
        $modalTanggungan.modal('show');
    }
}
