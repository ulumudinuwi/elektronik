$formPengalaman     = $('#form-pengalaman');
$notifKualifikasi   = $('#notif-kualifikasi');
$modalPengalaman    = $('#add_pengalaman_modal');

$.validator.addMethod("onlyNumber",
    function (value, element, options)
    {
        var req = /^\d+$/;

        return (value.match(req));
    },
    "Please enter a number."
);

$(function(){
    $formPengalaman.validate({
        rules: {
            "tahun_masuk": {
                required: "required",
                onlyNumber: true,
                maxlength: 10
            },
            "tahun_keluar": {
                required: "required",
                onlyNumber: true,
                maxlength: 10
            }
        },
        submitHandler: function(form) {
            var dataForm    = $(form).serializeArray();
            var target_url  = page_url+'save_pengalaman/'+pegawai_id;
            var data        = convert_array_to_object(dataForm);
            console.log(data);
            // save_pengalaman(target_url,data);
            MyModel.insert(target_url,data,function(data){
                console.log(data);
                Pengalaman.getPengalaman();
                $modalPengalaman.modal('hide');
                $notifKualifikasi.text(data.info);
                $notifKualifikasi.fadeIn('slow').delay('3500').fadeOut('slow');
                $formPengalaman[0].reset();
            });
            return false;
        }
    });
});

var Pengalaman = {
    getPengalaman: function() {
        var page_url    = '/api/hrd/pegawai/get_pengalaman/'+pegawai_id;
        var columns     = [
            {
                "data": "perusahaan",
            },
            {
                "data": "jabatan",
            },
            {
                "data": "spesialis",
            },
            {
                "data": "tanggal_masuk",
                "render":function(data){
                    if(data)
                    return moment(data).format('DD/MM/YYYY');
                    else
                    return '';
                }
            },
            {
                "data": "tanggal_keluar",
                "render":function(data){
                    if(data)
                    return moment(data).format('DD/MM/YYYY');
                    else
                    return '';
                }
            },
            {
                "data": "action",
                "render": function(data, type, row, meta) {
                    return '<span style="cursor:pointer" class="text-danger text-center" href="'+base_url+'/api/hrd/pegawai/hapus_pengalaman/' + row.uid + '" onClick="Pengalaman.deletePengalaman(this)"><i class="fa fa-trash"></i></span>';
                },
                "sortable":false
            }
        ];
        MyModel.page_url = page_url;
        MyModel.get('tabel-pengalaman',columns);
    },
    deletePengalaman: function(element){
        console.log($(element).attr('href'));
        var url = $(element).attr('href');
        MyModel.delete(url,function(data){
            console.log(data);
            Pengalaman.getPengalaman();
            $notifKualifikasi.text(data.info);
            $notifKualifikasi.fadeIn('slow').delay('3500').fadeOut('slow');
        });
        return false;
    },
    tambahPengalaman: function(){
        $modalPengalaman.modal('show');
    }
}
