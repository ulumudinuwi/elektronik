/**
 * Created by agungrizkyana on 9/13/16.
 */
angular.module('rsbt').controller('BpjsSepInsertController', bpjsSepInsertController);

bpjsSepInsertController.$inject = ['$scope', '$rootScope', '$http', 'CONFIG', 'EVENT', 'toastr', '$resource'];

function bpjsSepInsertController($scope, $rootScope, $http, CONFIG, EVENT, toastr, $resource) {
    var vm = this;
    vm.sep = {};
    $scope.result = {};
    vm.getInacbg = function () {
        var data = angular.copy(vm.sep);

        $http.get(base_url + '/api/bpjs/sep/inacbg?no_sep=' + data.noSep).then(
            function (result) {
                console.log("response dari inacbg, ", result);
                $scope.result = result;
            }, function (err) {
                console.log(err);
            });

    };

    console.log("controller bpjs inacbg peserta");


    console.log("controller bpjs peserta");
}