/**
 * Created by agungrizkyana on 9/13/16.
 */
angular.module('rsbt').controller('BpjsSepInsertController', bpjsSepInsertController);

bpjsSepInsertController.$inject = ['$scope', '$rootScope', '$http', 'CONFIG', 'EVENT', 'toastr', '$resource'];

function bpjsSepInsertController($scope, $rootScope, $http, CONFIG, EVENT, toastr, $resource) {
    var vm = this;
    vm.data = {
        peserta: {},
        sep: '',
        noKartu: ''
    };
    vm.sep = {};
    $scope.result = {};
    vm.getInacbg = function () {
        var data = angular.copy(vm.sep);

        $http.get(base_url + '/api/bpjs/sep/inacbg?no_sep=' + data.noSep).then(
            function (result) {
                console.log("response dari inacbg, ", result);
                $scope.result = result;
            }, function (err) {
                console.log(err);
            });

    };


    $scope.selected = selected;
    $scope.submit = submit;

    function selected(item) {
        if (item) {
            console.log(item);
            vm.data.peserta = item.originalObject;
        }
    }

    function submit() {
        var param = {
            nomor_kartu: vm.data.noKartu,
            nomor_sep: vm.data.sep,
            nomor_rm: vm.data.peserta.no_rekam_medis,
            nama_pasien: vm.data.peserta.nama,
            tgl_lahir: moment(vm.data.peserta.tgl_lahir).format('YYYY-MM-DD hh:mm:ss'),
            gender: vm.data.peserta.jenis_kelamin
        };
        $rootScope.$emit('ajax:progress');
        console.log(param);
        $.ajax({
            url: base_url + '/api/bpjs/inacbg/new_claim',
            method: 'POST',
            data: angular.copy(param),
            cache: false,
            dataType: 'json',
        }).then(function (result) {
            console.log("result, ", result);
            $rootScope.$emit('ajax:stop');

            if (result.metadata.code != 200) {
                toastr.error(result.metadata.message, result.metadata.code);
                return;
            }

            toastr.success(result.metada.message, result.metadata.code);


        }, function (err) {
            console.log("err, ", err);
            $rootScope.$emit('ajax:stop');
        });
    }

}
