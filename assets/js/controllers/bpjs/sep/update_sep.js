/**
 * Created by agungrizkyana on 9/13/16.
 */
angular.module('rsbt').controller('BpjsSepUpdateController', bpjsSepUpdateController);

bpjsSepUpdateController.$inject = ['$scope', '$rootScope', '$http', 'CONFIG', 'EVENT', 'toastr', '$resource', '$uibModal'];

function bpjsSepUpdateController($scope, $rootScope, $http, CONFIG, EVENT, toastr, $resource, $uibModal) {
    var vm = this;
    vm.sep = {
        ppkPelayanan: '0110R005',
        lokasiLaka: '-',
        noSep : ''
    };

    $scope.dataPoli = [];
    $.ajax({
        url: base_url + '/api/bpjs/sep/poli',
        type: 'GET',
        success: function (result) {
            var _result = JSON.parse(result);
            console.log(_result);

            if (_result.response.response) {
                $scope.dataPoli = _result.response.response.list;
            }

        },
        error: function (err) {
            console.log(err);
        }
    });
    $scope.urlDiagnosa = base_url + '/api/bpjs/referensi/diagnosa?diagnosa=';

    $scope.selected = function (item) {
        if (item)
            vm.sep.diagAwal = item.originalObject.kodeDiagnosa;
    };


    $scope.onChangeNoKartu = function(noKartu){
        $resource(base_url + '/api/bpjs/peserta/no_kartu/').get({
            no_kartu : noKartu
        }).$promise.then(function (result) {
            var result = JSON.parse(result.data);
            console.log(result);

            if(result.metadata.code == 200){
                if(result.response.peserta){
                    // vm.peserta = result.response.peserta;
                    vm.sep.klsRawat = result.response.peserta.kelasTanggungan.kdKelas;
                    vm.sep.ppkRujukan = result.response.peserta.provUmum.kdProvider;

                    toastr.success(result.metadata.code, result.metadata.message);
                }
            }else{
                toastr.warning(result.metadata.code, result.metadata.message);
            }



        }).catch(function (err) {
            console.log(err);
        });
    };

    $scope.onChangeNoSep = function(){
        $http.get(base_url + '/api/bpjs/sep/detail', {
            params: {
                no_sep: vm.sep.noSep
            }
        }).then(success);
        function success(result) {
            console.log(result.data.response);

            if (result.data.response) {
                var metadata = result.data.response.metadata;
                if (metadata.code != 200) {
                    toastr.warning(result.data.response.metadata.message, result.data.response.metadata.code);
                } else {
                    toastr.success('SEP di Temukan', 'Sukses !');
                    vm.sep.tglSep = result.data.response.response.tglSep;
                    vm.sep.tglRujukan = result.data.response.response.tglRujukan;
                    vm.sep.noRujukan = result.data.response.response.noRujukan;
                    vm.sep.catatan = result.data.response.response.catatan;
                    vm.sep.noMr = result.data.response.response.peserta.noMr;
                    vm.sep.noKartu = result.data.response.response.peserta.noKartu;
                    vm.sep.ppkRujukan = result.data.response.response.provRujukan.kdProvider;
                    vm.sep.ppkPelayanan = result.data.response.response.provPelayanan.kdProvider;
                    vm.sep.jnsPelayanan = result.data.response.response.jnsPelayanan == 'Inap' ? 2 : 1;
                    vm.sep.klsRawat = result.data.response.response.klsRawat.kdKelas;
                    vm.sep.lakaLantas = parseInt(result.data.response.response.lakaLantas.status) == 0 ? 2 : 1;
                    vm.sep.poli = result.data.response.response.poliTujuan.nmPoli;
                    vm.sep.diagAwal = result.data.response.response.diagAwal.nmDiag;

                    console.log(vm.sep);
                }
            } else {
                toastr.error('Tidak Ada Response dari BPJS', 'Sistem Error');
            }



        }
    };

    vm.update = function (isPrint) {
        vm.sep.tglSep = moment(vm.sep.tglSep).format('YYYY-MM-DD hh:mm:ss');
        vm.sep.tglRujukan = moment(vm.sep.tglRujukan).format('YYYY-MM-DD hh:mm:ss');
        var _data = {
            "request": {
                "t_sep": angular.copy(vm.sep)
            }
        };
        $rootScope.$emit('ajax:progress');
        $.ajax({
            url: base_url + '/api/bpjs/sep/update',
            type: 'POST',
            data: 'data=' + JSON.stringify(_data),
            success: function (result) {
                var _result = JSON.parse(result);
                console.log(_result);
                if (!_result) {
                    toastr.error('Sistem Error', '');
                    $rootScope.$emit('ajax:stop');
                    return;
                }

                if (isPrint == true) {
                    openModalPrintSep('lg');
                }

                if (_result.metadata.code != 200) {
                    toastr.warning(_result.metadata.message, _result.metadata.code);
                }
                $rootScope.$emit('ajax:stop');
            },
            error: function (err) {
                $rootScope.$emit('ajax:stop');
                console.log(err);
            }
        });


    };

    function openModalPrintSep(size) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'modalPrintSep.html',
            controller: 'ModalPrintSep',
            size: size,
            resolve: {
                items: function () {
                    return vm.sep;
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {


        }, function () {

        });
    }

    console.log("controller bpjs peserta");
}