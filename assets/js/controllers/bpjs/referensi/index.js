/**
 * Created by agungrizkyana on 9/19/16.
 */
app.controller('BpjsReferensiController', BpjsReferensiController);

BpjsReferensiController.$inject = ['$rootScope', '$scope', '$resource'];

function BpjsReferensiController($rootScope, $scope, $resource) {
    var vm = this;



    vm.ref = {
        faskes: '',
        diagnosa: ''
    };


    vm.cekDiagnosa = function () {

        $resource(base_url + '/api/bpjs/referensi/diagnosa').get({
            diagnosa: angular.copy(vm.ref.diagnosa)
        }).$promise.then(function (result) {
            console.log(result);
            // var result = JSON.parse(result.data);
            // console.log(result);
            vm.diagnosa = result.response.response;

            // if(result.response.peserta){
            //     vm.peserta = result.response.peserta;
            // }

        }).catch(function (err) {
            console.log(err);
        });
    };

    vm.cekFaskes = function () {

        $resource(base_url + '/api/bpjs/referensi/faskes').get({
            faskes : angular.copy(vm.ref.faskes),
            start : 0,
            limit : 50
        }).$promise.then(function (result) {
            console.log(result);
            // var result = JSON.parse(result.data);
            // console.log(result);
            vm.faskes = result.response.response;

            // if(result.response.peserta){
            //     vm.peserta = result.response.peserta;
            // }

        }).catch(function (err) {
            console.log(err);
        });


    };

    $(document).ready(function(){
      $('#table-poli').dataTable({
          ajax: {
              url: base_url + '/api/bpjs/referensi/poli',
              method: 'get'
          },
          serverSide: false,
          processing: true,
          columns: [
              {data: 'kdPoli'},
              {data: 'nmPoli'},
              {
                  data: 'kdPoli',
                  orderable: false,
                  sortable: false,
                  searchable: false,
                  render: function(data, type, row, meta){
                      return "";
                  }
              }
          ]
      });
    });
}