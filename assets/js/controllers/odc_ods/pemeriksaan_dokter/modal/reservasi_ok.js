/**
 * Created by agungrizkyana on 10/5/16.
 */
/**
 * Modal Reservasi OK
 * @type {string[]}
 */
app.controller('ModalReservasiOKController', modalReservasiOKController);
modalReservasiOKController.$inject = [
    '$rootScope',
    '$scope',
    '$http',
    '$uibModalInstance',
    '$log',
    'DTOptionsBuilder',
    'DTColumnBuilder',
    'DTColumnDefBuilder',
    '$compile',
    'items',
    'CONFIG',
    'toastr',
    '$timeout',
    'EVENT'
];
function modalReservasiOKController($rootScope,
                                    $scope,
                                    $http,
                                    $uibModalInstance,
                                    $log,
                                    DTOptionsBuilder,
                                    DTColumnBuilder,
                                    DTColumnDefBuilder,
                                    $compile,
                                    items,
                                    CONFIG,
                                    toastr,
                                    $timeout,
                                    EVENT) {
    // $rootScope.isProcessing = true;
    // $rootScope.$emit('LOADING:EVENT:PROCESSING');

    $scope.data = items;
    var array = JSON.parse("[" + angular.copy($scope.data.diagnosa) + "]");

    $scope.data.diagnosaText = "[";
    _.each(angular.copy((typeof $scope.data.diagnosa === 'string')?array[0]:$scope.data.diagnosa ), function (item) {

        $http.get( base_url + '/api/rawat_jalan/pemeriksaan_dokter/icd10_byid/'+item).then(
            function (results) {
                console.log('sukses');
                console.log(results);
                $scope.data.diagnosaText += results.data+'] [';
            });
    });
    $scope.openJadwalOperasi = false;
    $scope.jenisOperasi = [];
    $scope.dataRuangOk = [];

    $scope.onChangeTanggalOperasi = onChangeTanggalOperasi;
    $scope.close = close;
    $scope.dismiss = dismiss;
    $scope.pesanJadwal = pesanJadwal;
    $scope.batal = batal;

    $http.get(CONFIG.JENIS_OPERASI).then(successReqJenisOperasi, errorReqJenisOperasi);
    $http.get(CONFIG.RUANG_OK).then(successReqRuangOk, errorReqRuangOk);

    function successReqJenisOperasi(result) {
        $rootScope.isProcessing = false;
        $scope.jenisOperasi = result.data.response;
    }

    function errorReqJenisOperasi(err) {
        console.log(err);
    }

    function successReqRuangOk(result) {
        $rootScope.isProcessing = false;
        $scope.dataRuangOk = result.data.response
    }

    function errorReqRuangOk(err) {
        console.log(err);
    }

    function pesanJadwal() {
        console.log("ini item diagnosa");
        console.log(JSON.stringify(items.diagnosa));
        var reservasi = {
            tanggal: moment($scope.data.tanggalOperasi).format('YYYY-MM-DD'),
            mulai: $scope.data.jamMulaiOperasi,
            selesai: $scope.data.jamSelesaiOperasi,
            jenis_operasi: $scope.data.jenis_operasi,
            operasi: $scope.data.operasi,
            ruang_ok_id: $scope.data.ruang_ok,
            pasien: $scope.data,
            diagnosa: items.diagnosa,
            tingkat_urgency: $scope.data.tingkat_urgency
        };

        console.log("ini item reservasi");
        console.log(reservasi);
       // delete reservasi.kunjunganUlang;


        var req = {
            method: 'POST',
            url: CONFIG.RESERVASI_JADWAL_OK,
            data: $.param(reservasi),
            headers: {
                'Content-type': 'application/x-www-form-urlencoded'
            }
        };
        $http.post(req.url, angular.copy(reservasi)).then(success, error);
        function success(result) {
            console.log(result);
            toastr.success('Berhasil Reservasi OK', 'Sukses!');
            $timeout(function () {
                close({
                    result: result,
                    rujuk_ok : 1
                });
            }, 500);
        }

        function error(err) {
            console.log(err);
        }
    }

    function batal(){
        var uid = items.reservasiOk.data.result.uid;

        $http.post(base_url + '/api/ok/jadwal_operasi/batal', {uid: uid}).then(function(result){
            close({result: result, rujuk_ok: 0});
        }, function(err){
            console.log(err);
        });
    }

    function onChangeTanggalOperasi() {
        var tanggalOperasi = $scope.data.tanggalOperasi;
        $scope.openJadwalOperasi = true;


    }

    function close(reservasiOk) {
        $uibModalInstance.close(reservasiOk);
        $rootScope.$emit(EVENT.CANCEL_RESERVASI_OK);
    }

    function dismiss() {
        $uibModalInstance.dismiss();
    }

    $timeout(function(){
        var selectOperasi = $('#operasi'),
            selectJenisOperasi = $('#jenis_operasi'),
            selectRuangOk = $('#ruang_ok');
        selectOperasi.select2({
            placeholder: 'PILIH OPERASI'
        });
        selectJenisOperasi.select2({
            placeholder: 'PILIH JENIS OPERASI'
        });
        selectRuangOk.select2({
            placeholder: 'PILIH RUANG OK'
        });



    }, 1000);

}