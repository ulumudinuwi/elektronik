var the_table;
var urlReq = base_url+ "/odc_ods/master_bed/list_bed";
var def = false;
var phari = false
var ppoli=false;
var save_method; //for save method string
var table;

$(window).load(function () {
    //Bagian Pasien


    $('#daftar-jadwal').click(function(){
        if(typeof the_table != 'undefined'){
            the_table.destroy();
        }
        phari=false;
        ppoli=false;
        create_table();
    });


    phari=false;
    ppoli=false;
    create_table();

});

function create_table() {


    var hari = $('#hari').val();
    var poli = $('#poli').val();

    the_table = $("#dataTable_user").DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": urlReq,
            "type": "POST",

        },
        "columns": [
            {
                "data": "tipe",
                "render" : function (data,type,row,meta){
                    var _tipe = [
                        {
                            id: 0,
                            text: 'BED'
                        },
                        {
                            id: 1,
                            text: 'MESIN'
                        }
                    ];
                    var _pekerjaanFind = _.find(_tipe, {id: parseInt(row.tipe)});
                    return (_pekerjaanFind) ? _pekerjaanFind.text : '-';
                }

            },
            {
                "data": "kode"
            },
            {
                "data": "nama"
            },
            {
                "data": "merek"
            },
            {
                "data": "serial_number"
            },

            {
                "data": "statusbed",
                "render" : function (data,type,row,meta){
                    var _pekerjaan = [
                        {
                            id: 0,
                            text: 'KOSONG'
                        },
                        {
                            id: 1,
                            text: 'ISI'
                        },
                        {
                            id: 2,
                            text: 'SEDANG DIBERSIHKAN'
                        },
                        {
                            id: 3,
                            text: 'RUSAK'
                        }
                    ];
                    var _pekerjaanFind = _.find(_pekerjaan, {id: parseInt(row.statusbed)});
                    return (_pekerjaanFind) ? _pekerjaanFind.text : '-';
                }

            },
            {
                "data": "action",
                "orderable": false,
                "render": function (data, type, row, meta) {
                    return '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_person('+"'"+row.id+"'"+')"><i class="glyphicon glyphicon-pencil"></i></a>  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_person('+"'"+row.id+"'"+')"><i class="glyphicon glyphicon-trash"></i> </a>';
                }
            }
        ]
    });
}


$(document).ready(function() {
    // $('#mulai_praktek').bootstrapMaterialDatePicker({ date: false,format : 'HH:mm' ,switchOnClick:true  });
    // $('#akhir_praktek').bootstrapMaterialDatePicker({ date: false,format : 'HH:mm' ,switchOnClick:true  });
    // $("#mulai_praktek").inputmask("h:s",{ "placeholder": "hh/mm" });
    // $("#akhir_praktek").inputmask("h:s",{ "placeholder": "hh/mm" });

    //set input/textarea/select event when change value, remove class error and remove text help block
    $("input").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("textarea").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("select2").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });

});


function add_jadwal()
{
    save_method = 'add';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#addJadwal').modal('show'); // show bootstrap modal
    $('.modal-title').text('Tambah Bed / Alat'); // Set Title to Bootstrap modal title
}

function edit_person(id)
{
    save_method = 'update';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string

    //Ajax Load data from ajax
    $.ajax({
        url : base_url+"/odc_ods/master_bed/ajax_edit/"+ id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {   console.log(data);

            $('[name="id"]').val(data.id);
            //$('[name="tipe"]').val(data.tipe);
            $('input:radio[name=tipe][value='+data.tipe+']')[0].checked = true;


            $('[name="kode"]').val(data.kode);
            $('[name="nama"]').val(data.nama);
            $('[name="merek"]').val(data.merek);
            $('[name="serial_number"]').val(data.serial_number);
            $('[name="status_penggunaan"]').select2().val(data.status_penggunaan).trigger("change");
            $('[name="deskripsi"]').val(data.deskripsi);

            $('#addJadwal').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Bed / Alat'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}

function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax
}

function save()
{
    $('#btnSave').text('saving...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable
    var url;

    if(save_method == 'add') {
        url = base_url+"/odc_ods/master_bed/ajax_add";
    } else {
        url = base_url+"/odc_ods/master_bed/ajax_update";
    }

    // ajax adding data to database
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {
            console.log("INIIIIIIIII");
            console.log(data);
            if(data.status) //if success close modal and reload ajax table
            {
                toastr.success('Data Telah Tersimpan', 'BERHASIL!');
                $('#addJadwal').modal('hide');
                the_table.draw();
            }
            else
            {
                for (var i = 0; i < data.inputerror.length; i++)
                {
                    $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                    $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
                }
            }

            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable
            // toastr.success('Simpan Jadwal Berhasil !', 'Sukses');
            // the_table.destroy();
            // create_table();
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            toastr.error('Error adding / update data', 'GAGAL!');

            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable

        }
    });
}

function delete_person(id)
{
    swal({
            title: "Apakah anda yakin ?",
            text: "data yang telah dihapus tidak dapat dikembalikan!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "YA, Hapus",
            cancelButtonText: "TIDAK!, Batalkan",
            closeOnConfirm: true,
            closeOnCancel: true
        },
        function(isConfirm){
            if (isConfirm) {
                $.ajax({
                    url : base_url+"/odc_ods/master_bed/ajax_delete/"+id,
                    type: "POST",
                    dataType: "JSON",
                    success: function(data)
                    {
                        //if success reload ajax table
                        console.log("hapusssss");
                        $('#modal_form').modal('hide');
                        the_table.draw();
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert('Error deleting data');
                    }
                });

                toastr.success('Data Telah Terhapus', 'BERHASIL!');

            } else {
                //swal("Batal", "Data Tidak Terhapus :)", "error");
            }
        });

}