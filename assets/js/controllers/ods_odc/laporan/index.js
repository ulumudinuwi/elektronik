/**
 * Created by agungrizkyana on 9/19/16.
 */
app.controller('RawatJalanLaporanController', rawatJalanLaporanController);

rawatJalanLaporanController.$inject = ['$rootScope', '$scope', '$resource', 'toastr', '$http'];
function rawatJalanLaporanController($rootScope, $scope, $resource, toastr, $http){
    $scope.dataLaporan = [
        {
            id : 0,
            nama : 'Pilih Laporan'
        },
        {
            id : 1,
            nama : 'Daftar Nama Pasien Rawat Jalan'
        },
        {
            id : 2,
            nama : 'Data Sensus Pasien Rawat Jalan'
        },
        {
            id : 3,
            nama : 'Indeks Pasien Per Dokter'
        },
        {
            id : 4,
            nama : 'Indeks Pasien Per Pekerjaan'
        },
        {
            id : 5,
            nama : 'Daftar Pasien Rawat Jalan'
        },
        {
            id : 6,
            nama : 'Rekapitulasi Kunjungan Pasien Rawat Jalan & IGD per Klinik'
        },
        {
            id : 7,
            nama : 'Register Pasien Rawat Jalan'
        },
        {
            id : 8,
            nama : 'Rekapitulasi Harian Pasien Rawat Jalan'
        },
        {
            id : 9,
            nama : 'Indeks Pasien Per Penyakit'
        },
        {
            id : 10,
            nama : 'Indeks Pasien Per Poliklinik'
        },
        {
            id : 11,
            nama : 'Rekapitulasi Kunjungan Pasien Rawat Jalan & IGD per Bulan'
        },
        {
            id : 12,
            nama : 'Rekapitulasi Bulanan Pasien Rawat Jalan'
        },
        {
            id : 13,
            nama : 'Indeks Pasien per Pendidikan'
        },
        {
            id : 14,
            nama : 'Indeks Pasien per Wilayah'
        },
        {
            id : 14,
            nama : '10 Besar Penyakit dan Tindakan'
        }

    ];
    $scope.data = {

    };

    $scope.isNamaPasienRawatJalan = false;

    $scope.onChangeLaporan = function(laporan){
        $scope.data.laporan = laporan;
        switch (laporan.id) {
            case 1:
                $scope.isNamaPasienRawatJalan = true;
                $http.get(base_url + "/api/master/layanan/rawat_jalan").then(function(result){
                    $scope.dataLayanan = angular.copy(result.data);
                });
                break;
        }
    };



    $scope.pdf = function(){
        console.log($scope.data);
        if($scope.data.laporan.id == 0){
            toastr.error('Pilih Laporan Terlebih Dahulu !');
        }
        switch ($scope.data.laporan.id){
            case 1 :
                console.log($scope.data);
                window.location.href = base_url + '/api/rawat_jalan/laporan/daftar_nama_pasien_rawat_jalan_pdf?layanan=' + $scope.data.layananId.id + '&tanggal=' + $scope.data.tanggal;
                break;
        }
    };

    $scope.excel = function(){
        console.log($scope.data);
        if($scope.data.laporan.id == 0){
            toastr.error('Pilih Laporan Terlebih Dahulu !');
        }
        switch ($scope.data.laporan.id){
            case 1 :
                console.log($scope.data);
                window.location.href = base_url + '/api/rawat_jalan/laporan/daftar_nama_pasien_rawat_jalan_excel?layanan=' + $scope.data.layananId.id + '&tanggal=' + $scope.data.tanggal;
                break;
        }
    }
}