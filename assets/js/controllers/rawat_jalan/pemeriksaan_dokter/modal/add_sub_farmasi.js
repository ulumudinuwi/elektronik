app.controller('ModalAddSubFarmasiController', modalAddSubFarmasi);
modalAddSubFarmasi.$inject = ['$scope', 'api', '$http', '$uibModalInstance', '$log', '$uibModal'];
function modalAddSubFarmasi($scope, api, $http, $uibModalInstance, $log, $uibModal) {

    $scope.choose = {};
    $scope.qty = 1;

    api.Tindakan.tindakanForm().$promise.then(function (result) {
        console.log("result, ", result);
        $scope.dataTindakanOnModal = result;
    }).catch(function (err) {
        console.log(err);
    });

    $scope.onChoose = function (tindakan, qty) {
        // console.log(tindakan);
        $scope.choose = tindakan;
    };


    $scope.close = function () {
        console.log("data : ", angular.copy($scope.choose));
        $uibModalInstance.close(angular.copy($scope.choose));
    };

    $scope.dismiss = function () {
        $uibModalInstance.dismiss();
    };

}