/**
 * Created by agungrizkyana on 10/5/16.
 */
app.controller('ModalAddFarmasiController', modalAddFarmasi);

modalAddFarmasi.$inject = ['$scope', 'api', '$http', '$uibModalInstance', '$log', '$uibModal'];
function modalAddFarmasi($scope, api, $http, $uibModalInstance, $log, $uibModal) {

    $scope.choose = {};
    $scope.qty = 1;
    api.Tindakan.tindakanForm().$promise.then(function (result) {
        console.log("result, ", result);
        $scope.dataTindakanOnModal = result;
    }).catch(function (err) {
        console.log(err);
    });

    $scope.onChoose = function (tindakan, qty) {
        // console.log(tindakan);
        $scope.choose = tindakan;
    };

    $scope.openModalSubFarmasi = function (size) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'modalAddSubFarmasi.html',
            controller: 'ModalAddSubFarmasiController',
            size: size,
            resolve: {
                items: function () {
                    return $scope.items;
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            // $scope.selected = selectedItem;
            $scope.dataObat.push(selectedItem);
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };


    $scope.close = function () {
        console.log("data : ", angular.copy($scope.choose));
        $uibModalInstance.close(angular.copy($scope.choose));
    };

    $scope.dismiss = function () {
        $uibModalInstance.dismiss();
    };

}