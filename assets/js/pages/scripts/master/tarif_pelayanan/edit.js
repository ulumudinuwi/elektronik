var Edit = function () {
	
	var caraBayarId = 0;
	var kelas = 0;
	var bedah = 0;
	var numericOptions = {aSep: '.', aDec: ',', mDec: 2, vMax: '99999999999999.99', vMin: '-99999999999999.99'}

	var showMessage = function(title, msg, type) {
		var sClass = '';
		switch (type) {
			case 'primary':
				sClass = 'bg-primary';
				break;
			case 'danger':
				sClass = 'bg-danger';
				break;
			case 'success':
				sClass = 'bg-success';
				break;
			case 'warning':
				sClass = 'bg-warning';
				break;
			case 'info':
				sClass = 'bg-info';
				break;
		}
		new PNotify({
            title: title,
            text: msg,
            addclass: sClass
        });
	};
	
	var formHandle = function() {
		
		$('.komponen-tarif').autoNumeric('init', numericOptions);
		
		$(".styled").uniform({
			radioClass: 'choice'
		});
		
		if (parseInt($('#id').val()) === 0) {
			var caraBayarId = $('#cara_bayar_id').val();
			var kelas = parseInt($('#kelas').val());
			var kelasId = 0;
			var bedah = parseInt($('#bedah').val());
			var golonganOperasi = 0;
			if (kelas === 0) {
				kelasId = 0;
			}
			else {
				kelasId = $('#kelas_id').val();
			}
			if (bedah === 0) {
				golonganOperasi = 0;
			}
			else {
				golonganOperasi = $('#golongan_operasi').val();
			}
			var komponenTarif = '<tr>';
			$('.komponen-tarif').each(function() {
				var id = $(this).data('id');
				komponenTarif += '<td><input type="hidden" id="komponen_tarif_' + caraBayarId + '_' + kelasId + '_' + golonganOperasi + '_' + id + '" name="komponen_tarif_' +caraBayarId + '_0_0_' + id + '" value="0" /></td>';
			});
			komponenTarif += '</tr>';
			$('#komponen_tarif_table').append(komponenTarif);
		}
		
		$('#disp_kelas').on('change', function() {
			if ($(this).is(':checked')) {
				$('#kelas_section').show();
				$('#kelas').val(1);
			}
			else {
				$('#kelas_section').hide();
				$('#kelas').val(0);
			}
		});

		$('#disp_bedah').on('change', function() {
			if ($(this).is(':checked')) {
				$('#golongan_operasi_section').show();
				$('#bedah').val(1);
			}
			else {
				$('#golongan_operasi_section').hide();
				$('#bedah').val(0);
			}
		});

		$('#cara_bayar_id').on('change', function() {
			var caraBayarId = $('#cara_bayar_id').val();
			$('.komponen-tarif').each(function() {
				$(this).val($('#komponen_tarif_' + caraBayarId + '_0_0_' + $(this).data('id')).val());
			});
		});

		$('.komponen-tarif').on('focus', function() {
			$(this).select();
		});

		$('.komponen-tarif').on('change input keyup', function() {
			$('#komponen_tarif_' + $('#cara_bayar_id').val() + '_0_0_' + $(this).data('id')).val($(this).val());
		});
		
		var tarifPelayananApp = {
			initKasirForm: function () {
				$("#tarif_pelayanan_form").validate({
					rules: {
					},
					messages: {
					},
					submitHandler: function(form) {
						tarifPelayananApp.addKasir($(form));
					}
				});
			},
			addKasir: function(form) {
				//blockElement('#main_section');
				var url = url_simpan;
				var postData = form.serialize();
				$.post(url, postData, function(data, status) {
					if (status === "success") {
						showMessage('Simpan', 'Record telah di simpan!', 'success');
						return;
					}
					showMessage('Simpan', 'Record gagal di simpan!', 'danger');
				}, 'json');
			}
		};
		tarifPelayananApp.initKasirForm();
		
	}
	
    return {

        init: function() {
			
			formHandle();
            
            $(document).ajaxComplete(function(event, xhr, settings ) {
				if (typeof xhr.responseJSON != 'undefined') {
					switch (xhr.responseJSON.tag) {
						case 'simpan':
							//$('#main_section').unblock();
							window.location = url_index;
							break;
					};
				}
            });
			
        }

    };

}();