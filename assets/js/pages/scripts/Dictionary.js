function Dictionary() {

	this.datastore = new Array();
	
	this.add = function(key, value) {
		this.datastore[key] = value;
	}
	
	this.find = function(key) {
		return this.datastore[key];
	}
	
	this.remove = function(key) {
		delete this.datastore[key];
	}
	
	this.showAll = function() {
		for (var key in this.datastore) {
			console.log(key + " -> " + this.datastore[key]);
		}
	}
	
	this.exists = function(key) {
		return (key in this.datastore);
    }
	
	this.each = function(callback) {
        this.datastore.forEach(callback);
        return this;
    }
	
}