<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$lang['id'] = 'ID';
$lang['kode'] = 'KODE';
$lang['nama'] = 'NAMA';
$lang['nama'] = 'NAMA DETAIL';
$lang['statusbed'] = 'MEREK';
$lang['ruang'] = 'SERIAL NUMBER';
$lang['kelas'] = 'Kelas';
$lang['action'] = 'Aksi';


$lang['already_taken'] = '%s sudah dipakai.';

$lang['jadwal_operasi_delete_confirm'] = 'Anda yakin akan menghapus Jadwal Operasi ini?';
$lang['jadwal_operasi_added'] = 'Jadwal Operasi baru telah berhasil dibuat.';
$lang['jadwal_operasi_updated'] = 'Jadwal Operasi telah berhasil diperbarui.';
$lang['jadwal_operasi_deleted'] = 'Jadwal Operasi telah berhasil dihapus.';
