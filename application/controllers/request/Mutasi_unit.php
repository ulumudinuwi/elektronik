<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mutasi_unit extends Admin_Controller 
{
  protected $page_title = '<i class="icon-book"></i> Mutasi Unit ke Gudang Logistik';
  protected $table_def = 't_request_mutasi_unit';
  protected $unit;

  public function __construct()
  {
    parent::__construct();
    $this->lang->load('barang');
    $this->unit = $this->db->where('status', 1)
                        ->get('m_unitkerja')->result();
  }

  public function index()
  {
    $this->data['tabUid'] = base64_encode($this->table_def);
    $this->data['unit'] = $this->unit;
    $this->data['status'] = $this->config->item('status_mutasi');
    $this->data['page_icons'] = '<a href="'. site_url("request/mutasi_unit/add") .'" class="btn btn-primary btn-labeled"><b><i class="icon-plus-circle2"></i></b>Buat Mutasi</a>';
    $this->template
          ->set_js('plugins/tables/datatables/datatables.min', FALSE)
          ->set_js('plugins/notifications/bootbox.min', FALSE)
          ->set_js('plugins/notifications/sweet_alert.min', FALSE)
          ->set_js('plugins/ui/moment/moment.min', FALSE)
          ->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
          ->set_script('request/unit-kerja/mutasi/script-index')
          ->build('request/unit-kerja/mutasi/index', $this->data);
  }

  /**
   * Edit
   */
  function edit($uid)
  {
    $this->_updatedata($uid);
  }
  
  /**
   * Tambah
   */
  function add()
  {
    $this->_updatedata(0);
  }
  
  function _updatedata($uid = 0)
  {
    $this->data['uid'] = $uid;
    $this->data['unit'] = $this->unit;
    $this->template
          ->set_js('core/libraries/jquery_ui/interactions.min', TRUE)
          ->set_js('core/libraries/jquery_ui/widgets.min', TRUE)
          ->set_js('core/libraries/jquery_ui/effects.min', TRUE)
          ->set_js('plugins/forms/validation/validate.min.js', TRUE)
          ->set_js('plugins/tables/datatables/datatables.min', FALSE)
          ->set_js('plugins/notifications/bootbox.min', FALSE)
          ->set_js('plugins/notifications/sweet_alert.min', FALSE)
          ->set_js('plugins/ui/moment/moment.min', FALSE)
          ->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
          ->set_js('plugins/editors/wysihtml5/wysihtml5.min.js', TRUE)
          ->set_js('plugins/editors/wysihtml5/toolbar.js', TRUE)
          ->set_js('plugins/editors/wysihtml5/parsers.js', TRUE)
          ->set_script('request/unit-kerja/mutasi/script-form')
          ->build('request/unit-kerja/mutasi/form', $this->data);  
  }
}