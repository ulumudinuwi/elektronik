<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Stock extends Admin_Controller 
{
	protected $page_title = '<i class="icon-book"></i> Stock Barang';
	protected $table_def = 't_logistik_stock';
	protected $table_def_kartu_stock = 't_logistik_kartu_stock';
	protected $table_def_barang = 'm_barang'; 

	public function __construct()
	{
		parent::__construct();
		$this->load->model('logistik/Stock_model', 'main');
	}

	public function index()
	{
		$this->data['pUid'] = base64_encode($this->table_def);
		$this->template
			->set_js('plugins/tables/datatables/datatables.min', FALSE)
			->set_js('plugins/notifications/bootbox.min', FALSE)
			->set_js('plugins/notifications/sweet_alert.min', FALSE)
			->set_js('plugins/ui/moment/moment.min', FALSE)
			->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
			->build('logistik/stock/index', $this->data);
	}

	public function kartu_stock()
	{
		if(!$this->input->get('uid')) {
			show_404(); exit();
		}
		$id = json_decode(base64_decode($this->input->get('uid')));

		$aSelect = array(
	        "{$this->table_def}.barang_id",
	        "{$this->table_def_barang}.kode",
	        "{$this->table_def_barang}.nama",
	        "CONCAT(satuan_penggunaan.nama, ' (', satuan_penggunaan.singkatan, ')') satuan_penggunaan",
	    );
		$objStock = $this->main->get_by("WHERE {$this->table_def}.id = {$id}", $aSelect);

		$this->data['pUid'] = base64_encode($this->table_def_kartu_stock) ;
		$this->data['param'] = base64_encode($objStock->barang_id);
		$this->data['page_icons'] = '<a href="'. site_url("logistik/stock") .'" class="btn btn-default">Kembali</a>';
		$this->data['page_title'] = "Kartu Stock: <b>{$objStock->kode} - {$objStock->nama}</b> <span class='text-warning text-size-mini'>Satuan: <b>{$objStock->satuan_penggunaan}</b></span>";
		$this->template
			->set_js('plugins/tables/datatables/datatables.min', FALSE)
			->set_js('plugins/notifications/bootbox.min', FALSE)
			->set_js('plugins/notifications/sweet_alert.min', FALSE)
			->set_js('plugins/ui/moment/moment.min', FALSE)
			->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
			->build('logistik/stock/kartu-stock', $this->data);
	}
}