<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Penerimaan_mutasi extends Admin_Controller 
{
  protected $page_title = '<i class="icon-book"></i> Penerimaan Mutasi dari Unit';
  protected $table_def = 't_logistik_penerimaan_mutasi';
  protected $table_def_mutasi = 't_request_mutasi_unit';
  protected $def_uri = 'logistik/penerimaan-mutasi';
  protected $pabrik;

  public function __construct()
  {
    parent::__construct();
    $this->lang->load('barang');
    $this->unit = $this->db->where('status', 1)
                        ->get('m_unitkerja')->result();
  }

  public function index()
  {
    $this->data['tab1Uid'] = base64_encode($this->table_def_mutasi);
    $this->data['tab2Uid'] = base64_encode($this->table_def);
    $this->data['unit'] = $this->unit;
    $this->template
          ->set_js('plugins/tables/datatables/datatables.min', FALSE)
          ->set_js('plugins/notifications/bootbox.min', FALSE)
          ->set_js('plugins/notifications/sweet_alert.min', FALSE)
          ->set_js('plugins/ui/moment/moment.min', FALSE)
          ->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
          ->set_script($this->def_uri.'/script-index')
          ->build($this->def_uri.'/index', $this->data);
  }

  function form($uid = 0)
  {
    $this->_updatedata($uid);
  }
  
  function _updatedata($uid = 0)
  {
    $this->data['uid'] = $uid;
    $this->data['unit'] = $this->unit;
    $this->template
          ->set_js('core/libraries/jquery_ui/interactions.min', TRUE)
          ->set_js('core/libraries/jquery_ui/widgets.min', TRUE)
          ->set_js('core/libraries/jquery_ui/effects.min', TRUE)
          ->set_js('plugins/forms/validation/validate.min.js', TRUE)
          ->set_js('plugins/tables/datatables/datatables.min', FALSE)
          ->set_js('plugins/notifications/bootbox.min', FALSE)
          ->set_js('plugins/notifications/sweet_alert.min', FALSE)
          ->set_js('plugins/ui/moment/moment.min', FALSE)
          ->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
          ->set_js('plugins/editors/wysihtml5/wysihtml5.min.js', TRUE)
          ->set_js('plugins/editors/wysihtml5/toolbar.js', TRUE)
          ->set_js('plugins/editors/wysihtml5/parsers.js', TRUE)
          ->set_script($this->def_uri.'/script-form')
          ->build($this->def_uri.'/form', $this->data);  
  }
}