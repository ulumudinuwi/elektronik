<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Po extends Admin_Controller 
{
  protected $page_title = '<i class="icon-book"></i> Purchase Order (PO)';
  protected $table_def = 't_logistik_po';
  protected $table_def_stock = 't_logistik_stock';
  protected $def_uri = 'logistik/po';
  protected $pabrik;

  public function __construct()
  {
    parent::__construct();
    $this->lang->load('barang');
    $this->pabrik = $this->db->where('status', 1)
                        ->get('m_pabrik')->result();
  }

  public function index()
  {
    $this->data['tab1Uid'] = base64_encode($this->table_def_stock);
    $this->data['tab2Uid'] = base64_encode($this->table_def);
    $this->data['pabrik'] = $this->pabrik;
    $this->data['sifat'] = $this->config->item('sifat_po');
    $this->data['status'] = $this->config->item('status_po');
    $this->data['page_icons'] = '<a href="'. site_url($this->def_uri."/add") .'" class="btn btn-primary btn-labeled"><b><i class="icon-plus-circle2"></i></b>Buat Draft</a>';
    $this->template
          ->set_js('plugins/tables/datatables/datatables.min', FALSE)
          ->set_js('plugins/notifications/bootbox.min', FALSE)
          ->set_js('plugins/notifications/sweet_alert.min', FALSE)
          ->set_js('plugins/ui/moment/moment.min', FALSE)
          ->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
          ->set_script($this->def_uri.'/script-index')
          ->build($this->def_uri.'/index', $this->data);
  }

  /**
   * Edit
   */
  function edit($uid)
  {
    $this->_updatedata($uid);
  }
  
  /**
   * Tambah
   */
  function add()
  {
    $data = $this->input->get('data') ? $this->input->get('data') : "";
    $this->_updatedata(0, $data);
  }
  
  function _updatedata($uid = 0, $data = "")
  {
    $this->data['uid'] = $uid;
    $this->data['data'] = $data;
    $this->data['pabrik'] = $this->pabrik;
    $this->data['sifat'] = $this->config->item('sifat_po');
    $this->template
          ->set_js('core/libraries/jquery_ui/interactions.min', TRUE)
          ->set_js('core/libraries/jquery_ui/widgets.min', TRUE)
          ->set_js('core/libraries/jquery_ui/effects.min', TRUE)
          ->set_js('plugins/forms/validation/validate.min.js', TRUE)
          ->set_js('plugins/tables/datatables/datatables.min', FALSE)
          ->set_js('plugins/notifications/bootbox.min', FALSE)
          ->set_js('plugins/notifications/sweet_alert.min', FALSE)
          ->set_js('plugins/ui/moment/moment.min', FALSE)
          ->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
          ->set_js(assets_url_version('assets/js/pages/scripts/gudang/gudang.js'))
          ->set_script($this->def_uri.'/script-form')
          ->build($this->def_uri.'/form', $this->data);  
  }

  /*
  * Pengalihan
  */
  function pengalihan($uid = 0)
  {
    if($uid === 0) {
      show_404(); exit();
    } 

    $this->data['page_title'] = '<i class="icon-book"></i> Pengalihan PO';
    $this->data['uid'] = $uid;
    $this->data['pabrik'] = $this->pabrik;
    $this->data['sifat'] = $this->config->item('sifat_po');
    $this->template
          ->set_js('core/libraries/jquery_ui/interactions.min', TRUE)
          ->set_js('core/libraries/jquery_ui/widgets.min', TRUE)
          ->set_js('core/libraries/jquery_ui/effects.min', TRUE)
          ->set_js('plugins/forms/validation/validate.min.js', TRUE)
          ->set_js('plugins/tables/datatables/datatables.min', FALSE)
          ->set_js('plugins/notifications/bootbox.min', FALSE)
          ->set_js('plugins/notifications/sweet_alert.min', FALSE)
          ->set_js('plugins/ui/moment/moment.min', FALSE)
          ->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
          ->set_js('plugins/editors/wysihtml5/wysihtml5.min.js', TRUE)
          ->set_js('plugins/editors/wysihtml5/toolbar.js', TRUE)
          ->set_js('plugins/editors/wysihtml5/parsers.js', TRUE)
          ->set_script('logistik/po/pengalihan/script-form')
          ->build('logistik/po/pengalihan/form', $this->data);  
  }
}