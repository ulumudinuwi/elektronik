<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pengeluaran extends Admin_Controller 
{
  protected $page_title = '<i class="icon-book"></i> Pengeluaran';
  protected $table_def = 't_logistik_pengeluaran';
  protected $table_def_permintaan = 't_request_permintaan_unit';
  protected $def_uri = 'logistik/pengeluaran';
  protected $unit;

  public function __construct()
  {
    parent::__construct();
    $this->lang->load('barang');
    $this->unit = $this->db->where('status', 1)
                        ->get('m_unitkerja')->result();
  }

  public function index()
  {
    $status_request = $this->config->item('status_request');
    unset($status_request[$this->config->item('status_request_closed')]);

    $this->data['tab1Uid'] = base64_encode($this->table_def_permintaan);
    $this->data['tab2Uid'] = base64_encode($this->table_def);
    $this->data['unit'] = $this->unit;
    $this->data['sifat'] = $this->config->item('sifat_po');
    $this->data['status_request'] = $status_request;
    $this->data['status'] = $this->config->item('status_pengeluaran');
    $this->template
          ->set_js('plugins/tables/datatables/datatables.min', FALSE)
          ->set_js('plugins/notifications/bootbox.min', FALSE)
          ->set_js('plugins/notifications/sweet_alert.min', FALSE)
          ->set_js('plugins/ui/moment/moment.min', FALSE)
          ->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
          ->set_script($this->def_uri.'/script-index')
          ->build($this->def_uri.'/index', $this->data);
  }

  function edit($uid = 0)
  {
    $this->_updatedata($uid, 'edit');
  }

  function form($uid = 0)
  {
    $this->_updatedata($uid);
  }
  
  function _updatedata($uid = 0, $mode = "add")
  {
    $this->data['uid'] = $uid;
    $this->data['mode'] = $mode;
    $this->data['unit'] = $this->unit;
    $this->data['sifat'] = $this->config->item('sifat_po');
    $this->template
          ->set_js('core/libraries/jquery_ui/interactions.min', TRUE)
          ->set_js('core/libraries/jquery_ui/widgets.min', TRUE)
          ->set_js('core/libraries/jquery_ui/effects.min', TRUE)
          ->set_js('plugins/forms/validation/validate.min.js', TRUE)
          ->set_js('plugins/tables/datatables/datatables.min', FALSE)
          ->set_js('plugins/notifications/bootbox.min', FALSE)
          ->set_js('plugins/notifications/sweet_alert.min', FALSE)
          ->set_js('plugins/ui/moment/moment.min', FALSE)
          ->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
          ->set_js('plugins/editors/wysihtml5/wysihtml5.min.js', TRUE)
          ->set_js('plugins/editors/wysihtml5/toolbar.js', TRUE)
          ->set_js('plugins/editors/wysihtml5/parsers.js', TRUE)
          ->set_js(assets_url_version('assets/js/pages/scripts/gudang/gudang.js'))
          ->set_script($this->def_uri.'/script-form')
          ->build($this->def_uri.'/form', $this->data);  
  }
}