<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Retur_barang extends Admin_Controller 
{
  protected $page_title = '<i class="icon-book"></i> Retur Barang ke Distributor';
  protected $table_def = 't_logistik_retur_barang';
  protected $pabrik, $vendor;

  public function __construct()
  {
    parent::__construct();
    $this->lang->load('barang');
    $this->pabrik = $this->db->where('status', 1)
                        ->get('m_pabrik')->result();
  }

  public function index()
  {
    $this->data['tabUid'] = base64_encode($this->table_def);
    $this->data['pabrik'] = $this->pabrik;
    $this->data['page_icons'] = '<a href="'. site_url("logistik/retur_barang/add") .'" class="btn btn-primary btn-labeled"><b><i class="icon-plus-circle2"></i></b>Buat Retur</a>';
    $this->template
          ->set_js('plugins/tables/datatables/datatables.min', FALSE)
          ->set_js('plugins/notifications/bootbox.min', FALSE)
          ->set_js('plugins/notifications/sweet_alert.min', FALSE)
          ->set_js('plugins/ui/moment/moment.min', FALSE)
          ->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
          ->set_script('logistik/retur-barang/script-index')
          ->build('logistik/retur-barang/index', $this->data);
  }

  function add()
  {
    $this->_updatedata(0);
  }
  
  function _updatedata($uid = 0)
  {
    $this->data['uid'] = $uid;
    $this->data['pabrik'] = $this->pabrik;
    $this->template
          ->set_js('core/libraries/jquery_ui/interactions.min', TRUE)
          ->set_js('core/libraries/jquery_ui/widgets.min', TRUE)
          ->set_js('core/libraries/jquery_ui/effects.min', TRUE)
          ->set_js('plugins/forms/validation/validate.min.js', TRUE)
          ->set_js('plugins/tables/datatables/datatables.min', FALSE)
          ->set_js('plugins/notifications/bootbox.min', FALSE)
          ->set_js('plugins/notifications/sweet_alert.min', FALSE)
          ->set_js('plugins/ui/moment/moment.min', FALSE)
          ->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
          ->set_js('plugins/editors/wysihtml5/wysihtml5.min.js', TRUE)
          ->set_js('plugins/editors/wysihtml5/toolbar.js', TRUE)
          ->set_js('plugins/editors/wysihtml5/parsers.js', TRUE)
          ->set_js(assets_url_version('assets/js/pages/scripts/gudang/gudang.js'))
          ->set_script('logistik/retur-barang/script-form')
          ->build('logistik/retur-barang/form', $this->data);  
  }
}