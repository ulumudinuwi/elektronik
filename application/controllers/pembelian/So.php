<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class So extends Admin_Controller 
{
	protected $page_title = '<i class="icon-book"></i> Stock Opname';
	protected $table_def = 't_gudang_farmasi_stock_opname';

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->data['tabUid'] = base64_encode($this->table_def);
		$this->template
			->set_js('plugins/tables/datatables/datatables.min', FALSE)
            ->set_js('plugins/tables/datatables/extensions/fixed_columns.min', FALSE)
			->set_js('plugins/notifications/bootbox.min', FALSE)
			->set_js('plugins/notifications/sweet_alert.min', FALSE)
			->set_js('plugins/ui/moment/moment.min', FALSE)
			->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
			->set_js('plugins/forms/inputs/formatter.min')
			->set_script('gudang-farmasi/stock-opname/script-index')
			->build('gudang-farmasi/stock-opname/index', $this->data);
	}


	function double_check($uid = 0)
	{
		if($uid === 0) {
			show_404(); exit();
		} 

		$this->data['page_title'] = '<i class="icon-book"></i> Pemeriksaan Kedua - Stock Opname';
		$this->data['uid'] = $uid;
		$this->template
			->set_js('core/libraries/jquery_ui/interactions.min', TRUE)
			->set_js('core/libraries/jquery_ui/widgets.min', TRUE)
			->set_js('core/libraries/jquery_ui/effects.min', TRUE)
			->set_js('plugins/forms/validation/validate.min.js', TRUE)
			->set_js('plugins/tables/datatables/datatables.min', FALSE)
			->set_js('plugins/tables/datatables/extensions/fixed_columns.min', FALSE)
			->set_js('plugins/notifications/bootbox.min', FALSE)
			->set_js('plugins/notifications/sweet_alert.min', FALSE)
			->set_js('plugins/ui/moment/moment.min', FALSE)
			->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
			->set_js('plugins/editors/wysihtml5/wysihtml5.min.js', TRUE)
			->set_js('plugins/editors/wysihtml5/toolbar.js', TRUE)
			->set_js('plugins/editors/wysihtml5/parsers.js', TRUE)
			->set_js('plugins/forms/inputs/formatter.min')
			->set_script('gudang-farmasi/stock-opname/double-check/script-form')
			->build('gudang-farmasi/stock-opname/double-check/form', $this->data);  
	}
}