<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Stock extends Admin_Controller 
{
	protected $page_title = '<i class="icon-book"></i> Stock Barang';
	protected $table_def = 't_gudang_farmasi_stock';
	protected $table_def_kartu_stock = 't_gudang_farmasi_kartu_stock';
	protected $table_def_barang = 'm_barang'; 
	protected $table_def_pabrik = 'm_pabrik'; 

	protected $pabrik, $kategori;

	public function __construct()
	{
		parent::__construct();
		$this->lang->load('barang');
		$this->load->model('gudang_farmasi/Stock_model', 'main');
		$this->load->model('master/Kategori_barang_model', 'kategori_main');

		$this->pabrik = $this->db->where('status', 1)
								->get('m_pabrik')->result();

		$this->kategori = $this->kategori_main->get_all(0, 0, "")['data'];
	}

	public function index()
	{
		$this->data['pabrik'] = $this->pabrik;
		$this->data['kategori'] = $this->kategori;
		$this->data['pUid'] = base64_encode($this->table_def);
		$this->template
			->set_js('plugins/tables/datatables/datatables.min', FALSE)
			->set_js('plugins/notifications/bootbox.min', FALSE)
			->set_js('plugins/notifications/sweet_alert.min', FALSE)
			->set_js('plugins/ui/moment/moment.min', FALSE)
			->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
			->set_js(assets_url_version('assets/js/pages/scripts/gudang/gudang.js'))
			->build('gudang-farmasi/stock/index', $this->data);
	}

	public function kartu_stock()
	{
		if(!$this->input->get('uid')) {
			show_404(); exit();
		}
		$id = json_decode(base64_decode($this->input->get('uid')));

		$aSelect = array(
	        "{$this->table_def}.barang_id",
	        "{$this->table_def_barang}.kode",
	        "{$this->table_def_barang}.nama",
	        "CONCAT(satuan_penggunaan.nama, ' (', satuan_penggunaan.singkatan, ')') satuan_penggunaan",
	    );
		$objStock = $this->main->get_by("WHERE {$this->table_def}.id = {$id}", $aSelect);

		$this->data['pUid'] = base64_encode($this->table_def_kartu_stock) ;
		$this->data['param'] = base64_encode($objStock->barang_id);
		$this->data['page_icons'] = '<a href="'. site_url("gudang_farmasi/stock") .'" class="btn btn-default">Kembali</a>';
		$this->data['page_title'] = "Kartu Stock: <b>{$objStock->kode} - {$objStock->nama}</b> <span class='text-warning text-size-mini'>Satuan: <b>{$objStock->satuan_penggunaan}</b></span>";
		$this->template
			->set_js('plugins/tables/datatables/datatables.min', FALSE)
			->set_js('plugins/notifications/bootbox.min', FALSE)
			->set_js('plugins/notifications/sweet_alert.min', FALSE)
			->set_js('plugins/ui/moment/moment.min', FALSE)
			->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
			->build('gudang-farmasi/stock/kartu-stock', $this->data);
	}
}