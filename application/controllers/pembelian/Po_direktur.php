<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Po_direktur extends Admin_Controller 
{
	protected $page_title = '<i class="icon-book"></i> Purchase Order (PO) Direktur';
	protected $table_def = 't_gudang_farmasi_po';
	protected $def_uri = 'gudang_farmasi/po_direktur';
	protected $pabrik;

	public function __construct()
	{
		parent::__construct();
		$this->lang->load('kurs');
		$this->lang->load('barang');
		$this->load->model('master/Pabrik_model');
		$this->pabrik = $this->Pabrik_model->get_all(0, 0, 'WHERE m_pabrik.status = 1')['data'];
	}

	public function index()
	{
		$this->data['tabUid'] = base64_encode($this->table_def);
		$this->data['pabrik'] = $this->pabrik;
		$this->data['sifat'] = $this->config->item('sifat_po');
		$this->data['status'] = $this->config->item('status_po');
		$this->template
					->set_js('plugins/tables/datatables/datatables.min', FALSE)
					->set_js('plugins/notifications/bootbox.min', FALSE)
					->set_js('plugins/notifications/sweet_alert.min', FALSE)
					->set_js('plugins/ui/moment/moment.min', FALSE)
					->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
					->set_script("gudang-farmasi/po/direktur/script-index")
					->build("gudang-farmasi/po/direktur/index", $this->data);
	}

	/**
	 * Edit
	 */
	function form($uid)
	{
		if(!$uid) {
			show_404();
			exit();
		}

		$this->data['uid'] = $uid;
		$this->data['pabrik'] = $this->pabrik;
		$this->data['sifat'] = $this->config->item('sifat_po');
		$this->template
					->set_js('core/libraries/jquery_ui/interactions.min', TRUE)
					->set_js('core/libraries/jquery_ui/widgets.min', TRUE)
					->set_js('core/libraries/jquery_ui/effects.min', TRUE)
					->set_js('plugins/forms/validation/validate.min.js', TRUE)
					->set_js('plugins/tables/datatables/datatables.min', FALSE)
					->set_js('plugins/notifications/bootbox.min', FALSE)
					->set_js('plugins/notifications/sweet_alert.min', FALSE)
					->set_js('plugins/ui/moment/moment.min', FALSE)
					->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
					->set_js(assets_url_version('assets/js/pages/scripts/gudang/gudang.js'))
					->set_script("gudang-farmasi/po/direktur/script-form")
					->build("gudang-farmasi/po/direktur/form", $this->data);  
	}
}