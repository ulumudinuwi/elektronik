<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Konfirmasi_pembelian extends Admin_Controller 
{
	protected $page_title = '<i class="icon-people"></i> Konfirmasi Pembelian';
	protected $table_def = 't_pos_checkout';

	public function __construct()
	{
		parent::__construct();
		$this->lang->load('barang');
	}

	public function index()
	{
		$this->template
			->set_js('plugins/tables/datatables/datatables.min', FALSE)
			->set_js('plugins/notifications/bootbox.min', FALSE)
			->set_js('plugins/notifications/sweet_alert.min', FALSE)
			->set_js('plugins/ui/moment/moment.min', FALSE)
			->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
			->set_script('pembelian_online/konfirmasi_pembelian/script-index')
			->build('pembelian_online/konfirmasi_pembelian/index');
	}

}