<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Title management controller.
 *
 * @package App
 * @category Controller
 * @author Ardi Soebrata
 */
class Test extends Admin_Controller
{
  protected $controller = 'test';
  function index(){
    echo "test";
    $ci = & get_instance();
    $this->db = $ci->load->database('api', TRUE);
    print_r($this->db->query('SELECT * FROM `m_title`')->result_array());
  }

  function spesial_karakter(){
    /*$string="contoh kalimat \string / (adalah) 43%^%^&%^";
    echo preg_replace('/[^A-Za-z0-9\-\(\) ]/', '', $string);*/
    $data = [["900","1000"]];
    $data2 = ["900","1000"];
    echo '<h1>Data</h1>';
    foreach ($data as $key => $value) {
      print_r($value);
      echo '<br>';
    }
    echo '<h1>Data</h1>';
    foreach ($data2 as $key => $value) {
      print_r($value);
      echo '<br>';
    }
  }
}
