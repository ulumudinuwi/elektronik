<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Illuminate\Database\Capsule\Manager as DB;

class Laporan extends Admin_Controller 
{
    protected $page_title = '<i class="icon-chart"></i> Laporan';
	protected $def_uri = 'keuangan/laporan';
    
	public function __construct()
	{
		parent::__construct();
		$this->lang->load('sales'); 

		$this->dokter = $this->db->where('status', 1)->get('m_dokter_member')->result();
		$this->barang = $this->db->where('status', 1)->get('m_barang')->result();
	}

	/**
	 * 
	 */
	public function index()
	{
		$this->data['-'] = '<a href="'.base_url($this->def_uri.'/new_cicilan').'" class="btn btn-primary btn-labeled"><b><i class=" icon-plus-circle2"></i></b>Tambah</a>';
        $this->template
				->set_css('global_a')
				->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
                ->set_js('plugins/tables/datatables/datatables.min', FALSE)
                ->set_js('plugins/notifications/bootbox.min', FALSE)
                ->set_js('plugins/notifications/sweet_alert.min', FALSE)
                ->set_js('plugins/ui/moment/moment.min', FALSE)
                ->set_js('plugins/buttons/spin.min', FALSE)
				->set_js('plugins/buttons/ladda.min', FALSE)
                ->build($this->def_uri . '/index', $this->data);
	}

	public function laporan_001() {
		$this->load->view('keuangan/laporan/laporan-001');
	}
}