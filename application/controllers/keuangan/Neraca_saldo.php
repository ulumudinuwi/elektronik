<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Neraca_saldo extends Admin_Controller 
{
	protected $page_title = '<i class="icon-store"></i> Neraca Saldo';
	protected $table_def = 't_buku_besar';

	public function __construct()
	{
		parent::__construct();
		$this->lang->load('barang');
	}

	public function index()
	{
		$this->template
			->set_js('plugins/tables/datatables/datatables.min', FALSE)
			->set_js('plugins/notifications/bootbox.min', FALSE)
			->set_js('plugins/notifications/sweet_alert.min', FALSE)
			->set_js('plugins/ui/moment/moment.min', FALSE)
			->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
			->set_script('keuangan/neraca_saldo/script-index')
			->build('keuangan/neraca_saldo/index');
	}

}