<?php

class Pemeriksaan extends Admin_Controller
{
    protected $table_def = "m_rad_pemeriksaan";
    protected $table_def_ukuran_foto = "m_rad_ukuran_foto";
    protected $table_def_obat_bmhp = "m_rad_pemeriksaan_obat";

    public function __construct() {
        parent::__construct();

        $this->load->model('Data_mode_model');
        $this->load->model('master/radiologi/Pemeriksaan_model');
        $this->load->model('master/radiologi/Pemeriksaan_obat_model');
        $this->load->model('master/radiologi/Ukuran_foto_model');
    }

    /**
     * FETCH DATA
     */
    public function fetch_data() {
        if (! $this->input->is_ajax_request())
            exit();

        $aWheres = array(
            '1 = 1',
            // $this->table_def.".lvl > 0",
        );
        $aOrders = array(
            $this->table_def.".lft ASC",
        );

        $result = $this->Pemeriksaan_model->get_all(0, 0, "WHERE ".implode(' AND ', $aWheres), "ORDER BY ".implode(',', $aOrders));
        $data = array();
        foreach ($result['data'] as $row) {
            $data[$row->id] = $row;
        }
        $treeData = $this->buildTree($data);

        // TODO

        echo json_encode(['data' => $treeData]);
    }

    public function fetch_lookup_obat() {
        if (! $this->input->is_ajax_request())
            exit();

        $aSelect = array(
            'm_barang.*',
            'm_jenisbarang.nama as jenis',
        );
        $result = $this->db->select(implode(',', $aSelect))
            ->from('m_barang')
            ->join('m_jenisbarang', 'm_barang.jenis_id = m_jenisbarang.id', 'left')
            ->where_in('m_jenisbarang.kode', [1, 2])
            ->get()->result();

        echo json_encode([
            'data' => $result,
            'total_rows' => count($result),
        ]);
    }

    public function fetch_parent() {
        if (! $this->input->is_ajax_request())
            exit();

        $jenis = array(
            "'".$this->Pemeriksaan_model::JENIS_ROOT."'",
            "'".$this->Pemeriksaan_model::JENIS_KELOMPOK."'",
        );

        $result = $this->Pemeriksaan_model->get_all(0, 0, "WHERE {$this->table_def}.jenis IN (".implode(',', $jenis).")", "ORDER BY {$this->table_def}.lft ASC");

        echo json_encode($result);
    }

    public function fetch_ukuran_foto() {
        if (! $this->input->is_ajax_request())
            exit();

        $result = $this->Ukuran_foto_model->get_all(0, 0, null, "ORDER BY {$this->table_def_ukuran_foto}.kode ASC");

        echo json_encode($result);
    }

    public function get_data($uid) {
        if (!$this->input->is_ajax_request())
            exit();

        $output = array();
        if ($uid) {
            $obj = $this->Pemeriksaan_model->get_by("WHERE {$this->table_def}.uid = \"{$uid}\"");
            $obatResult = $this->Pemeriksaan_obat_model->get_all(0, 0, "WHERE {$this->table_def_obat_bmhp}.pemeriksaan_id = {$obj->id}");

            $obj->obats = [];

            // Obat & BMHP
            foreach ($obatResult['data'] as $obatRow) {
                $obj->obats[] = $obatRow;
            }

            $output['data'] = $obj;
        } else {
            $output['data'] = new stdClass();
        }

        echo json_encode($output);
    }

    /**
     * SEARCH SELECT2
     */
    public function search() {
        // TODO SEACH SELECT2
    }

    public function save() {
        if (!$this->input->is_ajax_request())
            exit();

        $obj = $this->_getDataObject();
        $result = null;
    
        if (isset($obj->id) && $obj->id != "0") { // Update
            $result = $this->Pemeriksaan_model->update($obj);
        } else { // Create
            $result = $this->Pemeriksaan_model->create($obj);
        }

        if (! $result) {
            $this->output->set_status_header(504);
            return;
        }

        $this->output->set_status_header(200)
            ->set_output(json_encode(['data' => $result]));
    }

    public function delete() {
        if (! $this->input->is_ajax_request())
            exit();

        $uid = $this->input->post('uid');
        $result = $this->Pemeriksaan_model->delete($uid);

        if (! $result) {
            $this->output->set_status_header(504);
            return;
        }

        $this->output->set_status_header(200)
            ->set_output(json_encode(['data' => $result]));
    }

    public function restore() {
        if (! $this->input->is_ajax_request())
            exit();

        $uid = $this->input->post('uid');
        $result = $this->Pemeriksaan_model->restore($uid);

        if (! $result) {
            $this->output->set_status_header(504);
            return;
        }

        $this->output->set_status_header(200)
            ->set_output(json_encode(['data' => $result]));
    }

    public function update_status() {
        if (!$this->input->is_ajax_request())
            exit();

        $uid = $this->input->post('uid');
        $status = $this->input->post('status');

        if (! $this->Pemeriksaan_model->update_status($uid, $status)) {
            $this->output->set_status_header(504);
            return;
        }

        $this->output->set_status_header(200)
            ->set_output(json_encode(['data' => $uid]));
    }

    private function _getDataObject() {
        $obj = new stdClass();
        $obj->id = $this->input->post('id') && ($this->input->post('id') != "0") ? $this->input->post('id') : 0;
        $obj->uid = $this->input->post('uid') && ($this->input->post('uid') != "0") ? $this->input->post('uid') : 0;
        $obj->kode = $this->input->post('kode');
        $obj->nama = $this->input->post('nama');
        $obj->jenis = $this->input->post('jenis');
        $obj->old_parent_id = $this->input->post('old_parent_id');
        $obj->parent_id = $this->input->post('parent_id');
        $obj->deskripsi = $this->input->post('deskripsi');

        $ukuran_foto = [];
        if (array_key_exists('ukuran_foto_id', $_POST)) {
            for ($i = 0; $i < count($_POST['ukuran_foto_id']); $i++) {
                if ($_POST['ukuran_foto_selected'][$i] == 1) {
                    $ukuran_foto[] = $_POST['ukuran_foto_id'][$i];
                }
            }
        }
        $obj->ukuran_foto = json_encode($ukuran_foto);

        // OBAT & BMHP
        $obatResult = $this->Pemeriksaan_obat_model->get_all(0, 0, "WHERE {$this->table_def_obat_bmhp}.pemeriksaan_id = {$obj->id}");
        $aObat = array();
        foreach ($obatResult['data'] as $obatRow) {
            $obatRow->data_mode = $this->Data_mode_model::DATA_MODE_DELETE;
            $aObat[$obatRow->id] = $obatRow;
        }
        if (array_key_exists('obat_id', $_POST)) {
            for ($i = 0; $i < count($_POST['obat_id']); $i++) {
                $obat = new stdClass();
                $obat->id = $_POST['obat_id'][$i];
                $obat->pemeriksaan_id = $_POST['obat_pemeriksaan_id'][$i];
                $obat->obat_id = $_POST['obat_obat_id'][$i];
                $obat->quantity = $_POST['obat_quantity'][$i];
                $obat->status = $_POST['obat_status'][$i];

                if (array_key_exists($obat->id, $aObat)) {
                    $obat->data_mode = $this->Data_mode_model::DATA_MODE_EDIT;
                    $aObat[$obat->id] = $obat;
                } else {
                    $obat->data_mode = $this->Data_mode_model::DATA_MODE_ADD;
                    $aObat[uniqid()] = $obat;
                }
            }
        }
        $obj->obats = $aObat;

        return $obj;
    }

    private function buildTree(array &$elements, $parentId = 0) {
        $branch = array();

        foreach ($elements as $element) {
            if ($element->parent_id == $parentId) {
                $children = $this->buildTree($elements, $element->id);
                if ($children) {
                    $element->children = $children;
                }
                $branch[$element->id] = $element;
                unset($elements[$element->id]);
            }
        }
        return $branch;
    }
}