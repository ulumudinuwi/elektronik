<?php
use Ryuna\Response;
class No_rekening extends Admin_Controller
{
    protected $table_def = 'm_no_rekening';

    public function __construct() {
        parent::__construct();

        $this->load->model('master/No_rekening_model');
    }

    public function load_data() {
        $aColumns = array('no_rekening', 'nama_pemilik', 'action');

        /* 
         * Paging
         */
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' ) {
            $iLimit = intval( $_GET['iDisplayLength'] );
            $iOffset = intval( $_GET['iDisplayStart'] );
        }

        /*
         * Ordering
         */
        $sOrder = "";
        $aOrders = array();
        if (isset($_GET['iSortCol_0'])) {
            for ($i = 0; $i <intval($_GET['iSortingCols']); $i++) {
                if ($_GET['bSortable_'.intval($_GET['iSortCol_'.$i])] == "true") {
                    switch ($aColumns[intval($_GET['iSortCol_'.$i])]) {
                        case 'no_rekening':
                            $aOrders[] = $this->table_def.".no_rekening ".($_GET['sSortDir_'.$i] === 'asc' ? 'asc' : 'desc');
                            break;
                        case 'nama_pemilik':
                            $aOrders[] = $this->table_def.".nama_pemilik ".($_GET['sSortDir_'.$i] === 'asc' ? 'asc' : 'desc');
                            break;
                        case 'action':
                            break;
                    }
                }
            }
        }
        if (count($aOrders) > 0) {
            $sOrder = implode(', ', $aOrders);
        }
        if (!empty($sOrder)) {
            $sOrder = "ORDER BY ".$sOrder;
        }

        /*
         * Where
         */
        $sWhere = "";
        $aWheres = array();

        if (count($aWheres) > 0) {
            $sWhere = implode(' AND ', $aWheres);
        }
        if (!empty($sWhere)) {
            $sWhere = "WHERE ".$sWhere;
        }

        $aLikes = array();
        if (isset($_GET['sSearch'])) {
            for ($i = 0; $i < count($aColumns); $i++) {
                switch ($aColumns[$i]) {
                    case 'no_rekening':
                        $aLikes[] = $this->table_def.".no_rekening LIKE '%".$_GET['sSearch']."%'";
                        break;
                    case 'nama_pemilik':
                        $aLikes[] = $this->table_def.".nama_pemilik LIKE '%".$_GET['sSearch']."%'";
                        break;
                    case 'action':
                        break;
                }
            }
        }

        if (count($aLikes) > 0) {
            $sLike = "(".implode(' OR ', $aLikes).")";
            $sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
        }

        $list = $this->No_rekening_model->get_all($iLimit, $iOffset, $sWhere, $sOrder);

        $rResult = $list['data'];
        $iFilteredTotal = $list['total_rows'];
        $iTotal = $list['total_rows'];

        /*
         * Output
         */
        $output = array(
            "sEcho" => intval($_GET['sEcho']),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iFilteredTotal,
            "aaData" => array()
        );

        $rows = array();
        $i = $iOffset;
        foreach ($rResult as $obj) {
            $rows[] = get_object_vars($obj);
        }
        $output['aaData'] = $rows;

        echo json_encode($output);
    }

    public function get_data($uid) {
        if (!$this->input->is_ajax_request())
            exit();

        $output = array();
        if ($uid) {
            $obj = $this->No_rekening_model->get_by("WHERE {$this->table_def}.uid = \"{$uid}\"");

            $output['data'] = $obj;
        } else {
            $output['data'] = new stdClass();
        }

        echo json_encode($output);
    }

    public function save() {
        if (!$this->input->is_ajax_request())
            exit();

        $obj = $this->_getDataObject();
        $result = null;
    
        if (isset($obj->id) && $obj->id != "0") { // Update
            $result = $this->No_rekening_model->update($obj);
        } else { // Create
            $result = $this->No_rekening_model->create($obj);
        }

        if (! $result) {
            $this->output->set_status_header(504);
            return;
        }

        $this->output->set_status_header(200)
            ->set_output(json_encode(['data' => $result]));
    }

    public function delete() {
        if (! $this->input->is_ajax_request())
            exit();

        $uid = $this->input->post('uid');
        $result = $this->No_rekening_model->delete($uid);

        if (! $result) {
            $this->output->set_status_header(504);
            return;
        }

        $this->output->set_status_header(200)
            ->set_output(json_encode(['data' => $result]));
    }

    public function restore() {
        if (! $this->input->is_ajax_request())
            exit();

        $uid = $this->input->post('uid');
        $result = $this->No_rekening_model->restore($uid);

        if (! $result) {
            $this->output->set_status_header(504);
            return;
        }

        $this->output->set_status_header(200)
            ->set_output(json_encode(['data' => $result]));
    }

    public function list_data() {

        // if (!$this->input->is_ajax_request())exit();
        
        $list = $this->No_rekening_model->get_all(0, 0);
        $output['list'] = $list['data'];
        return Response::json($output,200);
        
    }

    private function _getDataObject() {
        $obj = new stdClass();
        $obj->id = $this->input->post('id') && ($this->input->post('id') != "0") ? $this->input->post('id') : 0;
        $obj->uid = $this->input->post('uid') && ($this->input->post('uid') != "0") ? $this->input->post('uid') : 0;
        $obj->no_rekening = $this->input->post('no_rekening');
        $obj->nama_pemilik = $this->input->post('nama_pemilik');

        return $obj;
    }
}