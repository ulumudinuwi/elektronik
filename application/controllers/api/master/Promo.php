<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Promo extends CI_Controller 
{
	protected $table_def = "m_promo";
  protected $table_def_barang = "m_barang";

	function __construct()
	{
		parent::__construct();
		$this->load->model('master/Promo_model', 'main');
	}

	/**
	* Load data
	*/
	public function load_data() {
		$this->datatables->select("a.id, a.uid, a.description, a.status, b.nama as nama_barang, b.kode as kode_barang")
      ->join($this->table_def_barang.' b','a.barang_id = b.id', 'left')
      ->from($this->table_def.' a');
      if ($this->session->userdata('bpom') == 1) {
        $this->datatables->where('b.bpom', '1');
      }
		echo $this->datatables->generate();
	}

  public function save() {

    if (!$this->input->is_ajax_request())
      exit();
    $barang_id = $this->input->post('barang_id');
    $this->db->from('m_promo');
    $this->db->where('barang_id', $barang_id);  
    $dExist = $this->db->get();

    $obj = $this->_getDataObject();
    if (isset($obj->uid) && $obj->uid != "") {
      $result = $this->main->update($obj);
      $this->template->set_flashdata('success', "Data telah berhasil diperbarui.");
    }
    else {
      $result = $this->main->create($obj);
      $this->template->set_flashdata('success', "Data telah berhasil disimpan.");
    }
    echo json_encode($result);
  }

	public function update_status() {
		if (!$this->input->is_ajax_request())
			exit();

		$uid = $this->input->post('uid');
		$status = $this->input->post('status');

		$result = $this->main->update_status($uid, $status);
		echo json_encode($result);
	}

  public function get_all() {

    if (!$this->input->is_ajax_request())
      exit();

    $sWhere = "";
    $aWheres = array();
    $aWheres[] = "{$this->table_def}.status = 1";
    $aWheres[] = "DATE({$this->table_def}.expired_at) >= DATE(NOW())";
    if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
    if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;
    
    $list = $this->main->get_all(0, 0, $sWhere, "ORDER BY {$this->table_def}.barang_id ASC");
    $output['list'] = $list['data'];

    echo json_encode($output);
  }

	public function getData($uid = "") {
		if (!$this->input->is_ajax_request())
			exit();

		$output = array();
		if ($uid) {
			$obj = $this->main->get_by("WHERE {$this->table_def}.uid = \"{$uid}\"");
      if ($obj->banner_id && file_exists('./uploads/promo/'.$obj->banner_id)) {
        $obj->url_banner_id = base_url('./uploads/promo/'.$obj->banner_id);
      }
      else{
        $obj->url_banner_id = base_url('assets/img/no_image_available.png');
      }
      $output['data'] = $obj;
		}
		echo json_encode($output);
	}

  public function get_data($barang_id = "") {
    if (!$this->input->is_ajax_request())
      exit();

    $output = array();
    if ($barang_id) {
      $obj = $this->main->get_by("WHERE {$this->table_def}.barang_id = \"{$barang_id}\"");
      $output['data'] = $obj;
    }
    echo json_encode($output);
  }


  /**
   * Form Data Object
   * 
   * 
   */
  private function _getDataObject() {
    $obj = (object) $_POST;
    return $obj;
  }
}