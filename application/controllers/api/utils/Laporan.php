<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends Admin_Controller
{
    public function excel_rawat_jalan() {
        $tanggal = $this->input->get('tanggal');
        $sql = <<<EOT
SELECT 
    '$tanggal' as tanggal,
    m_layanan.nama as layanan,
    @pendaftaran := (SELECT COUNT(id) FROM t_pelayanan WHERE layanan_id = m_layanan.id AND t_pelayanan.status = 1 AND DATE(created_at) = DATE('$tanggal')) as pendaftaran,
    @pemeriksaan_awal := (SELECT count(t_rawatjalan.id) FROM t_rawatjalan LEFT JOIN t_pelayanan ON t_pelayanan.id = t_rawatjalan.pelayanan_id WHERE t_pelayanan.layanan_id = m_layanan.id AND DATE(t_rawatjalan.created_at) = DATE('$tanggal')) as pemeriksaan_awal,
    @pemeriksaan_dokter := (SELECT count(t_rawatjalan.id) FROM t_rawatjalan LEFT JOIN t_pelayanan ON t_pelayanan.id = t_rawatjalan.pelayanan_id WHERE t_pelayanan.layanan_id = m_layanan.id AND DATE(t_rawatjalan.pemeriksaan_dokter_at) = DATE('$tanggal') AND t_rawatjalan.status = 1) as pemeriksaan_dokter,
    CONCAT(IFNULL(@pemeriksaan_awal/@pendaftaran, 0) * 100, '%') as `pemeriksaan awal (%)`,
    CONCAT(IFNULL(@pemeriksaan_dokter/@pemeriksaan_awal, 0) * 100, '%') as `pemeriksaan dokter (%)`
FROM m_layanan
WHERE m_layanan.jenis = 1 AND m_layanan.day_care = 0;
EOT;
        $list = $this->db->query($sql)->result();

        # Prepare template
        $tpl_filename = 'misc/rawat_jalan.xls';
        $objPHPExcel = PHPExcel_IOFactory::load('assets/templates/' . $tpl_filename);
        $objPHPExcel->setActiveSheetIndex(0);
        $sheet = $objPHPExcel->getActiveSheet();

        # Apply data rows
        $data = array();
        $start = 2;
        foreach ($list as $i => $row) {
            unset($row->id);

            $aRow = get_object_vars($row);
            $colnum = 0;
            foreach ($aRow as $val) {
                $sheet->setCellValue(chr(65 + $colnum) . ($i + $start), $val);
                $colnum++;
            }
        }
        $end = $start + (count($list) - 1);

        # Apply styles
        $sheet->getStyle("A$start:G$end")->applyFromArray(array(
            'font' => array(
                'size' => 8
            )
        ));
        $end++;
        $sheet->getStyle("A$start:G$end")->applyFromArray(array(
            'numberformat' => array(
                'code' => '#,##0'
            )
        ));
        $sheet->getStyle("A$end:G$end")->applyFromArray(array(
            'font' => array(
                'size' => 8,
                'bold' => true
            )
        ));
        $objPHPExcel->getActiveSheet()->getStyle('A'.$start.':G' . $end)->applyFromArray(array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => 'FF555555'),
                ),
            ),
        ));

        # Send excel document
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="'.date('Y-m-d H.i.s', strtotime($tanggal)).' - RAWAT JALAN.xls"');
        header('Cache-Control: max-age=0');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: cache, must-revalidate');
        header('Pragma: public');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }
    public function excel_igd() {
        $tanggal = $this->input->get('tanggal');
        $sql = <<<EOT
SELECT 
    '$tanggal' as tanggal,
    m_layanan.nama as layanan,
    @pendaftaran := (SELECT COUNT(id) FROM t_pelayanan WHERE layanan_id = m_layanan.id AND t_pelayanan.status = 1 AND DATE(created_at) = DATE('$tanggal')) as pendaftaran,
    @pemeriksaan_awal := (SELECT count(t_igd.id) FROM t_igd LEFT JOIN t_pelayanan ON t_pelayanan.id = t_igd.pelayanan_id WHERE t_pelayanan.layanan_id = m_layanan.id AND DATE(t_igd.created_at) = DATE('$tanggal')) as pemeriksaan_awal,
    @pemeriksaan_dokter := (SELECT count(t_igd.id) FROM t_igd LEFT JOIN t_pelayanan ON t_pelayanan.id = t_igd.pelayanan_id WHERE t_pelayanan.layanan_id = m_layanan.id AND DATE(t_igd.pemeriksaan_dokter_at) = DATE('$tanggal') AND t_igd.status = 1) as pemeriksaan_dokter,
    CONCAT(IFNULL(@pemeriksaan_awal/@pendaftaran, 0) * 100, '%') as `pemeriksaan awal (%)`,
    CONCAT(IFNULL(@pemeriksaan_dokter/@pemeriksaan_awal, 0) * 100, '%') as `pemeriksaan dokter (%)`
FROM m_layanan
WHERE m_layanan.jenis = 2 AND m_layanan.day_care = 0;
EOT;
        $list = $this->db->query($sql)->result();

        # Prepare template
        $tpl_filename = 'misc/igd.xls';
        $objPHPExcel = PHPExcel_IOFactory::load('assets/templates/' . $tpl_filename);
        $objPHPExcel->setActiveSheetIndex(0);
        $sheet = $objPHPExcel->getActiveSheet();

        # Apply data rows
        $data = array();
        $start = 2;
        foreach ($list as $i => $row) {
            unset($row->id);

            $aRow = get_object_vars($row);
            $colnum = 0;
            foreach ($aRow as $val) {
                $sheet->setCellValue(chr(65 + $colnum) . ($i + $start), $val);
                $colnum++;
            }
        }
        $end = $start + (count($list) - 1);

        # Apply styles
        $sheet->getStyle("A$start:G$end")->applyFromArray(array(
            'font' => array(
                'size' => 8
            )
        ));
        $end++;
        $sheet->getStyle("A$start:G$end")->applyFromArray(array(
            'numberformat' => array(
                'code' => '#,##0'
            )
        ));
        $sheet->getStyle("A$end:G$end")->applyFromArray(array(
            'font' => array(
                'size' => 8,
                'bold' => true
            )
        ));
        $objPHPExcel->getActiveSheet()->getStyle('A'.$start.':G' . $end)->applyFromArray(array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => 'FF555555'),
                ),
            ),
        ));

        # Send excel document
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="'.date('Y-m-d H.i.s', strtotime($tanggal)).' - IGD.xls"');
        header('Cache-Control: max-age=0');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: cache, must-revalidate');
        header('Pragma: public');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }
    public function excel_odc_ods() {
        $tanggal = $this->input->get('tanggal');
        $sql = <<<EOT
SELECT 
    '$tanggal' as tanggal,
    m_layanan.nama as layanan,
    @pendaftaran := (SELECT COUNT(id) FROM t_pelayanan WHERE layanan_id = m_layanan.id AND t_pelayanan.status = 1 AND DATE(created_at) = DATE('$tanggal')) as pendaftaran,
    @pemeriksaan_awal := (SELECT count(t_odc_ods.id) FROM t_odc_ods LEFT JOIN t_pelayanan ON t_pelayanan.id = t_odc_ods.pelayanan_id WHERE t_pelayanan.layanan_id = m_layanan.id AND DATE(t_odc_ods.created_at) = DATE('$tanggal')) as pemeriksaan_awal,
    @pemeriksaan_dokter := (SELECT count(t_odc_ods.id) FROM t_odc_ods LEFT JOIN t_pelayanan ON t_pelayanan.id = t_odc_ods.pelayanan_id WHERE t_pelayanan.layanan_id = m_layanan.id AND DATE(t_odc_ods.pemeriksaan_dokter_at) = DATE('$tanggal') AND t_odc_ods.status = 1) as pemeriksaan_dokter,
    CONCAT(IFNULL(@pemeriksaan_awal/@pendaftaran, 0) * 100, '%') as `pemeriksaan awal (%)`,
    CONCAT(IFNULL(@pemeriksaan_dokter/@pemeriksaan_awal, 0) * 100, '%') as `pemeriksaan dokter (%)`
FROM m_layanan
WHERE m_layanan.jenis = 1 AND m_layanan.day_care > 0;
EOT;
        $list = $this->db->query($sql)->result();

        # Prepare template
        $tpl_filename = 'misc/odc_ods.xls';
        $objPHPExcel = PHPExcel_IOFactory::load('assets/templates/' . $tpl_filename);
        $objPHPExcel->setActiveSheetIndex(0);
        $sheet = $objPHPExcel->getActiveSheet();

        # Apply data rows
        $data = array();
        $start = 2;
        foreach ($list as $i => $row) {
            unset($row->id);

            $aRow = get_object_vars($row);
            $colnum = 0;
            foreach ($aRow as $val) {
                $sheet->setCellValue(chr(65 + $colnum) . ($i + $start), $val);
                $colnum++;
            }
        }
        $end = $start + (count($list) - 1);

        # Apply styles
        $sheet->getStyle("A$start:G$end")->applyFromArray(array(
            'font' => array(
                'size' => 8
            )
        ));
        $end++;
        $sheet->getStyle("A$start:G$end")->applyFromArray(array(
            'numberformat' => array(
                'code' => '#,##0'
            )
        ));
        $sheet->getStyle("A$end:G$end")->applyFromArray(array(
            'font' => array(
                'size' => 8,
                'bold' => true
            )
        ));
        $objPHPExcel->getActiveSheet()->getStyle('A'.$start.':G' . $end)->applyFromArray(array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => 'FF555555'),
                ),
            ),
        ));

        # Send excel document
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="'.date('Y-m-d H.i.s', strtotime($tanggal)).' - ODS_ODS.xls"');
        header('Cache-Control: max-age=0');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: cache, must-revalidate');
        header('Pragma: public');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }
    public function excel_rawat_inap_rujukan_masuk() {
        $tanggal = $this->input->get('tanggal');
        $sql = <<<EOT
SELECT 
    '$tanggal' as tanggal,
    m_layanan.nama as layanan,
    @rujukan := (SELECT COUNT(t_rawatinap.id) FROM t_rawatinap LEFT JOIN t_pelayanan ON t_rawatinap.pelayanan_id = t_pelayanan.id WHERE t_pelayanan.layanan_id = m_layanan.id AND DATE(t_rawatinap.tanggal) = DATE('$tanggal')) as rujukan,
    @masuk_ranap := (SELECT COUNT(t_rawatinap.id) FROM t_rawatinap LEFT JOIN t_pelayanan ON t_rawatinap.pelayanan_id = t_pelayanan.id WHERE t_pelayanan.layanan_id = m_layanan.id AND t_rawatinap.status = 1 AND DATE(t_rawatinap.tanggal) = DATE('$tanggal')) as masuk_rawat_inap,
    CONCAT(IFNULL(@masuk_ranap/@rujukan, 0) * 100, '%') as `Masuk Rawat Inap (%)`
FROM m_layanan
WHERE m_layanan.jenis IN (1,2);
EOT;
        $list = $this->db->query($sql)->result();

        # Prepare template
        $tpl_filename = 'misc/rawat_inap_rujukan_masuk.xls';
        $objPHPExcel = PHPExcel_IOFactory::load('assets/templates/' . $tpl_filename);
        $objPHPExcel->setActiveSheetIndex(0);
        $sheet = $objPHPExcel->getActiveSheet();

        # Apply data rows
        $data = array();
        $start = 2;
        foreach ($list as $i => $row) {
            unset($row->id);

            $aRow = get_object_vars($row);
            $colnum = 0;
            foreach ($aRow as $val) {
                $sheet->setCellValue(chr(65 + $colnum) . ($i + $start), $val);
                $colnum++;
            }
        }
        $end = $start + (count($list) - 1);

        # Apply styles
        $sheet->getStyle("A$start:E$end")->applyFromArray(array(
            'font' => array(
                'size' => 8
            )
        ));
        $end++;
        $sheet->getStyle("A$start:E$end")->applyFromArray(array(
            'numberformat' => array(
                'code' => '#,##0'
            )
        ));
        $sheet->getStyle("A$end:E$end")->applyFromArray(array(
            'font' => array(
                'size' => 8,
                'bold' => true
            )
        ));
        $objPHPExcel->getActiveSheet()->getStyle('A'.$start.':E' . $end)->applyFromArray(array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => 'FF555555'),
                ),
            ),
        ));

        # Send excel document
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="'.date('Y-m-d H.i.s', strtotime($tanggal)).' - Rawat Inap (Rujukan & Masuk).xls"');
        header('Cache-Control: max-age=0');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: cache, must-revalidate');
        header('Pragma: public');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }
    public function excel_rawat_inap() {
        $tanggal = $this->input->get('tanggal');
        $sql = <<<EOT
SELECT
    '$tanggal' as tanggal,
    'Rawat Inap' as layanan,
    (SELECT COUNT(t_rawat_inap.id) FROM t_rawat_inap WHERE t_rawat_inap.bayi = 1 AND t_rawat_inap.`close` = 0 AND t_rawat_inap.checkout = 0) as `Pasien Bayi`,
    (SELECT COUNT(t_rawat_inap.id) FROM t_rawat_inap WHERE t_rawat_inap.`close` = 1 AND t_rawat_inap.checkout = 1 AND DATE(t_rawat_inap.checkout_at) = DATE('$tanggal')) as `Pasien Keluar`
EOT;
        $list = $this->db->query($sql)->result();

        # Prepare template
        $tpl_filename = 'misc/rawat_inap.xls';
        $objPHPExcel = PHPExcel_IOFactory::load('assets/templates/' . $tpl_filename);
        $objPHPExcel->setActiveSheetIndex(0);
        $sheet = $objPHPExcel->getActiveSheet();

        # Apply data rows
        $data = array();
        $start = 2;
        foreach ($list as $i => $row) {
            unset($row->id);

            $aRow = get_object_vars($row);
            $colnum = 0;
            foreach ($aRow as $val) {
                $sheet->setCellValue(chr(65 + $colnum) . ($i + $start), $val);
                $colnum++;
            }
        }
        $end = $start + (count($list) - 1);

        # Apply styles
        $sheet->getStyle("A$start:D$end")->applyFromArray(array(
            'font' => array(
                'size' => 8
            )
        ));
        $end++;
        $sheet->getStyle("A$start:D$end")->applyFromArray(array(
            'numberformat' => array(
                'code' => '#,##0'
            )
        ));
        $sheet->getStyle("A$end:D$end")->applyFromArray(array(
            'font' => array(
                'size' => 8,
                'bold' => true
            )
        ));
        $objPHPExcel->getActiveSheet()->getStyle('A'.$start.':D' . $end)->applyFromArray(array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => 'FF555555'),
                ),
            ),
        ));

        # Send excel document
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="'.date('Y-m-d H.i.s', strtotime($tanggal)).' - Rawat Inap.xls"');
        header('Cache-Control: max-age=0');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: cache, must-revalidate');
        header('Pragma: public');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }
    public function excel_laboratorium() {
        $tanggal = $this->input->get('tanggal');
        $sql = <<<EOT
SELECT
    '$tanggal' as tanggal,
    IF(m_layanan.jenis = 4, 'Rujukan', m_layanan.nama) as layanan,
    @rujukan := (SELECT COUNT(t_laboratorium.id) FROM t_laboratorium WHERE t_laboratorium.layanan_id = m_layanan.id AND DATE(t_laboratorium.created_at) = DATE('$tanggal')) as `Rujukan`,
    @penata_jasa := (SELECT COUNT(t_laboratorium.id) FROM t_laboratorium WHERE t_laboratorium.layanan_id = m_layanan.id AND t_laboratorium.status >= 0 AND t_laboratorium.status <> 2 AND DATE(t_laboratorium.created_at) = DATE('$tanggal')) as `Penata Jasa`,
    @pemeriksaan := (SELECT COUNT(t_laboratorium.id) FROM t_laboratorium WHERE t_laboratorium.layanan_id = m_layanan.id AND t_laboratorium.status >= 1 AND t_laboratorium.status <> 2 AND DATE(t_laboratorium.created_at) = DATE('$tanggal')) as `Pemeriksaan`,
    @batal := (SELECT COUNT(t_laboratorium.id) FROM t_laboratorium WHERE t_laboratorium.layanan_id = m_layanan.id AND t_laboratorium.status = 2 AND DATE(t_laboratorium.created_at) = DATE('$tanggal')) as `Dibatalkan`,
    CONCAT(IFNULL(@penata_jasa/@rujukan, 0) * 100, '%') as `Penata Jasa (%)`,
    CONCAT(IFNULL(@pemeriksaan/@penata_jasa, 0) * 100, '%') as `Pemeriksaan (%)`,
    CONCAT(IFNULL(@batal/@rujukan, 0) * 100, '%') as `Batal (%)`
FROM t_laboratorium
LEFT JOIN m_layanan ON t_laboratorium.layanan_id = m_layanan.id
WHERE DATE(t_laboratorium.created_at) = DATE('$tanggal')
GROUP BY t_laboratorium.layanan_id;
EOT;
        $list = $this->db->query($sql)->result();

        # Prepare template
        $tpl_filename = 'misc/laboratorium.xls';
        $objPHPExcel = PHPExcel_IOFactory::load('assets/templates/' . $tpl_filename);
        $objPHPExcel->setActiveSheetIndex(0);
        $sheet = $objPHPExcel->getActiveSheet();

        # Apply data rows
        $data = array();
        $start = 2;
        foreach ($list as $i => $row) {
            unset($row->id);

            $aRow = get_object_vars($row);
            $colnum = 0;
            foreach ($aRow as $val) {
                $sheet->setCellValue(chr(65 + $colnum) . ($i + $start), $val);
                $colnum++;
            }
        }
        $end = $start + (count($list) - 1);

        # Apply styles
        $sheet->getStyle("A$start:I$end")->applyFromArray(array(
            'font' => array(
                'size' => 8
            )
        ));
        $end++;
        $sheet->getStyle("A$start:I$end")->applyFromArray(array(
            'numberformat' => array(
                'code' => '#,##0'
            )
        ));
        $sheet->getStyle("A$end:I$end")->applyFromArray(array(
            'font' => array(
                'size' => 8,
                'bold' => true
            )
        ));
        $objPHPExcel->getActiveSheet()->getStyle('A'.$start.':I' . $end)->applyFromArray(array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => 'FF555555'),
                ),
            ),
        ));

        # Send excel document
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="'.date('Y-m-d H.i.s', strtotime($tanggal)).' - LABORATORIUM.xls"');
        header('Cache-Control: max-age=0');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: cache, must-revalidate');
        header('Pragma: public');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }
    public function excel_radiologi() {
        $tanggal = $this->input->get('tanggal');
        $sql = <<<EOT
SELECT
    '$tanggal' as tanggal,
    IF(m_layanan.jenis = 4, 'Rujukan', m_layanan.nama) as layanan,
    @rujukan := (SELECT COUNT(t_radiologi.id) FROM t_radiologi WHERE t_radiologi.layanan_id = m_layanan.id AND DATE(t_radiologi.created_at) = DATE('$tanggal')) as `Rujukan`,
    @penata_jasa := (SELECT COUNT(t_radiologi.id) FROM t_radiologi WHERE t_radiologi.layanan_id = m_layanan.id AND t_radiologi.status >= 0 AND t_radiologi.status <> 2 AND DATE(t_radiologi.created_at) = DATE('$tanggal')) as `Penata Jasa`,
    @pemeriksaan := (SELECT COUNT(t_radiologi.id) FROM t_radiologi WHERE t_radiologi.layanan_id = m_layanan.id AND t_radiologi.status >= 1 AND t_radiologi.status <> 2 AND DATE(t_radiologi.created_at) = DATE('$tanggal')) as `Pemeriksaan`,
    @batal := (SELECT COUNT(t_radiologi.id) FROM t_radiologi WHERE t_radiologi.layanan_id = m_layanan.id AND t_radiologi.status = 2 AND DATE(t_radiologi.created_at) = DATE('$tanggal')) as `Dibatalkan`,
    CONCAT(IFNULL(@penata_jasa/@rujukan, 0) * 100, '%') as `Penata Jasa (%)`,
    CONCAT(IFNULL(@pemeriksaan/@penata_jasa, 0) * 100, '%') as `Pemeriksaan (%)`,
    CONCAT(IFNULL(@batal/@rujukan, 0) * 100, '%') as `Batal (%)`
FROM t_radiologi
LEFT JOIN m_layanan ON t_radiologi.layanan_id = m_layanan.id
WHERE DATE(t_radiologi.created_at) = DATE('$tanggal')
GROUP BY t_radiologi.layanan_id;
EOT;
        $list = $this->db->query($sql)->result();

        # Prepare template
        $tpl_filename = 'misc/radiologi.xls';
        $objPHPExcel = PHPExcel_IOFactory::load('assets/templates/' . $tpl_filename);
        $objPHPExcel->setActiveSheetIndex(0);
        $sheet = $objPHPExcel->getActiveSheet();

        # Apply data rows
        $data = array();
        $start = 2;
        foreach ($list as $i => $row) {
            unset($row->id);

            $aRow = get_object_vars($row);
            $colnum = 0;
            foreach ($aRow as $val) {
                $sheet->setCellValue(chr(65 + $colnum) . ($i + $start), $val);
                $colnum++;
            }
        }
        $end = $start + (count($list) - 1);

        # Apply styles
        $sheet->getStyle("A$start:I$end")->applyFromArray(array(
            'font' => array(
                'size' => 8
            )
        ));
        $end++;
        $sheet->getStyle("A$start:I$end")->applyFromArray(array(
            'numberformat' => array(
                'code' => '#,##0'
            )
        ));
        $sheet->getStyle("A$end:I$end")->applyFromArray(array(
            'font' => array(
                'size' => 8,
                'bold' => true
            )
        ));
        $objPHPExcel->getActiveSheet()->getStyle('A'.$start.':I' . $end)->applyFromArray(array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => 'FF555555'),
                ),
            ),
        ));

        # Send excel document
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="'.date('Y-m-d H.i.s', strtotime($tanggal)).' - RADIOLOGI.xls"');
        header('Cache-Control: max-age=0');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: cache, must-revalidate');
        header('Pragma: public');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }
    public function excel_fisioterapi() {
        $tanggal = $this->input->get('tanggal');
        $sql = <<<EOT
SELECT
    '$tanggal' as tanggal,
    IF(m_layanan.jenis = 4, 'Rujukan', m_layanan.nama) as layanan,
    @rujukan := (SELECT COUNT(t_fisioterapi.id) FROM t_fisioterapi WHERE t_fisioterapi.layanan_id = m_layanan.id AND DATE(t_fisioterapi.created_at) = DATE('$tanggal')) as `Rujukan`,
    @penata_jasa := (SELECT COUNT(t_fisioterapi.id) FROM t_fisioterapi WHERE t_fisioterapi.layanan_id = m_layanan.id AND t_fisioterapi.status >= 0 AND t_fisioterapi.status <> 2 AND DATE(t_fisioterapi.created_at) = DATE('$tanggal')) as `Penata Jasa`,
    @pemeriksaan := (SELECT COUNT(t_fisioterapi.id) FROM t_fisioterapi WHERE t_fisioterapi.layanan_id = m_layanan.id AND t_fisioterapi.status >= 1 AND t_fisioterapi.status <> 2 AND DATE(t_fisioterapi.created_at) = DATE('$tanggal')) as `Pemeriksaan`,
    @batal := (SELECT COUNT(t_fisioterapi.id) FROM t_fisioterapi WHERE t_fisioterapi.layanan_id = m_layanan.id AND t_fisioterapi.status = 2 AND DATE(t_fisioterapi.created_at) = DATE('$tanggal')) as `Dibatalkan`,
    CONCAT(IFNULL(@penata_jasa/@rujukan, 0) * 100, '%') as `Penata Jasa (%)`,
    CONCAT(IFNULL(@pemeriksaan/@penata_jasa, 0) * 100, '%') as `Pemeriksaan (%)`,
    CONCAT(IFNULL(@batal/@rujukan, 0) * 100, '%') as `Batal (%)`
FROM t_fisioterapi
LEFT JOIN m_layanan ON t_fisioterapi.layanan_id = m_layanan.id
WHERE DATE(t_fisioterapi.created_at) = DATE('$tanggal')
GROUP BY t_fisioterapi.layanan_id;
EOT;
        $list = $this->db->query($sql)->result();

        # Prepare template
        $tpl_filename = 'misc/fisioterapi.xls';
        $objPHPExcel = PHPExcel_IOFactory::load('assets/templates/' . $tpl_filename);
        $objPHPExcel->setActiveSheetIndex(0);
        $sheet = $objPHPExcel->getActiveSheet();

        # Apply data rows
        $data = array();
        $start = 2;
        foreach ($list as $i => $row) {
            unset($row->id);

            $aRow = get_object_vars($row);
            $colnum = 0;
            foreach ($aRow as $val) {
                $sheet->setCellValue(chr(65 + $colnum) . ($i + $start), $val);
                $colnum++;
            }
        }
        $end = $start + (count($list) - 1);

        # Apply styles
        $sheet->getStyle("A$start:I$end")->applyFromArray(array(
            'font' => array(
                'size' => 8
            )
        ));
        $end++;
        $sheet->getStyle("A$start:I$end")->applyFromArray(array(
            'numberformat' => array(
                'code' => '#,##0'
            )
        ));
        $sheet->getStyle("A$end:I$end")->applyFromArray(array(
            'font' => array(
                'size' => 8,
                'bold' => true
            )
        ));
        $objPHPExcel->getActiveSheet()->getStyle('A'.$start.':I' . $end)->applyFromArray(array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => 'FF555555'),
                ),
            ),
        ));

        # Send excel document
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="'.date('Y-m-d H.i.s', strtotime($tanggal)).' - FISIOTERAPI.xls"');
        header('Cache-Control: max-age=0');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: cache, must-revalidate');
        header('Pragma: public');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }
    public function excel_ctscan() {
        $tanggal = $this->input->get('tanggal');
        $sql = <<<EOT
SELECT
    '$tanggal' as tanggal,
    IF(m_layanan.jenis = 4, 'Rujukan', m_layanan.nama) as layanan,
    @rujukan := (SELECT COUNT(t_ctscan.id) FROM t_ctscan WHERE t_ctscan.layanan_id = m_layanan.id AND DATE(t_ctscan.created_at) = DATE('$tanggal')) as `Rujukan`,
    @penata_jasa := (SELECT COUNT(t_ctscan.id) FROM t_ctscan WHERE t_ctscan.layanan_id = m_layanan.id AND t_ctscan.status >= 0 AND t_ctscan.status <> 2 AND DATE(t_ctscan.created_at) = DATE('$tanggal')) as `Penata Jasa`,
    @pemeriksaan := (SELECT COUNT(t_ctscan.id) FROM t_ctscan WHERE t_ctscan.layanan_id = m_layanan.id AND t_ctscan.status >= 1 AND t_ctscan.status <> 2 AND DATE(t_ctscan.created_at) = DATE('$tanggal')) as `Pemeriksaan`,
    @batal := (SELECT COUNT(t_ctscan.id) FROM t_ctscan WHERE t_ctscan.layanan_id = m_layanan.id AND t_ctscan.status = 2 AND DATE(t_ctscan.created_at) = DATE('$tanggal')) as `Dibatalkan`,
    CONCAT(IFNULL(@penata_jasa/@rujukan, 0) * 100, '%') as `Penata Jasa (%)`,
    CONCAT(IFNULL(@pemeriksaan/@penata_jasa, 0) * 100, '%') as `Pemeriksaan (%)`,
    CONCAT(IFNULL(@batal/@rujukan, 0) * 100, '%') as `Batal (%)`
FROM t_ctscan
LEFT JOIN m_layanan ON t_ctscan.layanan_id = m_layanan.id
WHERE DATE(t_ctscan.created_at) = DATE('$tanggal')
GROUP BY t_ctscan.layanan_id;
EOT;
        $list = $this->db->query($sql)->result();

        # Prepare template
        $tpl_filename = 'misc/ctscan.xls';
        $objPHPExcel = PHPExcel_IOFactory::load('assets/templates/' . $tpl_filename);
        $objPHPExcel->setActiveSheetIndex(0);
        $sheet = $objPHPExcel->getActiveSheet();

        # Apply data rows
        $data = array();
        $start = 2;
        foreach ($list as $i => $row) {
            unset($row->id);

            $aRow = get_object_vars($row);
            $colnum = 0;
            foreach ($aRow as $val) {
                $sheet->setCellValue(chr(65 + $colnum) . ($i + $start), $val);
                $colnum++;
            }
        }
        $end = $start + (count($list) - 1);

        # Apply styles
        $sheet->getStyle("A$start:I$end")->applyFromArray(array(
            'font' => array(
                'size' => 8
            )
        ));
        $end++;
        $sheet->getStyle("A$start:I$end")->applyFromArray(array(
            'numberformat' => array(
                'code' => '#,##0'
            )
        ));
        $sheet->getStyle("A$end:I$end")->applyFromArray(array(
            'font' => array(
                'size' => 8,
                'bold' => true
            )
        ));
        $objPHPExcel->getActiveSheet()->getStyle('A'.$start.':I' . $end)->applyFromArray(array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => 'FF555555'),
                ),
            ),
        ));

        # Send excel document
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="'.date('Y-m-d H.i.s', strtotime($tanggal)).' - CT SCAN.xls"');
        header('Cache-Control: max-age=0');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: cache, must-revalidate');
        header('Pragma: public');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }
    public function excel_audiometri() {
        $tanggal = $this->input->get('tanggal');
        $sql = <<<EOT
SELECT
    '$tanggal' as tanggal,
    IF(m_layanan.jenis = 4, 'Rujukan', m_layanan.nama) as layanan,
    @rujukan := (SELECT COUNT(t_audiometri.id) FROM t_audiometri WHERE t_audiometri.layanan_id = m_layanan.id AND DATE(t_audiometri.created_at) = DATE('$tanggal')) as `Rujukan`,
    @penata_jasa := (SELECT COUNT(t_audiometri.id) FROM t_audiometri WHERE t_audiometri.layanan_id = m_layanan.id AND t_audiometri.status >= 0 AND t_audiometri.status <> 2 AND DATE(t_audiometri.created_at) = DATE('$tanggal')) as `Penata Jasa`,
    @pemeriksaan := (SELECT COUNT(t_audiometri.id) FROM t_audiometri WHERE t_audiometri.layanan_id = m_layanan.id AND t_audiometri.status >= 1 AND t_audiometri.status <> 2 AND DATE(t_audiometri.created_at) = DATE('$tanggal')) as `Pemeriksaan`,
    @batal := (SELECT COUNT(t_audiometri.id) FROM t_audiometri WHERE t_audiometri.layanan_id = m_layanan.id AND t_audiometri.status = 2 AND DATE(t_audiometri.created_at) = DATE('$tanggal')) as `Dibatalkan`,
    CONCAT(IFNULL(@penata_jasa/@rujukan, 0) * 100, '%') as `Penata Jasa (%)`,
    CONCAT(IFNULL(@pemeriksaan/@penata_jasa, 0) * 100, '%') as `Pemeriksaan (%)`,
    CONCAT(IFNULL(@batal/@rujukan, 0) * 100, '%') as `Batal (%)`
FROM t_audiometri
LEFT JOIN m_layanan ON t_audiometri.layanan_id = m_layanan.id
WHERE DATE(t_audiometri.created_at) = DATE('$tanggal')
GROUP BY t_audiometri.layanan_id;
EOT;
        $list = $this->db->query($sql)->result();

        # Prepare template
        $tpl_filename = 'misc/audiometri.xls';
        $objPHPExcel = PHPExcel_IOFactory::load('assets/templates/' . $tpl_filename);
        $objPHPExcel->setActiveSheetIndex(0);
        $sheet = $objPHPExcel->getActiveSheet();

        # Apply data rows
        $data = array();
        $start = 2;
        foreach ($list as $i => $row) {
            unset($row->id);

            $aRow = get_object_vars($row);
            $colnum = 0;
            foreach ($aRow as $val) {
                $sheet->setCellValue(chr(65 + $colnum) . ($i + $start), $val);
                $colnum++;
            }
        }
        $end = $start + (count($list) - 1);

        # Apply styles
        $sheet->getStyle("A$start:I$end")->applyFromArray(array(
            'font' => array(
                'size' => 8
            )
        ));
        $end++;
        $sheet->getStyle("A$start:I$end")->applyFromArray(array(
            'numberformat' => array(
                'code' => '#,##0'
            )
        ));
        $sheet->getStyle("A$end:I$end")->applyFromArray(array(
            'font' => array(
                'size' => 8,
                'bold' => true
            )
        ));
        $objPHPExcel->getActiveSheet()->getStyle('A'.$start.':I' . $end)->applyFromArray(array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => 'FF555555'),
                ),
            ),
        ));

        # Send excel document
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="'.date('Y-m-d H.i.s', strtotime($tanggal)).' - AUDIOMETRI.xls"');
        header('Cache-Control: max-age=0');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: cache, must-revalidate');
        header('Pragma: public');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }
    public function excel_diagnostik_fungsional() {
        $tanggal = $this->input->get('tanggal');
        $sql = <<<EOT
SELECT
    '$tanggal' as tanggal,
    IF(m_layanan.jenis = 4, 'Rujukan', m_layanan.nama) as layanan,
    @rujukan := (SELECT COUNT(t_diagnostikfungsional.id) FROM t_diagnostikfungsional WHERE t_diagnostikfungsional.layanan_id = m_layanan.id AND DATE(t_diagnostikfungsional.created_at) = DATE('$tanggal')) as `Rujukan`,
    @penata_jasa := (SELECT COUNT(t_diagnostikfungsional.id) FROM t_diagnostikfungsional WHERE t_diagnostikfungsional.layanan_id = m_layanan.id AND t_diagnostikfungsional.status >= 0 AND t_diagnostikfungsional.status <> 2 AND DATE(t_diagnostikfungsional.created_at) = DATE('$tanggal')) as `Penata Jasa`,
    @pemeriksaan := (SELECT COUNT(t_diagnostikfungsional.id) FROM t_diagnostikfungsional WHERE t_diagnostikfungsional.layanan_id = m_layanan.id AND t_diagnostikfungsional.status >= 1 AND t_diagnostikfungsional.status <> 2 AND DATE(t_diagnostikfungsional.created_at) = DATE('$tanggal')) as `Pemeriksaan`,
    @batal := (SELECT COUNT(t_diagnostikfungsional.id) FROM t_diagnostikfungsional WHERE t_diagnostikfungsional.layanan_id = m_layanan.id AND t_diagnostikfungsional.status = 2 AND DATE(t_diagnostikfungsional.created_at) = DATE('$tanggal')) as `Dibatalkan`,
    CONCAT(IFNULL(@penata_jasa/@rujukan, 0) * 100, '%') as `Penata Jasa (%)`,
    CONCAT(IFNULL(@pemeriksaan/@penata_jasa, 0) * 100, '%') as `Pemeriksaan (%)`,
    CONCAT(IFNULL(@batal/@rujukan, 0) * 100, '%') as `Batal (%)`
FROM t_diagnostikfungsional
LEFT JOIN m_layanan ON t_diagnostikfungsional.layanan_id = m_layanan.id
WHERE DATE(t_diagnostikfungsional.created_at) = DATE('$tanggal')
GROUP BY t_diagnostikfungsional.layanan_id;
EOT;
        $list = $this->db->query($sql)->result();

        # Prepare template
        $tpl_filename = 'misc/diagnostik_fungsional.xls';
        $objPHPExcel = PHPExcel_IOFactory::load('assets/templates/' . $tpl_filename);
        $objPHPExcel->setActiveSheetIndex(0);
        $sheet = $objPHPExcel->getActiveSheet();

        # Apply data rows
        $data = array();
        $start = 2;
        foreach ($list as $i => $row) {
            unset($row->id);

            $aRow = get_object_vars($row);
            $colnum = 0;
            foreach ($aRow as $val) {
                $sheet->setCellValue(chr(65 + $colnum) . ($i + $start), $val);
                $colnum++;
            }
        }
        $end = $start + (count($list) - 1);

        # Apply styles
        $sheet->getStyle("A$start:I$end")->applyFromArray(array(
            'font' => array(
                'size' => 8
            )
        ));
        $end++;
        $sheet->getStyle("A$start:I$end")->applyFromArray(array(
            'numberformat' => array(
                'code' => '#,##0'
            )
        ));
        $sheet->getStyle("A$end:I$end")->applyFromArray(array(
            'font' => array(
                'size' => 8,
                'bold' => true
            )
        ));
        $objPHPExcel->getActiveSheet()->getStyle('A'.$start.':I' . $end)->applyFromArray(array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => 'FF555555'),
                ),
            ),
        ));

        # Send excel document
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="'.date('Y-m-d H.i.s', strtotime($tanggal)).' - DIAGNOSTIK FUNGSIONAL.xls"');
        header('Cache-Control: max-age=0');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: cache, must-revalidate');
        header('Pragma: public');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }
    public function excel_cathlab() {
        $tanggal = $this->input->get('tanggal');
        $sql = <<<EOT
SELECT
    '$tanggal' as tanggal,
    IF(m_layanan.jenis = 4, 'Rujukan', m_layanan.nama) as layanan,
    @rujukan := (SELECT COUNT(t_cathlab.id) FROM t_cathlab WHERE t_cathlab.layanan_id = m_layanan.id AND DATE(t_cathlab.created_at) = DATE('$tanggal')) as `Rujukan`,
    @penata_jasa := (SELECT COUNT(t_cathlab.id) FROM t_cathlab WHERE t_cathlab.layanan_id = m_layanan.id AND t_cathlab.status >= 0 AND t_cathlab.status <> 2 AND DATE(t_cathlab.created_at) = DATE('$tanggal')) as `Penata Jasa`,
    @pemeriksaan := (SELECT COUNT(t_cathlab.id) FROM t_cathlab WHERE t_cathlab.layanan_id = m_layanan.id AND t_cathlab.status >= 1 AND t_cathlab.status <> 2 AND DATE(t_cathlab.created_at) = DATE('$tanggal')) as `Pemeriksaan`,
    @batal := (SELECT COUNT(t_cathlab.id) FROM t_cathlab WHERE t_cathlab.layanan_id = m_layanan.id AND t_cathlab.status = 2 AND DATE(t_cathlab.created_at) = DATE('$tanggal')) as `Dibatalkan`,
    CONCAT(IFNULL(@penata_jasa/@rujukan, 0) * 100, '%') as `Penata Jasa (%)`,
    CONCAT(IFNULL(@pemeriksaan/@penata_jasa, 0) * 100, '%') as `Pemeriksaan (%)`,
    CONCAT(IFNULL(@batal/@rujukan, 0) * 100, '%') as `Batal (%)`
FROM t_cathlab
LEFT JOIN m_layanan ON t_cathlab.layanan_id = m_layanan.id
WHERE DATE(t_cathlab.created_at) = DATE('$tanggal')
GROUP BY t_cathlab.layanan_id;
EOT;
        $list = $this->db->query($sql)->result();

        # Prepare template
        $tpl_filename = 'misc/cathlab.xls';
        $objPHPExcel = PHPExcel_IOFactory::load('assets/templates/' . $tpl_filename);
        $objPHPExcel->setActiveSheetIndex(0);
        $sheet = $objPHPExcel->getActiveSheet();

        # Apply data rows
        $data = array();
        $start = 2;
        foreach ($list as $i => $row) {
            unset($row->id);

            $aRow = get_object_vars($row);
            $colnum = 0;
            foreach ($aRow as $val) {
                $sheet->setCellValue(chr(65 + $colnum) . ($i + $start), $val);
                $colnum++;
            }
        }
        $end = $start + (count($list) - 1);

        # Apply styles
        $sheet->getStyle("A$start:I$end")->applyFromArray(array(
            'font' => array(
                'size' => 8
            )
        ));
        $end++;
        $sheet->getStyle("A$start:I$end")->applyFromArray(array(
            'numberformat' => array(
                'code' => '#,##0'
            )
        ));
        $sheet->getStyle("A$end:I$end")->applyFromArray(array(
            'font' => array(
                'size' => 8,
                'bold' => true
            )
        ));
        $objPHPExcel->getActiveSheet()->getStyle('A'.$start.':I' . $end)->applyFromArray(array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => 'FF555555'),
                ),
            ),
        ));

        # Send excel document
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="'.date('Y-m-d H.i.s', strtotime($tanggal)).' - CATHLAB.xls"');
        header('Cache-Control: max-age=0');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: cache, must-revalidate');
        header('Pragma: public');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }
    public function excel_farmasi_rajal() {
        $tanggal = $this->input->get('tanggal');
        $sql = <<<EOT
SELECT
    '$tanggal' as tanggal,
    layanan,
    `resep`,
    `antrian`,
    `konfirmasi`,
    `selesai`
FROM (
    SELECT 
        '' as tanggal,
        IFNULL(m_r.nama, m_l.nama) as layanan,
        COUNT(DISTINCT tor.pelayanan_id) as `resep`,
        0 as antrian,
        0 as konfirmasi,
        0 as selesai
    FROM t_obat_rujukan tor
    LEFT JOIN t_pelayanan tp ON tor.pelayanan_id = tp.id
    LEFT JOIN t_rawat_inap tri ON tri.pelayanan_id = tor.pelayanan_id
    LEFT JOIN m_ruang m_r ON tri.ruang_id = m_r.id
    LEFT JOIN m_layanan m_l ON tp.layanan_id = m_l.id
    WHERE DATE(tor.created_at) = DATE('$tanggal')
    AND tor.jenis_layanan <> 3
    GROUP BY m_r.id, m_l.id

    UNION ALL

    SELECT 
        '' as tanggal,
        IFNULL(m_r.nama, m_l.nama) as layanan,
        SUM(1) as `resep`,
        SUM(1) as antrian,
        SUM(IF(ta.status = 2 OR ta.ready = 1, 1, 0)) as konfirmasi,
        SUM(IF(ta.status = 2, 1, 0)) as selesai
    FROM t_apotek ta
    LEFT JOIN t_pelayanan tp ON ta.pelayanan_id = tp.id
    LEFT JOIN t_rawat_inap tri ON tri.pelayanan_id = ta.pelayanan_id
    LEFT JOIN m_ruang m_r ON tri.ruang_id = m_r.id
    LEFT JOIN m_layanan m_l ON tp.layanan_id = m_l.id
    WHERE DATE(ta.created_at) = DATE('$tanggal')
    AND ta.depo_id = 1
    GROUP BY m_r.id, m_l.id
) as t;
EOT;
        $list = $this->db->query($sql)->result();
        $tmpList = array();
        foreach ($list as $row) {
            if (! array_key_exists($row->layanan, $tmpList)) {
                $tmpList[$row->layanan] = $row;
            } else {
                $tmpList[$row->layanan]->resep += $row->resep;
                $tmpList[$row->layanan]->antrian += $row->antrian;
                $tmpList[$row->layanan]->konfirmasi += $row->konfirmasi;
                $tmpList[$row->layanan]->selesai += $row->selesai;
            }
        }

        $list = array_values($tmpList);
        for ($i = 0; $i < count($list); $i++) {
            if ($list[$i]->resep != 0) { 
                $value = $list[$i]->antrian / $list[$i]->resep;
                $list[$i]->antrian_persen = ($value * 100);
            } else {
                $list[$i]->antrian_persen = 0;
            }

            if ($list[$i]->antrian != 0) {
                $value = $list[$i]->konfirmasi / $list[$i]->antrian;
                $list[$i]->konfirmasi_persen = ($value * 100);
            } else {
                $list[$i]->konfirmasi_persen = 0;
            }
            
            if ($list[$i]->konfirmasi) {
                $value = $list[$i]->selesai / $list[$i]->konfirmasi;
                $list[$i]->selesai_persen = ($value * 100);
            } else {
                $list[$i]->selesai_persen = 0;
            }
        }


        # Prepare template
        $tpl_filename = 'misc/farmasi.xls';
        $objPHPExcel = PHPExcel_IOFactory::load('assets/templates/' . $tpl_filename);
        $objPHPExcel->setActiveSheetIndex(0);
        $sheet = $objPHPExcel->getActiveSheet();

        # Apply data rows
        $data = array();
        $start = 2;
        foreach ($list as $i => $row) {
            unset($row->id);

            $aRow = get_object_vars($row);
            $colnum = 0;
            foreach ($aRow as $val) {
                $sheet->setCellValue(chr(65 + $colnum) . ($i + $start), $val);
                $colnum++;
            }
        }
        $end = $start + (count($list) - 1);

        # Apply styles
        $sheet->getStyle("A$start:I$end")->applyFromArray(array(
            'font' => array(
                'size' => 8
            )
        ));
        $end++;
        $sheet->getStyle("A$start:I$end")->applyFromArray(array(
            'numberformat' => array(
                'code' => '#,##0'
            )
        ));
        $sheet->getStyle("A$end:I$end")->applyFromArray(array(
            'font' => array(
                'size' => 8,
                'bold' => true
            )
        ));
        $objPHPExcel->getActiveSheet()->getStyle('A'.$start.':I' . $end)->applyFromArray(array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => 'FF555555'),
                ),
            ),
        ));

        # Send excel document
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="'.date('Y-m-d H.i.s', strtotime($tanggal)).' - FARMASI RAJAL.xls"');
        header('Cache-Control: max-age=0');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: cache, must-revalidate');
        header('Pragma: public');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }
    public function excel_farmasi_ranap() {
        $tanggal = $this->input->get('tanggal');
        $sql = <<<EOT
SELECT
    '$tanggal' as tanggal,
    layanan,
    `resep`,
    `antrian`,
    `konfirmasi`,
    `selesai`
FROM (
    SELECT 
        '' as tanggal,
        IFNULL(m_r.nama, m_l.nama) as layanan,
        COUNT(DISTINCT tor.pelayanan_id) as `resep`,
        0 as antrian,
        0 as konfirmasi,
        0 as selesai
    FROM t_obat_rujukan tor
    LEFT JOIN t_pelayanan tp ON tor.pelayanan_id = tp.id
    LEFT JOIN t_rawat_inap tri ON tri.pelayanan_id = tor.pelayanan_id
    LEFT JOIN m_ruang m_r ON tri.ruang_id = m_r.id
    LEFT JOIN m_layanan m_l ON tp.layanan_id = m_l.id
    WHERE DATE(tor.created_at) = DATE('$tanggal')
    AND tor.jenis_layanan = 3
    GROUP BY m_r.id, m_l.id

    UNION ALL

    SELECT 
        '' as tanggal,
        IFNULL(m_r.nama, m_l.nama) as layanan,
        SUM(1) as `resep`,
        SUM(1) as antrian,
        SUM(IF(ta.status = 1, 1, 0)) as konfirmasi,
        SUM(IF(ta.status = 1, 1, 0)) as selesai
    FROM t_apotek ta
    LEFT JOIN t_pelayanan tp ON ta.pelayanan_id = tp.id
    LEFT JOIN t_rawat_inap tri ON tri.pelayanan_id = ta.pelayanan_id
    LEFT JOIN m_ruang m_r ON tri.ruang_id = m_r.id
    LEFT JOIN m_layanan m_l ON tp.layanan_id = m_l.id
    WHERE DATE(ta.created_at) = DATE('$tanggal')
    AND ta.depo_id = 2
    GROUP BY m_r.id, m_l.id
) as t;
EOT;
        $list = $this->db->query($sql)->result();
        $tmpList = array();
        foreach ($list as $row) {
            if (! array_key_exists($row->layanan, $tmpList)) {
                $tmpList[$row->layanan] = $row;
            } else {
                $tmpList[$row->layanan]->resep += $row->resep;
                $tmpList[$row->layanan]->antrian += $row->antrian;
                $tmpList[$row->layanan]->konfirmasi += $row->konfirmasi;
                $tmpList[$row->layanan]->selesai += $row->selesai;
            }
        }

        $list = array_values($tmpList);
        for ($i = 0; $i < count($list); $i++) {
            if ($list[$i]->resep != 0) { 
                $value = $list[$i]->antrian / $list[$i]->resep;
                $list[$i]->antrian_persen = ($value * 100);
            } else {
                $list[$i]->antrian_persen = 0;
            }

            if ($list[$i]->antrian != 0) {
                $value = $list[$i]->konfirmasi / $list[$i]->antrian;
                $list[$i]->konfirmasi_persen = ($value * 100);
            } else {
                $list[$i]->konfirmasi_persen = 0;
            }
            
            if ($list[$i]->konfirmasi) {
                $value = $list[$i]->selesai / $list[$i]->konfirmasi;
                $list[$i]->selesai_persen = ($value * 100);
            } else {
                $list[$i]->selesai_persen = 0;
            }
        }


        # Prepare template
        $tpl_filename = 'misc/farmasi.xls';
        $objPHPExcel = PHPExcel_IOFactory::load('assets/templates/' . $tpl_filename);
        $objPHPExcel->setActiveSheetIndex(0);
        $sheet = $objPHPExcel->getActiveSheet();

        # Apply data rows
        $data = array();
        $start = 2;
        foreach ($list as $i => $row) {
            unset($row->id);

            $aRow = get_object_vars($row);
            $colnum = 0;
            foreach ($aRow as $val) {
                $sheet->setCellValue(chr(65 + $colnum) . ($i + $start), $val);
                $colnum++;
            }
        }
        $end = $start + (count($list) - 1);

        # Apply styles
        $sheet->getStyle("A$start:I$end")->applyFromArray(array(
            'font' => array(
                'size' => 8
            )
        ));
        $end++;
        $sheet->getStyle("A$start:I$end")->applyFromArray(array(
            'numberformat' => array(
                'code' => '#,##0'
            )
        ));
        $sheet->getStyle("A$end:I$end")->applyFromArray(array(
            'font' => array(
                'size' => 8,
                'bold' => true
            )
        ));
        $objPHPExcel->getActiveSheet()->getStyle('A'.$start.':I' . $end)->applyFromArray(array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => 'FF555555'),
                ),
            ),
        ));

        # Send excel document
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="'.date('Y-m-d H.i.s', strtotime($tanggal)).' - FARMASI RANAP.xls"');
        header('Cache-Control: max-age=0');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: cache, must-revalidate');
        header('Pragma: public');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }
    public function excel_farmasi_rajal_penjualan() {
        $tanggal = $this->input->get('tanggal');
        $sql = <<<EOT
SELECT 
    '$tanggal' as tanggal,
    IFNULL(m_layanan.nama, 'Penjualan Obat Bebas'), 
    COUNT(t_apotek.id) as jumlah
FROM t_apotek
LEFT JOIN m_layanan ON t_apotek.layanan_id = m_layanan.id
WHERE DATE(t_apotek.created_at) = DATE('$tanggal')
AND t_apotek.depo_id = 1
GROUP BY t_apotek.layanan_id;
EOT;
        $list = $this->db->query($sql)->result();


        # Prepare template
        $tpl_filename = 'misc/farmasi_penjualan.xls';
        $objPHPExcel = PHPExcel_IOFactory::load('assets/templates/' . $tpl_filename);
        $objPHPExcel->setActiveSheetIndex(0);
        $sheet = $objPHPExcel->getActiveSheet();

        # Apply data rows
        $data = array();
        $start = 2;
        foreach ($list as $i => $row) {
            unset($row->id);

            $aRow = get_object_vars($row);
            $colnum = 0;
            foreach ($aRow as $val) {
                $sheet->setCellValue(chr(65 + $colnum) . ($i + $start), $val);
                $colnum++;
            }
        }
        $end = $start + (count($list) - 1);

        # Apply styles
        $sheet->getStyle("A$start:C$end")->applyFromArray(array(
            'font' => array(
                'size' => 8
            )
        ));
        $end++;
        $sheet->getStyle("A$start:C$end")->applyFromArray(array(
            'numberformat' => array(
                'code' => '#,##0'
            )
        ));
        $sheet->getStyle("A$end:C$end")->applyFromArray(array(
            'font' => array(
                'size' => 8,
                'bold' => true
            )
        ));
        $objPHPExcel->getActiveSheet()->getStyle('A'.$start.':C' . $end)->applyFromArray(array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => 'FF555555'),
                ),
            ),
        ));

        # Send excel document
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="'.date('Y-m-d H.i.s', strtotime($tanggal)).' - FARMASI RAJAL PENJUALAN.xls"');
        header('Cache-Control: max-age=0');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: cache, must-revalidate');
        header('Pragma: public');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }
    public function excel_farmasi_ranap_penjualan() {
        $tanggal = $this->input->get('tanggal');
        $sql = <<<EOT
SELECT 
    '$tanggal' as tanggal,
    -- IFNULL(m_layanan.nama, 'Penjualan Obat Bebas'),
    IFNULL(IFNULL(m_ruang.nama, m_layanan.nama), 'Penjualan Obat Bebas'), 
    COUNT(t_apotek.id) as jumlah
FROM t_apotek
LEFT JOIN m_layanan ON t_apotek.layanan_id = m_layanan.id
LEFT JOIN t_pelayanan ON t_apotek.pelayanan_id = t_pelayanan.id
LEFT JOIN t_rawat_inap ON t_rawat_inap.pelayanan_id = t_pelayanan.id
LEFT JOIN m_ruang ON t_rawat_inap.ruang_id = m_ruang.id
WHERE 
DATE(t_apotek.created_at) = DATE('$tanggal')
AND t_apotek.depo_id = 2
GROUP BY m_ruang.id;
EOT;
        $list = $this->db->query($sql)->result();


        # Prepare template
        $tpl_filename = 'misc/farmasi_penjualan.xls';
        $objPHPExcel = PHPExcel_IOFactory::load('assets/templates/' . $tpl_filename);
        $objPHPExcel->setActiveSheetIndex(0);
        $sheet = $objPHPExcel->getActiveSheet();

        # Apply data rows
        $data = array();
        $start = 2;
        foreach ($list as $i => $row) {
            unset($row->id);

            $aRow = get_object_vars($row);
            $colnum = 0;
            foreach ($aRow as $val) {
                $sheet->setCellValue(chr(65 + $colnum) . ($i + $start), $val);
                $colnum++;
            }
        }
        $end = $start + (count($list) - 1);

        # Apply styles
        $sheet->getStyle("A$start:C$end")->applyFromArray(array(
            'font' => array(
                'size' => 8
            )
        ));
        $end++;
        $sheet->getStyle("A$start:C$end")->applyFromArray(array(
            'numberformat' => array(
                'code' => '#,##0'
            )
        ));
        $sheet->getStyle("A$end:C$end")->applyFromArray(array(
            'font' => array(
                'size' => 8,
                'bold' => true
            )
        ));
        $objPHPExcel->getActiveSheet()->getStyle('A'.$start.':C' . $end)->applyFromArray(array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => 'FF555555'),
                ),
            ),
        ));

        # Send excel document
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="'.date('Y-m-d H.i.s', strtotime($tanggal)).' - FARMASI RANAP PENJUALAN.xls"');
        header('Cache-Control: max-age=0');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: cache, must-revalidate');
        header('Pragma: public');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }
    public function excel_update_stock() {
        $tanggal = $this->input->get('tanggal');
        $sql = <<<EOT
SELECT 
    '$tanggal' as tanggal,
    'Update Stock' as `query`,
    (SELECT COUNT(t_farmasi_stock_opname.id) FROM t_farmasi_stock_opname WHERE t_farmasi_stock_opname.selisih <> 0 AND DATE(t_farmasi_stock_opname.tanggal) = DATE('$tanggal')) as 'Gudang',
    (SELECT COUNT(t_apotek_stock_opname.id) FROM t_apotek_stock_opname WHERE t_apotek_stock_opname.selisih <> 0 AND DATE(t_apotek_stock_opname.tanggal) = DATE('$tanggal')) as 'Farmasi',
    (SELECT COUNT(t_apotek_stock_opname.id) FROM t_apotek_stock_opname WHERE t_apotek_stock_opname.selisih <> 0 AND DATE(t_apotek_stock_opname.tanggal) = DATE('$tanggal') AND t_apotek_stock_opname.depo_id = 1) as 'Farmasi RAJAL',
    (SELECT COUNT(t_apotek_stock_opname.id) FROM t_apotek_stock_opname WHERE t_apotek_stock_opname.selisih <> 0 AND DATE(t_apotek_stock_opname.tanggal) = DATE('$tanggal') AND t_apotek_stock_opname.depo_id = 2) as 'Farmasi RANAP',
    (SELECT COUNT(t_satelite_stock_opname.id) FROM t_satelite_stock_opname WHERE t_satelite_stock_opname.selisih <> 0 AND DATE(t_satelite_stock_opname.tanggal) = DATE('$tanggal')) as 'Satelite';
EOT;
        $list = $this->db->query($sql)->result();

        # Prepare template
        $tpl_filename = 'misc/update_stock.xls';
        $objPHPExcel = PHPExcel_IOFactory::load('assets/templates/' . $tpl_filename);
        $objPHPExcel->setActiveSheetIndex(0);
        $sheet = $objPHPExcel->getActiveSheet();

        # Apply data rows
        $data = array();
        $start = 2;
        foreach ($list as $i => $row) {
            unset($row->id);

            $aRow = get_object_vars($row);
            $colnum = 0;
            foreach ($aRow as $val) {
                $sheet->setCellValue(chr(65 + $colnum) . ($i + $start), $val);
                $colnum++;
            }
        }
        $end = $start + (count($list) - 1);

        # Apply styles
        $sheet->getStyle("A$start:G$end")->applyFromArray(array(
            'font' => array(
                'size' => 8
            )
        ));
        $end++;
        $sheet->getStyle("A$start:G$end")->applyFromArray(array(
            'numberformat' => array(
                'code' => '#,##0'
            )
        ));
        $sheet->getStyle("A$end:G$end")->applyFromArray(array(
            'font' => array(
                'size' => 8,
                'bold' => true
            )
        ));
        $objPHPExcel->getActiveSheet()->getStyle('A'.$start.':G' . $end)->applyFromArray(array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => 'FF555555'),
                ),
            ),
        ));

        # Send excel document
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="'.date('Y-m-d H.i.s', strtotime($tanggal)).' - UPDATE STOCK.xls"');
        header('Cache-Control: max-age=0');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: cache, must-revalidate');
        header('Pragma: public');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }
    public function excel_kasir_transaksi_masuk() {
        $tanggal = $this->input->get('tanggal');
        $sql = <<<EOT
SELECT
    '$tanggal' as tanggal,
    m_kelompokpasien.nama as `Kelompok Pasien`,
    (SELECT COUNT(DISTINCT t_tindakan.pelayanan_id) FROM t_tindakan LEFT JOIN t_pelayanan ON t_tindakan.pelayanan_id = t_pelayanan.id WHERE DATE(t_tindakan.tanggal) = DATE('$tanggal') AND t_pelayanan.kelompok_pasien_id = m_kelompokpasien.id) as `Transaksi`
FROM m_kelompokpasien;
EOT;
        $list = $this->db->query($sql)->result();

        # Prepare template
        $tpl_filename = 'misc/kasir_transaksi_masuk.xls';
        $objPHPExcel = PHPExcel_IOFactory::load('assets/templates/' . $tpl_filename);
        $objPHPExcel->setActiveSheetIndex(0);
        $sheet = $objPHPExcel->getActiveSheet();

        # Apply data rows
        $data = array();
        $start = 2;
        foreach ($list as $i => $row) {
            unset($row->id);

            $aRow = get_object_vars($row);
            $colnum = 0;
            foreach ($aRow as $val) {
                $sheet->setCellValue(chr(65 + $colnum) . ($i + $start), $val);
                $colnum++;
            }
        }
        $end = $start + (count($list) - 1);

        # Apply styles
        $sheet->getStyle("A$start:C$end")->applyFromArray(array(
            'font' => array(
                'size' => 8
            )
        ));
        $end++;
        $sheet->getStyle("A$start:C$end")->applyFromArray(array(
            'numberformat' => array(
                'code' => '#,##0'
            )
        ));
        $sheet->getStyle("A$end:C$end")->applyFromArray(array(
            'font' => array(
                'size' => 8,
                'bold' => true
            )
        ));
        $objPHPExcel->getActiveSheet()->getStyle('A'.$start.':C' . $end)->applyFromArray(array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => 'FF555555'),
                ),
            ),
        ));

        # Send excel document
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="'.date('Y-m-d H.i.s', strtotime($tanggal)).' - Kasir (Transaksi Masuk).xls"');
        header('Cache-Control: max-age=0');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: cache, must-revalidate');
        header('Pragma: public');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }
    public function excel_kasir_transaksi_selesai() {
        $tanggal = $this->input->get('tanggal');
        $sql = <<<EOT
SELECT
    '$tanggal' as tanggal,
    IF(t_kasir.kelompok_pasien_id = 0, 'Penjualan Obat Bebas', (SELECT nama FROM m_kelompokpasien WHERE id = t_kasir.kelompok_pasien_id)) as `Kelompok Pasien`,
    COUNT(t_kasir.id)
FROM t_kasir
WHERE DATE(t_kasir.created_at) = DATE('$tanggal')
GROUP BY t_kasir.kelompok_pasien_id;
EOT;
        $list = $this->db->query($sql)->result();

        # Prepare template
        $tpl_filename = 'misc/kasir_transaksi_selesai.xls';
        $objPHPExcel = PHPExcel_IOFactory::load('assets/templates/' . $tpl_filename);
        $objPHPExcel->setActiveSheetIndex(0);
        $sheet = $objPHPExcel->getActiveSheet();

        # Apply data rows
        $data = array();
        $start = 2;
        foreach ($list as $i => $row) {
            unset($row->id);

            $aRow = get_object_vars($row);
            $colnum = 0;
            foreach ($aRow as $val) {
                $sheet->setCellValue(chr(65 + $colnum) . ($i + $start), $val);
                $colnum++;
            }
        }
        $end = $start + (count($list) - 1);

        # Apply styles
        $sheet->getStyle("A$start:C$end")->applyFromArray(array(
            'font' => array(
                'size' => 8
            )
        ));
        $end++;
        $sheet->getStyle("A$start:C$end")->applyFromArray(array(
            'numberformat' => array(
                'code' => '#,##0'
            )
        ));
        $sheet->getStyle("A$end:C$end")->applyFromArray(array(
            'font' => array(
                'size' => 8,
                'bold' => true
            )
        ));
        $objPHPExcel->getActiveSheet()->getStyle('A'.$start.':C' . $end)->applyFromArray(array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => 'FF555555'),
                ),
            ),
        ));

        # Send excel document
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="'.date('Y-m-d H.i.s', strtotime($tanggal)).' - Kasir (Transaksi Selesai).xls"');
        header('Cache-Control: max-age=0');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: cache, must-revalidate');
        header('Pragma: public');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }
    public function excel_ok() {
        $tanggal = $this->input->get('tanggal');
        $sql = <<<EOT
SELECT
    '$tanggal' as tanggal,
    'OK' as layanan,
    (SELECT COUNT(t_okregistrasi.id) FROM t_okregistrasi WHERE DATE(t_okregistrasi.tgl_operasi) = DATE('$tanggal')) as `Registrasi`,
    (SELECT COUNT(t_okpelaksanaan.id) FROM t_okpelaksanaan WHERE DATE(t_okpelaksanaan.created_at) = DATE('$tanggal')) as `Terdaftar`;
EOT;
        $list = $this->db->query($sql)->result();

        # Prepare template
        $tpl_filename = 'misc/ok.xls';
        $objPHPExcel = PHPExcel_IOFactory::load('assets/templates/' . $tpl_filename);
        $objPHPExcel->setActiveSheetIndex(0);
        $sheet = $objPHPExcel->getActiveSheet();

        # Apply data rows
        $data = array();
        $start = 2;
        foreach ($list as $i => $row) {
            unset($row->id);

            $aRow = get_object_vars($row);
            $colnum = 0;
            foreach ($aRow as $val) {
                $sheet->setCellValue(chr(65 + $colnum) . ($i + $start), $val);
                $colnum++;
            }
        }
        $end = $start + (count($list) - 1);

        # Apply styles
        $sheet->getStyle("A$start:D$end")->applyFromArray(array(
            'font' => array(
                'size' => 8
            )
        ));
        $end++;
        $sheet->getStyle("A$start:D$end")->applyFromArray(array(
            'numberformat' => array(
                'code' => '#,##0'
            )
        ));
        $sheet->getStyle("A$end:D$end")->applyFromArray(array(
            'font' => array(
                'size' => 8,
                'bold' => true
            )
        ));
        $objPHPExcel->getActiveSheet()->getStyle('A'.$start.':D' . $end)->applyFromArray(array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => 'FF555555'),
                ),
            ),
        ));

        # Send excel document
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="'.date('Y-m-d H.i.s', strtotime($tanggal)).' - OK.xls"');
        header('Cache-Control: max-age=0');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: cache, must-revalidate');
        header('Pragma: public');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }
}
