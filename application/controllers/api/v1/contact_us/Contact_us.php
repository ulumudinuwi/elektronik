<?php defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH . '/core/Rest.php');
use \Firebase\JWT\JWT;

class Contact_us extends Rest {
	protected $table_def = "t_kontak";
	protected $auth_key, $continue;

	public function __construct() {
		parent::__construct();
		$this->load->model('Contact_us_model', 'contact');

		$this->continue = false;
		$this->auth_key = $this->input->get_request_header('Authorization');
		if($this->auth_key != $this->secretKey) {
			if($this->checkToken()) $this->continue = true;
		} else $this->continue = true;
	}

	public function insert_post() {
		$name = $this->input->post('name') ? : '';
		$email = $this->input->post('email') ? : '';
		$subject = $this->input->post('subject') ? : '';
		$phone_number = $this->input->post('phone_number') ? : '';
		$description = $this->input->post('description') ? : '';
		$created_by = $this->input->post('created_by') ? : 0;

		if($this->continue) {
			
			$obj = new stdClass();
            $obj->name = $name;
            $obj->email = $email;
            $obj->subject = $subject;
            $obj->phone_number = $phone_number;
            $obj->description = $description;
            $obj->created_by = $created_by;

            $insert = $this->contact->create($obj);
            
            if ($insert) {
				$this->output->set_status_header(200)
					->set_content_type('application/json')
		            ->set_output(json_encode(["message" => "Proses Berhasil."]));
			} else {
				$this->output->set_status_header(400)
						->set_content_type('application/json')
		            	->set_output(json_encode(["status" => 0, "message" => "Product ID tidak ditemukan."]));
			}
		} else {
			$this->output->set_status_header(400)
	    		->set_content_type('application/json')
              	->set_output(json_encode([
	            				'status' => 0,
              					'message' => 'Auth key tidak diperbolehkan.'
              				]));
		}
	}
}