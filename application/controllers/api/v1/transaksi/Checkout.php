<?php defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH . '/core/Rest.php');
use \Firebase\JWT\JWT;
use Mpdf\Mpdf;

class Checkout extends Rest {
	protected $limit = 0;
	protected $table_def = "t_pos_checkout";
	protected $table_def_pesanan = "t_pos_pesanan";
    protected $table_def_barang = 'm_barang';
	protected $table_def_foto = "m_barang_foto";
    protected $table_def_member = 'm_dokter_member';
	protected $ori_dir, $thumb_dir, $auth_key, $continue, $thumb_upload;

	public function __construct() {
		parent::__construct();
		$this->load->model('Checkout_model', 'main');
		$this->ori_dir = './uploads/barang/original';
        $this->thumb_dir = './uploads/barang/thumbnail';
		$this->thumb_upload = './uploads/bukti_pembayaran';

		$this->continue = false;
		$this->auth_key = $this->input->get_request_header('Authorization') ? : $this->secretKey;
		if($this->auth_key != $this->secretKey) {
			if($this->checkToken()) $this->continue = true;
		} else $this->continue = true;

        $this->load->library('image_lib');
	}

	public function insert_post() {
		$detailProduct = $this->post('detail');

		if($this->continue) {
			
			$bpom = array('data' => [], 'total' => 0);
			$nonbpom = array('data' => [], 'total' => 0);

			foreach ($detailProduct as $row) {
				$barang = $this->db->select('a.id, a.nama, a.bpom')
                                            ->where('a.uid', $row['barang_uid'])
                                            ->get($this->table_def_barang.' a')->row();

                if ($barang->bpom == 1) {
                	$bpom['data'][] = $row;
                	$bpom['total'] += $row['total_harga'] * $row['qty'];
                }else{
                	$nonbpom['data'][] = $row;
                	$nonbpom['total'] += $row['total_harga'] * $row['qty'];
                }
			}

			$successBpom = false;
			$successNonBpom = false;

			if (count($bpom['data']) > 0) {
				$successBpom = $this->_saveInvoice($bpom, 'O - PER%');
			}
			if (count($nonbpom['data']) > 0) {
				$successNonBpom = $this->_saveInvoice($nonbpom, 'O - SI%');
			}

            if ($successBpom || $successNonBpom) {
            	$this->output->set_status_header(200)
					->set_content_type('application/json')
		            ->set_output(json_encode(["message" => "Product has been Checkout."]));
			} else {
				$this->output->set_status_header(400)
						->set_content_type('application/json')
		            	->set_output(json_encode(["status" => 0, "message" => "Product ID tidak ditemukan."]));
			}
		} else {
			$this->output->set_status_header(400)
	    		->set_content_type('application/json')
              	->set_output(json_encode([
	            				'status' => 0,
              					'message' => 'Auth key tidak diperbolehkan.'
              				]));
		}
	}

	private function _saveInvoice($detailProduct = array(), $prefix = "O - SI%") {
		$ekspedisi = $this->post('ekspedisi');
		$member_id = $this->post('member_id');
		$ongkir = $this->post('ongkir');
		$sales_id = $this->post('sales_id');
		
		$obj = new stdClass();
        $obj->tanggal_transaksi = date('Y-m-d H:i:s');
        $obj->no_invoice = get_no_invoice_online($prefix);
        $obj->member_id = $member_id;
        $obj->sales_id = $sales_id;
        $obj->total_harga = $detailProduct['total'];
        $obj->ekspedisi = $ekspedisi;
        $obj->ongkir = $ongkir;
        $obj->created_by = $member_id;
        $obj->update_by = $member_id;

        $insert = $this->main->create($obj);
        
        if ($insert) {
        	foreach ($detailProduct['data'] as $row) {
        		$dataPesanan = new stdClass();
	            $dataPesanan->status = 2; // checkout
	            $dataPesanan->checkout_id = $insert->id;
	            $dataPesanan->qty = $row['qty'];
	            $dataPesanan->harga = $row['harga'];
	            $dataPesanan->diskon = $row['diskon'];
	            $dataPesanan->total = $row['total_harga'] * $row['qty'];
		        $dataPesanan->update_by = $obj->update_by;
	            $dataPesanan->update_at = date('Y-m-d H:i:s');

	            if($row['uid']) {
		            $this->db->where('uid', $row['uid']);
		            $this->db->update($this->table_def_pesanan, $dataPesanan);
        		} else {
        			$dataPesanan->member_id = $member_id;
		            $dataPesanan->product_uid = $row['barang_uid'];
		            $dataPesanan->created_by = $member_id;
		            $dataPesanan->created_at = date('Y-m-d H:i:s');
		            $this->db->set('uid', 'UUID()', FALSE)
		            	->insert($this->table_def_pesanan, $dataPesanan);
        		}
        	}

			return true;
		}
		return false;
	}

	public function list_get() {
		$member_id = $this->input->get('id') ? : '';
		$status = $this->input->get('status') ? : '';

		$limit = $this->input->get('limit') ? $this->input->get('limit') : $this->limit;
		$offset = $this->input->get('offset') ? $this->input->get('offset') : 0;
		$term = $this->input->get('term') ? $this->input->get('term') : '';

		if($this->continue) {
			/*
	         * Order
	         */
			$sOrder = "";
	        $aOrders = array();
	        $aOrders[] = "{$this->table_def}.tanggal_transaksi DESC";
	        if (count($aOrders) > 0) {
	            $sOrder = implode(', ', $aOrders);
	        }
	        if (!empty($sOrder)) {
	            $sOrder = "ORDER BY ".$sOrder;
	        }

	        /*
	         * Where
	         */
	        $sWhere = "";
	        $aWheres = array();
	        $aWheres[] = "{$this->table_def}.member_id = '{$member_id}'";
	        if ($status) {
	        	$aWheres[] = "{$this->table_def}.status = '{$status}'";
	        }
	        if (count($aWheres) > 0) {
	            $sWhere = implode(' AND ', $aWheres);
	        }
	        if (!empty($sWhere)) {
	            $sWhere = "WHERE ".$sWhere;
	        }

	        $aLikes = array();
	        if($term != "") {
	            $aLikes[] = "{$this->table_def}.no_invoice = '{$term}'";
	            $aLikes[] = "{$this->table_def}.tanggal_transaksi LIKE '%{$term}%'";
	        }

	        if (count($aLikes) > 0) {
	            $sLike = "(".implode(' OR ', $aLikes).")";
	            $sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
	        }

	        $aSelect = array(
	        	"{$this->table_def}.id",
	        	"{$this->table_def}.uid",
	        	"{$this->table_def}.tanggal_transaksi",
	        	"{$this->table_def}.no_invoice as no_invoice",
	        	"{$this->table_def}.total_harga as total_harga",
	        	"{$this->table_def}.bukti_pembayaran as bukti_pembayaran",
	        	"{$this->table_def}.ekspedisi as ekspedisi",
	        	"{$this->table_def}.ongkir as ongkir",
	        	"{$this->table_def}.no_resi as no_resi",
	        	"{$this->table_def}.alasan_penolakan as alasan_penolakan",
	        	"{$this->table_def}.status as status",
	        );

			$list = $this->main->get_all($limit, $offset * $limit, $sWhere, $sOrder, $aSelect);
			foreach ($list['data'] as $i => $obj) {
				$obj->tanggal_transaksi = date('d-m-Y H:i:s', strtotime($obj->tanggal_transaksi));
				switch ($obj->status) {
					case 1:
						$obj->status_label = 'Belum Bayar';
					break;
					case 2:
						$obj->status_label = 'Dikemas';
					break;
					case 3:
						$obj->status_label = 'Dikirim';
					break;
					case 4:
						$obj->status_label = 'Selesai';
					break;
					case 5:
						$obj->status_label = 'Dibatalkan';
					break;
					case 6:
						$obj->status_label = 'Dikembalikan';
					break;
				}

				$obj->dataDetail = $this->db->select('a.id checkout_id, b.nama,  b.bpom')
                                        ->join($this->table_def_barang.' b', 'a.product_uid=b.uid')
                                        ->where('a.checkout_id', $obj->id)
                                        ->where('a.status !=', '1')
                                        ->get($this->table_def_pesanan.' a')->result();
			}

			$output['total_data'] = $list['total_rows'];
			//$output['total_pages'] = (ceil($list['total_rows'] / $limit)) - 1;
			$output['sisa_data'] = $list['total_rows'] - (($offset + 1) * $limit) < 0 ? 0 : $list['total_rows'] - (($offset + 1) * $this->limit);
			$output['list'] = $list['data'];

			$this->output->set_status_header(200)
				->set_content_type('application/json')
	            ->set_output(json_encode($output));
	    } else {
	    	$this->output->set_status_header(400)
	    		->set_content_type('application/json')
              	->set_output(json_encode([
	            				'status' => 0,
              					'message' => 'Auth key tidak diperbolehkan.'
              				]));
	    }
	}

	public function list_badge_get() {
		$member_id = $this->input->get('id') ? : '';

		if($this->continue) {
			$output['list'] = $this->main->get_total_by_status($member_id);

			$this->output->set_status_header(200)
				->set_content_type('application/json')
	            ->set_output(json_encode($output));
	    } else {
	    	$this->output->set_status_header(400)
	    		->set_content_type('application/json')
              	->set_output(json_encode([
	            				'status' => 0,
              					'message' => 'Auth key tidak diperbolehkan.'
              				]));
	    }
	}

	public function list_detail_get() {
		$uid = $this->input->get('uid') ? : '';

		if($this->continue) {

	        $aSelect = array(
	        	"{$this->table_def}.id",
	        	"{$this->table_def}.uid",
	        	"{$this->table_def}.tanggal_transaksi",
	        	"{$this->table_def}.no_invoice as no_invoice",
	        	"{$this->table_def}.total_harga as total_harga",
	        	"{$this->table_def}.bukti_pembayaran as bukti_pembayaran",
	        	"{$this->table_def}.ekspedisi as ekspedisi",
	        	"{$this->table_def}.ongkir as ongkir",
	        	"{$this->table_def}.alasan_penolakan as alasan_penolakan",
	        	"{$this->table_def}.status as status",
	        );

			$list = $this->main->get_by("WHERE {$this->table_def}.uid = '{$uid}'", $aSelect);
			$list->bukti_pembayaran = $this->thumb_upload.'/'.$list->bukti_pembayaran;

			$list->bukti_pembayaran_url = base_url().'assets/img/no_image_available.png';
    		if (file_exists($list->bukti_pembayaran)) {
    			$list->bukti_pembayaran_url = base_url($list->bukti_pembayaran);
    		}

			switch ($list->status) {
				case 1:
					$list->status_label = 'Belum Bayar';
				break;
				case 2:
					$list->status_label = 'Dikemas';
				break;
				case 3:
					$list->status_label = 'Dikirim';
				break;
				case 4:
					$list->status_label = 'Selesai';
				break;
				case 5:
					$list->status_label = 'Dibatalkan';
				break;
			}

			$list->dataDetail = $this->db->select('a.id checkout_id, a.qty, a.harga, a.diskon,  b.nama')
                                        ->join($this->table_def_barang.' b', 'a.product_uid=b.uid')
                                        ->where('a.checkout_id', $list->id)
                                        ->where('a.status !=', '1')
                                        ->get($this->table_def_pesanan.' a')->result();

			$this->output->set_status_header(200)
				->set_content_type('application/json')
	            ->set_output(json_encode($list));
	    } else {
	    	$this->output->set_status_header(400)
	    		->set_content_type('application/json')
              	->set_output(json_encode([
	            				'status' => 0,
              					'message' => 'Auth key tidak diperbolehkan.'
              				]));
	    }
	}

	/*
	*  upload_pembayaran_post() Tidak digunakan
	*/
	public function upload_pembayaran_post() {
		$uid = $this->input->post('uid') ? : '';

		if($this->continue) {

            $bayar = $this->main->bayar($uid);
            
            if ($bayar) {
				$this->output->set_status_header(200)
					->set_content_type('application/json')
		            ->set_output(json_encode(["message" => "Pembayaran Berhasil."]));
			} else {
				$this->output->set_status_header(400)
						->set_content_type('application/json')
		            	->set_output(json_encode(["status" => 0, "message" => "UID tidak ditemukan."]));
			}
		} else {
			$this->output->set_status_header(400)
	    		->set_content_type('application/json')
              	->set_output(json_encode([
	            				'status' => 0,
              					'message' => 'Auth key tidak diperbolehkan.'
              				]));
		}
	}

	public function delete_checkout_post() {
		$uid = $this->input->post('uid') ? : '';

		if($this->continue) {

            $deleted = $this->main->delete($uid);
            
            if ($deleted) {
				$this->output->set_status_header(200)
					->set_content_type('application/json')
		            ->set_output(json_encode(["message" => "Product has been Canceled."]));
			} else {
				$this->output->set_status_header(400)
						->set_content_type('application/json')
		            	->set_output(json_encode(["status" => 0, "message" => "UID tidak ditemukan."]));
			}
		} else {
			$this->output->set_status_header(400)
	    		->set_content_type('application/json')
              	->set_output(json_encode([
	            				'status' => 0,
              					'message' => 'Auth key tidak diperbolehkan.'
              				]));
		}
	}

	public function upload_image_post(){
		$uid = $this->input->post('uid') ? : '';

		$dCheckout = $this->db->select('a.id, a.bukti_pembayaran, a.no_invoice, a.member_id')
                            ->where('a.uid', $uid)
                            ->get($this->table_def.' a')->row();

        if ($dCheckout) {
        	if (!empty($_FILES['file']['name'])) {
		      	if ($dCheckout->bukti_pembayaran) {
		      		make_directory('./uploads/bukti_pembayaran');
		        	if (file_exists('./uploads/bukti_pembayaran/'.$dCheckout->bukti_pembayaran)) {
		          		unlink('./uploads/bukti_pembayaran/'.$dCheckout->bukti_pembayaran);
		        	}
		      	}
		      	$file = proc_do_upload('file', './uploads/bukti_pembayaran', [
		      		'allowed_types' => 'png|jpg|jpeg'
		      	], TRUE);

		      	if(isset($file['message']['error'])) {
                    $this->output->set_status_header(400)
						->set_content_type('application/json')
		            	->set_output(json_encode(["status" => 0, "message" => "Jenis berkas tidak diperbolehkan.", 'data' => $file]));
                    return;
                }
		      	$foto = $file['file']['file_name'];
		    }
        }

        if(!isset($foto)) {
        	$this->output->set_status_header(400)
				->set_content_type('application/json')
            	->set_output(json_encode(["status" => 0, "message" => "Bukti pembayaran belum terisi."]));
	        return;
        }

        $dataCheckout = array(
            'bukti_pembayaran' => $foto,
            'status' => '2', //Upload Bukti Pembayaran
            'update_by' => $dCheckout->member_id,
            'update_at' => date('Y-m-d H:i:s'),
        );
        $success = $this->db->where('uid', $uid);
			       $this->db->update($this->table_def, $dataCheckout);
        if ($success) {
        	$this->output->set_status_header(200)
				->set_content_type('application/json')
	            ->set_output(json_encode(["message" => "Upload Bukti Pembayaran Berhasil."]));
		} else {
			$this->output->set_status_header(400)
					->set_content_type('application/json')
	            	->set_output(json_encode(["status" => 0, "message" => "Product ID tidak ditemukan."]));
		}
	}

    public function print_invoice_online_get(){
        $request = request_handler();
        $uid = $request->uid;
        
        $aSelect = array(
        	"{$this->table_def}.id",
        	"{$this->table_def}.uid",
        	"{$this->table_def}.tanggal_transaksi",
        	"{$this->table_def}.no_invoice as no_invoice",
        	"{$this->table_def}.total_harga as total_harga",
        	"{$this->table_def}.bukti_pembayaran as bukti_pembayaran",
        	"{$this->table_def}.ekspedisi as ekspedisi",
        	"{$this->table_def}.ongkir as ongkir",
        	"{$this->table_def}.alasan_penolakan as alasan_penolakan",
        	"{$this->table_def}.member_id as member_id",
        	"{$this->table_def}.status as status",
        );

		$list = $this->main->get_by("WHERE {$this->table_def}.uid = '{$uid}'", $aSelect);
		$list->bukti_pembayaran = $this->thumb_upload.'/'.$list->bukti_pembayaran;

		switch ($list->status) {
			case 1:
				$list->status_label = 'Belum Bayar';
			break;
			case 2:
				$list->status_label = 'Dikemas';
			break;
			case 3:
				$list->status_label = 'Dikirim';
			break;
			case 4:
				$list->status_label = 'Selesai';
			break;
			case 5:
				$list->status_label = 'Dibatalkan';
			break;
		}

		$list->dataDetail = $this->db->select('a.id checkout_id, a.qty, a.harga, a.total, a.diskon,  b.nama, b.bpom')
                                    ->join($this->table_def_barang.' b', 'a.product_uid=b.uid')
                                    ->where('a.checkout_id', $list->id)
                                    ->where('a.status !=', '1')
                                    ->get($this->table_def_pesanan.' a')->result();

		$list->dataMember = $this->db->select('a.id, a.nama, a.no_hp, a.alamat')
                                    ->where('a.id', $list->member_id)
                                    ->get($this->table_def_member.' a')->row();

        $data['list'] = $list; 
        if ($list->dataDetail[0]->bpom) {
            $data['no_rek'] =  $this->db->query("SELECT * FROM m_no_rekening WHERE id = 2 ORDER BY id")->row();
        }else{
            $data['no_rek'] =  $this->db->query("SELECT * FROM m_no_rekening ORDER BY id")->row();
        }

        $html = $this->load->view('invoice', $data, true);
        // Margins
        //$ml = 10;
        //$mr = 10;
        //$mt = 10;
        //$mb = 10;
        $ml = 15;
        $mr = 10;
        $mt = 15;
        $mb = 0;

        # Create PDF
        $mpdf = new Mpdf([
            'mode' => 'utf-8',
            'format' => [222.0, 150.0],
            'margin_left' => $ml,
            'margin_right' => $mr,
            'margin_top' => $mt,
            'margin_bottom' => $mb,
        ]);
        $mpdf->WriteHTML($html);
        $mpdf->SetHTMLFooter('
            <table width="100%" style="position: initial; top: 10px;">
                <tr>
                    <td colspan="5"></td>
                    <td colspan="1" valign="top"><p>&nbsp;HORMAT KAMI</p></td>
                </tr>
                <tr>
                    <td colspan="5"></td>
                    <td colspan="1" valign="top">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="5"></td>
                    <td colspan="1" valign="top">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="5"> <p style="font-size: 12px;" class="text-bold">NB: - <u>Pembayaran hanya melalui '.$data['no_rek']->nama_bank.' 
                    No. Rek : '.$data['no_rek']->no_rekening.' Atas Nama '.$data['no_rek']->nama_pemilik.'.</u></p></td>
                    <td colspan="1" valign="top">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="5"><p style="font-size: 12px;">&nbsp;&nbsp;&nbsp;&nbsp;- Barang yang telah dibeli tidak dapat dikembalikan / ditukar.</p></td>
                    <td colspan="1" valign="top"><p>(&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;)</p></td>
                </tr>
            </table>
            ');
        $name = "Invoice- ".$list->no_invoice.'.pdf';
        $mpdf->Output($name, 'I');
    }

    public function order_received_post() {
    	$this->load->helper('notifikasi_ecommerce');

		$uid = $this->input->post('id');
		$member_id = $this->input->post('member_id');

		if($this->continue) {
			$result = $this->main->order_received($uid, $member_id);

            if ($result) {
				$this->output->set_status_header(200)
					->set_content_type('application/json')
		            ->set_output(json_encode(["message" => "Pesanan telah diterima."]));
			} else {
				$this->output->set_status_header(500)
						->set_content_type('application/json')
		            	->set_output(json_encode(["status" => 0, "message" => "Terdapat kesalahan pada server."]));
			}
		} else {
			$this->output->set_status_header(400)
	    		->set_content_type('application/json')
              	->set_output(json_encode([
	            				'status' => 0,
              					'message' => 'Auth key tidak diperbolehkan.'
              				]));
		}
	}
}