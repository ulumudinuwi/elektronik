<?php

include_once(APPPATH . '/core/Rest.php');
use \Firebase\JWT\JWT;

use Illuminate\Database\Capsule\Manager as DB;
use Ryuna\Datatables;
use Ryuna\Auth;
use Ryuna\Response;
use Ryuna\Validasi;
use Carbon\Carbon;

class Sales extends Rest {

	public function __construct() {
        parent::__construct();
        $this->load->model('master/Sales_model');
        $this->load->model('sales/Target_sales_model');

        $this->auth_key = $this->input->get_request_header('Authorization');
        if($this->auth_key != $this->secretKey) {
            $this->checkToken();
        }
    }

    public function list_get(){
        $list = Sales_model::where('status','=',1)->lists('nama','id');
        return Response::json($list, 200);
    }

    public function list_sales_get(){
        $list = $this->db->select('id, nama, kode_promo, status')
                    ->where('status', 1)
                    ->where('kode_promo !=', null)
                    ->get('m_sales')->result();

        $this->output->set_status_header(200)
                    ->set_content_type('application/json')
                    ->set_output(json_encode($list));
    }
}