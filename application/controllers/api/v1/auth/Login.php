<?php defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH . '/core/Rest.php');
use \Firebase\JWT\JWT;

class Login extends Rest {
	protected $table_def_dokter_member = "m_dokter_member";
	public function __construct() {
		parent::__construct();

		$this->load->language('auth');
		$this->load->model('akun/Profile_model', 'profile');

		$date = new DateTime();
		$username = $this->post('email', TRUE);
		$password = $this->post('password', TRUE);

		if ($username && $password ) {
            $user = $this->user_model->get_by_username($username);
            if ($user && $user->verifikasi_email == 0) {
            	return $this->failToken('Email belum di Verifikasi !!!');
            }else{
	            if($user) {
		            if ($user && $this->user_model->check_password($password, $user->password)) {

		                $name = $user->first_name;
		                if($user->first_name != $user->last_name) $name .= ' '.$user->last_name;
		                $userImage = null;
						if (file_exists($user->image_profile)) {
							$userImage = base_url($user->image_profile);
	            		}

	            		/*$dataProfile = $this->profile->get_by("WHERE {$this->table_def_dokter_member}.id = '{$user->dokter_id}'");
	            		if($dataProfile) {
							$dataProfile->tgl_lahir = date("d/m/Y", strtotime($dataProfile->tgl_lahir));
							$dataProfile->url_foto_sip = file_exists(base_url($dataProfile->foto_sip)) ? base_url($dataProfile->foto_sip) : null;
	            		} else $dataProfile = null;*/

		                $payLoad['id'] = $user->id;
		                $payLoad['dokter_id'] = $user->dokter_id;
	                    $payLoad['email'] = $user->email;
	                    $payLoad['name'] = $name;
	                    $payLoad['role_id'] = $user->role_id;
	                    $payLoad['role_name'] = $user->role_name;
	                    $payLoad['image_profile'] = $userImage;
	                    $payLoad['iat'] = $date->getTimestamp(); //waktu di buat
	                    $payLoad['exp'] = $date->getTimestamp() + 86400; //satu hari
	                    
	                    $output['token'] = JWT::encode($payLoad, $this->secretKey);
	                    $output['data_user'] = $payLoad;
	                    // $output['data_profile'] = $dataProfile;
	                    $output['status'] = 1;
	                    $output['message'] = 'Login Berhasil !';
	                    return $this->response($output, REST_Controller::HTTP_OK);
		            } else return $this->failToken(lang('login_attempt_failed'));
		        } else return $this->failToken('Tidak ada akun dengan Username tersebut !!!');
            }
	    } else {
            if (($username === '') || ($password === '')) return $this->failToken(lang('username_or_password_empty'));
        }
	}
}