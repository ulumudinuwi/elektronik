<?php defined('BASEPATH') OR exit('No direct script access allowed');

	include_once(APPPATH . '/core/Rest.php');
	use \Firebase\JWT\JWT;

	class Akun extends Rest {
		protected $table_def = "m_dokter_member";
		protected $table_def_auth = "auth_users";
		protected $dir_profile = './uploads/photo_profile';
		protected $dir_photo_sip = './uploads/photo_sip';
		protected $dir_document = './uploads/document';
		protected $auth_key, $continue;

		public function __construct() {
			parent::__construct();
			$this->load->model('akun/Profile_model', 'profile');
        	$this->load->helper('phpmailer');

			$this->continue = false;
			$this->auth_key = $this->input->get_request_header('Authorization');
			if($this->auth_key != $this->secretKey) {
				if($this->checkToken()) $this->continue = true;
			} else $this->continue = true;
		}

		public function profile_detail_get() {
			$id = $this->input->get('id') ? : 0;
			
			if (!$id) {
				$this->output->set_status_header(200)
					->set_content_type('application/json')
		            ->set_output(json_encode(["data" => array()]));
			}
			else {
				if($this->continue) {
					if(!$id) {
						$this->output->set_status_header(400)
								->set_content_type('application/json')
				            	->set_output(json_encode(["status" => 0, "message" => "Parameter pencarian tidak ditemukan."]));
				        return;
					}

					$data = $this->profile->get_by("WHERE {$this->table_def}.id = '{$id}'");
					$data->tgl_lahir = date("d/m/Y", strtotime($data->tgl_lahir));
					$data->url_foto_sip = file_exists(base_url($data->foto_sip)) ? base_url($data->foto_sip) : null;
					// $data->url_foto = base_url($data->foto_profile);
					if(!$data) {
						$this->output->set_status_header(400)
								->set_content_type('application/json')
				            	->set_output(json_encode(["status" => 0, "message" => "Akun Tidak ditemukan."]));
				        return;
					}
					
					$this->output->set_status_header(200)
						->set_content_type('application/json')
			            ->set_output(json_encode(["data" => $data]));
			    } else {
			    	$this->output->set_status_header(400)
			    		->set_content_type('application/json')
		              	->set_output(json_encode([
			            				'status' => 0,
		              					'message' => 'Auth key tidak diperbolehkan.'
		              				]));
			    }
			}
		}

		public function change_profile_post() {
			$id = $this->input->post('id') ? : '';
			$name = $this->input->post('name') ? : '';
			$email = $this->input->post('email') ? : '';
			$no_hp = $this->input->post('phone_number') ? : '';
			$nama_klinik = $this->input->post('nama_klinik') ? : '';
			$jenis_kelamin = $this->input->post('jenis_kelamin') ? : 0;
			$tempat_lahir = $this->input->post('tempat_lahir') ? : '';
			$tmpTanggal_lahir = $this->input->post('tanggal_lahir') ? : '';
			$no_ktp = $this->input->post('no_ktp') ? : '';
			$no_sip = $this->input->post('no_sip') ? : '';
			$kode_promo = $this->input->post('kode_promo') ? : '';
			$date = new DateTime();

			$tanggal_lahir = date('Y-m-d', strtotime(str_replace('/', '-', $tmpTanggal_lahir)));

			if($this->continue) {
	            $obj = new stdClass();
	            $obj->id = $id;
	            $obj->nama = $name;
	            $obj->email = $email;
	            $obj->no_hp = $no_hp;
	            $obj->nama_klinik = $nama_klinik;
	            $obj->jenis_kelamin = $jenis_kelamin;
	            $obj->tempat_lahir = $tempat_lahir;
	            $obj->tgl_lahir = $tanggal_lahir;
	            $obj->no_ktp = $no_ktp;
	            $obj->no_sip = $no_sip;
	            $obj->kode_promo = $kode_promo;

	            $userId = $this->db->select('id')
	            				->where('dokter_id', $obj->id)
	            				->get($this->table_def_auth)->row();
				$user = $this->user_model->get_by_id($userId->id);
				
				$dokter = $this->profile->get_by("WHERE {$this->table_def}.id = '{$id}'");
	            
	            if(!empty($_FILES['image_profile']['name'])) {
	            	make_directory($this->dir_profile);
	            	if (file_exists($user->image_profile)) {
            	    	unlink($user->image_profile);
            	    }

		            $file_foto = proc_do_upload('image_profile', $this->dir_profile, [
		                'file_name' => 'profile-' . $name .'-'.$id.time(),
		                'allowed_types' => 'gif|jpg|png'
		            ]);

	                if(isset($file_foto['message']['error'])) {
	                    $this->output->set_status_header(400)
							->set_content_type('application/json')
			            	->set_output(json_encode(["status" => 0, "message" => "Jenis berkas tidak diperbolehkan.", 'data' => $file_foto]));
	                    return;
	                }
	                $obj->image_profile = $this->dir_profile . '/' . $file_foto['file']['file_name'];
	            }
				
				if(!empty($_FILES['foto_sip']['name'])) {
	            	make_directory($this->dir_photo_sip);
	            	if (file_exists($dokter->foto_sip)) {
            	    	unlink($dokter->foto_sip);
            	    }

		            $file_foto = proc_do_upload('foto_sip', $this->dir_photo_sip, [
		                'file_name' => 'foto-' . $name .'-'.$id.time(),
		                'allowed_types' => 'gif|jpg|png'
		            ]);

	                if(isset($file_foto['message']['error'])) {
	                    $this->output->set_status_header(400)
							->set_content_type('application/json')
			            	->set_output(json_encode(["status" => 0, "message" => "Jenis berkas tidak diperbolehkan.", 'data' => $file_foto]));
	                    return;
	                }
	                $obj->foto_sip = $this->dir_photo_sip . '/' . $file_foto['file']['file_name'];
	            }

	            $change = $this->profile->change_profile($obj);
				
	            if ($change) {
	            	$user = $this->user_model->get_by_id($userId->id);
					$userImage = null;
					if (file_exists($user->image_profile)) {
						$userImage = base_url($user->image_profile);
            		}

					$payLoad['id'] = $user->id;
	                $payLoad['dokter_id'] = $user->dokter_id;
	                $payLoad['email'] = $user->email;
	                $payLoad['name'] = $name;
	                $payLoad['role_id'] = $user->role_id;
	                $payLoad['role_name'] = $user->role_name;
	                $payLoad['image_profile'] = $userImage;
					
	                $output['data_user'] = $payLoad;
	                $output['status'] = 1;
	                $output['message'] = 'Profile berhasil diubah !';

					$this->output->set_status_header(200)
						->set_content_type('application/json')
			            ->set_output(json_encode(["data" => $output]));
				} else {
					$this->output->set_status_header(400)
							->set_content_type('application/json')
			            	->set_output(json_encode(["status" => 0, "message" => "ID tidak ditemukan."]));
				}
			} else {
				$this->output->set_status_header(400)
		    		->set_content_type('application/json')
	              	->set_output(json_encode([
		            				'status' => 0,
	              					'message' => 'Auth key tidak diperbolehkan.'
	              				]));
			}
		}

		public function change_address_post() {
			$id = $this->input->post('id') ? : '';
			$kecamatan = $this->input->post('kecamatan') ? : '';
			$kode_pos = $this->input->post('kode_pos') ? : '';
			$kota = $this->input->post('kota') ? : '';
			$nama_gedung = $this->input->post('nama_gedung') ? : '';
			$name = $this->input->post('name') ? : 0;
			$phone_number = $this->input->post('phone_number') ? : '';
			$provinsi = $this->input->post('provinsi') ? : '';
			$provinsi_text = $this->input->post('provinsi_text') ? : '';
			$kota_text = $this->input->post('kota_text') ? : '';
			$kecamatan_text = $this->input->post('kecamatan_text') ? : '';

			$date = new DateTime();

			if($this->continue) {
				
				$obj = new stdClass();
	            $obj->id = $id;
	            $obj->provinsi = $provinsi;
	            $obj->kota = $kota;
	            $obj->kecamatan = $kecamatan;
	            $obj->gedung_jalan = $nama_gedung;
	            $obj->kode_pos = $kode_pos;
	            $obj->provinsi_tmp = $provinsi_text;
	            $obj->kota_tmp = $kota_text;
	            $obj->kecamatan_tmp = $kecamatan_text;

	            $change = $this->profile->change_address($obj);
	            
	            if ($change) {
	            	$userId = $this->db->select('id')
	                				->where('dokter_id', $change->id)
	                				->get($this->table_def_auth)->row();

					$user = $this->user_model->get_by_id($userId->id);
					$userImage = null;
					if (file_exists($user->image_profile)) {
						$userImage = base_url($user->image_profile);
            		}

					$payLoad['id'] = $user->id;
	                $payLoad['dokter_id'] = $user->dokter_id;
	                $payLoad['email'] = $user->email;
	                $payLoad['name'] = $name;
	                $payLoad['role_id'] = $user->role_id;
	                $payLoad['role_name'] = $user->role_name;
	                $payLoad['image_profile'] = $userImage;
	                
	                $output['data_user'] = $payLoad;
	                $output['status'] = 1;
	                $output['message'] = 'Profile alamat berhasil diubah !';

					$this->output->set_status_header(200)
						->set_content_type('application/json')
			            ->set_output(json_encode(["data" => $output]));
				} else {
					$this->output->set_status_header(400)
							->set_content_type('application/json')
			            	->set_output(json_encode(["status" => 0, "message" => "ID tidak ditemukan."]));
				}
			} else {
				$this->output->set_status_header(400)
		    		->set_content_type('application/json')
	              	->set_output(json_encode([
		            				'status' => 0,
	              					'message' => 'Auth key tidak diperbolehkan.'
	              				]));
			}
		}

		public function change_password_post() {
			$id = $this->input->post('id') ? : '';
			$password_lama = $this->input->post('password_lama') ? : '';
			$confirm_password_baru = $this->input->post('confirm_password_baru') ? : '';
			$password_baru = $this->input->post('password_baru') ? : '';

			$date = new DateTime();

			if($this->continue) {
				
				$obj = new stdClass();
	            $obj->id = $id;
	            $obj->password_baru = $password_baru;
	            $obj->password_lama = $password_lama;
				
	        	$user = $this->db->select('id, password')
	            				->where('dokter_id', $id)
	            				->get($this->table_def_auth)->row();

				$checkPassword = $this->user_model->check_password($password_lama, $user->password);

				if ($checkPassword) {

					$change = $this->profile->change_password($obj);

		            if ($change) {
		            	$userId = $this->db->select('id')
		                				->where('dokter_id', $change->id)
		                				->get($this->table_def_auth)->row();

						$user = $this->user_model->get_by_id($userId->id);
						$userImage = null;
						if (file_exists($user->image_profile)) {
							$userImage = base_url($user->image_profile);
	            		}

						$payLoad['id'] = $user->id;
		                $payLoad['dokter_id'] = $user->dokter_id;
		                $payLoad['email'] = $user->email;
		                $payLoad['name'] = $user->first_name;
		                $payLoad['role_id'] = $user->role_id;
		                $payLoad['role_name'] = $user->role_name;
		                $payLoad['image_profile'] = $userImage;
		                
		                $output['data_user'] = $payLoad;
		                $output['status'] = 1;
		                $output['message'] = 'Password berhasil diubah !';

						$this->output->set_status_header(200)
							->set_content_type('application/json')
				            ->set_output(json_encode(["data" => $output]));
					} else {
						$this->output->set_status_header(400)
								->set_content_type('application/json')
				            	->set_output(json_encode(["status" => 0, "message" => "ID tidak ditemukan."]));
					}
				}else{
					$this->output->set_status_header(400)
								->set_content_type('application/json')
				            	->set_output(json_encode(["status" => 0, "message" => "Password Lama Salah."]));
				}	
			} else {
				$this->output->set_status_header(400)
		    		->set_content_type('application/json')
	              	->set_output(json_encode([
		            				'status' => 0,
	              					'message' => 'Auth key tidak diperbolehkan.'
	              				]));
			}
		}

		public function register_post() {
			$name = $this->post('name') ? : '';
			$email = $this->post('email') ? : ''; 
			$phone_number = $this->post('phone_number') ? : ''; 
			$no_sip = $this->post('no_sip') ? : ''; 
			$kode_promo = $this->post('kode_promo') ? : ''; 
			$sales_id = $this->post('sales_id') ? : ''; 
			$alamat = $this->post('alamat') ? : ''; 
			$username = $this->post('username') ? : ''; 
			$password = $this->post('password') ? : ''; 


			$obj = new stdClass();
            $obj->nama = $name;
            $obj->no_hp = $phone_number;
            $obj->alamat = $alamat;
            $obj->alamat_klinik = $alamat;
            $obj->email = $email;
            $obj->kode_promo = $kode_promo;
            $obj->sales_id = $sales_id;

			$pExist = $this->profile->get_by("WHERE {$this->table_def}.email = '{$email}' AND {$this->table_def}.status = 1");

        	$rolesDokter = $this->db->select('id')
            				->like('name', "dokter")
            				->get('acl_roles')->row();


			if ($pExist) {
				$this->output->set_status_header(400)
					->set_content_type('application/json')
		            ->set_output(json_encode([
		            	"message" => "Email Sudah Terdaftar.",
		            	"data" => $pExist
		            ]));
				return;
			}

			if(!empty($_FILES['foto_sip']['name'])) {
            	make_directory($this->dir_photo_sip);

	            $file_foto = proc_do_upload('foto_sip', $this->dir_photo_sip, [
	                'file_name' => 'foto-' . $name .time(),
	                'allowed_types' => 'gif|jpg|png'
	            ]);

                if(isset($file_foto['message']['error'])) {
                    $this->output->set_status_header(400)
						->set_content_type('application/json')
		            	->set_output(json_encode(["status" => 0, "message" => "Jenis berkas tidak diperbolehkan.", 'data' => $file_foto]));
                    return;
                }
                $obj->foto_sip = $this->dir_photo_sip . '/' . $file_foto['file']['file_name'];
            }

            $insert = $this->profile->create($obj);
            
            if ($insert) {
            	//User
	            $dataUser = new stdClass();
	            $dataUser->uid = uuid();
	            $dataUser->first_name = $name;
	            $dataUser->username = $username;
	            $dataUser->email = $email;
	            $dataUser->password = $this->passwordhash->HashPassword($password);
	            $dataUser->dokter_id = $insert->id;
	            $dataUser->role_id = $rolesDokter->id;
	            //$dataUser->verifikasi_email = 1;
	            $dataUser->registered = timestamp();
	            $dataUser->created_at = timestamp();
	            $dataUser->created_by = $insert->id;
	            $dataUser->update_at = timestamp();
	            $dataUser->update_by = $insert->id;

	            $user_id = $this->profile->createUser($dataUser);
                
                $kirim = $this->send_email(base64_encode($user_id->id), $email, $name);
                
                if ($kirim) {
					$this->output->set_status_header(200)
						->set_content_type('application/json')
			            ->set_output(json_encode(["message" => "Pendaftaran Berhasil."]));
                }

				$this->output->set_status_header(200)
					->set_content_type('application/json')
		            ->set_output(json_encode(["message" => "Pendaftaran Berhasil."]));
			} else {
				$this->output->set_status_header(400)
						->set_content_type('application/json')
		            	->set_output(json_encode(["status" => 0, "message" => "Pendaftaran Gagal."]));
			}
		}
		
	    public function send_email($user_id, $email, $first_name) 
	    {
	        $this->db->query('SET SESSION sql_mode=""');
	        $apikey = 'vcViKX8_VIAZwTkDyrMikA';
	        $this->load->library('mandrill');

	        /*$emails = $this->db->get('report_email')->result();
	        $recepients = array();
	        foreach ($emails as $email) {
	            $recepients[] = array(
	                'email' => $email,
	                'name' => 'Recipient Name',
	                'type' => 'to'
	            );
	        }*/

	        $recepients[] = array(
	            'email' => $email,
	            'name' => 'Recipient Name',
	            'type' => 'to'
	        );

	        // print_r($recepients);exit();

	        // $dailyReport = $this->_getPdfDailyReportBase64();

	        // $reports = array(
	        //     'Daily Report' => $this->Daily_report_model->getBase64(),
	        //     'Daily Book' => $this->Daily_book_model->getBase64(date('Y-m-d'), date('Y-m-d'), ""),
	        // );

	        
            $template = 'email';
            $subject = 'PENDAFTARAN SUKSES';
            $from = 'Prima Estetika Raksa';

	        $data = array(
	            'subject' => $subject,
	            'from' => $from,
	            'link' => site_url() . '/api/pesan/verify/' . $user_id,
	            'header' => 'Terimakasih Telah Mendaftar!',
	            'message' => 'Halo, ' . $first_name . ' Anda Telah Mendaftar di optimelife.com!, email ini dikirimkan untuk tujuan bahwa email anda aktif. dan segala info mengenai permohonan akan dikirimkan ke email ini. <br><h3>Terimakasih!</h3>',
	        );

	        foreach ($recepients as $recepient) {
	            $this->_send_email($recepient['email'], $apikey, $template, $subject, $from, $data);
	        }

	        return false;
	    }
	    
	    private function _send_email($email, $apikey, $template, $subject, $from, $data = array()) {
	        //echo "SENDING ".$email."<br/>".PHP_EOL;
	        try {
	            $mandrill = new Mandrill($apikey);
	            $message = array(
	                'html' => $this->load->view($template, $data, TRUE),
	                'text' => '',
	                'subject' => $subject,
	                'from_email' => 'noreply@vmt.co.id',
	                'from_name' => $from,
	                'to' => array(
	                    array(
	                        'email' => $email,
	                        'name' => 'Recipient Name',
	                        'type' => 'to'
	                    ),
	                ),
	                'attachments' => array(
	                    // array(
	                    //     'type' => 'application/pdf',
	                    //     'name' => 'Daily Report.pdf',
	                    //     'content' => $dailyReport,
	                    // )
	                ),
	            );

	            /*foreach ($reports as $name => $report) {
	                $message['attachments'][] = array(
	                    'type' => 'application/pdf',
	                    'name' => $name.'.pdf',
	                    'content' => $report,
	                );
	            }*/

	            $async = false;
	            $ip_pool = 'Main Pool';
	            $send_at = Date('Y-m-d');
	            $result = $mandrill->messages->send($message);
	            // print_r($result);
	            //echo json_encode(['status' => 1]);
	            return true;
	        } catch(Mandrill_Error $e) {
	            throw $e;
	            print_r($e);
	        }
	    }
	}
