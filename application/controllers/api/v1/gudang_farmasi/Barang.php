<?php defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH . '/core/Rest.php');
use \Firebase\JWT\JWT;

class Barang extends Rest {
	protected $limit = 0;
	protected $table_def = "m_barang";
	protected $table_def_foto = "m_barang_foto";
	protected $table_def_barang_kelompok = "m_barang_kelompok";
	protected $table_def_stock = "t_gudang_farmasi_stock";
	protected $table_def_kelompok = "m_kelompokbarang";
	protected $table_def_pos_checkout = "t_pos_checkout";
	protected $table_def_pos_checkout_detail = "t_pos_pesanan";
	protected $ori_dir, $thumb_dir, $auth_key, $continue;

	public function __construct() {
		parent::__construct();
		$this->ori_dir = './uploads/barang/original';
        $this->thumb_dir = './uploads/barang/thumbnail';

        $this->load->helper('harga_barang');
        $this->load->helper('gudang_farmasi');
		$this->load->model('gudang_farmasi/Stock_model', 'stock');

		$this->continue = false;
		$this->auth_key = $this->input->get_request_header('Authorization');
		if($this->auth_key != $this->secretKey) {
			if($this->checkToken()) $this->continue = true;
		} else $this->continue = true;
	}

	public function list_get() {
		$limit = $this->input->get('limit') ? : $this->limit;
		$offset = $this->input->get('offset') ? : 0;
		$rangePriceStart = $this->input->get('range_price_start') ? : '';
		$rangePriceEnd = $this->input->get('range_price_end') ? : '';
		$categoryUid = $this->input->get('category_uid') ? : '';
		$jenisMember = $this->input->get('jenis_member') ? : '';
		$groupUid = $this->input->get('group_uid') ? : '';
		$bpom = $this->input->get('bpom') ? : '';
		$term = $this->input->get('term') ? : '';

		if($this->continue) {
			/*
	         * Order
	         */
			$sOrder = "";
	        $aOrders = array();
	        $aOrders[] = "{$this->table_def}.nama ASC";
	        if (count($aOrders) > 0) {
	            $sOrder = implode(', ', $aOrders);
	        }
	        if (!empty($sOrder)) {
	            $sOrder = "ORDER BY ".$sOrder;
	        }

	        /*
	         * Where
	         */
	        $sWhere = "";
	        $aWheres = array();
	        $aWheres[] = "{$this->table_def}.status = 1";
	        // $aWheres[] = "{$this->table_def_stock}.qty > 0";
	        if($bpom != "") 
		    	$aWheres[] = "{$this->table_def}.bpom = {$bpom}";
	        switch ($jenisMember) {
	        	case 'non_priority':
	            	$aWheres[] = "jenis.nama LIKE 'local%'";
	        		break;
	        }


	        if($categoryUid != "") 
	            $aWheres[] = "kategori.uid = '{$categoryUid}'";
	        if($rangePriceStart != "" && $rangePriceEnd != "") {
	            $aWheres[] = "{$this->table_def}.harga_penjualan >= '{$rangePriceStart}'";
	            $aWheres[] = "{$this->table_def}.harga_penjualan <= '{$rangePriceEnd}'";
	        }
	        if($groupUid != "") {
	            //$aWheres[] = "kelompok.uid = '{$groupUid}'";
	            $aWheres[] = "(SELECT count(kelompok_id) FROM {$this->table_def_barang_kelompok} WHERE barang_id = {$this->table_def}.id AND kelompok_id IN (SELECT id FROM {$this->table_def_kelompok} WHERE uid = '{$groupUid}')) > 0";
	        }
	        if (count($aWheres) > 0) {
	            $sWhere = implode(' AND ', $aWheres);
	        }
	        if (!empty($sWhere)) {
	            $sWhere = "WHERE ".$sWhere;
	        }

	        $aLikes = array();
	        if($term != "") {
	            $aLikes[] = "{$this->table_def}.kode = '{$term}'";
	            $aLikes[] = "{$this->table_def}.nama LIKE '%{$term}%'";
	        }

	        if (count($aLikes) > 0) {
	            $sLike = "(".implode(' OR ', $aLikes).")";
	            $sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
	        }

	        $aSelect = array(
	        	"{$this->table_def}.id",
	        	"{$this->table_def}.uid",
	        	"{$this->table_def}.kode",
	        	"{$this->table_def}.nama",
	        	"{$this->table_def}.merk",
	        	"{$this->table_def}.diskon",
	        	"{$this->table_def}.deskripsi",
	        	"{$this->table_def}.deskripsi_information",
	        	"{$this->table_def}.deskripsi_benefits",
	        	"{$this->table_def}.deskripsi_ingredients",
	        	"{$this->table_def}.deskripsi_title",
	        	"{$this->table_def}.harga_penjualan",
	        	/*"({$this->table_def}.harga_pembelian / {$this->table_def}.isi_satuan_penggunaan) harga",*/
	        	"kategori.nama kategori",
	        	"{$this->table_def_stock}.qty stock"
	        );
			$list = $this->stock->get_all($limit, $offset * $limit, $sWhere, $sOrder, $aSelect);

			foreach ($list['data'] as $i => $obj) {
				//$obj->harga = getHargaDasar($obj->id, 'farmasi');
				$obj->harga = (int) $obj->harga_penjualan != 0 ? $obj->harga_penjualan : getHargaDasar($obj->id, 'farmasi');
				$obj->stock = $this->stock->getTotalCheckoutActive($obj);

				$fotos = $this->db->select('id, nama')
	                				->where('barang_id', $obj->id)
	                				->get($this->table_def_foto)->result();
	            $aFotos = array();
	            if(count($fotos) > 0) {
	            	foreach ($fotos as $row) {
	            		$fileori = base_url().'assets/img/no_image_available.png';
	            		$fileThumb = base_url().'assets/img/no_image_available.png';

	            		if (file_exists($this->ori_dir.'/'.$row->nama)) {
							$fileori 	= base_url($this->ori_dir).'/'.$row->nama;
	            			$fileThumb 	= base_url($this->thumb_dir).'/'.$row->nama;
	            		}

	            		$aFotos[] = [
	            			'original' => $fileori,
	            			'thumbnail' => $fileThumb,
	            		];
	            	}
	            }
	            $obj->foto = $aFotos;
	            unset($obj->id);
			}
			$output['total_data'] = $list['total_rows'];
			$output['total_pages'] = $limit == 0 ? 1 : (ceil($list['total_rows'] / $limit)) - 1;
			$output['sisa_data'] = $limit == 0 ? 0 : ($list['total_rows'] - (($offset + 1) * $limit) < 0 ? 0 : $list['total_rows'] - (($offset + 1) * $limit));
			$output['kelompok'] = $groupUid != "" ? $this->db->where('uid', $groupUid)->get($this->table_def_kelompok)->row() : null;
			$output['list'] = $list['data'];

			$this->output->set_status_header(200)
				->set_content_type('application/json')
	            ->set_output(json_encode($output));
	    } else {
	    	$this->output->set_status_header(400)
	    		->set_content_type('application/json')
              	->set_output(json_encode([
	            				'status' => 0,
              					'message' => 'Auth key tidak diperbolehkan.'
              				]));
	    }
	}

	public function detail_get() {
		$uid = $this->input->get('uid') ? : 0;

		if($this->continue) {
			if(!$uid) {
				$this->output->set_status_header(400)
						->set_content_type('application/json')
		            	->set_output(json_encode(["status" => 0, "message" => "Parameter pencarian tidak ditemukan."]));
		        return;
			}

	        $aSelect = array(
	        	"{$this->table_def}.id",
	        	"{$this->table_def}.uid",
	        	"{$this->table_def}.kode",
	        	"{$this->table_def}.nama",
	        	"{$this->table_def}.merk",
	        	"{$this->table_def}.diskon",
	        	"{$this->table_def}.deskripsi",
	        	"{$this->table_def}.deskripsi_information",
	        	"{$this->table_def}.deskripsi_benefits",
	        	"{$this->table_def}.deskripsi_ingredients",
	        	"{$this->table_def}.deskripsi_title",
	        	"{$this->table_def}.harga_penjualan",
	        	/*"({$this->table_def}.harga_pembelian / {$this->table_def}.isi_satuan_penggunaan) harga",*/
	        	"kategori.nama kategori",
	        	"{$this->table_def_stock}.qty stock"
	        );
			$data = $this->stock->get_by("WHERE {$this->table_def}.uid = '{$uid}'", $aSelect);
			if(!$data) {
				$this->output->set_status_header(400)
						->set_content_type('application/json')
		            	->set_output(json_encode(["status" => 0, "message" => "Product tidak ditemukan."]));
		        return;
			}
			$data->harga = (int) $data->harga_penjualan != 0 ? $data->harga_penjualan : getHargaDasar($data->id, 'farmasi');
			$data->stock = $this->stock->getTotalCheckoutActive($data);

			$fotos = $this->db->select('id, nama')
	                				->where('barang_id', $data->id)
	                				->get($this->table_def_foto)->result();
            $aFotos = array();
            if(count($fotos) > 0) {
            	foreach ($fotos as $row) {
            		$fileori = base_url().'assets/img/no_image_available.png';
            		$fileThumb = base_url().'assets/img/no_image_available.png';

            		if (file_exists($this->ori_dir.'/'.$row->nama)) {
						$fileori 	= base_url($this->ori_dir).'/'.$row->nama;
            			$fileThumb 	= base_url($this->thumb_dir).'/'.$row->nama;
            		}

            		$aFotos[] = [
            			'original' => $fileori,
            			'thumbnail' => $fileThumb,
            		];
            	}
            }
            $data->foto = $aFotos;
            unset($data->id);
			
			$this->output->set_status_header(200)
				->set_content_type('application/json')
	            ->set_output(json_encode(["data" => $data]));
	    } else {
	    	$this->output->set_status_header(400)
	    		->set_content_type('application/json')
              	->set_output(json_encode([
	            				'status' => 0,
              					'message' => 'Auth key tidak diperbolehkan.'
              				]));
	    }
	}

	public function process_stock_out() {
		$barang_uid = $this->input->post('product_uid') ? : '';
		$qty = $this->input->post('qty') ? : 0;
		$no_order = $this->input->post('no_order') ? : 0;

		if($this->continue) {
			if(!$barang_uid) {
				$this->output->set_status_header(400)
						->set_content_type('application/json')
		            	->set_output(json_encode(["status" => 0, "message" => "Product ID dibutuhkan."]));
		        return;
			}

			$oBarang = $this->db->select('id')
								->where('uid', $barang_uid)
								->get($this->table_def)->row();
			if($oBarang) {
				if($qty > 0) {
					$obj = new stdClass();
		            $obj->id = null;
		            $obj->tanggal = date('Y-m-d H:i:s');
		            $obj->kode = $no_order;
		            $obj->keterangan = 'Pengeluaran dari eCommerce NO.ORDER '.$no_order;

		            $detail = new stdClass();
		            $detail->id = null;
		            $detail->barang_id = $oBarang->id;
		            $detail->qty = $qty;

		            $detailBarang =  procStockKeluar($obj, $detail, $this->config->item('tipe_kartu_stock_pengeluaran'));

		            $this->output->set_status_header(200)
						->set_content_type('application/json')
			            ->set_output(json_encode(["data" => $detailBarang]));
				}
			} else {
				$this->output->set_status_header(400)
						->set_content_type('application/json')
		            	->set_output(json_encode(["status" => 0, "message" => "Product ID tidak ditemukan."]));
			}
		} else {
			$this->output->set_status_header(400)
	    		->set_content_type('application/json')
              	->set_output(json_encode([
	            				'status' => 0,
              					'message' => 'Auth key tidak diperbolehkan.'
              				]));
		}
	}

	public function process_stock_in() {
		$barang_uid = $this->input->post('product_uid') ? : '';
		$qty = $this->input->post('qty') ? : 0;
		$no_order = $this->input->post('no_order') ? : 0;
		$detail_barang = $this->input->post('detail_barang') ? json_decode($this->input->post('detail_barang')) : [];

		if($this->continue) {
			if(!$barang_uid) {
				$this->output->set_status_header(400)
						->set_content_type('application/json')
		            	->set_output(json_encode(["status" => 0, "message" => "Product ID tidak dibutuhkan."]));
		        return;
			}

			$oBarang = $this->db->select('id')
								->where('uid', $barang_uid)
								->get($this->table_def)->row();
			if($oBarang) {
				if($qty > 0) {
					$obj = new stdClass();
		            $obj->id = null;
		            $obj->tanggal = date('Y-m-d H:i:s');
		            $obj->kode = $no_order;
		            $obj->keterangan = 'Pengembalian dari eCommerce NO.ORDER '.$no_order;

		            $detail = new stdClass();
		            $detail->id = null;
		            $detail->barang_id = $oBarang->id;
		            $detail->qty = $qty;
		            $detail->data_barang = $detail_barang;

					procStockMasukByTransaksi($obj, $detail, $this->config->item('tipe_kartu_stock_penerimaan'));

					$this->output->set_status_header(200)
						->set_content_type('application/json')
			            ->set_output(json_encode(["message" => "Proses Berhasil."]));
				}
			} else {
				$this->output->set_status_header(400)
						->set_content_type('application/json')
		            	->set_output(json_encode(["status" => 0, "message" => "Product ID tidak ditemukan."]));
			}
		} else {
			$this->output->set_status_header(400)
	    		->set_content_type('application/json')
              	->set_output(json_encode([
	            				'status' => 0,
              					'message' => 'Auth key tidak diperbolehkan.'
              				]));
		}
	}
}