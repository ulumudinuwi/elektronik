<?php defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH . '/core/Rest.php');
use \Firebase\JWT\JWT;

class Promo extends Rest {
	protected $limit = 10;
	protected $table_def = "m_promo";
	protected $table_def_barang = "m_barang";
	protected $table_def_foto = "m_barang_foto";
	protected $ori_dir, $thumb_dir, $auth_key, $continue;

	public function __construct() {
		parent::__construct();
		$this->ori_dir = './uploads/barang/original';
        $this->thumb_dir = './uploads/barang/thumbnail';
		$this->load->model('master/Promo_model', 'main');

		$this->continue = false;
		$this->auth_key = $this->input->get_request_header('Authorization');
		if($this->auth_key != $this->secretKey) {
			if($this->checkToken()) $this->continue = true;
		} else $this->continue = true;
	}

	public function list_get() {
		$limit = $this->input->get('limit') ? $this->input->get('limit') : $this->limit;
		$offset = $this->input->get('offset') ? $this->input->get('offset') : 0;
		$term = $this->input->get('term') ? $this->input->get('term') : '';

		if($this->continue) {
			/*
	         * Order
	         */
			$sOrder = "";
	        $aOrders = array();
	        $aOrders[] = "{$this->table_def}.created_at DESC";
	        if (count($aOrders) > 0) {
	            $sOrder = implode(', ', $aOrders);
	        }
	        if (!empty($sOrder)) {
	            $sOrder = "ORDER BY ".$sOrder;
	        }

	        /*
	         * Where
	         */
	        $sWhere = "";
	        $aWheres = array();
	        $aWheres[] = "{$this->table_def}.status = 1";
    		$aWheres[] = "DATE({$this->table_def}.expired_at) >= DATE(NOW())";
	        if (count($aWheres) > 0) {
	            $sWhere = implode(' AND ', $aWheres);
	        }
	        if (!empty($sWhere)) {
	            $sWhere = "WHERE ".$sWhere;
	        }

	        $aLikes = array();
	        if($term != "") {
	            $aLikes[] = "{$this->table_def_barang}.kode = '{$term}'";
	            $aLikes[] = "{$this->table_def_barang}.nama LIKE '%{$term}%'";
	        }

	        if (count($aLikes) > 0) {
	            $sLike = "(".implode(' OR ', $aLikes).")";
	            $sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
	        }

			$list = $this->main->get_all($limit, $offset * $limit, $sWhere, $sOrder);
			foreach ($list['data'] as $i => $obj) {
				$fotos = $this->db->select('id, nama')
	                				->where('barang_id', $obj->barang_id)
	                				->get($this->table_def_foto)->result();
	            $aFotos = array();
	            if(count($fotos) > 0) {
	            	foreach ($fotos as $row) {
	            		$aFotos[] = [
	            			'original' => base_url($this->ori_dir).'/'.$row->nama,
	            			'thumbnail' => base_url($this->thumb_dir).'/'.$row->nama,
	            		];
	            	}
	            }
	            $obj->foto = $aFotos;
	            $obj->expired_at = date_to_view($obj->expired_at);
	            unset($obj->id);
			}

			$output['total_data'] = $list['total_rows'];
			$output['total_pages'] = (ceil($list['total_rows'] / $limit)) - 1;
			$output['sisa_data'] = $list['total_rows'] - (($offset + 1) * $limit) < 0 ? 0 : $list['total_rows'] - (($offset + 1) * $this->limit);
			$output['list'] = $list['data'];

			$this->output->set_status_header(200)
				->set_content_type('application/json')
	            ->set_output(json_encode($output));
	    } else {
	    	$this->output->set_status_header(400)
	    		->set_content_type('application/json')
              	->set_output(json_encode([
	            				'status' => 0,
              					'message' => 'Auth key tidak diperbolehkan.'
              				]));
	    }
	}

	public function detail_get() {
		$uid = $this->input->get('uid') ? : 0;

		if($this->continue) {
			if(!$uid) {
				$this->output->set_status_header(400)
						->set_content_type('application/json')
		            	->set_output(json_encode(["status" => 0, "message" => "Parameter pencarian tidak ditemukan."]));
		        return;
			}

			$data = $this->main->get_by("WHERE {$this->table_def}.uid = '{$uid}'");
			if(!$data) {
				$this->output->set_status_header(400)
						->set_content_type('application/json')
		            	->set_output(json_encode(["status" => 0, "message" => "Product tidak ditemukan."]));
		        return;
			}

			$fotos = $this->db->select('id, nama')
                				->where('barang_id', $data->barang_id)
                				->get($this->table_def_foto)->result();

            $aFotos = array();
            if(count($fotos) > 0) {
            	foreach ($fotos as $row) {
            		$aFotos[] = [
            			'original' => base_url($this->ori_dir).'/'.$row->nama,
            			'thumbnail' => base_url($this->thumb_dir).'/'.$row->nama,
            		];
            	}
            }
            $data->foto = $aFotos;
	        unset($data->id);
			
			$this->output->set_status_header(200)
				->set_content_type('application/json')
	            ->set_output(json_encode(["data" => $data]));
	    } else {
	    	$this->output->set_status_header(400)
	    		->set_content_type('application/json')
              	->set_output(json_encode([
	            				'status' => 0,
              					'message' => 'Auth key tidak diperbolehkan.'
              				]));
	    }
	}
}