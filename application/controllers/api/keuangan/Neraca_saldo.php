<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Neraca_saldo extends CI_Controller  {
	
	protected $table_def = "m_perkiraan";

	function __construct() {
		parent::__construct();
        $this->load->model('master/Perkiraan_model');
	}

	public function load_data() {
		//array
		$aHarta = array();

		$parentList = $this->getParent(0);
		foreach ($parentList as $row) {
			switch ((int) $row->klasifikasi) {
				case JENIS_PERKIRAAN_HARTA:
					$aHarta[] = $row;
					break;
				case JENIS_PERKIRAAN_KEWAJIBAN:
					$aKewajiban[] = $row;
					break;
				case JENIS_PERKIRAAN_MODAL:
					$aModal[] = $row;
					break;
				case JENIS_PERKIRAAN_PENDAPATAN:
					$aPendapatan[] = $row;
					break;
				case JENIS_PERKIRAAN_BEBAN_ATAS_PENDAPATAN:
					$aBebanataspendapatan[] = $row;
					break;
				case JENIS_PERKIRAAN_BEBAN_OPERASIONAL:
					$aBebanoperasional[] = $row;
					break;
				case JENIS_PERKIRAAN_BEBAN_NON_OPERASIONAL:
					$aBebannonoperasional[] = $row;
					break;
				case JENIS_PERKIRAAN_PENDAPATAN_LAIN_LAIN:
					$aPendapatanlainlain[] = $row;
					break;
				case JENIS_PERKIRAAN_BEBAN_LAIN_LAIN:
					$aBebanlainlain[] = $row;
					break;
			}
		}

		$rData['data'] = new stdClass();
		$rData['data']->harta 						= $aHarta;
		$rData['data']->kewajiban 					= $aKewajiban;
		$rData['data']->modal 						= $aModal;
		$rData['data']->pendapatan 					= $aPendapatan;
		$rData['data']->beban_atas_pendapatan 		= $aBebanataspendapatan;
		$rData['data']->bebanoperasional 			= $aBebanoperasional;
		$rData['data']->bebannonoperasional 		= $aBebannonoperasional;
		$rData['data']->pendapatanlainlain 			= $aPendapatanlainlain;
		$rData['data']->bebanlainlain 				= $aBebanlainlain;
		
		echo json_encode($rData);
	}

	private function getParent($parent_id = null){
		$tanggalDari = $this->input->get('tanggal_dari');
		$tanggalSampai = $this->input->get('tanggal_sampai');

		$parentList  = null;
		if (!is_null($parent_id)) {
			$parentList = $this->Perkiraan_model->get_all(0, 0, "WHERE (m_perkiraan.parent_id = {$parent_id}) AND m_perkiraan.deleted_flag != 1", "ORDER BY m_perkiraan.kode ASC")['data'];
			if(count($parentList) > 0) {
				foreach ($parentList as $row) {
					$getSaldoawalDebit = $this->db->select('SUM(total) as total')
							 ->where('perkiraan_id', $row->id)
							 ->where('tipe', '1')
							 ->where('DATE(created_at) <', $tanggalDari)
							 ->get('t_buku_besar')->row();

					$getSaldoawalKredit = $this->db->select('SUM(total) as total')
							 ->where('perkiraan_id', $row->id)
							 ->where('tipe', '2')
							 ->where('DATE(created_at) <', $tanggalDari)
							 ->get('t_buku_besar')->row();

					$saldoAwal = (float)$getSaldoawalDebit->total - (float) $getSaldoawalKredit->total;

					$row->saldo_awal_debit = $saldoAwal >= 0 ? $saldoAwal : 0;
					$row->saldo_awal_kredit = $saldoAwal < 0 ? $saldoAwal * (-1): 0;

					$getSaldopergerakanDebit = $this->db->select('SUM(total) as total')
							 ->where('perkiraan_id', $row->id)
							 ->where('tipe', '1')
							 ->where('DATE(created_at) >=', $tanggalDari)
							 ->where('DATE(created_at) <=', $tanggalSampai)
							 ->get('t_buku_besar')->row();
					
					$getSaldopergerakanKredit = $this->db->select('SUM(total) as total')
							 ->where('perkiraan_id', $row->id)
							 ->where('tipe', '2')
							 ->where('DATE(created_at) >=', $tanggalDari)
							 ->where('DATE(created_at) <=', $tanggalSampai)
							 ->get('t_buku_besar')->row();

					$saldoPergerakan = (float)$getSaldopergerakanDebit->total - (float)$getSaldopergerakanKredit->total;

					$row->saldo_pergerakan_debit = $saldoPergerakan >= 0 ? $saldoPergerakan : 0;
					$row->saldo_pergerakan_kredit = $saldoPergerakan < 0 ? $saldoPergerakan * (-1) : 0;

					$saldoAkhir = (float) $saldoAwal + (float) $saldoPergerakan;
					$row->saldo_akhir_debit = $saldoAkhir >= 0 ? $saldoAkhir : 0;
					$row->saldo_akhir_kredit = $saldoAkhir < 0 ? $saldoAkhir * (-1): 0;

					$row->sub_parent = $this->getParent($row->id ? : null);
				}
			}
		}
		return $parentList;
	}
}