<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Mutasi extends CI_Controller 
{
  protected $table_def = "t_request_mutasi_unit";
  protected $table_def_detail = "t_request_mutasi_unit_detail";
  protected $table_def_barang = "m_barang";
  
  function __construct() {
    parent::__construct();
    $this->lang->load('barang');

    $this->load->model('request/unit_kerja/Mutasi_model', 'main');
    $this->load->model('request/unit_kerja/Mutasi_detail_model', 'main_detail');

    $this->user = $this->user_model->get_by_id($this->auth->userid());
  }

  public function load_data(){
      $tanggal_dari  = $_POST['tanggal_dari'];
      $tanggal_sampai  = $_POST['tanggal_sampai'];
      $unit_id  = $_POST['unit_id'];
      $status  = $_POST['status'];

      $aColumns = array('kode', 'tanggal', 'status', '');
      /* 
       * Paging
       */
      if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
          $iLimit = intval( $_POST['length'] );
          $iOffset = intval( $_POST['start'] );
      }

      /*
       * Ordering
       */
      $sOrder = "";
      $aOrders = array();
      for ($i = 0; $i < count($aColumns); $i++) {
        if($_POST['columns'][$i]['orderable'] == "true") {
          if($i == $_POST['order'][0]['column']) {
            switch ($aColumns[$i]) {
              default:
                $aOrders[] = $this->table_def.'.'.$aColumns[$i].' '.($_POST['order'][0]['dir'] == 'asc' ? 'asc' : 'desc');
                break;
            }
          }
        }
      }
      if (count($aOrders) > 0) {
          $sOrder = implode(', ', $aOrders);
      }
      if (!empty($sOrder)) {
          $sOrder = "ORDER BY ".$sOrder;
      }

      /*
       * Where
       */
      $sWhere = "";
      $aWheres = array();
      if($tanggal_dari != "") $aWheres[] = "DATE({$this->table_def}.tanggal) >= '{$tanggal_dari}'";
      if($tanggal_sampai != "") $aWheres[] = "DATE({$this->table_def}.tanggal) <= '{$tanggal_sampai}'";
      if($unit_id != "") $aWheres[] = "{$this->table_def}.unitkerja_id = ".base64_decode($unit_id);
      if($status != "") $aWheres[] = "{$this->table_def}.status = {$status}";
      if (count($aWheres) > 0) {
          $sWhere = implode(' AND ', $aWheres);
      }
      if (!empty($sWhere)) {
          $sWhere = "WHERE ".$sWhere;
      }

      $aLikes = array();
      if($_POST['search']['value'] != "") {
          for ($i = 0; $i < count($aColumns); $i++) {
              if($_POST['columns'][$i]['searchable'] == "true") {
                  switch ($aColumns[$i]) {
                    default:
                      $aLikes[] = "{$this->table_def}.{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
                      break;
                  }
              }
          }
      }

      if (count($aLikes) > 0) {
          $sLike = "(".implode(' OR ', $aLikes).")";
          $sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
      }

      $aSelect = array(
        "{$this->table_def}.id",
        "{$this->table_def}.uid",
        "{$this->table_def}.kode",
        "{$this->table_def}.tanggal",
        "{$this->table_def}.pemohon",
        "{$this->table_def}.status",
        "{$this->table_def}.keterangan",
        "uk.nama unit_kerja",
      );
      $list = $this->main->get_all($iLimit, $iOffset, $sWhere, $sOrder, $aSelect);

      $rResult = $list['data'];
      $iFilteredTotal = $list['total_rows'];
      $iTotal = $list['total_rows'];

      /*
       * Output
       */
      $output = array(
          "draw" => intval($_POST['draw']),
          "recordsTotal" => $iTotal,
          "recordsFiltered" => $iFilteredTotal,
          "data" => array(),
      );

      $rows = array();
      $i = $iOffset;
      foreach ($rResult as $obj) {
          $obj->status_desc = $this->config->item('status_mutasi')[$obj->status];

          $data = get_object_vars($obj);
          $data['no'] = ($i+1);
          $rows[] = $data;
          $i++;
      }
      $output['data'] = $rows;

      echo json_encode($output);
  }

  public function save() {
    
    if (!$this->input->is_ajax_request())
      exit();

    $obj = $this->_getDataObject();
    if (isset($obj->uid) && $obj->uid != "") {
      $result = $this->main->update($obj);
    } else {
      $result = $this->main->create($obj);
    }

    if(!$result) 
      $this->output->set_status_header(500);

    $this->output->set_status_header(200);
    echo json_encode($result);
  }

  public function get_data($uid = 0) {
    if (!$this->input->is_ajax_request())
      exit();

    if ($uid) {
      $obj = $this->main->get_by("WHERE {$this->table_def}.uid = \"{$uid}\"");
      $obj->keterangan = $obj->keterangan ? $obj->keterangan : "-";
      $obj->status_desc = $this->config->item('status_mutasi')[$obj->status];
      $obj->details = $this->main_detail->get_all(0, 0, "WHERE {$this->table_def_detail}.mutasi_id = {$obj->id}")['data'];

      $label_pemohon = $obj->pemohon;
      $label_pemohon .= "<br/><span class='text-size-mini text-info'>Unit: {$obj->unit_kerja}</span>";
    } else {
      $label_pemohon = $this->user->first_name . ' ' . $this->user->last_name;
      //$label_pemohon .= "<br/><span class='text-size-mini text-info'>Unit: {$this->user->unit_kerja}</span>";

      $obj = new stdClass();
      $obj->label_pemohon = $label_pemohon;
      $obj->unitkerja_id = $this->user->unitkerja_id;
    }
    $obj->label_pemohon = $label_pemohon;
    echo json_encode(['data' => $obj]);
  }

  /**
   * Form Data Object
   */
  private function _getDataObject() {
    $his = date('H:i:s');
    $obj = new stdClass();
    $obj->id = $this->input->post('id');
    $obj->uid = $this->input->post('uid') && ($this->input->post('uid') != "") ? $this->input->post('uid') : "";
    $obj->tanggal = get_date_accepted_db($this->input->post('tanggal')).' '.$his;
    $obj->unitkerja_id = $this->input->post('unitkerja_id');
    $obj->pemohon = $this->input->post('pemohon');
    $obj->keterangan = $this->input->post('keterangan');
    
    $details = $this->main_detail->get_all(0, 0, "WHERE {$this->table_def_detail}.mutasi_id = {$obj->id}");
    $aDetails = array();
    array_map(function($each) use (&$aDetails) {
        unset($each->kode_barang);
        unset($each->barang);
        unset($each->satuan);
        $each->data_mode = Data_mode_model::DATA_MODE_DELETE;
        $aDetails[$each->id] = $each;

        return $each;
    }, $details['data']);

    if (isset($_POST['detail_id'])) {
      for ($i = 0; $i < count($_POST['detail_id']); $i++) {
        $detail_id = $_POST['detail_id'][$i];
        if (!array_key_exists($detail_id, $aDetails)) {
          $detail = new StdClass();
          $detail->id = $detail_id;
          $detail->barang_id = $_POST['barang_id'][$i];
          $detail->satuan_id = $_POST['satuan_id'][$i];
          $detail->isi_satuan = $_POST['isi_satuan'][$i];
          $detail->qty = $_POST['qty'][$i];
          $detail->data_mode    = Data_mode_model::DATA_MODE_ADD;
          $aDetails[uniqid()] = $detail;
        } else {
          $aDetails[$detail_id]->barang_id = $_POST['barang_id'][$i];
          $aDetails[$detail_id]->satuan_id = $_POST['satuan_id'][$i];
          $aDetails[$detail_id]->isi_satuan = $_POST['isi_satuan'][$i];
          $aDetails[$detail_id]->qty = $_POST['qty'][$i];
          $aDetails[$detail_id]->data_mode  = Data_mode_model::DATA_MODE_EDIT;
        }    
      }
    }
    $obj->details = $aDetails;
    return $obj;
  }

  public function cetak($uid = "")
  { 
    $obj = $this->main->get_by("WHERE ({$this->table_def}.uid = \"{$uid}\")");
    $obj->status_desc = $this->config->item('status_mutasi')[$obj->status];
    $obj->indo_tanggal = konversi_to_id(date("d M Y", strtotime($obj->tanggal)));
    $obj->details = $this->main_detail->get_all(0, 0, "WHERE ({$this->table_def_detail}.mutasi_id = ".$obj->id.")", "ORDER BY {$this->table_def_detail}.id ASC")['data'];

    # Get User Pencetak
    $current_user = "";
    $user = $this->user_model->get_by_id($this->auth->userid());
    if($user) {
      $current_user = $user->first_name." ".($user->last_name ? $user->last_name : "");
    }

    $current_date = konversi_to_id(date("d M Y")).' '.date('H:i');
    $data = array(
      'obj' => $obj,
      'current_date' => $current_date,
      'current_user' => $current_user,
    );

    $html = $this->load->view('request/unit-kerja/mutasi/cetak', $data, TRUE);
    
    # Margins
    $ml = 5;
    $mr = 5;
    $mt = 5;
    $mb = 5;

    # Create PDF
    $mpdf = new mPDF('c', 'A4-P', 0, null, $ml, $mr, $mt, $mb);
    $mpdf->WriteHTML($html);
    $mpdf->Output('Mutasi Unit '.$obj->kode.'.pdf', "I");
  }
}