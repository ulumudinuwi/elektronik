<?php defined('BASEPATH') OR exit('No direct script access allowed');
use Carbon\Carbon;

class Pengiriman_pembelian extends CI_Controller 
{
	protected $table_def = "t_pos_checkout";
	protected $table_def_pesanan = "t_pos_pesanan";
	protected $table_def_member = "m_dokter_member";
	protected $table_def_barang = "m_barang";
	protected $table_def_pos = "t_pos";
	protected $table_def_pos_detail = "t_pos_detail";
	
	function __construct() {
		parent::__construct();
		$this->lang->load('barang');

		$this->load->model('Pengiriman_pembelian_model', 'main');
		$this->load->model('Checkout_model', 'main');
		$this->ori_dir = './uploads/barang/original';
        $this->thumb_dir = './uploads/barang/thumbnail';
		$this->thumb_upload = './uploads/bukti_pembayaran';

        $this->load->helper('gudang_farmasi');
        $this->load->helper('notifikasi_ecommerce');
	}

	/**
	 * Load data
	 */
	public function load_data(){
		$tanggal_dari  = $_POST['tanggal_dari'];
		$tanggal_sampai  = $_POST['tanggal_sampai'];
		
		$aColumns = array('keterangan');
		/* 
		 * Paging
		 */
		if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
			$iLimit = intval( $_POST['length'] );
			$iOffset = intval( $_POST['start'] );
		}

		/*
		 * Ordering
		 */
		$sOrder = "";
		$aOrders = array();
		$aOrders[] = "{$this->table_def}.status desc";
		$aOrders[] = "{$this->table_def}.id asc";
		for ($i = 0; $i < count($aColumns); $i++) {
			if($_POST['columns'][$i]['orderable'] == "true") {
				if($i == $_POST['order'][0]['column']) {
					switch ($aColumns[$i]) {
						default:
							$aOrders[] = $this->table_def.'.'.$aColumns[$i].' '.($_POST['order'][0]['dir'] == 'asc' ? 'asc' : 'desc');
							break;
					}
				}
			}
		}
		if (count($aOrders) > 0) {
			$sOrder = implode(', ', $aOrders);
		}
		if (!empty($sOrder)) {
			$sOrder = "ORDER BY ".$sOrder;
		}

		/*
		 * Where
		 */
		$sWhere = "";
		$aWheres = array();
		$aWheres[] = "{$this->table_def}.no_resi IS NULL";
		$aWheres[] = "{$this->table_def}.status_pembayaran = 2";
		if($tanggal_dari != "") $aWheres[] = "DATE({$this->table_def}.tanggal_transaksi) >= '{$tanggal_dari}'";
		if($tanggal_sampai != "") $aWheres[] = "DATE({$this->table_def}.tanggal_transaksi) <= '{$tanggal_sampai}'";
		if (count($aWheres) > 0) {
			$sWhere = implode(' AND ', $aWheres);
		}
		if (!empty($sWhere)) {
			$sWhere = "WHERE ".$sWhere;
		}

		$aLikes = array();
		if($_POST['search']['value'] != "") {
			for ($i = 0; $i < count($aColumns); $i++) {
				if($_POST['columns'][$i]['searchable'] == "true") {
					switch ($aColumns[$i]) {
						default:
							$aLikes[] = "{$this->table_def}.{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
							break;
					}
				}
			}
		}

		if (count($aLikes) > 0) {
			$sLike = "(".implode(' OR ', $aLikes).")";
			$sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
		}

		$aSelect = array(
			"{$this->table_def}.id",
			"{$this->table_def}.uid",
			"{$this->table_def}.tanggal_transaksi",
			"{$this->table_def}.no_invoice",
			"{$this->table_def}.total_harga",
			"{$this->table_def}.bukti_pembayaran",
			"{$this->table_def}.status_pembayaran",
			"{$this->table_def}.status",
			"{$this->table_def}.alasan_penolakan",
			"dokter_member.nama",
			"dokter_member.no_member",
		);

        //$sWhere = !empty($sWhere) ? $sWhere." GROUP BY t_buku_besar.transaksi_id, t_buku_besar.total " : "GROUP BY t_buku_besar.transaksi_id, t_buku_besar.total";

		$list = $this->main->get_all($iLimit, $iOffset, $sWhere, $sOrder, $aSelect);

		$rResult = $list['data'];
		$iFilteredTotal = $list['total_rows'];
		$iTotal = $list['total_rows'];

		/*
		 * Output
		 */
		$output = array(
			"draw" => intval($_POST['draw']),
			"recordsTotal" => $iTotal,
			"recordsFiltered" => $iFilteredTotal,
			"data" => array(),
		);

		$rows = array();
		$i = $iOffset;
		foreach ($rResult as $obj) {
			$data = get_object_vars($obj);
			$data['no'] = ($i+1);
			$rows[] = $data;
			$i++;
		}
		$output['data'] = $rows;

		echo json_encode($output);
	}

	public function load_data_history(){
		$tanggal_dari  = $_POST['tanggal_dari'];
		$tanggal_sampai  = $_POST['tanggal_sampai'];
		
		$aColumns = array('keterangan');
		/* 
		 * Paging
		 */
		if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
			$iLimit = intval( $_POST['length'] );
			$iOffset = intval( $_POST['start'] );
		}

		/*
		 * Ordering
		 */
		$sOrder = "";
		$aOrders = array();
		$aOrders[] = "{$this->table_def}.status desc";
		$aOrders[] = "{$this->table_def}.id asc";
		for ($i = 0; $i < count($aColumns); $i++) {
			if($_POST['columns'][$i]['orderable'] == "true") {
				if($i == $_POST['order'][0]['column']) {
					switch ($aColumns[$i]) {
						default:
							$aOrders[] = $this->table_def.'.'.$aColumns[$i].' '.($_POST['order'][0]['dir'] == 'asc' ? 'asc' : 'desc');
							break;
					}
				}
			}
		}
		if (count($aOrders) > 0) {
			$sOrder = implode(', ', $aOrders);
		}
		if (!empty($sOrder)) {
			$sOrder = "ORDER BY ".$sOrder;
		}

		/*
		 * Where
		 */
		$sWhere = "";
		$aWheres = array();
		$aWheres[] = "{$this->table_def}.no_resi IS NOT NULL";
		$aWheres[] = "{$this->table_def}.status_pembayaran = 3";
		if($tanggal_dari != "") $aWheres[] = "DATE({$this->table_def}.tanggal_transaksi) >= '{$tanggal_dari}'";
		if($tanggal_sampai != "") $aWheres[] = "DATE({$this->table_def}.tanggal_transaksi) <= '{$tanggal_sampai}'";
		if (count($aWheres) > 0) {
			$sWhere = implode(' AND ', $aWheres);
		}
		if (!empty($sWhere)) {
			$sWhere = "WHERE ".$sWhere;
		}

		$aLikes = array();
		if($_POST['search']['value'] != "") {
			for ($i = 0; $i < count($aColumns); $i++) {
				if($_POST['columns'][$i]['searchable'] == "true") {
					switch ($aColumns[$i]) {
						default:
							$aLikes[] = "{$this->table_def}.{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
							break;
					}
				}
			}
		}

		if (count($aLikes) > 0) {
			$sLike = "(".implode(' OR ', $aLikes).")";
			$sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
		}

		$aSelect = array(
			"{$this->table_def}.id",
			"{$this->table_def}.uid",
			"{$this->table_def}.tanggal_transaksi",
			"{$this->table_def}.no_invoice",
			"{$this->table_def}.total_harga",
			"{$this->table_def}.bukti_pembayaran",
			"{$this->table_def}.status_pembayaran",
			"{$this->table_def}.status",
			"{$this->table_def}.alasan_penolakan",
			"dokter_member.nama",
			"dokter_member.no_member",
		);

        //$sWhere = !empty($sWhere) ? $sWhere." GROUP BY t_buku_besar.transaksi_id, t_buku_besar.total " : "GROUP BY t_buku_besar.transaksi_id, t_buku_besar.total";

		$list = $this->main->get_all($iLimit, $iOffset, $sWhere, $sOrder, $aSelect);

		$rResult = $list['data'];
		$iFilteredTotal = $list['total_rows'];
		$iTotal = $list['total_rows'];

		/*
		 * Output
		 */
		$output = array(
			"draw" => intval($_POST['draw']),
			"recordsTotal" => $iTotal,
			"recordsFiltered" => $iFilteredTotal,
			"data" => array(),
		);

		$rows = array();
		$i = $iOffset;
		foreach ($rResult as $obj) {
			$data = get_object_vars($obj);
			$data['no'] = ($i+1);
			$rows[] = $data;
			$i++;
		}
		$output['data'] = $rows;

		echo json_encode($output);
	}

	public function save() {
		
		if (!$this->input->is_ajax_request())
			exit();

		$obj = $this->_getDataObject();
		if($obj->uid == ''){
			$result = $this->main->create($obj);
		}else{
			$result = $this->main->update($obj);
		}

		if(!$result) 
			$this->output->set_status_header(500);

		$this->output->set_status_header(200);
		echo json_encode($result);
	}

	public function get_data() {
		if (!$this->input->is_ajax_request())
			exit();

		$uid = $this->input->get('uid') ? : '';
		/*
         * Order
         */
		$sOrder = "";
        $aOrders = array();
        $aOrders[] = "{$this->table_def}.tanggal_transaksi DESC";
        if (count($aOrders) > 0) {
            $sOrder = implode(', ', $aOrders);
        }
        if (!empty($sOrder)) {
            $sOrder = "ORDER BY ".$sOrder;
        }

        /*
         * Where
         */
        $sWhere = "";
        $aWheres = array();
        $aWheres[] = "{$this->table_def}.uid = '{$uid}'";
        if (count($aWheres) > 0) {
            $sWhere = implode(' AND ', $aWheres);
        }
        if (!empty($sWhere)) {
            $sWhere = "WHERE ".$sWhere;
        }

        $aLikes = array();

        if (count($aLikes) > 0) {
            $sLike = "(".implode(' OR ', $aLikes).")";
            $sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
        }

        $aSelect = array(
        	"{$this->table_def}.id",
        	"{$this->table_def}.uid",
        	"{$this->table_def}.tanggal_transaksi",
        	"{$this->table_def}.no_invoice as no_invoice",
        	"{$this->table_def}.total_harga as total_harga",
        	"{$this->table_def}.bukti_pembayaran as bukti_pembayaran",
        	"{$this->table_def}.ekspedisi as ekspedisi",
        	"{$this->table_def}.alasan_penolakan as alasan_penolakan",
        	"{$this->table_def}.ongkir as ongkir",
        	"{$this->table_def}.no_resi as no_resi",
        	"{$this->table_def}.status as status",
        	"dokter_member.nama as nama",
        	"dokter_member.provinsi as provinsi",
        	"dokter_member.kota as kota",
        	"dokter_member.kecamatan as kecamatan",
        );

		$list = $this->main->get_by("WHERE {$this->table_def}.uid = '{$uid}'", $aSelect);
		$list->bukti_pembayaran = $this->thumb_upload.'/'.$list->bukti_pembayaran;

			switch ($list->status) {
				case 1:
					$list->status_label = 'Belum Bayar';
				break;
				case 2:
					$list->status_label = 'Dikemas';
				break;
				case 3:
					$list->status_label = 'Dikirim';
				break;
				case 4:
					$list->status_label = 'Selesai';
				break;
				case 5:
					$list->status_label = 'Dibatalkan';
				break;
			}

		$list->dataDetail = $this->db->select('a.id checkout_id, a.qty, a.harga, a.diskon,  b.nama')
                                    ->join($this->table_def_barang.' b', 'a.product_uid=b.uid')
                                    ->where('a.checkout_id', $list->id)
                                    ->where('a.status !=', '1')
                                    ->get($this->table_def_pesanan.' a')->result();

		$this->output->set_status_header(200)
			->set_content_type('application/json')
            ->set_output(json_encode($list));
	}

    public function input_resi() {
        if (! $this->input->is_ajax_request())
            exit();

        $uid = $this->input->post('uid');
        $no_resi = $this->input->post('no_resi');
        $ekspedisi = $this->input->post('ekspedisi');

        $result = $this->main->input_resi($uid, $no_resi, $ekspedisi);

        if (! $result) {
            $this->output->set_status_header(504);
            return;
        }

        $data =	$this->db->select('no_invoice, member_id')
	            ->where('uid', $uid)
	            ->get($this->table_def)
	            ->row();
        notif_msg_member($data->member_id, 'Pesanan Dikirim', "Pesanan No. {$data->no_invoice} telah dikirim menggunakan jasa kurir {$ekspedisi} dengan No. Resi {$no_resi}.", "/akun/profile/pesanan");

        $this->output->set_status_header(200)
            ->set_output(json_encode(['data' => $result]));
    }
}