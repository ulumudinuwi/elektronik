<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Laporan extends CI_Controller {

    protected $current_user = "Administrator";
    public function __construct()
  	{
    	parent::__construct();
        $this->lang->load('barang');
        $this->load->helper('harga_barang');
        $this->load->model('gudang_farmasi/Laporan_model', 'main');

        if($this->session->has_userdata('first_name')) 
            $this->current_user = $this->session->userdata('first_name')." ".($this->session->userdata('last_name') ? $this->session->userdata('last_name') : "");
  	}

    // LAPORAN 001
    public function laporan_001(){
        $pabrik_id  = $_POST['pabrik_id'];

        $aColumns = array('kode', 'nama', 'satuan', 'stock', 'harga_penjualan');
        /* 
        * Paging
        */
        if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
            $iLimit = intval( $_POST['length'] );
            $iOffset = intval( $_POST['start'] );
        }

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if($pabrik_id != "") $aWheres[] = "pabrik_id = ".$pabrik_id;
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        $aLikes = array();
        if($_POST['search']['value'] != "") {
            for ($i = 0; $i < count($aColumns); $i++) {
                if($_POST['columns'][$i]['searchable'] == "true") {
                    switch ($aColumns[$i]) {
                        default:
                            $aLikes[] = "{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
                            break;
                    }
                }
            }
        }

        if (count($aLikes) > 0) {
            $sLike = "(".implode(' OR ', $aLikes).")";
            $sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
        }

        $list = $this->main->data_laporan("001", $iLimit, $iOffset, $sWhere, "ORDER BY nama ASC");

        $rResult = $list['data'];
        $iFilteredTotal = $list['total_rows'];
        $iTotal = $list['total_rows'];

        /*
        * Output
        */
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iTotal,
            "recordsFiltered" => $iFilteredTotal,
            "data" => array(),
        );

        $rows = array();
        $i = $iOffset;
        foreach ($rResult as $obj) {
            $obj->harga_penjualan = (int) $obj->harga_penjualan != 0 ? $obj->harga_penjualan : getHargaDasar($obj->id, 'gudang_farmasi');
            $obj->sub_total = $obj->harga_penjualan * $obj->stock;
            $obj->expired_date = str_replace(';', '<br/>', $obj->expired_date);

            $data = get_object_vars($obj);
            $data['no'] = ($i+1);
            $rows[] = $data;
            $i++;
        }
        $output['data'] = $rows;

        echo json_encode($output);
    }

    public function print_001(){
        $mode = $this->input->get("d");

        $pabrik_id  = $_GET['pabrik_id'];

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if($pabrik_id != "") $aWheres[] = "pabrik_id = ".$pabrik_id;
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        $pabrik = "SEMUA";
        if($pabrik_id) {
            $pabrik = $this->db->select('nama')
                                ->where('id', $pabrik_id)
                                ->get('m_pabrik')->row()->nama;
        }

        $list = $this->main->data_laporan("001", 0, 0, $sWhere, "ORDER BY nama ASC");

        switch ($mode) {
            case 'excel':
                # Prepare template
                $tpl_filename = 'gudang-farmasi/laporan/laporan-001.xlsx';
                $objPHPExcel = PHPExcel_IOFactory::load('assets/templates/' . $tpl_filename);
                $objPHPExcel->setActiveSheetIndex(0);
                $sheet = $objPHPExcel->getActiveSheet();
                $sheet->setCellValue('A1', get_option('rs_nama') ? strtoupper(get_option('rs_nama')) : "IMEDIS");
                $sheet->setCellValue('A2', "STOCK BARANG");
                $sheet->setCellValue('A3', strtoupper(lang('pabrik_label'))." : ".$pabrik);
                $sheet->setCellValue('A4', "TANGGAL : ".konversi_to_id(date("d M Y")));

                # Apply data rows
                $data = array();
                $no = 1;
                $start = 7;
                $increment = 7;
                if($list['total_rows'] > 0) {
                    foreach ($list['data'] as $i => $row) {
                        $row->harga_penjualan = (int) $row->harga_penjualan != 0 ? $row->harga_penjualan : getHargaDasar($row->id, 'gudang_farmasi');
                        $row->sub_total = $row->harga_penjualan * $row->stock;
                        $row->expired_date = str_replace(';', ', ', $row->expired_date);
                        
                        $aRow = get_object_vars($row);
                        unset($aRow['id']);
                        unset($aRow['pabrik_id']);
                        array_unshift($aRow, $no);

                        $no++;
                        $colnum = 0;
                        foreach ($aRow as $val) {
                            $sheet->setCellValue(chr(65 + $colnum) . $increment, $val);
                            $colnum++;
                        }
                        if($i != $list['total_rows'] - 1) $increment++;
                    }
                } else {
                    $sheet->mergeCells("A{$increment}:H{$increment}");
                    $sheet->setCellValue('A'.$increment, "TIDAK ADA DATA");
                    $sheet->getStyle('A'.$increment.':H'.$increment)->applyFromArray(array(
                        'font' => array(
                            'bold' => true
                        ),
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                    ));
                }
                # Apply styles
                $sheet->getStyle("A$start:H$increment")->applyFromArray(array(
                    'numberformat' => array(
                        'code' => '#,##0'
                    )
                ));

                $objPHPExcel->getActiveSheet()->getStyle('A'.$start.':H' . $increment)->applyFromArray(array(
                    'font' => array(
                        'size' => 8
                    ),
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('argb' => 'FF555555'),
                        ),
                    ),
                ));

                $increment++;

                # get current user
                $date_current_user = konversi_to_id(date("d M Y")).", {$this->current_user}";
                $sheet->mergeCells("G{$increment}:H{$increment}");
                $sheet->getStyle('G'.$increment)->applyFromArray(array(
                            'font' => array(
                                'size' => 8,
                                'bold' => true,
                            ),
                            'alignment' => array(
                                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                                'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                            )
                        ));
                $sheet->setCellValue('G'.$increment, $date_current_user);
                $sheet->setSelectedCell("A{$start}");

                # Send excel document
                ob_end_clean();
                header('Content-type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="Stock Barang.xls"');
                header('Cache-Control: max-age=0');
                header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
                header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
                header('Cache-Control: cache, must-revalidate');
                header('Pragma: public');
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
                $objWriter->save('php://output');
                break;
            case 'pdf':
                $data = array(
                    'title' => "STOCK BARANG",
                    'rows' => $list['data'],
                    'total_rows' => $list['total_rows'],
                    'current_date' => konversi_to_id(date("d M Y")),
                    'current_user' => $this->current_user,
                    'pabrik' => $pabrik,
                );

                $html = $this->load->view('gudang-farmasi/laporan/laporan-001-pdf', $data, TRUE);
                
                # Create PDF
                $mpdf = new \Mpdf\Mpdf([
                    'mode' => 'utf-8',
                    'format' => 'A4',
                    'orientation' => 'P',
                ]);
                $mpdf->setHTMLHeader('<p style="text-align: right;">HALAMAN: {PAGENO} / {nb}</p>');
                $mpdf->WriteHTML($html);
                $mpdf->Output('Stock Barang.pdf', "I");
                break;
        }
    }
}