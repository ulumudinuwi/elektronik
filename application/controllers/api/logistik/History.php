<?php defined('BASEPATH') OR exit('No direct script access allowed');

class History extends CI_Controller 
{
  protected $table_def_barang = "m_barang";
  protected $table_def_kartu_stock = "t_logistik_kartu_stock";
  
  function __construct() {
    parent::__construct();
    $this->lang->load('barang');

    $this->load->model('logistik/Stock_model', 'stock_main');
    $this->load->model('logistik/Kartu_stock_model', 'kartu_main');
  }

  /**
   * Load data
   */
  public function load_data(){
    $uid = json_decode(base64_decode($this->input->get('uid')));

    $aSelect = array(
      "CONCAT({$this->table_def_barang}.kode, '-', {$this->table_def_barang}.nama) barang",
      "CONCAT(satuan_penggunaan.nama, ' (', satuan_penggunaan.singkatan, ')') satuan",
    );
    $data = $this->stock_main->get_by("WHERE {$this->table_def_barang}.id = {$uid}", $aSelect);

    /*
     * Ordering
     */
    $sOrder = "";
    $aOrders = array();
    $aOrders[] = "{$this->table_def_kartu_stock}.created_at DESC, {$this->table_def_kartu_stock}.sisa_sum DESC";
    if (count($aOrders) > 0) {
      $sOrder = implode(', ', $aOrders);
    }
    if (!empty($sOrder)) {
      $sOrder = "ORDER BY ".$sOrder;
    }

    /*
     * Where
     */
    $sWhere = "";
    $aWheres = array();
    $aWheres[] = "{$this->table_def_kartu_stock}.barang_id = {$uid}";
    if (count($aWheres) > 0) {
      $sWhere = implode(' AND ', $aWheres);
    }
    if (!empty($sWhere)) {
      $sWhere = "WHERE ".$sWhere;
    }

    $aLikes = array();
    if (count($aLikes) > 0) {
      $sLike = "(".implode(' OR ', $aLikes).")";
      $sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
    }

    $list = $this->kartu_main->get_all(5, 0, $sWhere, $sOrder);
    foreach ($list['data'] as $row) {
      $row->tipe = $this->config->item('tipe_kartu_stock')[$row->tipe];
      $row->qty = abs($row->masuk - $row->keluar);
    }
    $data->details = $list['data'];

    echo json_encode(['data' => $data]);
  }  
}