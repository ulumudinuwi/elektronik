<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Pengalihan extends CI_Controller 
{
  protected $table_def = "t_logistik_po";
  protected $table_def_detail = "t_logistik_po_detail";
  protected $table_def_stock = "t_logistik_stock";
  protected $table_def_barang = "m_barang";
  
  function __construct() {
    parent::__construct();
    $this->lang->load('barang');

    $this->load->model('logistik/Po_model', 'main');
    $this->load->model('logistik/Stock_model', 'stock_main');
    $this->load->model('logistik/Po_detail_model', 'main_detail');
  }

  public function save() {
    
    if (!$this->input->is_ajax_request())
      exit();

    $obj = $this->_getDataObject();
    $result = $this->main->pengalihan($obj);
    
    if(!$result) 
      $this->output->set_status_header(500);

    $this->output->set_status_header(200);
    echo json_encode($result);
  }

  public function get_data($uid = 0) {
    if (!$this->input->is_ajax_request())
      exit();

    $obj = $this->main->get_by("WHERE {$this->table_def}.uid = \"{$uid}\"");
    $obj->tanggal_new = date('Y-m-d');
    $obj->sifat_desc = ucfirst($obj->sifat);
    $obj->status_desc = $this->config->item('status_po')[$obj->status];

    $obj->details_po = $this->main_detail->get_all(0, 0, "WHERE {$this->table_def_detail}.po_id = {$obj->id}")['data'];
    $obj->details = $this->main_detail->get_all(0, 0, "WHERE {$this->table_def_detail}.po_id = {$obj->id} AND {$this->table_def_detail}.qty > {$this->table_def_detail}.diterima")['data'];
    foreach ($obj->details as $detail) {
      $detail->qty = ($detail->qty - $detail->diterima);
      $detail->total = $detail->qty * $detail->harga;

      $aSelect = array(
        "({$this->table_def_stock}.maximum / {$this->table_def_barang}.isi_satuan_penggunaan) maximum",
        "({$this->table_def_stock}.minimum / {$this->table_def_barang}.isi_satuan_penggunaan) minimum",
        "({$this->table_def_stock}.qty / {$this->table_def_barang}.isi_satuan_penggunaan) stock",
      );
      $dataStock = $this->stock_main->get_by("WHERE {$this->table_def_stock}.barang_id = {$detail->barang_id}", $aSelect);
      if($dataStock) {
        $detail->maximum = $dataStock->maximum;
        $detail->minimum = $dataStock->minimum;
        $detail->stock = $dataStock->stock;
      }
    }
    echo json_encode(['data' => $obj]);
  }

  /**
   * Form Data Object
   */
  private function _getDataObject() {
    $his = date('H:i:s');
    $obj = new stdClass();
    $obj->id = 0;
    $obj->uid = "";
    $obj->tanggal = get_date_accepted_db($this->input->post('tanggal')).' '.$his;
    $obj->pabrik_id = $this->input->post('pabrik_id');
    $obj->vendor_id = $this->input->post('vendor_id');
    $obj->sifat = $this->input->post('sifat');
    $obj->total = $this->input->post('total');
    $obj->total_ppn = $this->input->post('total_ppn');
    $obj->grand_total = ($obj->total + $obj->total_ppn);
    
    if (isset($_POST['detail_id'])) {
      for ($i = 0; $i < count($_POST['detail_id']); $i++) {
        $detail = new StdClass();
        $detail->id = 0;
        $detail->barang_id = $_POST['barang_id'][$i];
        $detail->satuan_id = $_POST['satuan_id'][$i];
        $detail->isi_satuan = $_POST['isi_satuan'][$i];
        $detail->minimum = $_POST['minimum'][$i];
        $detail->stock = $_POST['stock'][$i];
        $detail->maximum = $_POST['maximum'][$i];
        $detail->qty = $_POST['qty'][$i];
        $detail->harga = $_POST['harga'][$i];
        $detail->total = ($_POST['qty'][$i] * $_POST['harga'][$i]);
        $detail->data_mode    = Data_mode_model::DATA_MODE_ADD;
        $aDetails[uniqid()] = $detail;
      }
    }
    $obj->details = $aDetails;

    // PO yang dialihkan
    $obj->po_id = $this->input->post('po_id');
    $obj->keterangan = $this->input->post('keterangan');
    return $obj;
  }
}