<?php defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH.'events/EventProcess.php');
use Ryuna\Response;

class Notifikasi extends Admin_Controller 
{
    protected $table_def_pasien = "t_notifikasi";
    protected $table_def_user = "auth_users";

    function __construct()
    {
        parent::__construct();
    }

    /*get Data*/
    public function getData($role_id) {
        if (!$this->input->is_ajax_request()) exit();

        if ($role_id) {
            $aSelect = array(
                't_notifikasi.*',
            );

            $badge = $this->db->select(implode(",", $aSelect))
                        ->where('role_id', $role_id)
                        ->where('status', '0')
                        ->get('t_notifikasi')
                        ->num_rows();

            $list = $this->db->select(implode(",", $aSelect))
                        ->where('role_id', $role_id)
                        ->where('status', '0')
                        ->order_by('created_at', 'desc')
                        ->get('t_notifikasi')
                        ->result();

            $data['badge'] = $badge;
            $data['list'] = $list;
        }

        echo json_encode($data);
    }

    public function listen_notifikasi_bc() {
        session_write_close(); # !Important
        require_once(APPPATH.'events/notifikasi/notifikasiEvent.php');

        $event_name = $this->input->get('event');

        $sse = new Sse\SSE();
        $sse->addEventListener($event_name, new notifikasiEvent());
        $sse->start();
    }

    public function listen_notifikasi(){
        session_write_close(); # !Important
        $event_name = $this->input->get('event');
        $uid  = base64_encode('t_notifikasi');

        if($event_name && $uid){
            // $sse = new Sse\SSE(); //create a libSSE instance
            // $sse->addEventListener($event_name, new EventProcess($uid));//register your event handler
            // $sse->start();//start the event loop
            $sse = new EventProcess($uid, $event_name);
            $sse->start();
        }

        // return Response::json([
        //     'status' => '400',
        //     'message' => 'Param Cannot Be Null',
        //     'param_key' => [
        //         'uid' => 'Base64encode from name table', 
        //         'event_name' => 'Event Name', 
        //     ],
        // ], 400);
    }

    public function updateNotif(){
        $id = $this->input->post('id');
        
        $this->db->set('status', '1', FALSE);
        $this->db->where('id', $id);
        $this->db->update('t_notifikasi');

        echo true;
    }
}