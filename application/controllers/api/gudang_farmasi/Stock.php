<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Stock extends CI_Controller 
{
	protected $table_def = "t_gudang_farmasi_stock";
	protected $table_def_kartu_stock = "t_gudang_farmasi_kartu_stock";
	protected $table_def_barang = "m_barang";
	protected $table_def_jenis = "m_jenisbarang";
	protected $table_def_satuan = "m_satuan";
	
	protected $current_user = "Administrator";
	function __construct() {
		parent::__construct();
		$this->load->helper('harga_barang');

		$this->load->model('gudang_farmasi/Stock_model', 'main');
		$this->load->model('gudang_farmasi/Kartu_stock_model', 'kartu_main');
		$this->load->model('gudang_farmasi/Stock_detail_model', 'stock_detail');

		if($this->session->has_userdata('first_name')) 
						$this->current_user = $this->session->userdata('first_name')." ".($this->session->userdata('last_name') ? $this->session->userdata('last_name') : "");
	}

	/**
	 * Load data
	 */
	public function load_data() {
		$pabrik_id  = $_POST['pabrik_id'];
		$kategori_id  = $_POST['kategori_id'];
		$status  = $_POST['status'];

		$aColumns = array('', 'kode', 'nama', 'satuan',  'maximum', 'minimum', 'reorder', 'qty');
		/* 
		 * Paging
		 */
		if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
			$iLimit = intval( $_POST['length'] );
			$iOffset = intval( $_POST['start'] );
		}

		/*
		 * Ordering
		 */
		$sOrder = "";
		$aOrders = array();
		for ($i = 0; $i < count($aColumns); $i++) {
			if($_POST['columns'][$i]['orderable'] == "true") {
				if($i == $_POST['order'][0]['column']) {
					switch ($aColumns[$i]) {
						case "kode":
						case "nama":
							$aOrders[] = $this->table_def_barang.'.'.$aColumns[$i].' '.($_POST['order'][0]['dir'] == 'asc' ? 'asc' : 'desc');
							break;
						default:
							$aOrders[] = $this->table_def.'.'.$aColumns[$i].' '.($_POST['order'][0]['dir'] == 'asc' ? 'asc' : 'desc');
							break;
					}
				}
			}
		}
		if (count($aOrders) > 0) {
			$sOrder = implode(', ', $aOrders);
		}
		if (!empty($sOrder)) {
			$sOrder = "ORDER BY ".$sOrder;
		}

		/*
		 * Where
		 */
		$sWhere = "";
		$aWheres = array();
		$aWheres[] = "{$this->table_def_barang}.status = 1";
        if ($this->session->userdata('bpom') == 1) {
            $aWheres[] = "{$this->table_def_barang}.bpom = ".$this->session->userdata('bpom');
        }
		if($status != "") {
			switch ($status) {
				case 1:
				$aWheres[] = "{$this->table_def}.qty > {$this->table_def}.reorder";
				break;
				case 2:
				$aWheres[] = "{$this->table_def}.qty BETWEEN ({$this->table_def}.minimum+1) AND {$this->table_def}.reorder";
				break;
				case 3:
				$aWheres[] = "{$this->table_def}.qty <= {$this->table_def}.minimum";
				break;
			}
		}
		if($pabrik_id != "") $aWheres[] = "{$this->table_def_barang}.pabrik_id = {$pabrik_id}";
		if($kategori_id != "") $aWheres[] = "{$this->table_def_barang}.kategori_id = {$kategori_id}";
		if (count($aWheres) > 0) {
			$sWhere = implode(' AND ', $aWheres);
		}
		if (!empty($sWhere)) {
			$sWhere = "WHERE ".$sWhere;
		}

		$aLikes = array();
		if($_POST['search']['value'] != "") {
			for ($i = 0; $i < count($aColumns); $i++) {
				if($_POST['columns'][$i]['searchable'] == "true") {
					switch ($aColumns[$i]) {
						case "kode":
						case "nama":
							$aLikes[] = "{$this->table_def_barang}.{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
							break;
						default:
							$aLikes[] = "{$this->table_def}.{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
							break;
					}
				}
			}
		}

		if (count($aLikes) > 0) {
			$sLike = "(".implode(' OR ', $aLikes).")";
			$sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
		}

		$aSelect = array(
			"{$this->table_def}.id",
			"{$this->table_def}.qty",
			"{$this->table_def}.maximum",
			"{$this->table_def}.minimum",
			"{$this->table_def}.reorder",
			"{$this->table_def}.dipesan",
			"{$this->table_def_barang}.kode",
			"{$this->table_def_barang}.nama",
			"{$this->table_def_barang}.alias",
			"pabrik.nama pabrik",
			"CONCAT(satuan_penggunaan.nama, ' (', satuan_penggunaan.singkatan, ')') satuan",
		);

		$list = $this->main->get_all($iLimit, $iOffset, $sWhere, $sOrder, $aSelect);

		$rResult = $list['data'];
		$iFilteredTotal = $list['total_rows'];
		$iTotal = $list['total_rows'];

		/*
		 * Output
		 */
		$output = array(
			"draw" => intval($_POST['draw']),
			"recordsTotal" => $iTotal,
			"recordsFiltered" => $iFilteredTotal,
			"data" => array(),
		);

		$rows = array();
		$i = $iOffset;
		foreach ($rResult as $obj) {
			$obj->harga_dasar = getHargaDasar($obj->id, 'gudang_farmasi');

			$data = get_object_vars($obj);
			$data['no'] = ($i+1);
			$rows[] = $data;
			$i++;
		}
		$output['data'] = $rows;

		echo json_encode($output);
	}

	public function load_detail_stock() {
		if (!$this->input->is_ajax_request())
			exit();

		$obj = json_decode(base64_decode($this->input->get('uid')));
		$id = $obj->id;
		$search_by = isset($obj->search_by) ? $obj->search_by : "";

		if($search_by == "barang_id") {
			$id = 0;
			$oStock = $this->db->select('id')
							->where('barang_id', $obj->id)
							->get($this->table_def)->row();
			if($oStock) $id = $oStock->id;
		}

		if ($id) {
			$aSelect = array(
				"id",
				//"SUM(qty) qty",
				"qty",
				"no_batch",
				"expired_date",
				"harga",
			);
			//$where = "WHERE stock_id = {$id} GROUP BY expired_date";
			$where = "WHERE stock_id = {$id}";
			$result = $this->stock_detail->get_all(0, 0, $where, "ORDER BY id asc", $aSelect)['data'];
			$this->output->set_status_header(200)
						->set_output(json_encode(['data' => $result]));
		} else {
			$this->output->set_status_header(500)
							->set_output(json_encode(['message' => 'Terjadi kesalahan saat mengambil data.']));
		}
	}

	public function load_data_modal(){
		$mode = $this->input->get('mode') ? : '';
		$pabrik_id  = $this->input->post('pabrik_id') ? : '';

		$aColumns = array('kode', 'nama');
		/* 
		 * Paging
		 */
		if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
			$iLimit = intval( $_POST['length'] );
			$iOffset = intval( $_POST['start'] );
		}

		/*
		 * Ordering
		 */
		$sOrder = "";
		$aOrders = array();
		for ($i = 0; $i < count($aColumns); $i++) {
			if($_POST['columns'][$i]['orderable'] == "true") {
				if($i == $_POST['order'][0]['column']) 
					$aOrders[] = $aColumns[$i].' '.($_POST['order'][0]['dir'] == 'asc' ? 'asc' : 'desc');
			}
		}
		if (count($aOrders) > 0) {
			$sOrder = implode(', ', $aOrders);
		}
		if (!empty($sOrder)) {
			$sOrder = "ORDER BY ".$sOrder;
		}

		/*
		 * Where
		 */
		$sWhere = "";
		$aWheres = array();
		$aWheres[] = "{$this->table_def_barang}.status = 1";
        if ($this->session->userdata('bpom') == 1) {
            $aWheres[] = "{$this->table_def_barang}.bpom = ".$this->session->userdata('bpom');
        }
		if($pabrik_id != "") $aWheres[] = "{$this->table_def_barang}.pabrik_id = {$pabrik_id}";
		if ($mode == "po") $aWheres[] = "{$this->table_def}.dipesan = 0";
		if (count($aWheres) > 0) {
			$sWhere = implode(' AND ', $aWheres);
		}
		if (!empty($sWhere)) {
			$sWhere = "WHERE ".$sWhere;
		}

		$aLikes = array();
		if($_POST['search']['value'] != "") {
			for ($i = 0; $i < count($aColumns); $i++) {
				if($_POST['columns'][$i]['searchable'] == "true") {
					switch ($aColumns[$i]) {
						case 'kode':
							$aLikes[] = "{$this->table_def_barang}.{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
							break;
						case 'nama':
							$aLikes[] = "({$this->table_def_barang}.nama LIKE '%".$_POST['search']['value']."%' OR {$this->table_def_barang}.alias LIKE '%".$_POST['search']['value']."%')";
							break;
						default:
							break;
					}
				}
			}
		}

		if (count($aLikes) > 0) {
			$sLike = "(".implode(' OR ', $aLikes).")";
			$sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
		}

		$aSelect = array(
			"{$this->table_def_barang}.id",
			"{$this->table_def_barang}.kode",
			"{$this->table_def_barang}.nama",
			"pabrik.nama pabrik",
		);

		switch ($mode) {
			case "po":
				$aSelect[] = "({$this->table_def}.minimum / {$this->table_def_barang}.isi_satuan_penggunaan) minimum";
				$aSelect[] = "({$this->table_def}.qty / {$this->table_def_barang}.isi_satuan_penggunaan) stock";
				$aSelect[] = "({$this->table_def}.maximum / {$this->table_def_barang}.isi_satuan_penggunaan) maximum";
				$aSelect[] = "{$this->table_def_barang}.harga_pembelian harga";
				$aSelect[] = "{$this->table_def_barang}.satuan_pembelian_id satuan_id";
				$aSelect[] = "{$this->table_def_barang}.isi_satuan_penggunaan isi_satuan";
				$aSelect[] = "CONCAT(satuan_pembelian.nama, ' (', satuan_pembelian.singkatan, ')') satuan";
				break;
			default:
				$aSelect[] = "{$this->table_def}.minimum";
				$aSelect[] = "{$this->table_def}.qty stock";
				$aSelect[] = "{$this->table_def}.maximum";
				$aSelect[] = "{$this->table_def_barang}.harga_penjualan";
				$aSelect[] = "{$this->table_def_barang}.satuan_penggunaan_id satuan_id";
				$aSelect[] = "1 isi_satuan";
				$aSelect[] = "CONCAT(satuan_penggunaan.nama, ' (', satuan_penggunaan.singkatan, ')') satuan";
				break;
		}

		$list = $this->main->get_all($iLimit, $iOffset, $sWhere, $sOrder, $aSelect);

		$rResult = $list['data'];
		$iFilteredTotal = $list['total_rows'];
		$iTotal = $list['total_rows'];

		/*
		 * Output
		 */
		$output = array(
			"draw" => intval($_POST['draw']),
			"recordsTotal" => $iTotal,
			"recordsFiltered" => $iFilteredTotal,
			"data" => array(),
		);

		$rows = array();
		$i = $iOffset;
		foreach ($rResult as $obj) {
			if($mode != "po") $obj->harga = (int) $obj->harga_penjualan != 0 ? $obj->harga_penjualan : getHargaDasar($obj->id, 'farmasi');
			$data = get_object_vars($obj);
			$data['no'] = ($i+1);
			$rows[] = $data;
			$i++;
		}
		$output['data'] = $rows;

		echo json_encode($output);
	}

	public function setting() {

		if (!$this->input->is_ajax_request())
			exit();

		$id = $this->input->post('setting_id');

		$setting = new stdClass();
		$setting->maximum = $this->input->post("setting_maximum");
		$setting->reorder = $this->input->post("setting_reorder");
		$setting->minimum = $this->input->post("setting_minimum");
		$setting->update_by = $this->session->userdata('auth_user');
		$setting->update_at = date('Y-m-d H:i:s');

		$this->db->where('id', $id)
							->update($this->table_def, $setting);

		$this->db->trans_complete();
		if ($this->db->trans_status() === TRUE) {
			$this->db->trans_commit();
			$this->output->set_status_header(200);
			echo json_encode(['status' => 'success']);
		}
		else {
			$this->db->trans_rollback();
			$this->output->set_status_header(500);
			echo json_encode(['status' => 'error']);
		}
	}

	// KARTU STOCK
	public function load_data_kartu_stock() 
	{
		$uid = $this->input->get('uid');
		$aColumns = array('created_at', 'kode', 'keterangan', 'masuk', 'keluar',  'saldo');
		
		/* 
		 * Paging
		 */
		if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
			$iLimit = intval( $_POST['length'] );
			$iOffset = intval( $_POST['start'] );
		}

		/*
		 * Ordering
		 */
		$sOrder = "";
		$aOrders = array();
		for ($i = 0; $i < count($aColumns); $i++) {
			if($_POST['columns'][$i]['orderable'] == "true") {
				if($i == $_POST['order'][0]['column']) {
					switch ($aColumns[$i]) {
						default:
						$aOrders[] = $this->table_def_kartu_stock.'.'.$aColumns[$i].' '.($_POST['order'][0]['dir'] == 'asc' ? 'asc' : 'desc');
							break;
					}
				}
			}
		}
		if (count($aOrders) > 0) {
			$sOrder = implode(', ', $aOrders);
		}
		if (!empty($sOrder)) {
			$sOrder = "ORDER BY ".$sOrder;
		}

		/*
		 * Where
		 */
		$sWhere = "";
		$aWheres = array();
		$aWheres[] = "{$this->table_def_kartu_stock}.barang_id = ".json_decode(base64_decode($uid));
		if (count($aWheres) > 0) {
			$sWhere = implode(' AND ', $aWheres);
		}
		if (!empty($sWhere)) {
			$sWhere = "WHERE ".$sWhere;
		}

		$aLikes = array();
		if($_POST['search']['value'] != "") {
			for ($i = 0; $i < count($aColumns); $i++) {
				if($_POST['columns'][$i]['searchable'] == "true") {
					switch ($aColumns[$i]) {
						default:
						$aLikes[] = "{$this->table_def_kartu_stock}.{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
							break;
					}
				}
			}
		}

		if (count($aLikes) > 0) {
			$sLike = "(".implode(' OR ', $aLikes).")";
			$sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
		}

		$list = $this->kartu_main->get_all($iLimit, $iOffset, $sWhere, $sOrder);

		$rResult = $list['data'];
		$iFilteredTotal = $list['total_rows'];
		$iTotal = $list['total_rows'];

		/*
		 * Output
		 */
		$output = array(
			"draw" => intval($_POST['draw']),
			"recordsTotal" => $iTotal,
			"recordsFiltered" => $iFilteredTotal,
			"data" => array(),
		);

		$rows = array();
		$i = $iOffset;
		foreach ($rResult as $obj) {
			$obj->keterangan = $obj->keterangan ? $obj->keterangan : "-";
			$data = get_object_vars($obj);
			$data['no'] = ($i+1);
			$rows[] = $data;
			$i++;
		}
		$output['data'] = $rows;

		echo json_encode($output);
	}

	public function print_kartu_stock()
	{
        $uid = $this->input->get('uid');
        $aSelect = array(
	        "{$this->table_def}.barang_id",
	        "{$this->table_def_barang}.kode",
	        "{$this->table_def_barang}.nama",
	        "pabrik.nama pabrik",
	        "CONCAT(satuan_penggunaan.nama, ' (', satuan_penggunaan.singkatan, ')') satuan_penggunaan",
	    );
		$objStock = $this->main->get_by("WHERE {$this->table_def_barang}.id = ".json_decode(base64_decode($uid)), $aSelect);
		$title = "Kartu Stock {$objStock->pabrik} - {$objStock->kode} - {$objStock->nama} - Satuan: {$objStock->satuan_penggunaan}";
        
        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        $aWheres[] = "{$this->table_def_kartu_stock}.barang_id = ".json_decode(base64_decode($uid));
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        $list = $this->kartu_main->get_all(0, 0, $sWhere, "ORDER BY {$this->table_def_kartu_stock}.created_at ASC");

        # Prepare template
        $tpl_filename = 'gudang-farmasi/kartu-stock.xlsx';
        $objPHPExcel = PHPExcel_IOFactory::load('assets/templates/' . $tpl_filename);
        $objPHPExcel->setActiveSheetIndex(0);
        $sheet = $objPHPExcel->getActiveSheet();
        $sheet->setCellValue('A1', get_option('rs_nama') ? strtoupper(get_option('rs_nama')) : "IMEDIS");
        $sheet->setCellValue('A2', "GUDANG FARMASI");
        $sheet->setCellValue('A3', $title);

        # Apply data rows
        $data = array();
        $no = 1;
        $start = 6;
        $increment = 6;
        if($list['total_rows'] > 0) {
            foreach ($list['data'] as $i => $row) {
                $obj = new stdClass();
                $obj->no = $no;
                $obj->tanggal = date('d/m/Y H:i', strtotime($row->created_at));
                $obj->kode = $row->kode;
                $obj->keterangan = $row->keterangan ? : '-';
                $obj->masuk = $row->masuk;
                $obj->keluar = $row->keluar;
                $obj->sisa_sum = $row->sisa_sum;
                $aRow = get_object_vars($obj);
                $no++;

                $colnum = 0;
                foreach ($aRow as $val) {
                	$labelColumn = chr(65 + $colnum);
                	if($labelColumn == "E") {
                		if($row->tipe == 7) {
	                		$sheet->mergeCells("E{$increment}:F{$increment}");
	            			$sheet->setCellValue("E{$increment}", $val);
	            			$sheet->getStyle("E{$increment}")->applyFromArray(array(
			                    'alignment' => array(
			                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
			                    )
			                ));
                		} else $sheet->setCellValue($labelColumn . $increment, $val);
                	} else $sheet->setCellValue($labelColumn . $increment, $val);
                	
                    $colnum++;
                }
                if($i != $list['total_rows'] - 1) $increment++;
            }
        } else {
            $sheet->mergeCells("A{$increment}:G{$increment}");
            $sheet->setCellValue('A'.$increment, "TIDAK ADA DATA");
            $sheet->getStyle('A'.$increment.':G'.$increment)->applyFromArray(array(
                'font' => array(
                    'bold' => true
                ),
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            ));
        }
        # Apply styles
        $sheet->getStyle("A$start:G$increment")->applyFromArray(array(
            'numberformat' => array(
                'code' => '#,##0'
            )
        ));

        $objPHPExcel->getActiveSheet()->getStyle('A'.$start.':G' . $increment)->applyFromArray(array(
            'font' => array(
                'size' => 8
            ),
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => 'FF555555'),
                ),
            ),
        ));

        $increment++;

        # get current user
        $date_current_user = konversi_to_id(date("d M Y")).", {$this->current_user}";
        $sheet->mergeCells("F{$increment}:G{$increment}");
        $sheet->getStyle('F'.$increment)->applyFromArray(array(
                    'font' => array(
                        'size' => 8,
                        'bold' => true,
                    ),
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                    )
                ));
        $sheet->setCellValue('F'.$increment, $date_current_user);
        $sheet->setSelectedCell("A{$start}");

        # Send excel document
		ob_end_clean();
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="Kartu Stock Gudang Farmasi.xls"');
        header('Cache-Control: max-age=0');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: cache, must-revalidate');
        header('Pragma: public');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');

    }
}