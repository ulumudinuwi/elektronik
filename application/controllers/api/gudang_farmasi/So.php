<?php defined('BASEPATH') OR exit('No direct script access allowed');

class So extends CI_Controller 
{
	protected $table_def = "t_gudang_farmasi_stock_opname";
	protected $table_def_detail = "t_gudang_farmasi_stock_opname_detail";
	protected $table_def_stock = "t_gudang_farmasi_stock";
	protected $table_def_barang = "m_barang";
	protected $table_def_satuan = "m_satuan";
	
	function __construct() {
		parent::__construct();
		$this->load->model('gudang_farmasi/So_model', 'main');
		$this->load->model('gudang_farmasi/So_detail_model', 'main_detail');
		$this->load->model('gudang_farmasi/Stock_model', 'stock');
		$this->load->model('gudang_farmasi/Stock_detail_model', 'stock_detail');
	}

	/**
	 * Load data
	 */
	public function load_data_barang(){
		/* 
		 * Paging
		 */
		$iLimit = 0;
		$iOffset = 0;
		if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
			$iLimit = intval( $_POST['length'] );
			$iOffset = intval( $_POST['start'] );
		}

		/*
		 * Where
		 */
		$sWhere = "";
		$aWheres = array();
		$aWheres[] = "{$this->table_def_barang}.status = 1";
        if ($this->session->userdata('bpom') == 1) {
            $aWheres[] = "{$this->table_def_barang}.bpom = ".$this->session->userdata('bpom');
        }
		$aWheres[] = "{$this->table_def_stock}.status = 1"; // YANG BELUM PEMERIKSAAN PERTAMA
		if (count($aWheres) > 0) {
			$sWhere = implode(' AND ', $aWheres);
		}
		if (!empty($sWhere)) {
			$sWhere = "WHERE ".$sWhere;
		}

		if(isset($_POST['search']['value'])) {
				$aLikes = array();
			if($_POST['search']['value'] != "") {
				$aLikes[] = "{$this->table_def_barang}.kode LIKE '%".$_POST['search']['value']."%'";
				$aLikes[] = "{$this->table_def_barang}.nama LIKE '%".$_POST['search']['value']."%'";
			}
			if (count($aLikes) > 0) {
				$sLike = "(".implode(' OR ', $aLikes).")";
				$sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
			}
		}

		$aSelect = array(
			"0 input_check",
			"0 selisih",
			"{$this->table_def_stock}.id stock_id",
			"{$this->table_def_stock}.barang_id",
			"{$this->table_def_stock}.qty stock_sistem",
			"{$this->table_def_stock}.qty stock_fisik",
			"{$this->table_def_barang}.kode kode_barang",
			"{$this->table_def_barang}.nama barang",
			"{$this->table_def_barang}.satuan_penggunaan_id satuan_id",
			"CONCAT(satuan_penggunaan.nama, ' (', satuan_penggunaan.singkatan, ')') satuan",
		);

		$list = $this->stock->get_all($iLimit, $iOffset, $sWhere, "", $aSelect);
		$rResult = $list['data'];
		$iFilteredTotal = $list['total_rows'];
		$iTotal = $list['total_rows'];

		/*
		 * Output
		 */
		$output = array(
			"draw" => isset($_POST['draw']) ? intval($_POST['draw']) : 1,
			"recordsTotal" => $iTotal,
			"recordsFiltered" => $iFilteredTotal,
			"data" => array(),
		);

		$rows = array();
		foreach ($rResult as $obj) {
			$aSelect = array(
				"id",
				//"SUM(qty) qty",
				"qty",
				"no_batch",
				"expired_date",
			);
			//$stockDetail = $this->stock_detail->get_all(0, 0, "WHERE stock_id = {$obj->stock_id} AND qty > 0 GROUP BY expired_date", "ORDER BY id asc", $aSelect)['data'];
			$stockDetail = $this->stock_detail->get_all(0, 0, "WHERE stock_id = {$obj->stock_id} AND qty > 0", "ORDER BY id asc", $aSelect)['data'];
			if(count($stockDetail) <= 0) {
				$stockDetail[] = array(
					"id" => 0, 
					"qty" => 0, 
					"no_batch" => "", 
					"expired_date" => "",
					"mode" => "add",
				);
			}
			$obj->details = $stockDetail;

			$data = get_object_vars($obj);
			$rows[] = $data;
		}
		$output['data'] = $rows;
		
		echo json_encode($output);
	}  

	public function load_data(){
		$browse = $this->input->get('browse');
		$tanggal_dari  = isset($_POST['tanggal_dari']) ? $_POST['tanggal_dari'] : "";
		$tanggal_sampai  = isset($_POST['tanggal_sampai']) ? $_POST['tanggal_sampai'] : "";

		$aColumns = array('kode', 'tanggal', '');
		/* 
		 * Paging
		 */
		if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
			$iLimit = intval( $_POST['length'] );
			$iOffset = intval( $_POST['start'] );
		}

		/*
		 * Ordering
		 */
		$sOrder = "";
		$aOrders = array();
		for ($i = 0; $i < count($aColumns); $i++) {
			if($_POST['columns'][$i]['orderable'] == "true") {
				if($i == $_POST['order'][0]['column']) {
					switch ($aColumns[$i]) {
						default:
							$aOrders[] = $this->table_def.'.'.$aColumns[$i].' '.($_POST['order'][0]['dir'] == 'asc' ? 'asc' : 'desc');
						break;
					}
				}
			}
		}
		if (count($aOrders) > 0) {
			$sOrder = implode(', ', $aOrders);
		}
		if (!empty($sOrder)) {
			$sOrder = "ORDER BY ".$sOrder;
		}

		/*
		 * Where
		 */
		$sWhere = "";
		$aWheres = array();
		if($tanggal_dari != "") $aWheres[] = "DATE({$this->table_def}.tanggal) >= '{$tanggal_dari}'";
		if($tanggal_sampai != "") $aWheres[] = "DATE({$this->table_def}.tanggal) <= '{$tanggal_sampai}'";
		if($browse == 1) {
			$aWheres[] = "{$this->table_def}.status = 1";
		} else $aWheres[] = "{$this->table_def}.status = 2";
		if (count($aWheres) > 0) {
			$sWhere = implode(' AND ', $aWheres);
		}
		if (!empty($sWhere)) {
			$sWhere = "WHERE ".$sWhere;
		}

		$aLikes = array();
		if($_POST['search']['value'] != "") {
			for ($i = 0; $i < count($aColumns); $i++) {
				if($_POST['columns'][$i]['searchable'] == "true") {
					switch ($aColumns[$i]) {
						default:
							$aLikes[] = "{$this->table_def}.{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
							break;
					}
				}
			}
		}

		if (count($aLikes) > 0) {
			$sLike = "(".implode(' OR ', $aLikes).")";
			$sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
		}

		$aSelect = array(
			"{$this->table_def}.uid",
			"{$this->table_def}.kode",
			"{$this->table_def}.tanggal",
			"{$this->table_def}.pengecekan1_at",
			"{$this->table_def}.pengecekan2_at",
			"CONCAT(pengecekan1_by.first_name, ' ',pengecekan1_by.last_name) pengecekan1_by",
			"CONCAT(pengecekan2_by.first_name, ' ',pengecekan2_by.last_name) pengecekan2_by",
		);

		$list = $this->main->get_all($iLimit, $iOffset, $sWhere, $sOrder, $aSelect);

		$rResult = $list['data'];
		$iFilteredTotal = $list['total_rows'];
		$iTotal = $list['total_rows'];

		/*
		 * Output
		 */
		$output = array(
			"draw" => intval($_POST['draw']),
			"recordsTotal" => $iTotal,
			"recordsFiltered" => $iFilteredTotal,
			"data" => array(),
		);

		$rows = array();
		$i = $iOffset;
		foreach ($rResult as $obj) {
			$data = get_object_vars($obj);
			$data['no'] = ($i+1);
			$rows[] = $data;
			$i++;
		}
		$output['data'] = $rows;

		echo json_encode($output);
	}  

	public function get_data($uid = 0) {
		if (!$this->input->is_ajax_request())
			exit();
		$mode = $this->input->get('mode') ? $this->input->get('mode') : '';
		$obj = $this->main->get_by("WHERE {$this->table_def}.uid = \"{$uid}\"");
		$obj->details = $this->main_detail->get_all(0, 0, "WHERE {$this->table_def_detail}.so_id = {$obj->id}")['data'];
		if(count($obj->details) > 0) {
			foreach ($obj->details as $row) {
				$row->pengecekan1_detail_barang = json_decode($row->pengecekan1_detail_barang);
				$row->pengecekan2_detail_barang = $row->pengecekan2_detail_barang ? json_decode($row->pengecekan2_detail_barang) : $row->pengecekan1_detail_barang;
				if($mode == "pengecekan") {
					$row->pengecekan2_stock_fisik = $row->pengecekan2_stock_fisik != 0 ? $row->pengecekan2_stock_fisik : $row->pengecekan1_stock_fisik;
					$row->pengecekan2_selisih = $row->pengecekan2_selisih != 0 ? $row->pengecekan2_selisih : $row->pengecekan1_selisih;
				}
			}
		}
		echo json_encode(['data' => $obj]);
	}

	public function save() {
		
		if (!$this->input->is_ajax_request())
			exit();

		$his = date('H:i:s');
		$obj = new stdClass();
		if ($this->input->post('uid')) {
			$obj->id = $this->input->post('id');
			$obj->uid = $this->input->post('uid');
			$obj->kode = $this->input->post('kode');
			$obj->tanggal = $this->input->post('tanggal');
			$obj->status = 2;
			$obj->pengecekan2_at = date('Y-m-d H:i:s');
			$obj->pengecekan2_by = $this->auth->userid();

			$aDetails = array();
			if (isset($_POST['detail_id'])) {
				for ($i = 0; $i < count($_POST['detail_id']); $i++) {
					$detail_id = $_POST['detail_id'][$i];
					$aDetailBarang = $this->_formDetailBarang($i);

					$detail = new StdClass();
					$detail->id = $detail_id;
					$detail->stock_id = $_POST['stock_id'][$i];
					$detail->pengecekan2_stock_fisik = $_POST['stock_fisik'][$i];
					$detail->pengecekan2_selisih = $_POST['selisih'][$i];
					$detail->detail_barang = $aDetailBarang;
					$detail->data_mode = Data_mode_model::DATA_MODE_EDIT;
					$aDetails[$detail_id] = $detail;
				}
			}
			$obj->details = $aDetails;
			$result = $this->main->saveSecondCheck($obj);
		} else {
			$obj->tanggal = get_date_accepted_db($this->input->post('tanggal')).' '.$his;
			$obj->pengecekan1_at = date('Y-m-d H:i:s');
			$obj->pengecekan1_by = $this->auth->userid();
			$obj->created_at = date('Y-m-d H:i:s');
			$obj->created_by = $this->auth->userid();

			$aDetails = array();
			if (isset($_POST['barang_id'])) {
				for ($i = 0; $i < count($_POST['barang_id']); $i++) {
					$aDetailBarang = $this->_formDetailBarang($i);

					$detail = new StdClass();
					$detail->stock_id = $_POST['stock_id'][$i];
					$detail->barang_id = $_POST['barang_id'][$i];
					$detail->satuan_id = $_POST['satuan_id'][$i];
					$detail->isi_satuan = $_POST['isi_satuan'][$i];
					$detail->stock_sistem = $_POST['stock_sistem'][$i];
					$detail->pengecekan1_stock_fisik = $_POST['stock_fisik'][$i];
					$detail->pengecekan1_selisih = $_POST['selisih'][$i];
					$detail->detail_barang = $aDetailBarang;
					$detail->data_mode = Data_mode_model::DATA_MODE_ADD;
					$aDetails[uniqid()] = $detail;
				}
			}
			$obj->details = $aDetails;
			$result = $this->main->saveFirstCheck($obj);
		}

		if(!$result) 
			$this->output->set_status_header(500);

		$this->output->set_status_header(200)
				->set_output(json_encode(['data' => $obj]));
	}

	private function _formDetailBarang($i) {
		$barangId = $_POST['barang_id'][$i];
		$aDetail = array();
		if (isset($_POST['no_batch_'.$barangId])) {
			for ($j = 0; $j < count($_POST['no_batch_'.$barangId]); $j++) {
				$oDB = new StdClass();
				$oDB->id = $_POST['id_'.$barangId][$j];
				$oDB->no_batch = $_POST['no_batch_'.$barangId][$j];
				$oDB->expired_date = $_POST['expired_date_'.$barangId][$j] == "" ? null : get_date_accepted_db($_POST['expired_date_'.$barangId][$j]);
				$oDB->qty = $_POST['qty_'.$barangId][$j];
				$aDetail[$j] = $oDB;
			}    
		}
		return $aDetail;
	}
}