<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Afkir extends CI_Controller 
{
	protected $table_def = "t_gudang_farmasi_afkir";
	protected $table_def_detail = "t_gudang_farmasi_afkir_detail";
	
	function __construct() {
		parent::__construct();
		$this->lang->load('barang');

		$this->load->model('gudang_farmasi/Afkir_model', 'main');
		$this->load->model('gudang_farmasi/Afkir_detail_model', 'main_detail');
	}

	/**
	 * Load data
	 */
	public function load_data(){
		$tanggal_dari  = $_POST['tanggal_dari'];
		$tanggal_sampai  = $_POST['tanggal_sampai'];
		
		$aColumns = array('kode', 'tanggal');
		/* 
		 * Paging
		 */
		if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
			$iLimit = intval( $_POST['length'] );
			$iOffset = intval( $_POST['start'] );
		}

		/*
		 * Ordering
		 */
		$sOrder = "";
		$aOrders = array();
		for ($i = 0; $i < count($aColumns); $i++) {
			if($_POST['columns'][$i]['orderable'] == "true") {
				if($i == $_POST['order'][0]['column']) {
					switch ($aColumns[$i]) {
						default:
							$aOrders[] = $this->table_def.'.'.$aColumns[$i].' '.($_POST['order'][0]['dir'] == 'asc' ? 'asc' : 'desc');
							break;
					}
				}
			}
		}
		if (count($aOrders) > 0) {
			$sOrder = implode(', ', $aOrders);
		}
		if (!empty($sOrder)) {
			$sOrder = "ORDER BY ".$sOrder;
		}

		/*
		 * Where
		 */
		$sWhere = "";
		$aWheres = array();
		if($tanggal_dari != "") $aWheres[] = "DATE({$this->table_def}.tanggal) >= '{$tanggal_dari}'";
		if($tanggal_sampai != "") $aWheres[] = "DATE({$this->table_def}.tanggal) <= '{$tanggal_sampai}'";
		if (count($aWheres) > 0) {
			$sWhere = implode(' AND ', $aWheres);
		}
		if (!empty($sWhere)) {
			$sWhere = "WHERE ".$sWhere;
		}

		$aLikes = array();
		if($_POST['search']['value'] != "") {
			for ($i = 0; $i < count($aColumns); $i++) {
				if($_POST['columns'][$i]['searchable'] == "true") {
					switch ($aColumns[$i]) {
						default:
							$aLikes[] = "{$this->table_def}.{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
							break;
					}
				}
			}
		}

		if (count($aLikes) > 0) {
			$sLike = "(".implode(' OR ', $aLikes).")";
			$sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
		}

		$aSelect = array(
			"{$this->table_def}.id",
			"{$this->table_def}.uid",
			"{$this->table_def}.kode",
			"{$this->table_def}.tanggal",
			"{$this->table_def}.keterangan",
			"CONCAT(created_by.first_name, ' ',created_by.last_name) created_by",
		);
		$list = $this->main->get_all($iLimit, $iOffset, $sWhere, $sOrder, $aSelect);

		$rResult = $list['data'];
		$iFilteredTotal = $list['total_rows'];
		$iTotal = $list['total_rows'];

		/*
		 * Output
		 */
		$output = array(
			"draw" => intval($_POST['draw']),
			"recordsTotal" => $iTotal,
			"recordsFiltered" => $iFilteredTotal,
			"data" => array(),
		);

		$rows = array();
		$i = $iOffset;
		foreach ($rResult as $obj) {
			$data = get_object_vars($obj);
			$data['no'] = ($i+1);
			$rows[] = $data;
			$i++;
		}
		$output['data'] = $rows;

		echo json_encode($output);
	}

	public function save() {
		
		if (!$this->input->is_ajax_request())
			exit();

		$obj = $this->_getDataObject();
		$result = $this->main->create($obj);

		if(!$result) 
			$this->output->set_status_header(500);

		$this->output->set_status_header(200);
		echo json_encode($result);
	}

	public function get_data($uid = 0) {
		if (!$this->input->is_ajax_request())
			exit();

		if ($uid) {
			$obj = $this->main->get_by("WHERE {$this->table_def}.uid = \"{$uid}\"");
			$obj->details = $this->main_detail->get_all(0, 0, "WHERE {$this->table_def_detail}.afkir_id = {$obj->id}")['data'];
		}
		echo json_encode(['data' => $obj]);
	}

	/**
	 * Form Data Object
	 */
	private function _getDataObject() {
		$his = date('H:i:s');
		$obj = new stdClass();
		$obj->id = $this->input->post('id');
		$obj->uid = $this->input->post('uid') && ($this->input->post('uid') != "") ? $this->input->post('uid') : "";
		$obj->tanggal = get_date_accepted_db($this->input->post('tanggal')).' '.$his;
		$obj->keterangan = $this->input->post('keterangan');
		
		$aDetails = array();
		if (isset($_POST['detail_id'])) {
			for ($i = 0; $i < count($_POST['detail_id']); $i++) {
				$detail = new StdClass();
				$detail->id = $_POST['detail_id'][$i];
				$detail->barang_id = $_POST['barang_id'][$i];
				$detail->satuan_id = $_POST['satuan_id'][$i];
				$detail->isi_satuan = $_POST['isi_satuan'][$i];
				$detail->qty = $_POST['qty'][$i];
				$detail->expired_date = $_POST['expired_date'][$i] == "" ? null : get_date_accepted_db($_POST['expired_date'][$i]);
				$detail->keterangan = isset($_POST['keterangan'][$i]) ? $_POST['keterangan'][$i] : null;
				$detail->data_mode    = Data_mode_model::DATA_MODE_ADD;
				$aDetails[uniqid()] = $detail;
			}
		}
		$obj->details = $aDetails;
		return $obj;
	}
}