<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Penerimaan extends CI_Controller 
{
	protected $table_def = "t_gudang_farmasi_penerimaan";
	protected $table_def_detail = "t_gudang_farmasi_penerimaan_detail";
	protected $table_def_po = "t_gudang_farmasi_po";
	protected $table_def_po_detail = "t_gudang_farmasi_po_detail";
	
	function __construct() {
		parent::__construct();
		$this->lang->load('kurs');
		$this->lang->load('barang');
		$this->load->model('gudang_farmasi/Penerimaan_model', 'main');
		$this->load->model('gudang_farmasi/Penerimaan_detail_model', 'main_detail');
		$this->load->model('gudang_farmasi/Po_model', 'po_main');
		$this->load->model('gudang_farmasi/Po_detail_model', 'po_detail');
	}

	/**
	 * Load data
	 */
	public function load_data_po(){
			$sifat  = $_POST['sifat'];
			$pabrik_id  = $_POST['pabrik_id'];
			$status  = $_POST['status'];

			$aColumns = array('kode', 'tanggal', 'pabrik_id', 'sifat', 'status');
			/* 
			 * Paging
			 */
			if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
					$iLimit = intval( $_POST['length'] );
					$iOffset = intval( $_POST['start'] );
			}

			/*
			 * Ordering
			 */
			$sOrder = "";
			$aOrders = array();
			for ($i = 0; $i < count($aColumns); $i++) {
				if($_POST['columns'][$i]['orderable'] == "true") {
					if($i == $_POST['order'][0]['column']) {
						switch ($aColumns[$i]) {
							default:
								$aOrders[] = $this->table_def_po.'.'.$aColumns[$i].' '.($_POST['order'][0]['dir'] == 'asc' ? 'asc' : 'desc');
								break;
						}
					}
				}
			}
			if (count($aOrders) > 0) {
					$sOrder = implode(', ', $aOrders);
			}
			if (!empty($sOrder)) {
					$sOrder = "ORDER BY ".$sOrder;
			}

			/*
			 * Where
			 */
			$sWhere = "";
			$aWheres = array();
			if($sifat != "") $aWheres[] = "{$this->table_def_po}.sifat = '{$sifat}'";
			if($pabrik_id != "") $aWheres[] = "{$this->table_def_po}.pabrik_id = ".base64_decode($pabrik_id);
			if($status != "") { 
				$aWheres[] = "{$this->table_def_po}.status = {$status}";
			} else $aWheres[] = "({$this->table_def_po}.status = {$this->config->item('status_po_waiting_for_delivery')} OR {$this->table_def_po}.status = {$this->config->item('status_po_partial_received')})";
			if (count($aWheres) > 0) {
					$sWhere = implode(' AND ', $aWheres);
			}
			if (!empty($sWhere)) {
					$sWhere = "WHERE ".$sWhere;
			}

			$aLikes = array();
			if($_POST['search']['value'] != "") {
					for ($i = 0; $i < count($aColumns); $i++) {
							if($_POST['columns'][$i]['searchable'] == "true") {
									switch ($aColumns[$i]) {
										default:
											$aLikes[] = "{$this->table_def_po}.{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
											break;
									}
							}
					}
			}

			if (count($aLikes) > 0) {
					$sLike = "(".implode(' OR ', $aLikes).")";
					$sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
			}

			$aSelect = array(
				"{$this->table_def_po}.id",
				"{$this->table_def_po}.uid",
				"{$this->table_def_po}.kode",
				"{$this->table_def_po}.tanggal",
				"{$this->table_def_po}.sifat",
				"{$this->table_def_po}.status",
				"CONCAT(pb.kode, ' - ', pb.nama) pabrik",
				"CONCAT(pengajuan_by.first_name, ' ',pengajuan_by.last_name) pengajuan_by"
			);
			$list = $this->po_main->get_all($iLimit, $iOffset, $sWhere, $sOrder, $aSelect);

			$rResult = $list['data'];
			$iFilteredTotal = $list['total_rows'];
			$iTotal = $list['total_rows'];

			/*
			 * Output
			 */
			$output = array(
					"draw" => intval($_POST['draw']),
					"recordsTotal" => $iTotal,
					"recordsFiltered" => $iFilteredTotal,
					"data" => array(),
			);

			$rows = array();
			$i = $iOffset;
			foreach ($rResult as $obj) {
					$obj->sifat = ucfirst($obj->sifat);
					$obj->status_desc = $this->config->item('status_po')[$obj->status];

					$data = get_object_vars($obj);
					$data['no'] = ($i+1);
					$rows[] = $data;
					$i++;
			}
			$output['data'] = $rows;

			echo json_encode($output);
	}

	public function load_data(){
			$tanggal_dari  = $_POST['tanggal_dari'];
			$tanggal_sampai  = $_POST['tanggal_sampai'];
			$sifat  = $_POST['sifat'];
			$pabrik_id  = $_POST['pabrik_id'];
			$status  = $_POST['status'];

			$aColumns = array('kode', 'kode_po', 'tanggal', 'pabrik_id', 'sifat', 'status');
			/* 
			 * Paging
			 */
			if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
					$iLimit = intval( $_POST['length'] );
					$iOffset = intval( $_POST['start'] );
			}

			/*
			 * Ordering
			 */
			$sOrder = "";
			$aOrders = array();
			for ($i = 0; $i < count($aColumns); $i++) {
				if($_POST['columns'][$i]['orderable'] == "true") {
					if($i == $_POST['order'][0]['column']) {
						switch ($aColumns[$i]) {
							default:
								$aOrders[] = $this->table_def.'.'.$aColumns[$i].' '.($_POST['order'][0]['dir'] == 'asc' ? 'asc' : 'desc');
								break;
						}
					}
				}
			}
			if (count($aOrders) > 0) {
					$sOrder = implode(', ', $aOrders);
			}
			if (!empty($sOrder)) {
					$sOrder = "ORDER BY ".$sOrder;
			}

			/*
			 * Where
			 */
			$sWhere = "";
			$aWheres = array();
			if($tanggal_dari != "") $aWheres[] = "DATE({$this->table_def}.tanggal) >= '{$tanggal_dari}'";
			if($tanggal_sampai != "") $aWheres[] = "DATE({$this->table_def}.tanggal) <= '{$tanggal_sampai}'";
			if($sifat != "") $aWheres[] = "{$this->table_def}.sifat = '{$sifat}'";
			if($pabrik_id != "") $aWheres[] = "{$this->table_def}.pabrik_id = ".base64_decode($pabrik_id);
			if($status != "") $aWheres[] = "{$this->table_def}.status = {$status}";
			if (count($aWheres) > 0) {
					$sWhere = implode(' AND ', $aWheres);
			}
			if (!empty($sWhere)) {
					$sWhere = "WHERE ".$sWhere;
			}

			$aLikes = array();
			if($_POST['search']['value'] != "") {
					for ($i = 0; $i < count($aColumns); $i++) {
							if($_POST['columns'][$i]['searchable'] == "true") {
									switch ($aColumns[$i]) {
										case 'kode_po':
											$aLikes[] = "{$this->table_def_po}.kode LIKE '%".$_POST['search']['value']."%'";
											break;
										default:
											$aLikes[] = "{$this->table_def}.{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
											break;
									}
							}
					}
			}

			if (count($aLikes) > 0) {
					$sLike = "(".implode(' OR ', $aLikes).")";
					$sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
			}

			$aSelect = array(
				"{$this->table_def}.uid",
				"{$this->table_def}.kode",
				"{$this->table_def}.tanggal",
				"{$this->table_def}.sifat",
				"{$this->table_def}.status",
				"{$this->table_def_po}.uid po_uid",
				"{$this->table_def_po}.kode kode_po",
				"CONCAT(pb.kode, ' - ', pb.nama) pabrik",
				"CONCAT(diproses_by.first_name, ' ',diproses_by.last_name) diproses_by"
			);
			$list = $this->main->get_all($iLimit, $iOffset, $sWhere, $sOrder, $aSelect);

			$rResult = $list['data'];
			$iFilteredTotal = $list['total_rows'];
			$iTotal = $list['total_rows'];

			/*
			 * Output
			 */
			$output = array(
					"draw" => intval($_POST['draw']),
					"recordsTotal" => $iTotal,
					"recordsFiltered" => $iFilteredTotal,
					"data" => array(),
			);

			$rows = array();
			$i = $iOffset;
			foreach ($rResult as $obj) {
					$obj->sifat = ucfirst($obj->sifat);
					$obj->status_desc = $this->config->item('status_penerimaan')[$obj->status];

					$data = get_object_vars($obj);
					$data['no'] = ($i+1);
					$rows[] = $data;
					$i++;
			}
			$output['data'] = $rows;

			echo json_encode($output);
	}

	public function get_data($uid = "", $mode = "form") {
		if (!$this->input->is_ajax_request())
			exit();
		
		$obj = $this->po_main->get_by("WHERE {$this->table_def_po}.uid = \"{$uid}\"");
		$obj->sifat_desc = ucfirst($obj->sifat);
		$obj->status_desc = $this->config->item('status_po')[$obj->status];
		$obj->details = $this->po_detail->get_all(0, 0, "WHERE {$this->table_def_po_detail}.po_id = {$obj->id}")['data'];
		foreach ($obj->details as $row) {
			$row->sisa = $row->qty - $row->diterima;
			$row->diskon = 0;
			if($row->harga_diterima <= 0) $row->harga_diterima = $row->harga;
		}

		$history = $this->main->get_all(0, 0, "WHERE {$this->table_def}.po_id = \"{$obj->id}\"")['data'];
		if(count($history) > 0) {
			foreach ($history as $i => $row) {
				$row->details = $this->main_detail->get_all(0, 0, "WHERE {$this->table_def_detail}.penerimaan_id = {$row->id}")['data'];
				foreach ($row->details as $j => $detail) {
					$dataBarang = json_decode($detail->data_barang);

					$detail->expired_date = "-";
					$detail->no_batch = "-";
					$aExpDate = array();
					$aNoBatch = array();
					if(isset($dataBarang)) {
						for ($i = 0; $i < count($dataBarang); $i++) {
							$oData = $dataBarang[$i];
							if(!is_null($oData->expired_date)) 
								$aExpDate[] = date('d-m-Y', strtotime($oData->expired_date))." (".$oData->qty.")";

							if(!is_null($oData->no_batch) || $oData->no_batch != "") 
								$aNoBatch[] = $oData->no_batch." (".$oData->qty.")";
						}
						if (count($aExpDate) > 0) $detail->expired_date = implode(', ', $aExpDate);
						if (count($aNoBatch) > 0) $detail->no_batch = implode(', ', $aNoBatch);
					}
				}
			}
		}
		$obj->history = $history;
		echo json_encode(['data' => $obj]);
	}

	public function save() {
		
		if (!$this->input->is_ajax_request())
			exit();

		$obj = $this->_getDataObject();
		$result = $this->main->create($obj);
		
		if(!$result) 
			$this->output->set_status_header(500);

		$this->output->set_status_header(200);
		echo json_encode($result);
	}

	private function _getDataObject($mode = "form") {
		$his = date('H:i:s');
		$obj = new stdClass();
		$obj->id = 0;
		$obj->uid = "";
		$obj->tanggal = get_date_accepted_db($this->input->post('tanggal')).' '.$his;
		$obj->po_id = $this->input->post('po_id');
		$obj->pabrik_id = $this->input->post('pabrik_id');
		$obj->sifat = $this->input->post('sifat');
		$obj->kurs_id = $this->input->post('kurs_id') ? : null;
		$obj->kurs = $this->input->post('kurs');
		$obj->no_faktur = $this->input->post('no_faktur');
		$obj->keterangan = $this->input->post('keterangan');
		$obj->total = $this->input->post('total');
		$obj->total_kurs = $this->input->post('total_kurs');
		$obj->diskon = $this->input->post('diskon');
		$obj->total_diskon = $this->input->post('total_diskon');
		$obj->total_diskon_kurs = $this->input->post('total_diskon_kurs');
		$obj->total_ppn = $this->input->post('total_ppn');
		$obj->total_ppn_kurs = $this->input->post('total_ppn_kurs');
		$obj->grand_total = $this->input->post('grand_total');
		$obj->grand_total_kurs = $this->input->post('grand_total_kurs');
		
		$total = 0;
		$aDetails = [];
		if (isset($_POST['detail_id'])) {
			for ($i = 0; $i < count($_POST['detail_id']); $i++) {
				$checkBarang = $_POST['check_barang'][$i];
				if($checkBarang) {
					$barangId = $_POST['barang_id'][$i];

					# untuk exp.date dan no_batch
					$aDataBarang = array();
					if (isset($_POST['data_barang_no_batch_'.$barangId])) {
						for ($j = 0; $j < count($_POST['data_barang_no_batch_'.$barangId]); $j++) {
							$data_barang = new StdClass();
							$data_barang->no_batch = $_POST['data_barang_no_batch_'.$barangId][$j];
							$data_barang->expired_date = $_POST['data_barang_expired_date_'.$barangId][$j] == "" ? null : get_date_accepted_db($_POST['data_barang_expired_date_'.$barangId][$j]);
							$data_barang->qty = $_POST['data_barang_qty_'.$barangId][$j];
							$aDataBarang[$j] = $data_barang;
						}    
					}

					$detail = new StdClass();
					$detail->id = 0;
					$detail->po_detail_id = $_POST['po_detail_id'][$i];
					$detail->barang_id = $_POST['barang_id'][$i];
					$detail->satuan_id = $_POST['satuan_id'][$i];
					$detail->isi_satuan = $_POST['isi_satuan'][$i];
					$detail->qty_po = $_POST['qty_po'][$i];
					$detail->qty = $_POST['qty'][$i];
					$detail->sisa = $_POST['sisa'][$i];
					$detail->harga_po = $_POST['harga_po'][$i];
					$detail->harga_po_kurs = $_POST['harga_po'][$i] * $obj->kurs;
					$detail->harga = $_POST['harga'][$i];
					$detail->harga_kurs = $_POST['harga'][$i] * $obj->kurs;
					$detail->diskon = $_POST['diskon_item'][$i] ? : 0;

					$total = $detail->qty * $detail->harga;
					$total_kurs = $total * $obj->kurs;
					$total_diskon = $total * ($detail->diskon / 100);
					$total_diskon_kurs = $total_diskon * $obj->kurs;

					$detail->total = $total - $total_diskon;
					$detail->total_kurs = $total_kurs - $total_diskon_kurs;
					$detail->data_barang = $aDataBarang;
					$detail->data_mode    = Data_mode_model::DATA_MODE_ADD;
					$aDetails[uniqid()] = $detail;
				}
			}
		}
		$obj->details = $aDetails;
		return $obj;
	}

	public function cetak($uid = "")
	{ 
		$obj = $this->main->get_by("WHERE ({$this->table_def}.uid = \"{$uid}\")");
		$obj->sifat_desc = ucfirst($obj->sifat);
		$obj->status_desc = $this->config->item('status_penerimaan')[$obj->status];
		$obj->indo_tanggal = konversi_to_id(date("d M Y", strtotime($obj->tanggal)));
		$obj->details = $this->main_detail->get_all(0, 0, "WHERE ({$this->table_def_detail}.penerimaan_id = ".$obj->id.")", "ORDER BY {$this->table_def_detail}.id ASC")['data'];

		# Get User Pencetak
		$current_user = "";
		$user = $this->user_model->get_by_id($this->auth->userid());
		if($user) {
			$current_user = $user->first_name." ".($user->last_name ? $user->last_name : "");
		}

		$current_date = konversi_to_id(date("d M Y")).' '.date('H:i');
		$data = array(
			'obj' => $obj,
			'current_date' => $current_date,
			'current_user' => $current_user,
		);

		$html = $this->load->view('gudang-farmasi/penerimaan/cetak', $data, TRUE);
		
		# Create PDF
		$mpdf = new \Mpdf\Mpdf([
			'mode' => 'utf-8',
			'format' => 'A4',
			'orientation' => 'P',
		]);
		$mpdf->WriteHTML($html);
		$mpdf->Output('Penerimaan '.$obj->kode.'.pdf', "I");
	}
}