<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User management API.
 * 
 * @package App
 * @category Controller
 * @author Ardi Soebrata
 */
class User extends Admin_Controller 
{
	public function index() 
	{
		echo $this->user_model->datatable();
	}

	public function load_unit_kerja() {
		$this->load->model('master/Unit_kerja_model');

		$unit_usaha_id = $this->input->get('unit_usaha_id');

		$unit_kerja = $this->Unit_kerja_model->dropdown_options(array('unit_usaha_id' => $unit_usaha_id));

		echo json_encode($unit_kerja);
	}
}