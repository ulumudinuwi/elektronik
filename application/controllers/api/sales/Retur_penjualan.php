<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Retur_penjualan extends Admin_Controller 
{
    protected $table_def = "t_pos";
    protected $table_def_detail = "t_pos_detail";
    protected $table_def_sales = "m_sales";
    protected $table_def_member = "m_dokter_member";

    function __construct()
    {
        parent::__construct();
        $this->load->model('sales/Retur_penjualan_model');
        $this->load->model('sales/PosDetail_model');
        //model
    }

    /* Search Invoice */
    public function search_invoice() {
        if (! $this->input->is_ajax_request())
            exit();

        $q = $this->input->get('q');
        $page = $this->input->get('page') ? : 1;
        $data = array();
        $total_rows = 0;
        if(!empty($q)){
            //$this->db->where('status', 1);
            $this->db->like('no_invoice', $q);
            //$this->db->or_like('nama', $q);
            $count = $this->db->select('count(id) as total_rows')
            ->get('t_pos')
            ->row();
            $total_rows = $count->total_rows;

            $aSelect = array(
                't_pos.id',
                't_pos.uid',
                't_pos.no_invoice',
                't_pos.tanggal_transaksi',
                'm_dokter_member.id as id_dokter',
                'm_dokter_member.nama as nama_dokter',
                'm_dokter_member.no_member',
                'm_dokter_member.no_hp',
                'm_dokter_member.alamat',
                'm_sales.nama as nama_sales',
                'm_sales.id as id_sales',
            );
            $this->db->where('t_pos.is_rejected', 0);
            $this->db->where('t_pos.status_lunas', 0);
            if ($this->session->userdata('bpom') == 1) {
                $this->db->where('t_pos.bpom', $this->session->userdata('bpom'));
            }
            $this->db->like('t_pos.no_invoice', $q);
            $this->db->or_like('m_dokter_member.nama', $q);
            $this->db->or_like('m_dokter_member.alamat', $q);
            $this->db->or_like('m_sales.nama', $q);
            $query = $this->db->select(implode(",", $aSelect))
            ->from('t_pos')
            ->join('m_dokter_member', 't_pos.member_id = m_dokter_member.id', 'left')
            ->join('m_sales', 't_pos.sales_id = m_sales.id', 'left')
            ->order_by('t_pos.no_invoice')
            ->limit(20)
            ->get();
            $data = $query->result();
        }
        
        echo json_encode([
            'incomplete_results' => false,
            'items' => $data,
            'total_count' => $total_rows
        ]);
    }

    public function getData($q) {
        if (! $this->input->is_ajax_request())
            exit();

        $uid = $q ? base64_decode($q) : 0;

        if ($uid) {
            $aSelect = array(
                't_pos.id as id',
                't_pos.uid as uid',
                't_pos.no_invoice as no_invoice',
                't_pos.tanggal_transaksi as tanggal_transaksi',
                't_pos.status_lunas as status_lunas',
                't_pos.tgl_jatuh_tempo as tgl_jatuh_tempo',
                't_pos.ongkir',
                't_pos.ongkir_free',
                't_pos.total_pembayaran',
                'm_sales.id as id_sales',
                'm_sales.nama as nama_sales',
                'm_dokter_member.id as id_dokter',
                'm_dokter_member.no_member as no_member_dokter',
                'm_dokter_member.nama as nama_dokter',
                'm_dokter_member.no_hp as no_hp_dokter',
                'm_dokter_member.alamat as alamat_dokter',
                'm_dokter_member.email as email_dokter',
                'm_dokter_member.no_ktp as no_ktp_dokter',
                'm_dokter_member.no_npwp as no_npwp_dokter',
                'm_dokter_member.nama_klinik as nama_klinik_dokter',
                'm_dokter_member.alamat_klinik as alamat_klinik_dokter',
                'm_dokter_member.no_tlp_klinik as no_tlp_klinik_dokter',
                'm_dokter_member.status as status_dokter',
                'm_dokter_member.plafon as plafon_dokter',
                'm_dokter_member.point as point_dokter',
                'm_area.nama as nama_area',
            );

            $query = $this->db->select(implode(",", $aSelect))
            ->join('m_dokter_member', 't_pos.member_id = m_dokter_member.id', 'left')
            ->join('m_area', 'm_area.id = m_dokter_member.area', 'left')
            ->join('m_sales', 't_pos.sales_id = m_sales.id', 'left')
            ->where('t_pos.uid', $uid)
            ->get('t_pos');

            $data = $query->row();
            if ($data) {
                $transaksi = $this->db->select('t_pos_detail.*, m_barang.nama as nama_barang, m_barang.harga_penjualan')
                ->join('m_barang', 'm_barang.id = t_pos_detail.barang_id', 'left')
                ->where('t_pos_detail.retur', 0)
                ->where('t_pos_detail.is_bonus', 0)
                ->where('t_pos_detail.pos_id', $data->id)
                ->get('t_pos_detail')->result();
            }
            $data->detail_transaksi = $transaksi;
        }

        echo json_encode(['data' => $data]);
    }

    public function save() {

        if (!$this->input->is_ajax_request())
            exit();

        $this->db->trans_start();

        $obj = $this->_getDataObject();
        $result = null;
    
        if (isset($obj->id) && $obj->id != "0") { // Update
            $result = $this->Retur_penjualan_model->update($obj);
        } else { // Create
            $result = $this->Retur_penjualan_model->create($obj);
        }

        $this->db->trans_complete();
        if ($this->db->trans_status() === TRUE) {
            $this->db->trans_commit();
        } else {
            $this->db->trans_rollback();
        }
        
        echo json_encode(['action' => 'simpan']);
    }

    private function _getDataObject() {
        //Generate Kode
        $obj = new stdClass();
        $obj->id = $this->input->post('pos_id') && ($this->input->post('pos_id') != "0") ? $this->input->post('pos_id') : 0;
        $obj->uid = $this->input->post('pos_uid') && ($this->input->post('pos_uid') != "0") ? $this->input->post('pos_uid') : 0;
        $obj->grand_total = $this->input->post('grand_total');
        $obj->member_id = $this->input->post('member_id');
        $obj->ongkir = $this->input->post('ongkos_kirim');
        $obj->sales_id = $this->input->post('sales_id');
        $obj->tgl_jatuh_tempo = $this->input->post('tgl_jatuh_tempo');
        $obj->total_harga_before_diskon = $this->input->post('total_harga_barang');
        $obj->total_harga = $this->input->post('total_harga_barang');
        $obj->total_pembayaran = $this->input->post('total_pembayaran');
        $obj->sisa_pembayaran = intVal($this->input->post('grand_total')) - intval($this->input->post('total_pembayaran'));

        $DetailList = $this->PosDetail_model->get_all(0, 0, "WHERE (t_pos_detail.pos_id = {$obj->id}) AND retur = 0");
        $aDetailList = array();
        foreach ($DetailList['data'] as $detail) {
            $detail->data_mode = 3;
            $detail->qty    = $detail->qty;
            $detail->barang_id    = $detail->barang_id;
            $detail->pos_id = $_POST['pos_id'];
            $detail->tanggal_transaksi = $_POST['tanggal_transaksi'];
            $detail->no_invoice = $_POST['no_invoice'];
            $detail->data_barang = json_decode($detail->json_response);
            $detail->retur = 1; 
            $aDetailList[$detail->id] = $detail;
        }

        if (array_key_exists('detail_id', $_POST)) {
            for ($i = 0; $i < count($_POST['detail_id']); $i++) {
                $detail = new stdClass();
                $detail->id = $_POST['detail_id'][$i];
                $detail->uid = $_POST['detail_uid'][$i];
                
                if ($_POST['new_barang_id'][$i] == 0) {
                    $barang_id = $_POST['barang_id'][$i];
                }else{
                    $barang_id = $_POST['new_barang_id'][$i];
                }

                $detail->pos_id = $_POST['pos_id'];
                $detail->tanggal_transaksi = $_POST['tanggal_transaksi'];
                $detail->no_invoice = $_POST['no_invoice'];
                $detail->member_id = $_POST['member_id'];
                $detail->sales_id = $_POST['sales_id'];
                $detail->old_barang_id = $_POST['barang_id'][$i];
                $detail->new_barang_id = $_POST['new_barang_id'][$i];
                $detail->barang_id = $barang_id;
                $detail->harga = $_POST['harga'][$i];
                $detail->old_qty = $_POST['old_qty'][$i];
                $detail->qty = $_POST['qty'][$i];
                $detail->diskon = $_POST['diskon'][$i];
                $detail->harga = $_POST['harga'][$i];
                $detail->total = $_POST['jumlah'][$i];
                $detail->jenis_diskon = 'persen';
                $detail->grand_total = $_POST['jumlah'][$i];

                if (! array_key_exists($detail->id, $aDetailList)) {
                    $detail->no_fbp = get_no_fbp();
                    $detail->data_mode = 1;
                    $aDetailList[uniqid()] = $detail;
                } else {
                    $detail->data_mode = 2;
                    $detail->data_barang = $aDetailList[$detail->id]->data_barang;
                    $aDetailList[$detail->id] = $detail;
                }
            }
        }

        $obj->detail_list = $aDetailList;
        
        // print_r($_POST);exit();
        // print_r($obj);exit();
        
        return $obj;
    }
}