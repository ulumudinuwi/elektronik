<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Mpdf\Mpdf;

class Laporan extends CI_Controller {

    protected $current_user = "Administrator";
    public function __construct()
  	{
    	parent::__construct();
        $this->lang->load('barang');
        $this->load->helper('harga_barang');
        $this->load->model('sales/Laporan_model', 'main');

        if($this->session->has_userdata('first_name')) 
            $this->current_user = $this->session->userdata('first_name')." ".($this->session->userdata('last_name') ? $this->session->userdata('last_name') : "");
  	}

    // LAPORAN 001
  	public function laporan_001(){
        $tanggal_dari  = $_POST['tanggal_dari'];
        $tanggal_sampai  = $_POST['tanggal_sampai'];
        $sales_id  = $_POST['sales_id'];
        $member_id  = $_POST['member_id'];
        $barang_id  = $_POST['barang_id'];

        $aColumns = array('no_fbp', 'no_invoice','tanggal_transaksi', 'qty', 'harga', 'total', 'marketing');
        /* 
        * Paging
        */
        if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
            $iLimit = intval( $_POST['length'] );
            $iOffset = intval( $_POST['start'] );
        }

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if ($this->session->userdata('bpom') == 1) {
            $aWheres[] = "t_pos.bpom = ".$this->session->userdata('bpom');
        }
        if($tanggal_dari != "") $aWheres[] = "DATE(tanggal_transaksi) >= '{$tanggal_dari}'";
        if($tanggal_sampai != "") $aWheres[] = "DATE(tanggal_transaksi) <= '{$tanggal_sampai}'";
        if($sales_id != "") $aWheres[] = "t_pos_detail.sales_id = ".$sales_id;
        if($member_id != "") $aWheres[] = "t_pos_detail.member_id = ".$member_id;
        if($barang_id != "") $aWheres[] = "t_pos_detail.barang_id = ".$barang_id;
        if (is_manager()) {
            $aWheres[] = "m_sales.manager_sales_id = 1";
        }
        $aWheres[] = "t_pos.is_rejected != 1";
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        $aLikes = array();
        if($_POST['search']['value'] != "") {
            for ($i = 0; $i < count($aColumns); $i++) {
                if($_POST['columns'][$i]['searchable'] == "true") {
                    switch ($aColumns[$i]) {
                        default:
                            $aLikes[] = "{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
                            break;
                    }
                }
            }
        }

        if (count($aLikes) > 0) {
            $sLike = "(".implode(' OR ', $aLikes).")";
            $sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
        }

        $list = $this->main->get_all($iLimit, $iOffset, $sWhere, "ORDER BY tanggal_transaksi, t_pos_detail.sales_id ASC");

        $rResult = $list['data'];
        $iFilteredTotal = $list['total_rows'];
        $iTotal = $list['total_rows'];

        /*
        * Output
        */
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iTotal,
            "recordsFiltered" => $iFilteredTotal,
            "data" => array(),
        );

        $rows = array();
        $grandTotal = 0;
        $grandTotalSD = 0;
        $i = $iOffset;
        foreach ($rResult as $obj) {
            $data = get_object_vars($obj);
            $data['no'] = ($i+1);
            $rows[] = $data;
            $grandTotal += $data['total'];
            $grandTotalSD += $data['jumlah'];
            $i++;
        }
        $output['data'] = $rows;
        $output['total'] = $grandTotal;
        $output['total_sd'] = $grandTotalSD;

        echo json_encode($output);
    }

    public function print_001(){
        $mode = $this->input->get("d");

        $tanggal_dari  = $_GET['tanggal_dari'];
        $tanggal_sampai  = $_GET['tanggal_sampai'];
        $sales_id  = $_GET['sales_id'];
        $member_id  = $_GET['member_id']; 
        $barang_id  = $_GET['barang_id']; 

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if ($this->session->userdata('bpom') == 1) {
            $aWheres[] = "t_pos.bpom = ".$this->session->userdata('bpom');
        }
        if($tanggal_dari != "") $aWheres[] = "DATE(tanggal_transaksi) >= '{$tanggal_dari}'";
        if($tanggal_sampai != "") $aWheres[] = "DATE(tanggal_transaksi) <= '{$tanggal_sampai}'";
        if($sales_id != "") $aWheres[] = "t_pos_detail.sales_id = ".$sales_id;
        if($member_id != "") $aWheres[] = "t_pos_detail.member_id = ".$member_id;
        if($barang_id != "") $aWheres[] = "t_pos_detail.barang_id = ".$barang_id;
        if (is_manager()) {
            $aWheres[] = "m_sales.manager_sales_id = 1";
        }
        $aWheres[] = "t_pos.is_rejected != 1";
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;
        if($tanggal_dari == $tanggal_sampai) {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)));
        } else {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)))." s/d ".konversi_to_id(date("d M Y", strtotime($tanggal_sampai)));
        }

        $list = $this->main->get_all(0, 0, $sWhere, "ORDER BY tanggal_transaksi, t_pos_detail.sales_id ASC");
        switch ($mode) {
            case 'excel':
                # Prepare template
                $tpl_filename = 'sales/laporan/laporan-001.xlsx';
                $objPHPExcel = PHPExcel_IOFactory::load('assets/templates/' . $tpl_filename);
                $objPHPExcel->setActiveSheetIndex(0);
                $sheet = $objPHPExcel->getActiveSheet();
                $sheet->setCellValue('B2', "REKAP SALES OPTIME DAN FILORGA");
                $sheet->setCellValue('B3', "PERIODE : ".$periode_date);

                # Apply data rows
                $data = array();
                $no = 1;
                $start = 5;
                $increment = 5;
                $subTotalCells = array();
                if($list['total_rows'] > 0) {
                    foreach ($list['data'] as $i => $row) {
                        $total = $row->total;
                        $grandTotal += $row->total;
                        
                        $obj = new stdClass();
                        //$obj->no = $no;
                        $obj->no_fbp = $row->no_fbp;
                        $obj->no_invoice = $row->no_invoice;
                        $obj->tanggal_transaksi = $row->tanggal_transaksi;
                        $obj->dokter = $row->dokter;
                        $obj->barang = $row->barang ? $row->barang : "-";
                        $obj->marketing = $row->marketing;
                        if ($row->status_lunas == 1) {
                            $obj->status_lunas = 'Lunas';
                        }else{
                            $obj->status_lunas = 'Belum Lunas';
                        }
                        $obj->qty = $row->qty;
                        if ($row->is_bonus == 1) {
                            # code...
                            $obj->harga = 'FREE';
                            $obj->jumlah = 'FREE';
                            $obj->diskon = 'FREE';
                            $obj->total = 'FREE';
                        }else{
                            $obj->harga = $row->harga;
                            $obj->jumlah = $row->jumlah;
                            $obj->diskon = $row->diskon.' '.($row->diskon_plus ? ' + '.$row->diskon_plus : '');
                            $obj->total = $row->total;
                        }
                        $aRow = get_object_vars($obj);
                        //$no++;


                        $colnum = 0;
                        foreach ($aRow as $val) {
                            $sheet->setCellValue(chr(65 + $colnum) . $increment, $val);
                            $colnum++;
                        }
                        $subTotalCells[] = "I{$increment}";
                        $increment++;
                    }
                    
                    # Row Grand Total
                    //$grandTotal = implode('+', $subTotalCells);
                    $sheet->mergeCells("A{$increment}:K{$increment}");
                    $sheet->setCellValue("A{$increment}", "TOTAL");
                    $sheet->setCellValue("L" . ($increment), "=$grandTotal");
                    $sheet->getStyle('A'.$increment.':L'.$increment)->applyFromArray(array(
                        'font' => array(
                            'bold' => true
                        ),
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                    ));
                } else {
                    $sheet->mergeCells("A{$increment}:L{$increment}");
                    $sheet->setCellValue('A'.$increment, "TIDAK ADA DATA");
                    $sheet->getStyle('A'.$increment.':L'.$increment)->applyFromArray(array(
                        'font' => array(
                            'bold' => true
                        ),
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                    ));
                }

                $objPHPExcel->getActiveSheet()->getStyle('A'.$start.':L' . $increment)->applyFromArray(array(
                    'font' => array(
                        'size' => 9
                    ),
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('argb' => 'FF555555'),
                        ),
                    ),
                ));

                $increment++;

                # get current user
                $date_current_user = konversi_to_id(date("d M Y")).", {$this->current_user}";
                $sheet->mergeCells("I{$increment}:L{$increment}");
                $sheet->getStyle('I'.$increment)->applyFromArray(array(
                            'font' => array(
                                'size' => 8,
                                'bold' => true,
                            ),
                            'alignment' => array(
                                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                                'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                            )
                        ));
                $sheet->setCellValue('I'.$increment, $date_current_user);
                $sheet->setSelectedCell("A{$start}");

                # Send excel document
                ob_end_clean();
                header('Content-type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="Rekap Sales.xls"');
                header('Cache-Control: max-age=0');
                header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
                header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
                header('Cache-Control: cache, must-revalidate');
                header('Pragma: public');
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
                $objWriter->save('php://output');
                break;
            case 'pdf':
                $data = array(
                    'rows' => $list['data'],
                    'total_rows' => $list['total_rows'],
                    'current_date' => konversi_to_id(date("d M Y")),
                    'current_user' => $this->current_user,
                    'periode_date' => $periode_date,
                );

                $html = $this->load->view('sales/laporan/laporan-001-pdf', $data, TRUE);
                
                # Create PDF
                $mpdf = new Mpdf([
                    'mode' => 'utf-8',
                    'format' => 'A4-L',
                    'margin_left' => '5',
                    'margin_right' => '5',
                    'margin_top' => '15',
                    'margin_bottom' => '5',]);
                $mpdf->setHTMLHeader('<p style="text-align: right;">HALAMAN: {PAGENO} / {nb}</p>');
                $mpdf->WriteHTML($html);
                $mpdf->Output('Rekap Sales Perbulan.pdf', "I");
                break;
        }
    }

    // LAPORAN 002
    public function laporan_002(){
        $tanggal_dari  = $_POST['tanggal_dari'];
        $tanggal_sampai  = $_POST['tanggal_sampai'];
        $sales_id  = $_POST['sales_id'];
        $member_id  = $_POST['member_id'];
        $barang_id  = $_POST['barang_id'];

        $aColumns = array('no_fbp', 'no_invoice','tanggal_transaksi', 'qty', 'harga', 'total', 'marketing');
        /* 
        * Paging
        */
        if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
            $iLimit = intval( $_POST['length'] );
            $iOffset = intval( $_POST['start'] );
        }

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if ($this->session->userdata('bpom') == 1) {
            $aWheres[] = "t_pos.bpom = ".$this->session->userdata('bpom');
        }
        if($tanggal_dari != "") $aWheres[] = "DATE(tanggal_transaksi) >= '{$tanggal_dari}'";
        if($tanggal_sampai != "") $aWheres[] = "DATE(tanggal_transaksi) <= '{$tanggal_sampai}'";
        if($sales_id != "") $aWheres[] = "t_pos_detail.sales_id = ".$sales_id;
        if($member_id != "") $aWheres[] = "t_pos_detail.member_id = ".$member_id;
        if($barang_id != "") $aWheres[] = "t_pos_detail.barang_id = ".$barang_id;
        
        $aWheres[] = "t_pos.piutang = 1";
        $aWheres[] = "t_pos.is_rejected != 1";

        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        $aLikes = array();
        if($_POST['search']['value'] != "") {
            for ($i = 0; $i < count($aColumns); $i++) {
                if($_POST['columns'][$i]['searchable'] == "true") {
                    switch ($aColumns[$i]) {
                        default:
                            $aLikes[] = "{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
                            break;
                    }
                }
            }
        }

        if (count($aLikes) > 0) {
            $sLike = "(".implode(' OR ', $aLikes).")";
            $sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
        }

        $list = $this->main->get_all($iLimit, $iOffset, $sWhere, "ORDER BY tanggal_transaksi, t_pos_detail.sales_id ASC");

        $rResult = $list['data'];
        $iFilteredTotal = $list['total_rows'];
        $iTotal = $list['total_rows'];

        /*
        * Output
        */
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iTotal,
            "recordsFiltered" => $iFilteredTotal,
            "data" => array(),
        );

        $rows = array();
        $grandTotal = 0;
        $sisaPembayaran = 0;
        $i = $iOffset;
        foreach ($rResult as $obj) {
            $data = get_object_vars($obj);
            $data['no'] = ($i+1);
            $rows[] = $data;
            $grandTotal += $data['jumlah'];
            $sisaPembayaran += $data['sisa_pembayaran'];
            $i++;
        }
        $output['data'] = $rows;
        $output['total'] = $grandTotal;
        $output['total_sd'] = $sisaPembayaran;

        echo json_encode($output);
    }

    public function print_002(){
        $mode = $this->input->get("d");

        $tanggal_dari  = $_GET['tanggal_dari'];
        $tanggal_sampai  = $_GET['tanggal_sampai'];
        $sales_id  = $_GET['sales_id'];
        $member_id  = $_GET['member_id']; 
        $barang_id  = $_GET['barang_id']; 

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if ($this->session->userdata('bpom') == 1) {
            $aWheres[] = "t_pos.bpom = ".$this->session->userdata('bpom');
        }
        if($tanggal_dari != "") $aWheres[] = "DATE(tanggal_transaksi) >= '{$tanggal_dari}'";
        if($tanggal_sampai != "") $aWheres[] = "DATE(tanggal_transaksi) <= '{$tanggal_sampai}'";
        if($sales_id != "") $aWheres[] = "t_pos_detail.sales_id = ".$sales_id;
        if($member_id != "") $aWheres[] = "t_pos_detail.member_id = ".$member_id;
        if($barang_id != "") $aWheres[] = "t_pos_detail.barang_id = ".$barang_id;
        
        $aWheres[] = "t_pos.piutang = 1";
        $aWheres[] = "t_pos.is_rejected != 1";

        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        if($tanggal_dari == $tanggal_sampai) {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)));
        } else {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)))." s/d ".konversi_to_id(date("d M Y", strtotime($tanggal_sampai)));
        }

        $list = $this->main->get_all(0, 0, $sWhere, "ORDER BY tanggal_transaksi, t_pos_detail.sales_id ASC");

        switch ($mode) {
            case 'excel':
                # Prepare template
                $tpl_filename = 'sales/laporan/laporan-002.xlsx';
                $objPHPExcel = PHPExcel_IOFactory::load('assets/templates/' . $tpl_filename);
                $objPHPExcel->setActiveSheetIndex(0);
                $sheet = $objPHPExcel->getActiveSheet();
                $sheet->setCellValue('B2', "LAPORAN PIUTANG");
                $sheet->setCellValue('B3', "PERIODE : ".$periode_date);

                # Apply data rows
                $data = array();
                $no = 1;
                $start = 5;
                $increment = 5;
                $subTotalCells = array();
                if($list['total_rows'] > 0) {
                    foreach ($list['data'] as $i => $row) {
                        $total = $row->total;
                        $grandTotal += $row->total;
                        $grandSisa_pembayaran += $row->sisa_pembayaran;
                        
                        $obj = new stdClass();
                        //$obj->no = $no;
                        $obj->no_fbp = $row->no_fbp;
                        $obj->no_invoice = $row->no_invoice;
                        $obj->tanggal_transaksi = $row->tanggal_transaksi;
                        $obj->tgl_jatuh_tempo = $row->tgl_jatuh_tempo;
                        $obj->dokter = $row->dokter;
                        $obj->barang = $row->barang ? $row->barang : "-";
                        $obj->marketing = $row->marketing;
                        $obj->sisa_pembayaran = $row->sisa_pembayaran;
                        $obj->total = $row->total;
                        $aRow = get_object_vars($obj);
                        //$no++;

                        $colnum = 0;
                        foreach ($aRow as $val) {
                            $sheet->setCellValue(chr(65 + $colnum) . $increment, $val);
                            $colnum++;
                        }
                        $subTotalCells[] = "I{$increment}";
                        $increment++;
                    }
                    
                    # Row Grand Total
                    //$grandTotal = implode('+', $subTotalCells);
                    $sheet->mergeCells("A{$increment}:G{$increment}");
                    $sheet->setCellValue("A{$increment}", "TOTAL");
                    $sheet->setCellValue("H" . ($increment), "=$grandSisa_pembayaran");
                    $sheet->setCellValue("I" . ($increment), "=$grandTotal");
                    $sheet->getStyle('A'.$increment.':I'.$increment)->applyFromArray(array(
                        'font' => array(
                            'bold' => true
                        ),
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                    ));
                } else {
                    $sheet->mergeCells("A{$increment}:I{$increment}");
                    $sheet->setCellValue('A'.$increment, "TIDAK ADA DATA");
                    $sheet->getStyle('A'.$increment.':I'.$increment)->applyFromArray(array(
                        'font' => array(
                            'bold' => true
                        ),
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                    ));
                }

                $objPHPExcel->getActiveSheet()->getStyle('A'.$start.':I' . $increment)->applyFromArray(array(
                    'font' => array(
                        'size' => 9
                    ),
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('argb' => 'FF555555'),
                        ),
                    ),
                ));

                $increment++;

                # get current user
                $date_current_user = konversi_to_id(date("d M Y")).", {$this->current_user}";
                $sheet->mergeCells("H{$increment}:I{$increment}");
                $sheet->getStyle('H'.$increment)->applyFromArray(array(
                            'font' => array(
                                'size' => 8,
                                'bold' => true,
                            ),
                            'alignment' => array(
                                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                                'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                            )
                        ));
                $sheet->setCellValue('H'.$increment, $date_current_user);
                $sheet->setSelectedCell("A{$start}");

                # Send excel document
                ob_end_clean();
                header('Content-type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="Laporan Piutang.xls"');
                header('Cache-Control: max-age=0');
                header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
                header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
                header('Cache-Control: cache, must-revalidate');
                header('Pragma: public');
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
                $objWriter->save('php://output');
                break;
            case 'pdf':
                $data = array(
                    'rows' => $list['data'],
                    'total_rows' => $list['total_rows'],
                    'current_date' => konversi_to_id(date("d M Y")),
                    'current_user' => $this->current_user,
                    'periode_date' => $periode_date,
                );

                $html = $this->load->view('sales/laporan/laporan-002-pdf', $data, TRUE);
                
                # Create PDF
                $mpdf = new Mpdf([
                    'mode' => 'utf-8',
                    'format' => 'A4-L',
                    'margin_left' => '5',
                    'margin_right' => '5',
                    'margin_top' => '15',
                    'margin_bottom' => '5',]);
                $mpdf->setHTMLHeader('<p style="text-align: right;">HALAMAN: {PAGENO} / {nb}</p>');
                $mpdf->WriteHTML($html);
                $mpdf->Output('LAPORAN PIUTANG.pdf', "I");
                break;
        }
    }

    // LAPORAN 003
    public function laporan_003(){
        $tanggal_dari  = $_POST['tanggal_dari'];
        $tanggal_sampai  = $_POST['tanggal_sampai'];
        $sales_id  = $_POST['sales_id'];
        $member_id  = $_POST['member_id'];
        $barang_id  = $_POST['barang_id'];

        $aColumns = array('no_fbp', 'no_invoice','tanggal_transaksi', 'qty', 'harga', 'total', 'marketing');
        /* 
        * Paging
        */
        if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
            $iLimit = intval( $_POST['length'] );
            $iOffset = intval( $_POST['start'] );
        }

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if ($this->session->userdata('bpom') == 1) {
            $aWheres[] = "t_pos.bpom = ".$this->session->userdata('bpom');
        }
        if($tanggal_dari != "") $aWheres[] = "DATE(tanggal_transaksi) >= '{$tanggal_dari}'";
        if($tanggal_sampai != "") $aWheres[] = "DATE(tanggal_transaksi) <= '{$tanggal_sampai}'";
        if($sales_id != "") $aWheres[] = "t_pos_detail.sales_id = ".$sales_id;
        if($member_id != "") $aWheres[] = "t_pos_detail.member_id = ".$member_id;
        if($barang_id != "") $aWheres[] = "t_pos_detail.barang_id = ".$barang_id;

        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        $aLikes = array();
        if($_POST['search']['value'] != "") {
            for ($i = 0; $i < count($aColumns); $i++) {
                if($_POST['columns'][$i]['searchable'] == "true") {
                    switch ($aColumns[$i]) {
                        default:
                            $aLikes[] = "{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
                            break;
                    }
                }
            }
        }

        if (count($aLikes) > 0) {
            $sLike = "(".implode(' OR ', $aLikes).")";
            $sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
        }

        $list = $this->main->get_allMou($iLimit, $iOffset, $sWhere, "ORDER BY tanggal_transaksi, t_diskon_mou.id ASC");

        $rResult = $list['data'];
        $iFilteredTotal = $list['total_rows'];
        $iTotal = $list['total_rows'];

        /*
        * Output
        */
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iTotal,
            "recordsFiltered" => $iFilteredTotal,
            "data" => array(),
        );

        $rows = array();
        $totalDiskon = 0;
        $Diskon = 0;
        $i = $iOffset;
        foreach ($rResult as $obj) {
            $data = get_object_vars($obj);
            $data['no'] = ($i+1);
            $rows[] = $data;
            $totalDiskon += $data['total_diskon'];
            $Diskon += $data['diskon'];
            $i++;
        }
        $output['data'] = $rows;
        $output['total'] = $totalDiskon;
        $output['total_sd'] = $Diskon;

        echo json_encode($output);
    }

    public function print_003(){
        $mode = $this->input->get("d");

        $tanggal_dari  = $_GET['tanggal_dari'];
        $tanggal_sampai  = $_GET['tanggal_sampai'];
        $sales_id  = $_GET['sales_id'];
        $member_id  = $_GET['member_id']; 
        $barang_id  = $_GET['barang_id']; 

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if ($this->session->userdata('bpom') == 1) {
            $aWheres[] = "t_pos.bpom = ".$this->session->userdata('bpom');
        }
        if($tanggal_dari != "") $aWheres[] = "DATE(tanggal_transaksi) >= '{$tanggal_dari}'";
        if($tanggal_sampai != "") $aWheres[] = "DATE(tanggal_transaksi) <= '{$tanggal_sampai}'";
        if($sales_id != "") $aWheres[] = "t_pos_detail.sales_id = ".$sales_id;
        if($member_id != "") $aWheres[] = "t_pos_detail.member_id = ".$member_id;
        if($barang_id != "") $aWheres[] = "t_pos_detail.barang_id = ".$barang_id;

        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        if($tanggal_dari == $tanggal_sampai) {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)));
        } else {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)))." s/d ".konversi_to_id(date("d M Y", strtotime($tanggal_sampai)));
        }

        $list = $this->main->get_allMou(0, 0, $sWhere, "ORDER BY tanggal_transaksi, t_diskon_mou.sales_id ASC");

        switch ($mode) {
            case 'excel':
                # Prepare template
                $tpl_filename = 'sales/laporan/laporan-003.xlsx';
                $objPHPExcel = PHPExcel_IOFactory::load('assets/templates/' . $tpl_filename);
                $objPHPExcel->setActiveSheetIndex(0);
                $sheet = $objPHPExcel->getActiveSheet();
                $sheet->setCellValue('B2', "LAPORAN DISKON MOU MEMBER");
                $sheet->setCellValue('B3', "PERIODE : ".$periode_date);

                # Apply data rows
                $data = array();
                $no = 1;
                $hDiskon = 0;
                $hTotaldiskon = 0;
                $start = 5;
                $increment = 5;
                $subTotalCells = array();
                if($list['total_rows'] > 0) {
                    foreach ($list['data'] as $i => $row) {
                        $hDiskon += $row->diskon;
                        $hTotaldiskon += $row->total_diskon;
                        
                        $obj = new stdClass();
                        //$obj->no = $no;
                        $obj->no_invoice = $row->no_invoice;
                        $obj->tanggal_transaksi = $row->tanggal_transaksi;
                        $obj->dokter = $row->dokter;
                        $obj->barang = $row->barang ? $row->barang : "-";
                        $obj->marketing = $row->marketing;
                        $obj->diskon = $row->diskon;
                        $obj->total_diskon = $row->total_diskon;
                        $aRow = get_object_vars($obj);
                        //$no++;

                        $colnum = 0;
                        foreach ($aRow as $val) {
                            $sheet->setCellValue(chr(65 + $colnum) . $increment, $val);
                            $colnum++;
                        }
                        $subTotalCells[] = "F{$increment}";
                        $increment++;
                    }
                    
                    # Row Grand Total
                    //$hDiskon = implode('+', $subTotalCells);
                    $sheet->mergeCells("A{$increment}:F{$increment}");
                    $sheet->setCellValue("A{$increment}", "TOTAL");
                    $sheet->setCellValue("G" . ($increment), "=$hTotaldiskon");
                    /*$sheet->setCellValue("G" . ($increment), "=$hDiskon");*/
                    $sheet->getStyle('A'.$increment.':G'.$increment)->applyFromArray(array(
                        'font' => array(
                            'bold' => true
                        ),
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                    ));
                } else {
                    $sheet->mergeCells("A{$increment}:G{$increment}");
                    $sheet->setCellValue('A'.$increment, "TIDAK ADA DATA");
                    $sheet->getStyle('A'.$increment.':G'.$increment)->applyFromArray(array(
                        'font' => array(
                            'bold' => true
                        ),
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                    ));
                }

                $objPHPExcel->getActiveSheet()->getStyle('A'.$start.':G' . $increment)->applyFromArray(array(
                    'font' => array(
                        'size' => 9
                    ),
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('argb' => 'FF555555'),
                        ),
                    ),
                ));

                $increment++;

                # get current user
                $date_current_user = konversi_to_id(date("d M Y")).", {$this->current_user}";
                $sheet->mergeCells("F{$increment}:G{$increment}");
                $sheet->getStyle('F'.$increment)->applyFromArray(array(
                            'font' => array(
                                'size' => 8,
                                'bold' => true,
                            ),
                            'alignment' => array(
                                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                                'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                            )
                        ));
                $sheet->setCellValue('F'.$increment, $date_current_user);
                $sheet->setSelectedCell("A{$start}");

                # Send excel document
                ob_end_clean();
                header('Content-type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="Laporan Diskon Mou Member.xls"');
                header('Cache-Control: max-age=0');
                header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
                header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
                header('Cache-Control: cache, must-revalidate');
                header('Pragma: public');
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
                $objWriter->save('php://output');
                break;
            case 'pdf':
                $data = array(
                    'rows' => $list['data'],
                    'total_rows' => $list['total_rows'],
                    'current_date' => konversi_to_id(date("d M Y")),
                    'current_user' => $this->current_user,
                    'periode_date' => $periode_date,
                );

                $html = $this->load->view('sales/laporan/laporan-003-pdf', $data, TRUE);
                
                # Create PDF
                $mpdf = new Mpdf([
                    'mode' => 'utf-8',
                    'format' => 'A4-L',
                    'margin_left' => '5',
                    'margin_right' => '5',
                    'margin_top' => '15',
                    'margin_bottom' => '5',]);
                $mpdf->setHTMLHeader('<p style="text-align: right;">HALAMAN: {PAGENO} / {nb}</p>');
                $mpdf->WriteHTML($html);
                $mpdf->Output('LAPORAN DISKON MOU MEMBER.pdf', "I");
                break;
        }
    }

    // LAPORAN 004
    public function laporan_004(){
        $tanggal_dari  = $_POST['tanggal_dari'];
        $tanggal_sampai  = $_POST['tanggal_sampai'];
        $sales_id  = $_POST['sales_id'];
        $member_id  = $_POST['member_id'];
        $barang_id  = $_POST['barang_id'];

        $aColumns = array('no_fbp', 'no_invoice','tanggal_transaksi', 'qty', 'harga', 'total', 'marketing');
        /* 
        * Paging
        */
        if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
            $iLimit = intval( $_POST['length'] );
            $iOffset = intval( $_POST['start'] );
        }

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if ($this->session->userdata('bpom') == 1) {
            $aWheres[] = "t_pos.bpom = ".$this->session->userdata('bpom');
        }
        if($tanggal_dari != "") $aWheres[] = "DATE(tanggal_transaksi) >= '{$tanggal_dari}'";
        if($tanggal_sampai != "") $aWheres[] = "DATE(tanggal_transaksi) <= '{$tanggal_sampai}'";
        if($sales_id != "") $aWheres[] = "t_pos_detail.sales_id = ".$sales_id;
        if($member_id != "") $aWheres[] = "t_pos_detail.member_id = ".$member_id;
        if($barang_id != "") $aWheres[] = "t_pos_detail.barang_id = ".$barang_id;
        
        $aWheres[] = "t_pos.piutang = 0";
        $aWheres[] = "t_pos.is_rejected != 1";

        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        $aLikes = array();
        if($_POST['search']['value'] != "") {
            for ($i = 0; $i < count($aColumns); $i++) {
                if($_POST['columns'][$i]['searchable'] == "true") {
                    switch ($aColumns[$i]) {
                        default:
                            $aLikes[] = "{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
                            break;
                    }
                }
            }
        }

        if (count($aLikes) > 0) {
            $sLike = "(".implode(' OR ', $aLikes).")";
            $sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
        }

        $list = $this->main->get_all($iLimit, $iOffset, $sWhere, "ORDER BY tanggal_transaksi, t_pos_detail.sales_id ASC");

        $rResult = $list['data'];
        $iFilteredTotal = $list['total_rows'];
        $iTotal = $list['total_rows'];

        /*
        * Output
        */
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iTotal,
            "recordsFiltered" => $iFilteredTotal,
            "data" => array(),
        );

        $rows = array();
        $grandTotal = 0;
        $sisaPembayaran = 0;
        $i = $iOffset;
        foreach ($rResult as $obj) {
            $data = get_object_vars($obj);
            $data['no'] = ($i+1);
            $rows[] = $data;
            $grandTotal += $data['total'];
            $i++;
        }
        $output['data'] = $rows;
        $output['total'] = $grandTotal;

        echo json_encode($output);
    }

    public function print_004(){
        $mode = $this->input->get("d");

        $tanggal_dari  = $_GET['tanggal_dari'];
        $tanggal_sampai  = $_GET['tanggal_sampai'];
        $sales_id  = $_GET['sales_id'];
        $member_id  = $_GET['member_id']; 
        $barang_id  = $_GET['barang_id']; 

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if ($this->session->userdata('bpom') == 1) {
            $aWheres[] = "t_pos.bpom = ".$this->session->userdata('bpom');
        }
        if($tanggal_dari != "") $aWheres[] = "DATE(tanggal_transaksi) >= '{$tanggal_dari}'";
        if($tanggal_sampai != "") $aWheres[] = "DATE(tanggal_transaksi) <= '{$tanggal_sampai}'";
        if($sales_id != "") $aWheres[] = "t_pos_detail.sales_id = ".$sales_id;
        if($member_id != "") $aWheres[] = "t_pos_detail.member_id = ".$member_id;
        if($barang_id != "") $aWheres[] = "t_pos_detail.barang_id = ".$barang_id;
        
        $aWheres[] = "t_pos.piutang = 0";
        $aWheres[] = "t_pos.is_rejected != 1";

        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        if($tanggal_dari == $tanggal_sampai) {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)));
        } else {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)))." s/d ".konversi_to_id(date("d M Y", strtotime($tanggal_sampai)));
        }

        $list = $this->main->get_all(0, 0, $sWhere, "ORDER BY tanggal_transaksi, t_pos_detail.sales_id ASC");

        switch ($mode) {
            case 'excel':
                # Prepare template
                $tpl_filename = 'sales/laporan/laporan-004.xlsx';
                $objPHPExcel = PHPExcel_IOFactory::load('assets/templates/' . $tpl_filename);
                $objPHPExcel->setActiveSheetIndex(0);
                $sheet = $objPHPExcel->getActiveSheet();
                $sheet->setCellValue('B2', "LAPORAN INVOICE LUNAS");
                $sheet->setCellValue('B3', "PERIODE : ".$periode_date);

                # Apply data rows
                $data = array();
                $no = 1;
                $start = 5;
                $increment = 5;
                $subTotalCells = array();
                if($list['total_rows'] > 0) {
                    foreach ($list['data'] as $i => $row) {
                        $total = $row->total;
                        $grandTotal += $row->total;
                        $grandSisa_pembayaran += $row->sisa_pembayaran;
                        
                        $obj = new stdClass();
                        //$obj->no = $no;
                        $obj->no_fbp = $row->no_fbp;
                        $obj->no_invoice = $row->no_invoice;
                        $obj->tanggal_transaksi = $row->tanggal_transaksi;
                        $obj->dokter = $row->dokter;
                        $obj->barang = $row->barang ? $row->barang : "-";
                        $obj->marketing = $row->marketing;
                        $obj->total = $row->total;
                        $aRow = get_object_vars($obj);
                        //$no++;

                        $colnum = 0;
                        foreach ($aRow as $val) {
                            $sheet->setCellValue(chr(65 + $colnum) . $increment, $val);
                            $colnum++;
                        }
                        $subTotalCells[] = "G{$increment}";
                        $increment++;
                    }
                    
                    # Row Grand Total
                    //$grandTotal = implode('+', $subTotalCells);
                    $sheet->mergeCells("A{$increment}:F{$increment}");
                    $sheet->setCellValue("A{$increment}", "TOTAL");
                    $sheet->setCellValue("G" . ($increment), "=$grandTotal");
                    $sheet->getStyle('A'.$increment.':I'.$increment)->applyFromArray(array(
                        'font' => array(
                            'bold' => true
                        ),
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                    ));
                } else {
                    $sheet->mergeCells("A{$increment}:G{$increment}");
                    $sheet->setCellValue('A'.$increment, "TIDAK ADA DATA");
                    $sheet->getStyle('A'.$increment.':G'.$increment)->applyFromArray(array(
                        'font' => array(
                            'bold' => true
                        ),
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                    ));
                }

                $objPHPExcel->getActiveSheet()->getStyle('A'.$start.':G' . $increment)->applyFromArray(array(
                    'font' => array(
                        'size' => 9
                    ),
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('argb' => 'FF555555'),
                        ),
                    ),
                ));

                $increment++;

                # get current user
                $date_current_user = konversi_to_id(date("d M Y")).", {$this->current_user}";
                $sheet->mergeCells("F{$increment}:G{$increment}");
                $sheet->getStyle('F'.$increment)->applyFromArray(array(
                            'font' => array(
                                'size' => 8,
                                'bold' => true,
                            ),
                            'alignment' => array(
                                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                                'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                            )
                        ));
                $sheet->setCellValue('F'.$increment, $date_current_user);
                $sheet->setSelectedCell("A{$start}");

                # Send excel document
                ob_end_clean();
                header('Content-type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="LAPORAN INVOICE LUNAS.xls"');
                header('Cache-Control: max-age=0');
                header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
                header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
                header('Cache-Control: cache, must-revalidate');
                header('Pragma: public');
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
                $objWriter->save('php://output');
                break;
            case 'pdf':
                $data = array(
                    'rows' => $list['data'],
                    'total_rows' => $list['total_rows'],
                    'current_date' => konversi_to_id(date("d M Y")),
                    'current_user' => $this->current_user,
                    'periode_date' => $periode_date,
                );

                $html = $this->load->view('sales/laporan/laporan-004-pdf', $data, TRUE);
                
                # Create PDF
                $mpdf = new Mpdf([
                    'mode' => 'utf-8',
                    'format' => 'A4-L',
                    'margin_left' => '5',
                    'margin_right' => '5',
                    'margin_top' => '15',
                    'margin_bottom' => '5',]);
                $mpdf->setHTMLHeader('<p style="text-align: right;">HALAMAN: {PAGENO} / {nb}</p>');
                $mpdf->WriteHTML($html);
                $mpdf->Output('LAPORAN INVOICE LUNAS.pdf', "I");
                break;
        }
    }

    // LAPORAN 005
    public function laporan_005(){
        $tanggal_dari  = $_POST['tanggal_dari'];
        $tanggal_sampai  = $_POST['tanggal_sampai'];
        $sales_id  = $_POST['sales_id'];
        $member_id  = $_POST['member_id'];

        $aColumns = array('no_fbp', 'no_invoice','tanggal_transaksi', 'qty', 'harga', 'total', 'marketing');
        /* 
        * Paging
        */
        if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
            $iLimit = intval( $_POST['length'] );
            $iOffset = intval( $_POST['start'] );
        }

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if ($this->session->userdata('bpom') == 1) {
            $aWheres[] = "t_pos.bpom = ".$this->session->userdata('bpom');
        }
        if($tanggal_dari != "") $aWheres[] = "DATE(tanggal_transaksi) >= '{$tanggal_dari}'";
        if($tanggal_sampai != "") $aWheres[] = "DATE(tanggal_transaksi) <= '{$tanggal_sampai}'";
        if($sales_id != "") $aWheres[] = "t_pos.sales_id = ".$sales_id;
        if($member_id != "") $aWheres[] = "t_pos.member_id = ".$member_id;
        
        $aWheres[] = "t_pos.is_rejected != 1";

        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        $aLikes = array();
        if($_POST['search']['value'] != "") {
            for ($i = 0; $i < count($aColumns); $i++) {
                if($_POST['columns'][$i]['searchable'] == "true") {
                    switch ($aColumns[$i]) {
                        default:
                            $aLikes[] = "{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
                            break;
                    }
                }
            }
        }

        if (count($aLikes) > 0) {
            $sLike = "(".implode(' OR ', $aLikes).")";
            $sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
        }

        $sWhere = !empty($sWhere) ? $sWhere." GROUP BY t_retur.pos_id " : "GROUP BY t_retur.pos_id ";

        $list = $this->main->get_all_retur($iLimit, $iOffset, $sWhere, "ORDER BY tanggal_transaksi, t_pos.sales_id ASC");

        $rResult = $list['data'];
        $iFilteredTotal = $list['total_rows'];
        $iTotal = $list['total_rows'];

        /*
        * Output
        */
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iTotal,
            "recordsFiltered" => $iFilteredTotal,
            "data" => array(),
        );

        $rows = array();
        $i = $iOffset;
        foreach ($rResult as $obj) {
            $data = get_object_vars($obj);
            $data['no'] = ($i+1);
            $rows[] = $data;
            $i++;
        }
        $output['data'] = $rows;

        echo json_encode($output);
    }

    public function print_005(){
        $mode = $this->input->get("d");

        $tanggal_dari  = $_GET['tanggal_dari'];
        $tanggal_sampai  = $_GET['tanggal_sampai'];
        $sales_id  = $_GET['sales_id'];
        $member_id  = $_GET['member_id']; 

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        if ($this->session->userdata('bpom') == 1) {
            $aWheres[] = "t_pos.bpom = ".$this->session->userdata('bpom');
        }
        if($tanggal_dari != "") $aWheres[] = "DATE(tanggal_transaksi) >= '{$tanggal_dari}'";
        if($tanggal_sampai != "") $aWheres[] = "DATE(tanggal_transaksi) <= '{$tanggal_sampai}'";
        if($sales_id != "") $aWheres[] = "t_pos.sales_id = ".$sales_id;
        if($member_id != "") $aWheres[] = "t_pos.member_id = ".$member_id;
        
        $aWheres[] = "t_pos.is_rejected != 1";

        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        if($tanggal_dari == $tanggal_sampai) {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)));
        } else {
            $periode_date = konversi_to_id(date("d M Y", strtotime($tanggal_dari)))." s/d ".konversi_to_id(date("d M Y", strtotime($tanggal_sampai)));
        }

        $sWhere = !empty($sWhere) ? $sWhere." GROUP BY t_retur.pos_id " : "GROUP BY t_retur.pos_id ";

        $list = $this->main->get_all_retur(0, 0, $sWhere, "ORDER BY tanggal_transaksi, t_pos.sales_id ASC");

        switch ($mode) {
            case 'excel':
                # Prepare template
                $tpl_filename = 'sales/laporan/laporan-005.xlsx';
                $objPHPExcel = PHPExcel_IOFactory::load('assets/templates/' . $tpl_filename);
                $objPHPExcel->setActiveSheetIndex(0);
                $sheet = $objPHPExcel->getActiveSheet();
                $sheet->setCellValue('B2', "LAPORAN RETUR PENJUALAN");
                $sheet->setCellValue('B3', "PERIODE : ".$periode_date);

                # Apply data rows
                $data = array();
                $no = 1;
                $start = 5;
                $increment = 5;
                $subTotalCells = array();
                if($list['total_rows'] > 0) {
                    foreach ($list['data'] as $i => $row) {                        
                        $obj = new stdClass();
                        //$obj->no = $no;
                        $obj->no_invoice = $row->no_invoice;
                        $obj->tanggal_transaksi = $row->tanggal_transaksi;
                        $obj->dokter = $row->dokter;
                        $obj->marketing = $row->marketing;
                        $aRow = get_object_vars($obj);
                        //$no++;

                        $colnum = 0;
                        foreach ($aRow as $val) {
                            $sheet->setCellValue(chr(65 + $colnum) . $increment, $val);
                            $colnum++;
                        }
                        $subTotalCells[] = "G{$increment}";
                        $increment++;
                    }
                    
                    # Row Grand Total
                    //$grandTotal = implode('+', $subTotalCells);
                    //$sheet->mergeCells("A{$increment}:F{$increment}");
                    //$sheet->setCellValue("A{$increment}", "TOTAL");
                    //$sheet->setCellValue("G" . ($increment), "=$grandTotal");
                    $sheet->getStyle('A'.$increment.':I'.$increment)->applyFromArray(array(
                        'font' => array(
                            'bold' => true
                        ),
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                    ));
                } else {
                    $sheet->mergeCells("A{$increment}:D{$increment}");
                    $sheet->setCellValue('A'.$increment, "TIDAK ADA DATA");
                    $sheet->getStyle('A'.$increment.':D'.$increment)->applyFromArray(array(
                        'font' => array(
                            'bold' => true
                        ),
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                    ));
                }

                $objPHPExcel->getActiveSheet()->getStyle('A'.$start.':D' . $increment)->applyFromArray(array(
                    'font' => array(
                        'size' => 9
                    ),
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('argb' => 'FF555555'),
                        ),
                    ),
                ));

                $increment++;

                # get current user
                $date_current_user = konversi_to_id(date("d M Y")).", {$this->current_user}";
                $sheet->mergeCells("C{$increment}:D{$increment}");
                $sheet->getStyle('C'.$increment)->applyFromArray(array(
                            'font' => array(
                                'size' => 8,
                                'bold' => true,
                            ),
                            'alignment' => array(
                                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                                'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                            )
                        ));
                $sheet->setCellValue('C'.$increment, $date_current_user);
                $sheet->setSelectedCell("A{$start}");

                # Send excel document
                ob_end_clean();
                header('Content-type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="LAPORAN RETUR PENJUALAN.xls"');
                header('Cache-Control: max-age=0');
                header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
                header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
                header('Cache-Control: cache, must-revalidate');
                header('Pragma: public');
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
                $objWriter->save('php://output');
                break;
            case 'pdf':
                $data = array(
                    'rows' => $list['data'],
                    'total_rows' => $list['total_rows'],
                    'current_date' => konversi_to_id(date("d M Y")),
                    'current_user' => $this->current_user,
                    'periode_date' => $periode_date,
                );

                $html = $this->load->view('sales/laporan/laporan-005-pdf', $data, TRUE);
                
                # Create PDF
                $mpdf = new Mpdf([
                    'mode' => 'utf-8',
                    'format' => 'A4-P',
                    'margin_left' => '5',
                    'margin_right' => '5',
                    'margin_top' => '15',
                    'margin_bottom' => '5',]);
                $mpdf->setHTMLHeader('<p style="text-align: right;">HALAMAN: {PAGENO} / {nb}</p>');
                $mpdf->WriteHTML($html);
                $mpdf->Output('LAPORAN RETUR PENJUALAN.pdf', "I");
                break;
        }
    }

    public function get_dataRetur() {
        if(!$this->input->is_ajax_request()) 
            exit();

        $id = $this->input->get('id');
        $output = array();
        if($id) {
            $obj = $this->main->get_by_retur("WHERE t_retur.pos_id = '{$id}'");
            $obj->detail = $this->db->select('a.qty, a.old_qty, a.status, b.nama as barang_old, c.nama as barang_new')
                                ->join('m_barang b', 'b.id = a.barang_id', 'left')
                                ->join('m_barang c', 'c.id = a.new_barang_id', 'left')
                                ->where('pos_id', $obj->id)
                                ->get('t_retur a')->result();
        } else {
            $obj = new stdClass();
            $obj->id = 0;
            $obj->id = "";
            $obj->kode = "";
            $obj->nama = "";
            $obj->merk = "";
        }
        $output['data'] = $obj;
        echo json_encode($output);
    }  
}