<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pesan extends Admin_Controller 
{
	protected $page_title = '<i class="fa fa-envelope-o"></i> Pesan';
	protected $table_def = 't_kontak';

	public function __construct()
	{
		parent::__construct();
		$this->lang->load('barang');
        $this->load->model('Pesan_model');
	}

	public function index()
	{
		$this->data['tabUid'] = base64_encode($this->table_def);
		$this->template
			->set_js('plugins/tables/datatables/datatables.min', FALSE)
			->set_js('plugins/notifications/bootbox.min', FALSE)
			->set_js('plugins/notifications/sweet_alert.min', FALSE)
			->set_js('plugins/ui/moment/moment.min', FALSE)
			->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
			->set_script('pesan/script-index')
			->build('pesan/index', $this->data);
	}
}