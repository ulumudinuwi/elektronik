<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Permintaan extends Admin_Controller 
{
  protected $page_title = '<i class="icon-book"></i> Permintaan dan Penerimaan';
  protected $table_def_permintaan = 't_farmasi_permintaan';
  protected $table_def_penerimaan = 't_farmasi_penerimaan';
  protected $unit, $def_uri = 'farmasi/permintaan';

  public function __construct()
  {
    parent::__construct();
    $this->lang->load('barang');
    $this->unit = $this->db->where('status', 1)
                        ->get('m_farmasi_unit')->result();
  }

  public function index()
  {
    $this->data['tab1Uid'] = base64_encode($this->table_def_permintaan);
    $this->data['tab2Uid'] = base64_encode($this->table_def_penerimaan);
    $this->data['unit'] = $this->unit;
    $this->data['sifat'] = $this->config->item('sifat_po');
    $this->data['status'] = $this->config->item('status_request');
    $this->data['status_penerimaan'] = $this->config->item('status_penerimaan');
    $this->data['page_icons'] = '<a href="'. site_url($this->def_uri."/add") .'" class="btn btn-primary btn-labeled"><b><i class="icon-plus-circle2"></i></b>Buat Permintaan</a>';
    $this->template
          ->set_js('plugins/tables/datatables/datatables.min', FALSE)
          ->set_js('plugins/notifications/bootbox.min', FALSE)
          ->set_js('plugins/notifications/sweet_alert.min', FALSE)
          ->set_js('plugins/ui/moment/moment.min', FALSE)
          ->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
          ->set_script($this->def_uri.'/script-index')
          ->build($this->def_uri.'/index', $this->data);
  }

  /**
   * Edit
   */
  function edit($uid)
  {
    $this->_updatedata($uid);
  }
  
  /**
   * Tambah
   */
  function add()
  {
    $this->_updatedata(0);
  }
  
  function _updatedata($uid = 0)
  {
    $this->data['page_title'] = '<i class="icon-book"></i> Permintaan Barang ke Gudang';
    $this->data['uid'] = $uid;
    $this->data['unit'] = $this->unit;
    $this->data['sifat'] = $this->config->item('sifat_po');
    $this->template
          ->set_js('core/libraries/jquery_ui/interactions.min', TRUE)
          ->set_js('core/libraries/jquery_ui/widgets.min', TRUE)
          ->set_js('core/libraries/jquery_ui/effects.min', TRUE)
          ->set_js('plugins/forms/validation/validate.min.js', TRUE)
          ->set_js('plugins/tables/datatables/datatables.min', FALSE)
          ->set_js('plugins/notifications/bootbox.min', FALSE)
          ->set_js('plugins/notifications/sweet_alert.min', FALSE)
          ->set_js('plugins/ui/moment/moment.min', FALSE)
          ->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
          ->set_js('plugins/editors/wysihtml5/wysihtml5.min.js', TRUE)
          ->set_js('plugins/editors/wysihtml5/toolbar.js', TRUE)
          ->set_js('plugins/editors/wysihtml5/parsers.js', TRUE)
          ->set_js(assets_url_version('assets/js/history-barang.js'))
          ->set_script($this->def_uri.'/script-form')
          ->build($this->def_uri.'/form', $this->data);  
  }

  /*
  * Penerimaan
  */
  function penerimaan($uid = 0)
  {
    if($uid === 0) {
      show_404(); exit();
    } 

    $this->data['page_title'] = '<i class="icon-book"></i> Penerimaan Barang dari Gudang';
    $this->data['uid'] = $uid;
    $this->data['unit'] = $this->unit;
    $this->data['sifat'] = $this->config->item('sifat_po');
    $this->template
          ->set_js('core/libraries/jquery_ui/interactions.min', TRUE)
          ->set_js('core/libraries/jquery_ui/widgets.min', TRUE)
          ->set_js('core/libraries/jquery_ui/effects.min', TRUE)
          ->set_js('plugins/forms/validation/validate.min.js', TRUE)
          ->set_js('plugins/tables/datatables/datatables.min', FALSE)
          ->set_js('plugins/notifications/bootbox.min', FALSE)
          ->set_js('plugins/notifications/sweet_alert.min', FALSE)
          ->set_js('plugins/ui/moment/moment.min', FALSE)
          ->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
          ->set_js('plugins/editors/wysihtml5/wysihtml5.min.js', TRUE)
          ->set_js('plugins/editors/wysihtml5/toolbar.js', TRUE)
          ->set_js('plugins/editors/wysihtml5/parsers.js', TRUE)
          ->set_script('farmasi/penerimaan/script-form')
          ->build('farmasi/penerimaan/form', $this->data);  
  }
}