<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Ryuna\Auth;
use Ryuna\Response;
use Carbon\Carbon;
use Illuminate\Database\Capsule\Manager as DB;

class Sales extends Admin_Controller 
{
    protected $page_title = '<i class="icon-users"></i> Sales';
	protected $def_uri = 'master/sales';
    
	public function __construct()
	{
		parent::__construct();
		$this->lang->load('sales'); 
		$this->load->model('master/Sales_model');
		$this->load->model('sales/Target_sales_model');
	}

	/**
	 * 
	 */
	public function index()
	{
		if(is_manager() || is_admin()){
			$this->data['page_icons'] = '<a href="'. site_url($this->def_uri . "/form") .'" class="btn btn-primary btn-labeled"><b><i class="icon-plus-circle2"></i></b>Tambah</a>';
		}else{
			$this->data['page_icons'] = '';
		}
		$this->template
				->set_layout('es6')
				//Plugin
				->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
                ->set_js('plugins/tables/datatables/datatables.min', FALSE)
                ->set_js('plugins/notifications/bootbox.min', FALSE)
                ->set_js('plugins/notifications/sweet_alert.min', FALSE)
                ->set_js('plugins/ui/moment/moment.min', FALSE)
                ->set_js('plugins/buttons/spin.min', FALSE)
				->set_js('plugins/buttons/ladda.min', FALSE)
				->set_js('jquery.mask.min', TRUE)
				//Ryuna Addon
				->set_css('global_a')
				->set_css(script_url('assets/css/pages/master/sales/index.css'))
				->set_js(script_url('assets/js/app/pages/master/sales/index.js'), TRUE)
				//Build
                ->build($this->def_uri . '/index', $this->data);
	}

	/**
	 * 
	 */
	public function form()
	{	
		$bulan = (Int) Carbon::now()->format('m');
		$tahun_now = Carbon::now()->format('Y');
		$array_bulan = [
			'1' => 'JANUARI',
			'2' => 'FEBRUARI',
			'3' => 'MARET',
			'4' => 'APRIL',
			'5' => 'MEI',
			'6' => 'JUNI',
			'7' => 'JULI',
			'8' => 'AGUSTUS',
			'9' => 'SEPTEMBER',
			'10' => 'OKTOBER',
			'11' => 'NOVEMBER',
			'12' => 'DESEMBER',
		];
		$uid = $this->input->get('uid');
		$this->data['method'] = 'store';
		if($uid){
			$this->data['method'] = 'update';
			$this->data['sales'] = (Object) Sales_model::where('uid', $uid)->first();
        	$akun_user = DB::table('auth_users')->where('sales_id',$this->data['sales']->id)->first();
        	if (!$akun_user) {
        		$akun_user = new stdClass();
        		$akun_user->username = '';
        		$akun_user->email = '';
        	}
        	$this->data['akun_user'] = (Object) $akun_user;
			
			$results = Target_sales_model::where('sales_id', $this->data['sales']->id)->orderBy('tahun','DESC')->orderBy('bulan','ASC')->get();
			$resources = [];
			for($i = 0; $i < count($results); $i++){
				$id = $results[$i]['id'];
				$tahun = $results[$i]['tahun'];
				$bulan = $results[$i]['bulan'];
				// if(!array_key_exists($tahun,$resource)){
				$resources[$tahun][$bulan]['id'] = $results[$i]['id'];
				$resources[$tahun][$bulan]['uid'] = $results[$i]['uid'];
				$resources[$tahun][$bulan]['bulan'] = $results[$i]['bulan'];
				$resources[$tahun][$bulan]['tahun'] = $results[$i]['tahun'];
				$resources[$tahun][$bulan]['bulan_label'] = $array_bulan[$results[$i]['bulan']];
				$resources[$tahun][$bulan]['target'] = $results[$i]['target'];
				// }
			}
			$this->data['target_sales'] = $resources;
			$new_target_sales = Target_sales_model::where('sales_id', $this->data['sales']->id)->where('tahun', $tahun_now)->count();
			// dd($new_target_sales);
			$this->data['new_target_sales'] = $new_target_sales == 0 ? TRUE : FALSE;
		}
		// $array_bulan = array_filter($array_bulan, function($value, $index) use($bulan) {
		// 	return $index >= $bulan;
		// }, ARRAY_FILTER_USE_BOTH);
		$this->data['array_bulan'] = $array_bulan;
		$this->data['tahun'] = $tahun_now;
		$this->data['page_icons'] = '<a href="javascript:window.history.go(-1);" class="btn btn-default btn-labeled"><b><i class=" icon-arrow-left8"></i></b>Kembali</a>';
		$this->template
				->set_layout('es6')
				//Plugin
				->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
                ->set_js('plugins/notifications/bootbox.min', FALSE)
                ->set_js('plugins/notifications/sweet_alert.min', FALSE)
                ->set_js('plugins/ui/moment/moment.min', FALSE)
                ->set_js('plugins/buttons/spin.min', FALSE)
				->set_js('plugins/buttons/ladda.min', FALSE)
				->set_js('plugins/forms/styling/uniform.min', TRUE)
				->set_js('plugins/forms/styling/switchery.min', TRUE)
				->set_js('plugins/forms/styling/switch.min', TRUE)
				->set_js('jquery.mask.min', TRUE)
				//Ryuna Addon
				->set_css('global_a')
				->set_css('pages/master/sales/form')
				->set_js('app/pages/master/sales/form', TRUE)
				//Build
                ->build($this->def_uri . '/form', $this->data);
	}

	public function detail()
	{
		Sales_model::redraw_sales();

		$array_bulan = [
			'1' => 'JANUARI',
			'2' => 'FEBRUARI',
			'3' => 'MARET',
			'4' => 'APRIL',
			'5' => 'MEI',
			'6' => 'JUNI',
			'7' => 'JULI',
			'8' => 'AGUSTUS',
			'9' => 'SEPTEMBER',
			'10' => 'OKTOBER',
			'11' => 'NOVEMBER',
			'12' => 'DESEMBER',
		];

		$this->data['tahun'] = Carbon::now()->format('Y');
		$uid = isset($_GET['uid']) ? $_GET['uid'] : '' ;
		if(!$uid){
			$uid = DB::table('m_sales')->where('id', Auth::user()->sales_id)->first()['uid'];
		}

		$sales = (Object) DB::table('m_sales')->where('uid', $uid)->first();
		
		$query = DB::table('m_dokter_member');
		if(is_manager() || is_admin()){
            $get_id_with_sales_bawahan = get_bawahan_sales_id();
            $get_id_with_sales_bawahan[] = $sales->id;
            // var_dump($get_id_with_sales_bawahan);
            if(count($get_id_with_sales_bawahan) > 0) {
                $query->whereIn('sales_id',$get_id_with_sales_bawahan);
            }
        }else if(is_sales()){
            $query->where('sales_id', $sales->id);
        }
		
		// dd($member_sales);
		$member_sales = (Object) $query->get();
		
		// cek_dulu($member_sales);
		$this->data['member_sales'] = $member_sales;
		$results = Target_sales_model::where('sales_id', $sales->id)->orderBy('tahun','DESC')->orderBy('bulan','ASC')->get();
		$resources = [];
		for($i = 0; $i < count($results); $i++){
			$id = $results[$i]['id'];
			$tahun = $results[$i]['tahun'];
			$bulan = $results[$i]['bulan'];
			// if(!array_key_exists($tahun,$resources)){
			$resources[$tahun][$bulan]['id'] = $results[$i]['id'];
			$resources[$tahun][$bulan]['uid'] = $results[$i]['uid'];
			$resources[$tahun][$bulan]['bulan'] = $results[$i]['bulan'];
			$resources[$tahun][$bulan]['tahun'] = $results[$i]['tahun'];
			$resources[$tahun][$bulan]['bulan_label'] = $array_bulan[$results[$i]['bulan']];
			$resources[$tahun][$bulan]['data'] = $results[$i];
			// }
		}
		// dd($resources['2019']['1']['data']);
		$this->data['target_sales'] = $resources;
		$this->data['sales'] = $sales;
		$this->data['page_icons'] = '<a href="javascript:window.history.go(-1);" class="btn btn-default btn-labeled"><b><i class=" icon-arrow-left8"></i></b>Kembali</a>';
		$this->template
				->set_layout('es6')
				//Plugin
				->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
                ->set_js('plugins/tables/datatables/datatables.min', FALSE)
                ->set_js('plugins/notifications/bootbox.min', FALSE)
                ->set_js('plugins/notifications/sweet_alert.min', FALSE)
                ->set_js('plugins/ui/moment/moment.min', FALSE)
                ->set_js('plugins/buttons/spin.min', FALSE)
				->set_js('plugins/buttons/ladda.min', FALSE)
				->set_js('jquery.mask.min', TRUE)
				//Ryuna Addon
				->set_css('global_a')
				->set_css('pages/master/sales/detail')
				->set_js('app/pages/master/sales/detail', TRUE)
				//Build
                ->build($this->def_uri . '/detail', $this->data);
	
	}

	public function insentif()
	{
		$uid = $_GET['uid'];
		$sales = (Object) DB::table('m_sales')->where('uid', $uid)->first();
		$member_sales = DB::table('m_dokter_member')->where('sales_id', $sales->id)->get();
		// cek_dulu($member_sales);
		$this->data['member_sales'] = $member_sales;
		$this->data['page_icons'] = '<a href="javascript:window.history.go(-1);" class="btn btn-default btn-labeled"><b><i class=" icon-arrow-left8"></i></b>Kembali</a>';
		$this->template
				->set_layout('es6')
				//Plugin
				->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
                ->set_js('plugins/tables/datatables/datatables.min', FALSE)
                ->set_js('plugins/notifications/bootbox.min', FALSE)
                ->set_js('plugins/notifications/sweet_alert.min', FALSE)
                ->set_js('plugins/ui/moment/moment.min', FALSE)
                ->set_js('plugins/buttons/spin.min', FALSE)
				->set_js('plugins/buttons/ladda.min', FALSE)
				->set_js('jquery.mask.min', TRUE)
				//Ryuna Addon
				->set_css('global_a')
				->set_css('pages/master/sales/detail')
				->set_js('app/pages/master/sales/detail', TRUE)
				//Build
                ->build($this->def_uri . '/detail', $this->data);
	}

}