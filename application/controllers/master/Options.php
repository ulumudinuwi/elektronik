<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Options extends Admin_Controller {
    public function __construct() {
        parent::__construct();
    }

    public function index()
    {
        $this->data['page_icons'] = '<a href="'. site_url("master/options/form_create") .'" class="btn btn-primary btn-labeled"><b><i class="icon-plus-circle2"></i></b> Tambah</a>';
        $this->data['page_title'] = '<i class="icon-clipboard"></i> Options';
        $this->template
            ->set_js('plugins/tables/datatables/datatables.min', FALSE)
            ->set_js('plugins/notifications/bootbox.min', FALSE)
            ->set_js('plugins/notifications/sweet_alert.min', FALSE)
            ->set_js('plugins/ui/moment/moment.min', FALSE)
            ->set_js('plugins/buttons/spin.min', FALSE)
            ->set_js('plugins/buttons/ladda.min', FALSE)
            ->set_script('master/options/script-index')
            ->build('master/options/index', $this->data);
    }

    public function form_create() {
        $tablesResult = $this->db->query('SHOW TABLES')
            ->result();
        $tables = array();
        foreach ($tablesResult as $row) {
            $temp = array_values(get_object_vars($row));
            $table = array_pop($temp);

            if (preg_match('/^m\_/', $table)) {
                $tables[] = $table;
            }
        }
        
        $this->data['tables'] = $tables;
        $this->data['page_title'] = '<i class="icon-clipboard"></i> Options';
        $this->template
            ->set_js('core/libraries/jquery_ui/interactions.min', TRUE)
            ->set_js('core/libraries/jquery_ui/effects.min', TRUE)
            ->set_js('plugins/forms/validation/validate.min.js', TRUE)
            ->set_js('plugins/tables/datatables/datatables.min', FALSE)
            ->set_js('plugins/notifications/bootbox.min', FALSE)
            ->set_js('plugins/notifications/sweet_alert.min', FALSE)
            ->set_js('plugins/ui/moment/moment.min', FALSE)
            ->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
            ->set_js('plugins/trees/fancytree_all.min', TRUE)
            ->set_js('plugins/trees/fancytree_childcounter', TRUE)
            ->set_js('plugins/editors/wysihtml5/wysihtml5.min', FALSE)
            ->set_js('plugins/editors/wysihtml5/toolbar', FALSE)
            ->set_js('plugins/editors/wysihtml5/parsers', FALSE)
            ->set_js('plugins/editors/wysihtml5/locales/bootstrap-wysihtml5.ua-UA', FALSE)
            ->set_js('plugins/forms/styling/uniform.min', TRUE)
            ->set_js('plugins/forms/styling/switchery.min', TRUE)
            ->set_js('plugins/forms/styling/switch.min', TRUE)
            ->set_script('master/options/script-form-create')
            ->build('master/options/form-create', $this->data);
    }

    public function form_edit($option_id) {
        $tablesResult = $this->db->query('SHOW TABLES')
            ->result();
        $tables = array();
        foreach ($tablesResult as $row) {
            $temp = array_values(get_object_vars($row));
            $table = array_pop($temp);

            if (preg_match('/^m\_/', $table)) {
                $tables[] = $table;
            }

            if (preg_match('/^hrd\_karyawan/', $table)) {
                $tables[] = $table;
            }
        }
        
        $this->data['tables'] = $tables;
        
        $this->data['option_id'] = $option_id;
        $this->data['page_title'] = '<i class="icon-clipboard"></i> Options';
        $this->template
            ->set_js('core/libraries/jquery_ui/interactions.min', TRUE)
            ->set_js('core/libraries/jquery_ui/effects.min', TRUE)
            ->set_js('plugins/forms/validation/validate.min.js', TRUE)
            ->set_js('plugins/tables/datatables/datatables.min', FALSE)
            ->set_js('plugins/notifications/bootbox.min', FALSE)
            ->set_js('plugins/notifications/sweet_alert.min', FALSE)
            ->set_js('plugins/ui/moment/moment.min', FALSE)
            ->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
            ->set_js('plugins/trees/fancytree_all.min', TRUE)
            ->set_js('plugins/trees/fancytree_childcounter', TRUE)
            ->set_js('plugins/editors/wysihtml5/wysihtml5.min', FALSE)
            ->set_js('plugins/editors/wysihtml5/toolbar', FALSE)
            ->set_js('plugins/editors/wysihtml5/parsers', FALSE)
            ->set_js('plugins/editors/wysihtml5/locales/bootstrap-wysihtml5.ua-UA', FALSE)
            ->set_js('plugins/forms/styling/uniform.min', TRUE)
            ->set_js('plugins/forms/styling/switchery.min', TRUE)
            ->set_js('plugins/forms/styling/switch.min', TRUE)
            ->set_script('master/options/script-form-edit')
            ->build('master/options/form-edit', $this->data);
    }
}