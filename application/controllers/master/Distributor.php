<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Distributor extends Admin_Controller 
{
  protected $page_title;
  protected $def_uri = 'master/vendor';

  public function __construct()
  {
    parent::__construct();
    $this->lang->load('barang');
  }

  /**
   * 
   */
  public function index()
  {
    $this->data['page_title'] = '<i class="icon-store"></i> '.$this->lang->line('vendor_label');
    $this->data['page_icons'] = '<a href="'. site_url("master/distributor/form") .'" class="btn btn-primary btn-labeled"><b><i class="icon-plus-circle2"></i></b>Tambah</a>';
    $this->template
          ->set_js('plugins/tables/datatables/datatables.min', FALSE)
          ->set_js('plugins/notifications/bootbox.min', FALSE)
          ->set_js('plugins/notifications/sweet_alert.min', FALSE)
          ->set_js('plugins/ui/moment/moment.min', FALSE)
          ->set_js('plugins/buttons/spin.min', FALSE)
          ->set_js('plugins/buttons/ladda.min', FALSE)
          ->build($this->def_uri . '/index', $this->data);
  }

  public function form($uid = "")
  {
    $this->data['uid'] = $uid;

    if ($uid === "") 
      $this->data['page_title'] = '<i class="icon-store"></i> Tambah '.$this->lang->line('vendor_label');
    else 
      $this->data['page_title'] = '<i class="icon-store"></i> Edit '.$this->lang->line('vendor_label');
    $this->template
      ->set_js('core/libraries/jquery_ui/interactions.min', TRUE)
      ->set_js('core/libraries/jquery_ui/widgets.min', TRUE)
      ->set_js('core/libraries/jquery_ui/effects.min', TRUE)
      ->set_js('plugins/forms/validation/validate.min.js', TRUE)
      ->set_js('plugins/tables/datatables/datatables.min', FALSE)
      ->set_js('plugins/notifications/bootbox.min', FALSE)
      ->set_js('plugins/notifications/sweet_alert.min', FALSE)
      ->set_js('plugins/buttons/spin.min', FALSE)
      ->set_js('plugins/buttons/ladda.min', FALSE)
      ->set_js('plugins/ui/moment/moment.min', FALSE)
      ->set_js('plugins/editors/wysihtml5/wysihtml5.min.js', TRUE)
      ->set_js('plugins/editors/wysihtml5/toolbar.js', TRUE)
      ->set_js('plugins/editors/wysihtml5/parsers.js', TRUE)
      ->set_script($this->def_uri . '/script-form')
      ->build($this->def_uri . '/form', $this->data);
  }

}