<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tarif_pelayanan extends Admin_Controller 
{
	protected $page_title = '<i class="icon-store"></i> Tarif Pelayanan';
	protected $def_uri = 'master/tarif_pelayanan';

	public function __construct()
	{
		parent::__construct();
		$this->load->model('master/Layanan_model');
		$this->load->model('master/Cara_bayar_model');
		$this->load->model('master/Komponen_tarif_model');
	}

	public function index()
	{
		$this->data['page_icons'] = '<a href="'. site_url($this->def_uri . "/form") .'" class="btn btn-primary btn-labeled"><b><i class="icon-plus-circle2"></i></b>Tambah</a>';
		$this->template
			->set_js('plugins/tables/datatables/datatables.min', FALSE)
			->set_js('plugins/notifications/bootbox.min', FALSE)
			->set_js('plugins/notifications/sweet_alert.min', FALSE)
			->set_js('plugins/ui/moment/moment.min', FALSE)
			->set_js('plugins/buttons/spin.min', FALSE)
			->set_js('plugins/buttons/ladda.min', FALSE)
			->build($this->def_uri . '/index', $this->data);
	}

	public function form($uid = "")
	{
		$this->data['uid'] = $uid;
		if ($uid === "") 
			$this->data['page_title'] = '<i class="icon-store"></i> Tambah Tarif Pelayanan';
		else 
			$this->data['page_title'] = '<i class="icon-store"></i> Edit Tarif Pelayanan';
		
		$layananList = $this->Layanan_model->get_all(0, 0, "", "");
		$this->data['layanan_list'] = $layananList["data"];
		
		$caraBayarList = $this->Cara_bayar_model->get_all(0, 0, "", "");
		$this->data['cara_bayar_list'] = $caraBayarList["data"];
		
		$komponenTarifList = $this->Komponen_tarif_model->get_all(0, 0, "", "ORDER BY m_komponen_tarif.ordering ASC");
		$this->data['komponen_tarif_list'] = $komponenTarifList["data"];
		
		$this->template
			->set_js('core/libraries/jquery_ui/interactions.min', TRUE)
			->set_js('core/libraries/jquery_ui/widgets.min', TRUE)
			->set_js('core/libraries/jquery_ui/effects.min', TRUE)
			->set_js('plugins/forms/validation/validate.min.js', TRUE)
			->set_js('plugins/tables/datatables/datatables.min', FALSE)
			->set_js('plugins/notifications/bootbox.min', FALSE)
			->set_js('plugins/notifications/sweet_alert.min', FALSE)
			->set_js('plugins/buttons/spin.min', FALSE)
			->set_js('plugins/buttons/ladda.min', FALSE)
			->set_js('plugins/ui/moment/moment.min', FALSE)
			->set_js('plugins/autoNumeric/autoNumeric-min', FALSE)
			->set_script($this->def_uri . '/script-form')
			->build($this->def_uri . '/form', $this->data);
	}

}