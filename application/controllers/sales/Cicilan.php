<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cicilan extends Admin_Controller 
{
    protected $page_title = '<i class="icon-chart"></i> Pembayaran Cicilan';
	protected $def_uri = 'sales/cicilan';
    
	public function __construct()
	{
		parent::__construct();
		$this->lang->load('sales'); 
	}

	/**
	 * 
	 */
	public function list_pembayaran()
	{
		$this->template
			->set_layout('vue')
			//Plugin
			->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
			->set_js('plugins/tables/datatables/datatables.min', FALSE)
			->set_js('plugins/notifications/bootbox.min', FALSE)
			->set_js('plugins/ui/moment/moment.min', FALSE)
			->set_js('plugins/buttons/spin.min', FALSE)
			->set_js('plugins/buttons/ladda.min', FALSE)
			->set_js('jquery.mask.min', TRUE)
			//Ryuna Addon
			->set_css('global_a')
			->set_css('pages/sales/cicilan/index')
			->set_js('app/pages/sales/cicilan/index', TRUE)
			// ->set_js('app/vue', FALSE)
			//Build
			->build($this->def_uri . '/index');
	}

	/**
	 * 
	 */
	public function detail_cicilan()
	{	
	
		$this->data['page_icons'] = '<a href="javascript:window.history.go(-1);" class="btn btn-default btn-labeled"><b><i class=" icon-arrow-left8"></i></b>Kembali</a>';
        $this->template
				->set_css('global_a')
				->set_css('page/pos')
				->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
                ->set_js('plugins/tables/datatables/datatables.min', FALSE)
                ->set_js('plugins/notifications/bootbox.min', FALSE)
                ->set_js('plugins/notifications/sweet_alert.min', FALSE)
                ->set_js('plugins/ui/moment/moment.min', FALSE)
                ->set_js('plugins/buttons/spin.min', FALSE)
				->set_js('plugins/buttons/ladda.min', FALSE)
				->set_js('app/page/pos', TRUE)
                ->build($this->def_uri.'/detail_cicilan', $this->data);
	}

	public function new_pembayaran()
	{
		$this->data['page_icons'] = '<a href="javascript:window.history.go(-1);" class="btn btn-default btn-labeled"><b><i class=" icon-arrow-left8"></i></b>Kembali</a>';
        $this->template
			->set_layout('vue')
			//Plugin
			->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
			->set_js('plugins/tables/datatables/datatables.min', FALSE)
			->set_js('plugins/notifications/bootbox.min', FALSE)
			->set_js('plugins/ui/moment/moment.min', FALSE)
			->set_js('plugins/buttons/spin.min', FALSE)
			->set_js('plugins/buttons/ladda.min', FALSE)
			->set_js('jquery.mask.min', TRUE)
			//Ryuna Addon
			->set_css('global_a')
			->set_css('pages/sales/cicilan/cicilan_baru')
			->set_js('app/pages/sales/cicilan/cicilan_baru', TRUE)
			// ->set_js('app/vue', FALSE)
			//Build
			->build($this->def_uri . '/cicilan_baru');
    }
    

	public function form($uid = "")
	{
	
	}

}