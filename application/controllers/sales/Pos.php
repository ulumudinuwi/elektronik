<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pos extends Admin_Controller 
{
    protected $page_title = '<i class="icon-chart"></i> Point Of Sales (POS)';
	protected $def_uri = 'sales/pos';
    
	public function __construct()
	{
		parent::__construct();
		$this->lang->load('sales'); 
		$this->load->model('sales/Pos_model');
	}

	/**
	 * 
	 */
	public function transaksi()
	{
		// dd(get_no_invoice());
		$this->template
				->set_layout('vue')
				//Plugin
           		->set_css(bower_url('lightslider/dist/css/lightslider.min.css'))
				->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
                ->set_js('plugins/tables/datatables/datatables.min', FALSE)
                ->set_js('plugins/notifications/bootbox.min', FALSE)
                ->set_js('plugins/ui/moment/moment.min', FALSE)
                ->set_js('plugins/buttons/spin.min', FALSE)
				->set_js('plugins/buttons/ladda.min', FALSE)
				->set_js('jquery.mask.min', TRUE)
				//Ryuna Addon
				->set_css('global_a')
				->set_css(script_url('assets/css/pages/sales/pos/index.css'))
				->set_js(script_url('assets/js/app/pages/sales/pos/index.js'), TRUE)
	            ->set_js(bower_url('lightslider/dist/js/lightslider.min.js'), FALSE)
				// ->set_js('app/vue', FALSE)
				//Build
                ->build($this->def_uri . '/index');
	}

	public function list_transaksi()
	{
		// dd(get_no_invoice());
		$this->template
				->set_layout('vue')
				//Plugin
				->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
                ->set_js('plugins/tables/datatables/datatables.min', FALSE)
                ->set_js('plugins/notifications/bootbox.min', FALSE)
                ->set_js('plugins/ui/moment/moment.min', FALSE)
                ->set_js('plugins/buttons/spin.min', FALSE)
				->set_js('plugins/buttons/ladda.min', FALSE)
				->set_js('jquery.mask.min', TRUE)
				//Ryuna Addon
				->set_css('global_a')
				->set_css('pages/sales/pos/list')
				->set_js('app/pages/sales/pos/list', TRUE)
				// ->set_js('app/vue', FALSE)
				//Build
                ->build($this->def_uri . '/list');
	}

	public function list_approval()
	{
		// dd(get_no_invoice());
		$this->template
				->set_layout('vue')
				//Plugin
				->set_js('plugins/autoNumeric/autoNumeric-min', FALSE)
                ->set_js('plugins/tables/datatables/datatables.min', FALSE)
                ->set_js('plugins/notifications/bootbox.min', FALSE)
                ->set_js('plugins/ui/moment/moment.min', FALSE)
                ->set_js('plugins/buttons/spin.min', FALSE)
				->set_js('plugins/buttons/ladda.min', FALSE)
				->set_js('jquery.mask.min', TRUE)
				//Ryuna Addon
				->set_css('global_a')
				->set_css('pages/sales/pos/list_approval')
				->set_js('app/pages/sales/pos/list_approval', TRUE)
				// ->set_js('app/vue', FALSE)
				//Build
                ->build($this->def_uri . '/list_approval');
	}

	/**
	 * 
	 */
	public function history_detail()
	{	
		$type = $this->input->get('type');
		
		if(!isset($type)) abort(404);

		if($type == "point_of_sales_online"){
			$url_view = $this->def_uri . '/history_detail_1';
		}else if($type == "website" || $type == "mobile"){
			$url_view = $this->def_uri . '/history_detail_2';
		}else{
			abort(404);
		}

		$this->data['page_icons'] = '<a href="javascript:window.history.go(-1);" class="btn btn-default btn-labeled"><b><i class=" icon-arrow-left8"></i></b>Kembali</a>';
        $this->template
				->set_css('global_a')
				->set_css('page/pos')
				->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
                ->set_js('plugins/tables/datatables/datatables.min', FALSE)
                ->set_js('plugins/notifications/bootbox.min', FALSE)
                ->set_js('plugins/ui/moment/moment.min', FALSE)
                ->set_js('plugins/buttons/spin.min', FALSE)
				->set_js('plugins/buttons/ladda.min', FALSE)
				->set_js('app/page/pos', TRUE)
                ->build($url_view, $this->data);
	}

	// public function pos_create()
	// {
	// 	$this->data['page_icons'] = '<a href="javascript:window.history.go(-1);" class="btn btn-default btn-labeled"><b><i class=" icon-arrow-left8"></i></b>Kembali</a>';
    //     $this->template
	// 			->set_css('global_a')
	// 			->set_css('page/pos')
	// 			->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
    //             ->set_js('plugins/tables/datatables/datatables.min', FALSE)
    //             ->set_js('plugins/notifications/bootbox.min', FALSE)
    //             ->set_js('plugins/notifications/sweet_alert.min', FALSE)
    //             ->set_js('plugins/ui/moment/moment.min', FALSE)
    //             ->set_js('plugins/buttons/spin.min', FALSE)
	// 			->set_js('plugins/buttons/ladda.min', FALSE)
	// 			->set_js('app/page/pos', TRUE)
    //             ->build($this->def_uri . '/pos_create', $this->data);
	// }

	public function approve_manager()
	{
		$this->data['page_icons'] = '<a href="javascript:window.history.go(-1);" class="btn btn-default btn-labeled"><b><i class=" icon-arrow-left8"></i></b>Kembali</a>';
        $this->template
				->set_css('global_a')
				->set_css('page/pos')
				->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
                ->set_js('plugins/tables/datatables/datatables.min', FALSE)
                ->set_js('plugins/notifications/bootbox.min', FALSE)
                ->set_js('plugins/ui/moment/moment.min', FALSE)
                ->set_js('plugins/buttons/spin.min', FALSE)
				->set_js('plugins/buttons/ladda.min', FALSE)
				->set_js('app/page/pos', TRUE)
                ->build($this->def_uri . '/pos_create', $this->data);
	}

	public function detail_transaksi()
	{
		$request = request_handler();
		$found = Pos_model::where('uid',$request->uid)->first();
		if($request && !isset($request->uid)) abort('404', 'Halaman Tidak Tersedia'); 
		if(!$found) abort('404', 'Data dengan UID tersebut tidak ditemukan');
		$this->template
			->set_layout('vue')
			//Plugin
			->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
			->set_js('plugins/tables/datatables/datatables.min', FALSE)
			->set_js('plugins/notifications/bootbox.min', FALSE)
			->set_js('plugins/ui/moment/moment.min', FALSE)
			->set_js('plugins/buttons/spin.min', FALSE)
			->set_js('plugins/buttons/ladda.min', FALSE)
			->set_js('jquery.mask.min', TRUE)
			//Ryuna Addon
			->set_css('global_a')
			->set_css('pages/sales/pos/index')
			->set_js('app/pages/sales/pos/detail_transaksi', TRUE)
			//Build
			->build($this->def_uri . '/detail_transaksi');
	}

}