<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Ryuna\Auth;
use Ryuna\Response;
use Carbon\Carbon;
use Illuminate\Database\Capsule\Manager as DB;

class Insentif extends Admin_Controller 
{
    protected $page_title = '<i class="icon-users"></i> Sales';
	protected $def_uri = 'master/sales';
    
	public function __construct()
	{
		parent::__construct();
		$this->lang->load('sales'); 
		$this->load->model('master/Sales_model');
		$this->load->model('sales/Target_sales_model');
	}

	/**
	 * 
	 */
    public function index()
	{
		Sales_model::redraw_sales();

		$array_bulan = [
			'1' => 'JANUARI',
			'2' => 'FEBRUARI',
			'3' => 'MARET',
			'4' => 'APRIL',
			'5' => 'MEI',
			'6' => 'JUNI',
			'7' => 'JULI',
			'8' => 'AGUSTUS',
			'9' => 'SEPTEMBER',
			'10' => 'OKTOBER',
			'11' => 'NOVEMBER',
			'12' => 'DESEMBER',
		];

		$this->data['tahun'] = Carbon::now()->format('Y');
		$uid = isset($_GET['uid']) ? $_GET['uid'] : '' ;
		if(!$uid){
			$uid = DB::table('m_sales')->where('id', Auth::user()->sales_id)->first()['uid'];
		}

		$sales = (Object) DB::table('m_sales')->where('uid', $uid)->first();
		
		$query = DB::table('m_dokter_member');
		if(is_manager()){
            $get_id_with_sales_bawahan = get_bawahan_sales_id();
            $get_id_with_sales_bawahan[] = sales_id();
            // var_dump($get_id_with_sales_bawahan);
            if(count($get_id_with_sales_bawahan) > 0) {
                $query->whereIn('sales_id',$get_id_with_sales_bawahan);
            }
        }else if(is_sales()){
            $query->where('sales_id', sales_id());
        }
		
		$member_sales = (Object) $query->get();
		// dd($query->toSql());
		// cek_dulu($member_sales);
		$this->data['member_sales'] = $member_sales;
		$results = Target_sales_model::where('sales_id', $sales->id)->orderBy('tahun','DESC')->orderBy('bulan','ASC')->get();
		$resources = [];
		for($i = 0; $i < count($results); $i++){
			$id = $results[$i]['id'];
			$tahun = $results[$i]['tahun'];
			$bulan = $results[$i]['bulan'];
			// if(!array_key_exists($tahun,$resource)){
			$resource[$tahun][$bulan]['id'] = $results[$i]['id'];
			$resource[$tahun][$bulan]['uid'] = $results[$i]['uid'];
			$resource[$tahun][$bulan]['bulan'] = $results[$i]['bulan'];
			$resource[$tahun][$bulan]['tahun'] = $results[$i]['tahun'];
			$resource[$tahun][$bulan]['bulan_label'] = $array_bulan[$results[$i]['bulan']];
			$resource[$tahun][$bulan]['data'] = $results[$i];
			// }
		}
		// dd($resource['2019']['1']['data']);
		$this->data['target_sales'] = $resource;
		$this->data['page_icons'] = '<a href="javascript:window.history.go(-1);" class="btn btn-default btn-labeled"><b><i class=" icon-arrow-left8"></i></b>Kembali</a>';
		$this->template
				->set_layout('es6')
				//Plugin
				->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
                ->set_js('plugins/tables/datatables/datatables.min', FALSE)
                ->set_js('plugins/notifications/bootbox.min', FALSE)
                ->set_js('plugins/notifications/sweet_alert.min', FALSE)
                ->set_js('plugins/ui/moment/moment.min', FALSE)
                ->set_js('plugins/buttons/spin.min', FALSE)
				->set_js('plugins/buttons/ladda.min', FALSE)
				->set_js('jquery.mask.min', TRUE)
				//Ryuna Addon
				->set_css('global_a')
				->set_css('pages/master/sales/detail')
				->set_js('app/pages/master/sales/detail', TRUE)
				//Build
                ->build($this->def_uri . '/detail', $this->data);
	}

}