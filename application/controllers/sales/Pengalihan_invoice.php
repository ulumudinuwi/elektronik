<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pengalihan_invoice extends Admin_Controller 
{
    protected $page_title = '<i class="icon-chart"></i> Pengalihan Invoice';
	protected $def_uri = 'sales/pengalihan_invoice';
    
	public function __construct()
	{
		parent::__construct();
		$this->lang->load('sales'); 
		$this->load->model('sales/Pos_model');
	}

	public function daftar_invoice()
	{
		// dd(get_no_invoice());
		$this->template
				->set_layout('vue')
				//Plugin
				->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
                ->set_js('plugins/tables/datatables/datatables.min', FALSE)
                ->set_js('plugins/notifications/bootbox.min', FALSE)
                ->set_js('plugins/ui/moment/moment.min', FALSE)
                ->set_js('plugins/buttons/spin.min', FALSE)
				->set_js('plugins/buttons/ladda.min', FALSE)
				->set_js('jquery.mask.min', TRUE)
				//Ryuna Addon
				->set_css('global_a')
				->set_css('pages/sales/pos/list')
				->set_js('app/pages/sales/pengalihan_invoice/daftar_invoice', TRUE)
				// ->set_js('app/vue', FALSE)
				//Build
                ->build($this->def_uri . '/daftar_invoice');
	}

	public function list_approval()
	{
		// dd(get_no_invoice());
		$this->template
				->set_layout('vue')
				//Plugin
				->set_js('plugins/autoNumeric/autoNumeric-min', FALSE)
                ->set_js('plugins/tables/datatables/datatables.min', FALSE)
                ->set_js('plugins/notifications/bootbox.min', FALSE)
                ->set_js('plugins/ui/moment/moment.min', FALSE)
                ->set_js('plugins/buttons/spin.min', FALSE)
				->set_js('plugins/buttons/ladda.min', FALSE)
				->set_js('jquery.mask.min', TRUE)
				//Ryuna Addon
				->set_css('global_a')
				->set_css('pages/sales/pos/list_approval')
				->set_js('app/pages/sales/pengalihan_invoice/list_approval', TRUE)
				// ->set_js('app/vue', FALSE)
				//Build
                ->build($this->def_uri . '/list_approval');
	}

	public function detail_transaksi()
	{
		$request = request_handler();
		$found = Pos_model::where('uid',$request->uid)->first();
		if($request && !isset($request->uid)) abort('404', 'Halaman Tidak Tersedia'); 
		if(!$found) abort('404', 'Data dengan UID tersebut tidak ditemukan');
		$this->template
			->set_layout('vue')
			//Plugin
			->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
			->set_js('plugins/tables/datatables/datatables.min', FALSE)
			->set_js('plugins/notifications/bootbox.min', FALSE)
			->set_js('plugins/ui/moment/moment.min', FALSE)
			->set_js('plugins/buttons/spin.min', FALSE)
			->set_js('plugins/buttons/ladda.min', FALSE)
			->set_js('jquery.mask.min', TRUE)
			//Ryuna Addon
			->set_css('global_a')
			->set_css('pages/sales/pos/index')
			->set_js('app/pages/sales/pengalihan_invoice/detail_transaksi', TRUE)
			//Build
			->build($this->def_uri . '/detail_transaksi');
	}

}