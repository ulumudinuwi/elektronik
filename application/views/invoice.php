<html>
<body>
<style>
body {
    line-height: 1.2em;
    font-size: 12px;
    font-family: 'Arial, Helvetica', sans-serif;
    font-weight: bold;
}
h1, h2, h3, h4, h5, h6 {
    font-family: inherit;
    font-weight: 400;
    line-height: 1.5384616;
    color: inherit;
    margin-top: 0;
    margin-bottom: 5px;
    text-align: center;
}
h1 {
    font-size: 28px;
}
h2 {
    font-size: 20px;
}
h3 {
    font-size: 18px;
}
h4 {
    font-size: 16px;
}
h5 {
    font-size: 14px;
}
h6 {
    font-size: 10px;
}
table {
    border-collapse: collapse;
    font-size: 12px;
}
.table {
    border-spacing: 0;
    width: 100%;
    font-size: 12px;
}
.table thead th,
.table tbody td {
    vertical-align: middle;
    padding: 5px 10px;
    line-height: 1.5384616;
}
.table thead th {
    color: #fff;
    background-color: #607D8B;
    font-weight: bold;
    text-align: center;
}
.text-center {
    text-align: center;
}
.text-left {
    text-align: left;
}
.text-right {
    text-align: right;
}
p {
    margin: 0;
}

.text-lvl-0 {
    font-size: 1.1em;
    font-weight: bold;
}
.text-lvl-1 {
    font-size: 1.0em;
    font-weight: bold;
}
.text-lvl-2 {
    font-size: 0.9em;
}
.text-lvl-3 {
    font-size: 0.9em;
}
.text-bold {
    font-weight: bold;
}
.text-danger {
    color: red;
}
.pr10 {
    padding-left: 10px;
}

#table_header td {
    padding-top: ;
    padding-bottom: 0;
    margin-top: 0;
    margin-bottom: 0px;
}
 #header{
    font-size: 14px;
    text-transform: uppercase;
    font-weight: bold;
 }
.text-muted {
    color: #a9a9a9;
}
</style>
    <body>
    <table id="table_header">
        <?php if ($list->dataDetail[0]->bpom == 0): ?>
            <tr>
                <td id="header">
                    <img src="<?php echo image_url('logo_prima.png') ?>" alt="" style="height: 60px">
                </td>
                <td id="header">
                    <!-- PT.PRIMA ESTETIKA RAKSA  -->
                    PT. PRIME AESTHETIC
                    <br>
                    FAKTUR PENJUALAN
                </td>
            </tr>
        <?php else: ?>
            <tr>
                <td id="header">
                    <img src="<?php echo image_url('logo_per.jpg') ?>" alt="" style="height: 40px">
                </td>
                <td id="header">
                    PT. PRIMA ESTETIKA RAKSA 
                    <br>
                    FAKTUR PENJUALAN
                </td>
            </tr>
        <?php endif ?>
    </table>

    <br>
    <table style="width: 100%;">
        <tr>
            <td valign="top" width="15%;">NAMA DOKTER</td>
            <td valign="top" width="1%;">:</td>
            <td valign="top" width="35%;"><?php echo $list->dataMember->nama ?> </td>
            <td valign="top" style="width: 2%;">&nbsp;</td>
            <td valign="top" width="13%;">NO.FAKTUR</td>
            <td valign="top" width="1%;">:</td>
            <td valign="top" width="23%;"><?php echo $list->no_invoice ?></td>
        </tr>
        <tr>
            <td valign="top">TELEPON / Klinik</td>
            <td valign="top">:</td>
            <td valign="top"><?php echo $list->dataMember->no_hp ?>  </td>
            <td valign="top" style="width: 2%;">&nbsp;</td>
            <td valign="top">TANGGAL</td>
            <td valign="top">:</td>
            <td valign="top"><?php echo $list->tanggal_transaksi ?></td>
        </tr>
        <tr>
            <td valign="top" >ALAMAT</td>
            <td valign="top">:</td>
            <td valign="top"><?php echo $list->dataMember->alamat ?> </td>    
        </tr>
    </table>

    <br/>

    <table class="table">
        <tr>
            <th class="text-bold" style="width: 5%;border-top: 1px solid #000; border-bottom: 1px solid #000;">NO</th>
            <td class="text-bold" style="border-top: 1px solid #000; border-bottom: 1px solid #000;">NAMA BARANG</td>
            <th class="text-bold" style="width: 5%;border-top: 1px solid #000; border-bottom: 1px solid #000;">QTY</th>
            <th class="text-bold" style="width: 20%;border-top: 1px solid #000; border-bottom: 1px solid #000;">HARGA (Rp.)</th>
            <th class="text-bold" style="width: 20%;border-top: 1px solid #000; border-bottom: 1px solid #000;">DISC(%)</th>
            <th class="text-bold" style="width: 20%;border-top: 1px solid #000; border-bottom: 1px solid #000;">TOTAL (Rp.)</th>
        </tr>
        <?php 
            $no = 1; 
            $totalharga = 0;
        ?>
        <?php foreach($list->dataDetail as $key => $data) { ?>
            <?php $style = ( $key != count( $list->dataDetail ) -1 ) ? '' : 'border-bottom: 1px solid #000;'; ?>
            <tr>
                <?php 
                    $totalharga += $data->total;
                ?>
                <td class="text-center" style=""><?php echo $no ?></td>
                <td style=""><?php echo $data->nama ?></td>
                <td class="text-center" style=""><?php echo $data->qty ?></td>
                <td class="text-right" style=" padding-right: 20px"><?php echo toRupiah($data->harga); ?></td>
                <td class="text-center" style=""><?php echo $data->diskon; ?> %</td>
                <td class="text-right" style=" padding-right: 20px"><?php echo toRupiah($data->total); ?></td>
                <?php
                    $no++;
                ?>
            </tr>
        <?php } ?>

        <tr>
            <td colspan="4" valign="top"></td>
            <td class="text-left">SUB TOTAL</td>
            <td class="text-right" style="padding-right: 20px"><?php echo toRupiah($totalharga); ?> </td>
        </tr>
        <tr>
            <td colspan="4" valign="top"></td>
            <td class="text-left">ONGKIR</td>
            <td class="text-right" style="padding-right: 20px"><?php echo toRupiah($list->ongkir); ?> </td>
        </tr>
        <?php 
            // if ($list->dataDetail[0]->bpom == 0): 
                // $grandTotal = $data->total + ( $data->total * (10 / 100));
        ?>
        <!-- <tr>
            <td colspan="4" valign="top"></td>
            <td class="text-left text-bold">PPN (10.00 %)</td>
            <td class="text-right text-bold" style="padding-right: 20px"><?php echo toRupiah($data->total * (10 / 100)); ?></td>
        </tr> -->
        <?php 
            // else: 
                $grandTotal = $data->total;
            // endif;
        ?>
        <tr>
            <td colspan="4" valign="top"></td>
            <td class="text-left text-bold">TOTAL</td>
            <td class="text-right text-bold" style="padding-right: 20px"><?php echo toRupiah($grandTotal); ?></td>
        </tr>
    </table>

    </body>
</html>