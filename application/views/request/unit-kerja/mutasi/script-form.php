<script>
var UID = "<?php echo $uid; ?>",
    form = '#form';

var url = {
  index: "<?php echo site_url('request/mutasi_unit'); ?>",
  save: "<?php echo site_url('api/request/unit_kerja/mutasi/save'); ?>",
  getData: "<?php echo site_url('api/request/unit_kerja/mutasi/get_data/:UID'); ?>",
  loadDataBarang: "<?php echo site_url('api/logistik/stock/load_data_modal?mode=request-unit'); ?>",
};

var tableDetail = $('#table_detail'),
    tableDetailDt,
    curRowBarang = null;

/* variabel di modal */
var tableBarang = $('#table-barang'),
    tableBarangDt,
    modalBarang = $('#list-barang-modal');

function fillForm(uid) {
  tableDetailDt = tableDetail.DataTable({
    "info": false,
    "ordering": false,
    "paginate": false,
    "drawCallback": function (settings) {
      $('.disp_qty').autoNumeric('init', {aSep: '.', aDec: ',', mDec: '2'});

      let tr = tableDetail.find("tbody tr");
      tr.each(function() {
        let id = $(this).find('.input-barang_id').val();
        $(this).prop('id', "row-barang-" + id);
      });
    },
  });

  blockElement($(form));
  $.getJSON(url.getData.replace(':UID', uid), function(data, status) {
    if (status === 'success') {
      data = data.data;
      $("#unitkerja_id").val(data.unitkerja_id).trigger('change');
      if(data.unitkerja_id !== null) $("#unitkerja_id").prop('disabled', true);
      if(parseInt(UID) === 0) {
        $("#label_pemohon").html(data.label_pemohon);
        $("#pemohon").val(data.label_pemohon);
        $(form).unblock();
        return;
      }

      $("#id").val(data.id);
      $("#uid").val(data.uid);
      $("#kode").val(data.kode);
      $("#pemohon").val(data.pemohon);
      $("#label_pemohon").html(data.pemohon);
      $("#tanggal").data("daterangepicker").setStartDate(moment(data.tanggal).isValid() ? moment(data.tanggal) : moment())
      $("#tanggal").data("daterangepicker").setEndDate(moment(data.tanggal).isValid() ? moment(data.tanggal) : moment());
      $("#keterangan").data("wysihtml5").editor.setValue(data.keterangan);
      
      for (var i = 0; i < data.details.length; i++) {
        columnBarang(data.details[i]);
      }
      $(form).unblock();
    }
  });
}

function columnBarang(data) {
  tableDetailDt.row.add([
    // Col 1
    `<input type="hidden" class="input-detail_id" name="detail_id[]" value="${data.id}">` +
    `<input type="hidden" class="input-barang_id" name="barang_id[]" value="${data.barang_id}">` +
    `<div class="input-group">` +
      `<input class="form-control disp_kode_barang" readonly="readonly" placeholder="Cari Barang" value="${data.kode_barang}">` +
      `<div class="input-group-btn">` +
        `<button type="button" class="btn btn-primary cari-barang" data-id="row-barang-${data.barang_id}"><i class="fa fa-search"></i></button>` +
      `</div>` +
    `</div>` +
    `<span class="text-slate-300 text-bold text-size-mini disp_barang mt-10">${data.barang}</span>`,
    // Col 2
    `<input type="hidden" class="input-satuan_id" name="satuan_id[]" value="${data.satuan_id}">` +
    `<input type="hidden" class="input-isi_satuan" name="isi_satuan[]" value="${data.isi_satuan}">` +
    `<label class="label-satuan">${data.satuan}</label>`,
    // Col 3
    `<input type="hidden" class="input-qty" name="qty[]" value="${data.qty}">` +
    `<input class="form-control disp_qty text-right" value="${data.qty}">`,
    // Col 4
    `<button type="button" class="btn btn-xs btn-danger remove-row"><i class="fa fa-trash"></i></button>`,
  ]).draw(false);
}

$(document).ready(function() {
  $(".input-decimal").autoNumeric('init', {aSep: '.', aDec: ',', mDec: '2'});
  $(".input-bulat").autoNumeric('init', {aSep: '.', aDec: ',', mDec: '0'});

  $(".wysihtml5-min").wysihtml5({
    parserRules:  wysihtml5ParserRules
  });
  $(".wysihtml5-toolbar").remove();

  $('.btn-save').on('click', function(e) {
    e.preventDefault();
    $(form).submit();
  });

  $(".btn-batal").click(function() {
    window.location.assign(url.index);
  });

  $(".disp_tanggal").daterangepicker({
    singleDatePicker: true,
    startDate: moment("<?php echo date('Y-m-d'); ?>"),
    endDate: moment("<?php echo date('Y-m-d'); ?>"),
    applyClass: "bg-slate-600",
    cancelClass: "btn-default",
    opens: "center",
    autoApply: true,
    locale: {
      format: "DD/MM/YYYY"
    }
  });

  $("#btn_tanggal").click(function () {
    let parent = $(this).parent();
    parent.find('input').data("daterangepicker").toggle();
  });

  tableBarangDt = tableBarang.DataTable({
    "processing": true,
    "serverSide": true,
    "ajax": {
        "url": url.loadDataBarang,
        "type": "POST"
    },
    "columns": [
        { 
            "data": "kode",
            "render": function (data, type, row, meta) {
                var template = '<a href="#" class="add-barang" data-id="{{ID}}" data-kode="{{KODE}}" data-nama="{{NAMA}}" data-satuan_id="{{SATUAN_ID}}" data-satuan="{{SATUAN}}" data-isi_satuan="{{ISI}}">{{KODE}}</a>';
                return template
                    .replace(/\{\{ID\}\}/g, row.id)
                    .replace(/\{\{KODE\}\}/g, row.kode)
                    .replace(/\{\{NAMA\}\}/g, row.nama)
                    .replace(/\{\{SATUAN_ID\}\}/g, row.satuan_id)
                    .replace(/\{\{SATUAN\}\}/g, row.satuan)
                    .replace(/\{\{ISI\}\}/g, row.isi_satuan);
            },
            "className": "text-center"
        },
        { "data": "nama" },
        { 
            "data": "satuan",
            "orderable": false,
            "searchable": false
        },
    ]
  });

  tableBarang.on('click', '.add-barang', function (e) {
    e.preventDefault();
    let id = $(this).data('id');
    let kode = $(this).data('kode');
    let nama = $(this).data('nama');
    let satuan_id = $(this).data('satuan_id');
    let satuan = $(this).data('satuan');
    let isi_satuan = parseFloat($(this).data('isi_satuan'));
    
    let notif = false;
    $('[name="barang_id[]"').each(function () {
      if(parseInt(id) === parseInt($(this).val())) notif = true;
    });

    if(notif) {
      errorMessage('Peringatan !', 'Anda telah memilih barang ini sebelumnya. Silahkan pilih kembali.');
      return;
    }

    if (curRowBarang != null) { // Row Exists
      let tr = $("#" + curRowBarang);
      let qty = tr.find('.input-qty').val();

      tr.prop('id', "row-barang-" + id);
      tr.find('.cari-barang').data('id', "row-barang-" + id).attr('data-id', "row-barang-" + id);
      tr.find('.input-barang_id').val(id);
      tr.find('.input-satuan_id').val(satuan_id);
      tr.find('.input-isi_satuan').val(isi_satuan);
      tr.find('.disp_kode_barang').val(kode);
      tr.find('.disp_barang').html(nama);
      tr.find('.label-satuan').html(satuan);
      tr.find('.disp_qty').focus().select();
    } else {
      columnBarang({
          id: 0,
          barang_id: id,
          kode_barang: kode,
          barang: nama,
          satuan_id: satuan_id,
          isi_satuan: isi_satuan,
          satuan: satuan,
          qty: 1,
      });
    }
    modalBarang.modal('hide');
  });

  $("#btn-tambah").click(function (e) {
      curRowBarang = null;
      tableBarangDt.draw(false);
      modalBarang.modal('show');
  });

  /* EVENTS TABLE DETAIL */
  tableDetail.on('click', '.cari-barang', function() {
    curRowBarang = $(this).data('id');
    tableBarangDt.draw(false);
    modalBarang.modal('show');
  });

  tableDetail.on('change blur keyup', '.disp_qty', function() {
    let val = isNaN(parseFloat($(this).autoNumeric('get'))) ? 0 : parseFloat($(this).autoNumeric('get'));
    let tr = $(this).parent().parent();
    tr.find('.input-qty').val(val);
  });

  tableDetail.on('click', '.remove-row', function() {
    tableDetailDt
        .row($(this).parents('tr'))
        .remove()
        .draw();
  });

  $(form).validate({
    rules: {
      tanggal: { required: true },
      unitkerja_id: { required: true },
    },
    focusInvalid: true,
    errorPlacement: function(error, element) {
        var placement = $(element).closest('.input-group');
        if (placement.length > 0) {
            error.insertAfter(placement);
        } else {
            error.insertAfter($(element));
        }
    },
    submitHandler: function (form) {
      tableDetailDt.search('').draw(false);
      let input = $('input[name="barang_id[]"]');
      if (input.length <= 0) {
        swal({
            title: "Peringatan!",
            text: "Pilih Barang terlebih dahulu.",
            html: true,
            type: "warning",
            confirmButtonColor: "#2196F3"
        });
        return;
      }

      swal({
        title: "Konfirmasi?",
        type: "warning",
        text: "Apakah data yang dimasukan benar??",
        showCancelButton: true,
        confirmButtonText: "Ya",
        confirmButtonColor: "#2196F3",
        cancelButtonText: "Batal",
        cancelButtonColor: "#FAFAFA",
        closeOnConfirm: true,
        showLoaderOnConfirm: true,
      },
      function() {
        $('.input-decimal').each(function() {
          $(this).val($(this).autoNumeric('get'));
        });

        $('.input-bulat').each(function() {
          $(this).val($(this).autoNumeric('get'));
        });

        $('input, textarea, select').prop('disabled', false);

        blockPage('Sedang diproses ...');
        var formData = $(form).serialize();
        $.ajax({
          data: formData,
          type: 'POST',
          dataType: 'JSON', 
          url: url.save,
          success: function(data){
              $.unblockUI();
              successMessage('Berhasil', "Mutasi berhasil disimpan.");
              window.location.assign(url.index);
          },
          error: function(data){
              $.unblockUI();
              errorMessage('Peringatan', "Terjadi kesalahan saat memproses data.");
          }
        });
        return false;
      });
    }
  });

  fillForm(UID);
});
</script>