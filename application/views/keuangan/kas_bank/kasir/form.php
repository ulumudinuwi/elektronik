<style>
	.form-control {
		text-transform: none !important;
	}
	.form-group {
		margin-bottom: 2px;
	}
	.heading-elements {
		right: 20px;
	}
	.table.no-border tr td, 
	.table.no-border tr th {
		border-width: 0;
	}
	hr {
		margin-bottom: 10px;
		margin-top: 5px;
	}
	#table_detail td:nth-child(2),
	#table_detail td:nth-child(3),
	#table_detail td:nth-child(4),
	#table_detail td:nth-child(5) {
		text-align: right;
	}
	#lookup_tarif_pelayanan_modal .modal-body {
        max-height: 500px;
 		overflow-y: auto;
    }
	#lookup_tarif_pelayanan_modal .modal-dialog {
		width: 800px;
	}
	#lookup_tarif_pelayanan_table > tbody > tr > td {
		padding: 2px 10px;
	}
</style>
<?php $this->load->view('keuangan/kas_bank/kasir/template'); ?>
<form class="form-horizontal" id="kasir_form" name="kasir_form" method="post" action="">
	<div class="panel panel-white" id="main_section">
		<div class="panel-heading">
			<h5 class="panel-title">Kasir</h5>
			<div class="heading-elements">
				<div class="heading-btn">
					<button class="btn btn-success btn-labeled" type="submit" id="simpan_1_button"><b><i class="icon-floppy-disk"></i></b> Simpan </button>
					<button class="btn btn-default" type="button" id="batal_1_button"> Batal </button>
				</div>
			</div>
		</div>
		
		<div class="panel-body">
				
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-4 control-label">Tanggal</label>
						<div class="col-md-8">
							<div class="input-group">
								<span class="input-group-addon"><i class="icon-calendar22"></i></span>
								<input type="hidden" id="tanggal" name="tanggal" value="" />
								<input class="form-control" type="text" id="disp_tanggal" value="" autocomplete="off" />
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-4 control-label">No. Kwitansi</label>
						<div class="col-md-8">
							<input type="text" class="form-control" id="no_kwitansi" name="no_kwitansi" value="" readonly="readonly" />
						</div>
					</div>
				</div>
			</div>
				
			<hr/>
			
			<div class="row">
				<div class="col-md-6">
					
					<div class="form-group" id="no_reg_rawat_inap_section">
						<label class="col-md-4 control-label">No. Reg. Rawat Inap</label>
						<div class="col-md-8">
							<input class="form-control" type="text" id="disp_no_reg_rawat_inap" value="" readonly="readonly" />
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-md-4 control-label">No. RM</label>
						<div class="col-md-8">
							<input class="form-control" type="text" id="disp_no_rm" value="" readonly="readonly" />
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-md-4 control-label">Nama</label>
						<div class="col-md-8">
							<input class="form-control" type="text" id="disp_nama" value="" readonly="readonly" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label">Alamat</label>
						<div class="col-md-8">
							<textarea class="form-control" rows="2" id="disp_alamat" readonly="readonly"></textarea>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-4 control-label">Kelompok Pasien</label>
						<div class="col-md-8">
							<input class="form-control" type="text" id="disp_kelompok_pasien" value="" readonly="readonly" />
						</div>
					</div>
					<div class="form-group" id="perusahaan_section">
						<label class="col-md-4 control-label">Perusahaan</label>
						<div class="col-md-8">
							<input class="form-control" type="text" id="disp_perusahaan" value="" readonly="readonly" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label">Layanan</label>
						<div class="col-md-8">
							<input class="form-control" type="text" id="disp_layanan" value="" readonly="readonly" />
						</div>
					</div>
					<div class="form-group" id="poliklinik_section">
						<label class="col-md-4 control-label">Poliklinik</label>
						<div class="col-md-8">
							<input class="form-control" type="text" id="disp_poliklinik" value="" readonly="readonly" />
						</div>
					</div>
					<div class="form-group" id="kelas_section">
						<label class="col-md-4 control-label">Kelas</label>
						<div class="col-md-8">
							<input class="form-control" type="text" id="disp_kelas" value="" readonly="readonly" />
						</div>
					</div>
					<div class="form-group" id="ruang_section">
						<label class="col-md-4 control-label">Ruang</label>
						<div class="col-md-8">
							<input class="form-control" type="text" id="disp_ruang" value="" readonly="readonly" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label">Dokter DPJP</label>
						<div class="col-md-8">
							<input class="form-control" type="text" id="disp_dokter" value="" readonly="readonly" />
						</div>
					</div>
				</div>
			</div>
			<hr />
			<div class="table-responsive">
				<table class="table table-bordered table-xxs dataTable" id="kasir_detail_table">
					<thead>
						<tr>
							<th style="width: 15%;">Kode</th>
							<th>Deskripsi</th>
							<th style="width: 15%;">Harga Satuan</th>
							<th style="width: 10%;">Qty</th>
							<th style="width: 10%;">Disc.</th>
							<th style="width: 15%;">Jumlah</th>
							<th style="width: 7%;"></th>
						</tr>
					</thead>
					<tbody>
						<tr class="border-solid" id="kasir_footer_section">
							<td colspan="7" style="padding: 2px;">
								<button class="btn btn-primary btn-labeled btn-xs" type="button" id="tambah_button">
									<b><i class="icon-plus-circle2"></i></b>
									Tambah
								</button>
							</th>
						</tr>
						<tr>
							<td rowspan="5" colspan="3"></td>
							<td colspan="2" style="font-weight:700;text-align:right">Sub Total</td>
							<td style="text-align: right;">
								<input type="hidden" id="sub_total" name="sub_total" value="0" />
								<label id="label_sub_total" style="margin-bottom:0;">0</label>
							</td>
							<td></td>
						</tr>
						<tr class="section_pembayaran" id="discount_section">
							<td colspan="2" style="padding:0;text-align:right;">
								<div class="input-group">
									<span class="input-group-addon" style="font-weight:700;width:100%;text-align:right;border:none;">Discount</span>
									<span class="input-group-addon" id="discount_symbol">%</span>
									<span class="input-group-btn">
										<button type="button" class="btn btn-default dropdown-toggle btn-icon" data-toggle="dropdown" id="btn-toggle-discount">
											<span class="caret"></span>
										</button>
										<ul class="dropdown-menu">
											<li><a id="discount_persen" href="#">Persen</a></li>
											<li><a id="discount_nominal" href="#">Nominal</a></li>
										</ul>
									</span>
									<input type="hidden" id="jenis_discount" name="jenis_discount" value="0" />
									<input type="hidden" id="discount" name="discount" value="0" />
								</div>
							</td>
							<td style="padding: 0;">
								<div class="input-group">
									<input class="form-control" type="text" id="disp_discount_persen" value="0" autocomplete="off" style="text-align: right;" />
									<input class="form-control" type="text" id="disp_discount_nominal" value="0" autocomplete="off" style="text-align: right; display: none;" />
								</div>
							</td>
							<td></td>
						</tr>
						<tr>
							<td colspan="2" style="font-weight:700;text-align:right">Total Harus Bayar</td>
							<td style="text-align: right;">
								<input type="hidden" id="total" name="total" value="0" />
								<label id="label_total" style="margin-bottom:0;">0</label>
							</td>
							<td></td>
						</tr>
						<tr id="uang_diterima_section">
							<td colspan="2" id="td_label_uang_diterima" style="font-weight:700;text-align:right;">Uang Diterima</td>
							<td style="text-align: right; padding: 0;">
								<input type="hidden" id="uang_diterima" name="uang_diterima" value="0" />
								<input class="form-control" type="text" id="disp_uang_diterima" name="disp_uang_diterima" value="0" autocomplete="off" style="text-align: right;" />
							</td>
							<td></td>
						</tr>
						<tr id="kembalian_section">
							<td colspan="2" style="font-weight:700;text-align:right;">Kembalian</td>
							<td style="text-align: right;">
								<input type="hidden" id="kembalian" name="kembalian" value="0" />
								<label id="label_kembalian" style="margin-bottom:0;">0</label>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<div class="panel-footer">
			<div class="heading-elements">
				<div class="heading-btn pull-right">
					<button class="btn-success btn-labeled btn" type="submit" id="simpan_2_button"><b><i class="icon-floppy-disk"></i></b> Simpan </button>
					<button class="btn btn-default" type="button" id="batal_2_button"> Batal </button>
				</div>
			</div>
		</div>
		<div>
			<input type="hidden" id="kasir_line_no" name="kasir_line_no" value="0" />
			
			<input type="hidden" id="id" name="id" value="0" />
			<input type="hidden" id="uid" name="uid" value="" />
			<input type="hidden" id="no_reg_rawat_inap" name="no_reg_rawat_inap" value="" />
			<input type="hidden" id="shift_id" name="shift_id" value="0" />
			<input type="hidden" id="kasir_shift_id" name="kasir_shift_id" value="" />
			<input type="hidden" id="pelayanan_id" name="pelayanan_id" value="0" />
			<input type="hidden" id="pasien_id" name="pasien_id" value="0" />
			<input type="hidden" id="jenis_kelompok_pasien" name="jenis_kelompok_pasien" value="0" />
			<input type="hidden" id="kelompok_pasien_id" name="kelompok_pasien_id" value="0" />
			<input type="hidden" id="perusahaan_id" name="perusahaan_id" value="0" />
			<input type="hidden" id="layanan_id" name="layanan_id" value="0" />
			<input type="hidden" id="jenis_layanan" name="jenis_layanan" value="0" />
			<input type="hidden" id="dokter_id" name="dokter_id" value="0" />
			<input type="hidden" id="kelas_id" name="kelas_id" value="0" />
			<input type="hidden" id="ruang_id" name="ruang_id" value="0" />
			<input type="hidden" id="kp_pelayanan_id" name="kp_pelayanan_id" value="0" />
			<input type="hidden" id="kp_kas_id" name="kp_kas_id" value="0" />
			<input type="hidden" id="kp_piutang_id" name="kp_piutang_id" value="0" />
			<input type="hidden" id="created_by" name="created_by" value="<?php echo $auth_user["id"]; ?>" />
			<input type="hidden" id="update_by" name="update_by" value="<?php echo $auth_user["id"]; ?>" />
			
			<input type="hidden" id="bayar_kredit" name="bayar_kredit" value="0" />
			<input type="hidden" id="status" name="status" value="0" />
			
			<input type="hidden" id="no_jaminan" name="no_jaminan" value="" />
			<input type="hidden" id="inacbgs" name="inacbgs" value="0" />
		</div>
	</div>
</form>