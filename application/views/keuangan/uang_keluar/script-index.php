<script type="text/javascript">
	var tanggalDari = "<?php echo date('Y-m-01'); ?>", tanggalSampai = "<?php echo date('Y-m-d'); ?>";
	var table;
	var tableDetail = $('#table-detail');
	var detailModal = $('#detail-modal');
	$("#detail_total").autoNumeric('init', {aSep: '.', aDec: ',', mDec: '0'});
	var url = {
		loadData : "<?php echo site_url('api/keuangan/uang_keluar/load_data'); ?>",
		getData : "<?php echo site_url('api/keuangan/uang_keluar/get_data?transaksi_id=:TRANSAKSI_ID&modul=:MODUL&total=:TOTAL'); ?>",
	};

	function subsDate(range, tipe) {
		let date = range.substr(0, 10);
		if(tipe === "sampai") date = range.substr(13, 10);
		return getDate(date);
	}

	function showDetail(uid, transaksi_id, modul, total) {
		$.getJSON(url.getData.replace(':TRANSAKSI_ID', transaksi_id).replace(':MODUL', modul).replace(':TOTAL', total), function (data, status) {
			if (status === 'success') {
				data = data.data;
				let totalDebit = 0,
					totalKredit = 0;
				tableDetail.find('tbody').empty();
				if (data.length > 0) {
					for (var i = 0, n = data.length; i < n; i++) {
						addDetail(data[i]);
						if (data[i].tipe == 1) {
							totalDebit += parseFloat(data[i].total);
						} else {
							totalKredit += parseFloat(data[i].total);
						}
					}
					tableDetail.find('tbody').append(`<tr>
														<td class="text-right">TOTAL</td>
														<td class="text-right text-bold">${numeral(totalDebit).format()}</td>
														<td class="text-right text-bold">${numeral(totalKredit).format()}</td>
													  </tr>`);
				}else{
					tableDetail.find('tbody').empty();
					tableDetail.find('tbody').append('<tr><td colspan="3" class="text-center">Tidak Ada Data</td></tr>');
				}

				detailModal.modal('show');
			}
		});
	}

	function addDetail(data) {
		data = data || {};

		var tbody = tableDetail.find('tbody');

		var count = $('[name="detail_id[]"]').length;

		var ID = (count + 1);

		var tr = $("<tr/>")
		.prop('id', 'row-cb-' + ID)
		.appendTo(tbody);

		var tdPerkiraan = $("<td/>")
		.appendTo(tr);
		var inputPerkiraan = $("<label/>")
		.html(data.nama)
		.appendTo(tdPerkiraan);

		var tdDebit = $("<td/>")
		.addClass('text-right')
		.appendTo(tr);
		var inputDebit = $("<label/>")
		.html(parseInt(data.tipe) === 1 ? data.total : 0)
		.appendTo(tdDebit);

		var tdKredit = $("<td/>")
		.addClass('text-right')
		.appendTo(tr);
		var inputKredit = $("<label/>")
		.html(parseInt(data.tipe) === 2 ? data.total : 0)
		.appendTo(tdKredit);

		inputDebit.autoNumeric('init', {aSep: '.', aDec: ',', mDec: '0'});
		inputKredit.autoNumeric('init', {aSep: '.', aDec: ',', mDec: '0'});
	}

	function handleLoadTable() {
		table = $("#table").DataTable({
			"processing": true,
			"serverSide": true,
			"ordering": false,
			"ajax": {
					"url": url.loadData,
					"type": "POST",
					"data": function(p) {
						p.tanggal_dari = subsDate($("#search_range_tanggal").val(), 'dari');
						p.tanggal_sampai = subsDate($("#search_range_tanggal").val(), 'sampai');
					}
			},
			"columns": [
				{ 
					"data": "update_at",
					"render": function (data, type, row, meta) {
							let tmp = '<a class="show-row btn btn-success" data-uid="' + row.uid + '" data-modul="' + row.modul + '" data-total="' + row.total + '" data-transaksi_id="' + row.transaksi_id + '" data-toggle="tooltip" data-title="Lihat Detail" data-placement="top">' + moment(data).format('DD-MM-YYYY HH:mm'); + '</a>';
							return tmp;
					},
				},
				{ 
					"data": "keterangan",
					"orderable": false,
					"searchable": false,
				},
				{ 
					"data": "total",
					"render": function (data, type, row, meta) {
							let tmp = numeral(data).format();
							return tmp;
					},
				},
			],
			"fnDrawCallback": function (oSettings) {
				$('[data-toggle=tooltip]').tooltip();
			},
		});
	}

	$(window).ready(function() {
		$(".rangetanggal-form").daterangepicker({
			autoApply: true,
			locale: {
					format: "DD/MM/YYYY",
			},
			startDate: moment(tanggalDari),
			endDate: moment(tanggalSampai),
		});

		$("#search_range_tanggal").on('apply.daterangepicker', function (ev, picker) {
			table.draw();
		});

		$("#btn_search_tanggal").click(function () {
			$("#search_range_tanggal").data('daterangepicker').toggle();
		});

		$("#search_range_tanggal").on('change', function() {
			table.draw();
		});

		$("#table").on("click", ".show-row", function () {
			let uid = $(this).data('uid'),
				transaksi_id = $(this).data('transaksi_id'),
				modul = $(this).data('modul');
				total = $(this).data('total');
			showDetail(uid, transaksi_id, modul, total);
		});

		handleLoadTable();
	});
</script>