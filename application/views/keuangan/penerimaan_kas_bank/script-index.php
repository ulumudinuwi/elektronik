<script type="text/javascript">
	var tanggalDari = "<?php echo date('Y-m-01'); ?>", tanggalSampai = "<?php echo date('Y-m-d'); ?>";
	var table;
	var tableDetail = $('#table-detail');
	var detailModal = $('#detail-modal');
	$("#detail_total").autoNumeric('init', {aSep: '.', aDec: ',', mDec: '0'});
	var url = {
		loadData : "<?php echo site_url('api/keuangan/kas_bank/penerimaan_kas_bank/load_data'); ?>",
		getData : "<?php echo site_url('api/keuangan/kas_bank/penerimaan_kas_bank/get_data/:UID'); ?>",
		edit : "<?php echo site_url('keuangan/kas_bank/penerimaan_kas_bank/add/:UID'); ?>",
		delete : "<?php echo site_url('api/keuangan/kas_bank/penerimaan_kas_bank/delete'); ?>",
	};

	function subsDate(range, tipe) {
		let date = range.substr(0, 10);
		if(tipe === "sampai") date = range.substr(13, 10);
		return getDate(date);
	}

	function showDetail(uid) {
		$.getJSON(url.getData.replace(':UID', uid), function (data, status) {
			if (status === 'success') {
				data = data.data;

				$("#detail_kode").html(data.no_pengeluaran);
				$("#detail_tanggal").html(moment(data.tanggal).format('DD/MM/YYYY'));
				$("#detail_diterima_oleh").html(data.diterima_oleh);
				$("#detail_dibayar_oleh").html(data.dibayar_oleh);

				let total = 0;
				tableDetail.find('tbody').empty();
				if (data.details.length > 0) {
					for (var i = 0, n = data.details.length; i < n; i++) {
						addDetail(data.details[i]);
						total += parseFloat(data.details[i].jumlah);
					}
				}else{
					tableDetail.find('tbody').empty();
					tableDetail.find('tbody').append('<tr><td colspan="2" class="text-center">Tidak Ada Data</td></tr>');
				}
				$("#detail_total").autoNumeric('set', total);

				detailModal.modal('show');
			}
		});
	}

	function addDetail(data) {
		data = data || {};

		var tbody = tableDetail.find('tbody');

		var count = $('[name="detail_id[]"]').length;

		var ID = (count + 1);

		var tr = $("<tr/>")
		.prop('id', 'row-cb-' + ID)
		.appendTo(tbody);

		var tdKeterangan = $("<td/>")
		.appendTo(tr);
		var inputKeterangan = $("<label/>")
		.html(data.keterangan)
		.appendTo(tdKeterangan);

		var tdJumlah = $("<td/>")
		.appendTo(tr);
		var inputJumlah = $("<label/>")
		.html(data.jumlah)
		.appendTo(tdJumlah);

		inputJumlah.autoNumeric('init', {aSep: '.', aDec: ',', mDec: '0'});
	}

	function handleLoadTable() {
		table = $("#table").DataTable({
			"processing": true,
			"serverSide": true,
			"ajax": {
					"url": url.loadData,
					"type": "POST",
					"data": function(p) {
						p.tanggal_dari = subsDate($("#search_range_tanggal").val(), 'dari');
						p.tanggal_sampai = subsDate($("#search_range_tanggal").val(), 'sampai');
					}
			},
			"order": [1, "desc"],
			"columns": [
				{ 
					"data": "tanggal",
					"render": function (data, type, row, meta) {
							let tmp = '<a class="show-row" data-uid="' + row.uid + '" data-toggle="tooltip" data-title="Lihat Detail" data-placement="top">' + moment(data).format('DD-MM-YYYY'); + '</a>';
							return tmp;
					},
				},
				{ 
					"data": "no_pengeluaran",
					"orderable": false,
					"searchable": false,
				},
				{ 
					"data": "diterima_oleh",
					"orderable": false,
					"searchable": false,
				},
				{ 
					"data": "tanggal",
					"render": function (data, type, row, meta) {
							let tmp = '<a class="edit-row" data-uid="' + row.uid + '" data-toggle="tooltip" data-title="Edit Data" data-placement="top"><b><i class="fa fa-pencil"></i></b></a> | <a class="delete-row" data-uid="' + row.uid + '" data-toggle="tooltip" data-title="Hapus Data" data-placement="top"><b><i class="fa fa-trash-o text-danger"></i></b></a>';
							return tmp;
					},
				},
			],
			"fnDrawCallback": function (oSettings) {
				$('[data-toggle=tooltip]').tooltip();
			},
		});
	}

	$(window).ready(function() {
		$(".rangetanggal-form").daterangepicker({
			autoApply: true,
			locale: {
					format: "DD/MM/YYYY",
			},
			startDate: moment(tanggalDari),
			endDate: moment(tanggalSampai),
		});

		$("#search_range_tanggal").on('apply.daterangepicker', function (ev, picker) {
			table.draw();
		});

		$("#btn_search_tanggal").click(function () {
			$("#search_range_tanggal").data('daterangepicker').toggle();
		});

		$("#search_range_tanggal").on('change', function() {
			table.draw();
		});

		$("#table").on("click", ".show-row", function () {
			let uid = $(this).data('uid');
			showDetail(uid);
		});

		$("#table").on("click", ".edit-row", function () {
			let uid = $(this).data('uid');
			window.location.assign(url.edit.replace(':UID', uid));
		});

		$("#table").on("click", ".delete-row", function () {
			let data = {
                uid: $(this).data('uid')
            };
			confirmDialog({
                title: 'Hapus Data Tersebut?',
                text: '',
                btn_confirm: 'Hapus',
                url: url.delete,
                data: data,
                onSuccess: (res) => {
                    successMessage('Success', 'Data Berhasil Dihapus.');
					table.draw();
                },
                onError: (error) => {
                    errorMessage('Error', 'Data Gagal Dihapus.');
                },
            });
		});

		handleLoadTable();
	});
</script>