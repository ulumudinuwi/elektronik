<html>
<head>
</head>
<body>
<style>
body {
    line-height: 1.2em;
    font-size: 10px;
    font-family: 'courier new', sans-serif;
}
h1, h2, h3, h4, h5, h6 {
    font-family: inherit;
    font-weight: 400;
    line-height: 1.5384616;
    color: inherit;
    margin-top: 0;
    margin-bottom: 5px;
    text-align: center;
}
h1 {
    font-size: 28px;
}
h2 {
    font-size: 20px;
}
h3 {
    font-size: 18px;
}
h4 {
    font-size: 16px;
}
h5 {
    font-size: 14px;
}
h6 {
    font-size: 12px;
}
table {
    border-collapse: collapse;
    font-size: 12px;
}
.table {
    border-spacing: 0;
    width: 100%;
    font-size: 10px;
}
.table thead th,
.table tbody td {
    vertical-align: middle;
    padding: 5px 10px;
    line-height: 1.5384616;
}
.table thead th {
    color: #fff;
    background-color: #607D8B;
    font-weight: bold;
    text-align: center;
}
.text-center {
    text-align: center;
}
.text-left {
    text-align: left;
}
.text-right {
    text-align: right;
}
p {
    margin: 0;
}

.text-lvl-0 {
    font-size: 1.1em;
    font-weight: bold;
}
.text-lvl-1 {
    font-size: 1.0em;
    font-weight: bold;
}
.text-lvl-2 {
    font-size: 0.9em;
}
.text-lvl-3 {
    font-size: 0.9em;
}
.text-bold {
    font-weight: bold;
}
.text-danger {
    color: red;
}
.pr10 {
    padding-left: 10px;
}
.text-muted {
    color: #a9a9a9;
}

#table_header td {
    padding-top: 0;
    padding-bottom: 0;
    margin-top: 0;
    margin-bottom: 20px;
}
#header{
    font-size: 16px;
    text-transform: uppercase;
    font-weight: bold;
}

</style>

<table id="table_header">
    <tr>
        <td id="header">
            <img src="<?php echo image_url('logo_prima.png') ?>" alt="" style="height: 60px">
        </td>
        <td id="header">
            <!-- PT.PRIMA ESTETIKA RAKSA  -->
            PT. PRIME AESTHETIC
            <br>
            KWITANSI
        </td>
        <tr>
            <td colspan="3">&nbsp;</td>
        </tr>  
    </tr>
</table>
<hr/>
<h6 class="text-bold text-center"><u>KWITANSI</u></h6>
<h6 class="text-bold text-center">No: <?= $param['no_kuitansi'] ?></h6>

<table style="width: 100%">
    <tr>
        <td style="width: 30%;">Telah Terima Dari</td>
        <td>: <?php echo $detail_member['member']['nama'] ?></td>
    </tr>
    <tr>
        <td>Tanggal / Jam Transaksi</td>
        <td>: <?= konversi_to_id(date('d M Y H:i', strtotime($detail_transaksi['tanggal_transaksi'])));?></td>
    </tr>
    <!-- <tr>
        <td>Tanggal Pembayaran</td>
        <td>: 
            <?php 
                if($param['tanggal_transaksi'] == 'null'){
                    echo konversi_to_id(date('d M Y H:i', strtotime($detail_transaksi['tanggal_transaksi'])));
                }else{
                 echo konversi_to_id(date('d M Y', strtotime($param['tanggal_transaksi'])));
                }
            ?>
        </td>
    </tr> -->
    <tr>
        <td>Uang Sejumlah</td>
        <td>: Rp. <?php echo toRupiah($param['nominal']) ?></td>
    </tr>
    <tr>
        <td>Terbilang</td>
        <td>: <?= ucwords(numbers_to_words($param['nominal'])); ?></td>
    </tr>
    <tr>
        <td>Untuk Pembayaran</td>
        <td>: <?= $param['ket']; ?></td>
    </tr>
    <tr>
        <td>Sisa Pembayaran</td>
        <td>: 
            <?php if ($param['sisa_pembayaran'] == 0): ?>
                Lunas
            <?php else: ?>
                Rp. <?= toRupiah($param['sisa_pembayaran']) ?>
             <?php endif ?> 
        </td>
    </tr>
</table>
<br/>
<br/>
<br/>
<table style="width: 100%">
    <tr>
        <td style="width: 50%;">&nbsp;</td>
        <td class="text-center"><?= ucwords('Tanggerang'); ?>, <?= konversi_to_id(date('d M Y')); ?></td>
    </tr>
    <tr>
        <td class="text-center">
            <table>
                <tr>
                    <!-- <td class="text-center text-bold"><h3><?= $detail_transaksi['status_lunas'] ? 'L U N A S' : 'B E L U M  L U N AS' ?></h3></td> -->
                    <td class="text-center text-bold">&nbsp;</td>
                </tr>
            </table>
        </td>
        <td class="text-center">
            <table style="width: 35%;">
                <tr>
                    <td class="text-center text-bold" style="height: 64px;"><h6></h6></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td class="text-center">(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</td>
    </tr>
</table>
<hr/>
</body>
</html>