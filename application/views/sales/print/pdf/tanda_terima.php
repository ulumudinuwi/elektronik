<html>
<body>
<style>
body {
    line-height: 1.2em;
    font-size: 10px;
    font-family: 'courier new', sans-serif;
}
h1, h2, h3, h4, h5, h6 {
    font-family: inherit;
    font-weight: 400;
    line-height: 1.5384616;
    color: inherit;
    margin-top: 0;
    margin-bottom: 5px;
    text-align: center;
}
h1 {
    font-size: 28px;
}
h2 {
    font-size: 20px;
}
h3 {
    font-size: 18px;
}
h4 {
    font-size: 16px;
}
h5 {
    font-size: 14px;
}
h6 {
    font-size: 12px;
}
table {
    border-collapse: collapse;
    font-size: 12px;
}
.table {
    border-spacing: 0;
    width: 100%;
    font-size: 12px;
}
.table thead th,
.table tbody td {
    vertical-align: middle;
    padding: 5px 10px;
    line-height: 1.5384616;
}
.table thead th {
    color: #fff;
    background-color: #607D8B;
    font-weight: bold;
    text-align: center;
}
.text-center {
    text-align: center;
}
.text-left {
    text-align: left;
}
.text-right {
    text-align: right;
}
p {
    margin: 0;
}

.text-lvl-0 {
    font-size: 1.1em;
    font-weight: bold;
}
.text-lvl-1 {
    font-size: 1.0em;
    font-weight: bold;
}
.text-lvl-2 {
    font-size: 0.9em;
}
.text-lvl-3 {
    font-size: 0.9em;
}
.text-bold {
    font-weight: bold;
}
.text-danger {
    color: red;
}
.pr10 {
    padding-left: 10px;
}

#table_header td {
    padding-top: 0;
    padding-bottom: 0;
    margin-top: 0;
    margin-bottom: 20px;
}
 #header{
    font-size: 16px;
    text-transform: uppercase;
    font-weight: bold;
 }
.text-muted {
    color: #a9a9a9;
}
</style>
<?php 

?>
<table id="table_header">
    <tr>
        <td id="header">
            <img src="<?php echo image_url('logo_prima.png') ?>" alt="" style="height: 60px">
        </td>
        <td id="header">
            <!-- PT.PRIMA ESTETIKA RAKSA  -->
            PT. PRIME AESTHETIC
            <br>
            SURAT JALAN
        </td>
        <tr>
            <td colspan="3">&nbsp;</td>
        </tr>  
    </tr>
</table>

<h6 class="text-bold"></h6>
<table style="width: 100%;">
    <tr>
        <td valign="top" width="15%;">NAMA DOKTER</td>
        <td valign="top" width="1%;">:</td>
        <td valign="top" width="35%;"><?php echo $detail_member['member']['nama'] ?> </td>
        <td valign="top" style="width: 2%;">&nbsp;</td>
        <td valign="top" width="11%;">NO.FAKTUR</td>
        <td valign="top" width="1%;">:</td>
        <td valign="top" width="23%;"><?php echo $detail_transaksi['no_invoice'] ?></td>
    </tr>
    <tr>
        <td valign="top">TELEPON / Klinik</td>
        <td valign="top">:</td>
        <td valign="top"><?php echo $detail_member['member']['no_hp'].' / '.$detail_member['member']['nama_klinik'] ?>  </td>
        <td valign="top" style="width: 2%;">&nbsp;</td>
        <td valign="top">TANGGAL</td>
        <td valign="top">:</td>
        <td valign="top"><?php echo Carbon\Carbon::parse($detail_transaksi['tanggal_transaksi'])->format('d/m/Y') ?> </td>
    </tr>
    <tr>
        <td valign="top">ALAMAT</td>
        <td valign="top">:</td>
        <td valign="top"><?php echo $detail_member['member']['alamat'] ?> </td>    
        <td valign="top" style="width: 2%;">&nbsp;</td>
    </tr>
</table>


<br/><br/>
<?php 
    function semua_diskon($detail_transaksi){
        if($detail_transaksi['jenis_diskon'] == 'persen') {
            echo toRupiah($detail_transaksi['diskon']).' %';
        }else if($detail_transaksi['jenis_diskon'] == 'nominal'){
            echo 'Rp. '.toRupiah($detail_transaksi['diskon']);
        }
    }
?>
<table class="table">
    <tr>
        <th width="4px"class="text-bold" style="width: 10%;border-top: 1px solid #000; border-bottom: 1px solid #000;">NO</th>
        <td class="text-bold" style="border-top: 1px solid #000; border-bottom: 1px solid #000;" >NAMA BARANG</td>
        <th class="text-bold" style="width: 40%;border-top: 1px solid #000; border-bottom: 1px solid #000;">QTY</th>
    </tr>
    <?php $no = 1; ?>
    <?php foreach($detail_pembelian as $key => $data) { ?>
        <?php $style = ( $key != count( $detail_pembelian ) -1 ) ? '' : 'border-bottom: 1px solid #000'; ?>
        <tr>
            <?php 
                if($data['is_bonus'] == 1):
                    $detailBonus = json_decode($data['is_bonus_detail']); 
            ?>
                <td style="<?php echo $style ?>">&nbsp;</td>
                <td style="<?php echo $style ?>">
                    <?php echo $data['nama'] ?> <span class="text-bold"><?php echo 'Bonus dari Barang '.$detailBonus->nama; ?></span>        
                </td>
                <th style="<?php echo $style ?>"><?php echo $data['qty'] ?></th>
            <?php else: ?>
            <td class="text-center" style="<?php echo $style ?>"><?php echo $no ?></td>
            <td style="<?php echo $style ?>"><?php echo $data['nama'] ?></td>
            <th style="<?php echo $style ?>"><?php echo $data['qty'] ?></th>
            <?php
                $no++;
                endif; 
            ?>
        </tr>
    <?php } ?>
    <tr>
        <td colspan="3">&nbsp;</td>
    </tr>
</table>
</body>
</html>