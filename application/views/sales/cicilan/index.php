<div class="content content-custom" id="list_pembayaran">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-flat">
                <div class="panel-heading no-padding-bottom uppercase">
                    <div>
                        <label>Tanggal : </label>
                        <vue-datepicker-local v-model="filter_range" :local="setupDefault.datepicker.lang" range-separator="/" @confirm="drawFilter()" show-buttons></vue-datepicker-local>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="table table-responsive">
                        <table class="table table-bordered table-striped dataTable no-footer" id="myTable">
                            <thead class="bg-slate">
                                <tr>
                                    <th>TANGGAL</th>
                                    <th>NO.MEMBER</th>
                                    <th>NO.KUITANSI</th>
                                    <th>NAMA</th>
                                    <th>NO.INVOICE</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>