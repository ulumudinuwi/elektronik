<div id="detail_transaksi" class="content content-custom">
    <div class="col-md-12">
        <div class="panel panel-white">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-7">
                        <div class="panel panel-white">
                            <div class="panel-heading">
                                <h6 class="panel-title">
                                    <i class="icon-user-tie"></i> Detail Member   
                                </h6>
                            </div>
                            <div class="panel-body" style="display: block;" v-if="detail_member.member">
                                <!-- <pre>{{ member_bio }}</pre> -->
                                <div class="detail-group">
                                    <div class="detail-label">
                                        <label for="" class="text-bold">No Member</label>
                                    </div>
                                    <div class="detail-info">
                                        <span class="text-semibold">{{ detail_member.member.no_member}}</span>
                                    </div>
                                </div>
                                <div class="detail-group">
                                    <div class="detail-label">
                                        <label for="" class="text-bold">Nama Member</label>
                                    </div>
                                    <div class="detail-info">
                                        <span class="text-semibold">{{ detail_member.member.nama}}</span>
                                    </div>
                                </div>
                                <div class="detail-group">
                                    <div class="detail-label">
                                        <label for="" class="text-bold">Email Member</label>
                                    </div>
                                    <div class="detail-info">
                                        <span class="text-semibold">{{ detail_member.member.email}}</span>
                                    </div>
                                </div>
                                <div class="detail-group">
                                    <div class="detail-label">
                                        <label for="" class="text-bold">No.Tlp Member</label>
                                    </div>
                                    <div class="detail-info">
                                        <span class="text-semibold">{{ detail_member.member.no_hp}}</span>
                                    </div>
                                </div>
                                <div class="detail-group">
                                    <div class="detail-label">
                                        <label for="" class="text-bold">Point</label>
                                    </div>
                                    <div class="detail-info">
                                        <span class="text-semibold">{{ detail_member.member.point}}</span>
                                    </div>
                                </div>
                                <div class="detail-group">
                                    <div class="detail-label">
                                        <label for="" class="text-bold">Plafon</label>
                                    </div>
                                    <div class="detail-info">
                                        <span class="text-semibold"> Rp. {{ parseFloat(detail_member.member.plafon) | to_rupiah}}</span>
                                    </div>
                                </div>
                                <div class="detail-group">
                                    <div class="detail-label">
                                        <label for="" class="text-bold">Alamat</label>
                                    </div>
                                    <div class="detail-info">
                                        <span class="text-semibold">{{ detail_member.member.alamat }}</span>
                                    </div>
                                </div>
                                <div class="detail-group">
                                    <div class="detail-label">
                                        <label for="" class="text-bold">Nama Klinik</label>
                                    </div>
                                    <div class="detail-info">
                                        <span class="text-semibold">{{ detail_member.member.nama_klinik}}</span>
                                    </div>
                                </div>
                                <div class="detail-group">
                                    <div class="detail-label">
                                        <label for="" class="text-bold">No.Tlp Klinik</label>
                                    </div>
                                    <div class="detail-info">
                                        <span class="text-semibold">{{ detail_member.member.no_tlp_klinik}}</span>
                                    </div>
                                </div>
                                <div class="detail-group">
                                    <div class="detail-label">
                                        <label for="" class="text-bold">Alamat Klinik</label>
                                    </div>
                                    <div class="detail-info">
                                        <span class="text-semibold">{{ detail_member.member.alamat_klinik}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>   
                    </div>
                    <div class="col-md-5">
                        <div class="panel panel-white">
                            <div class="panel-heading">
                                <h6 class="panel-title">
                                    <i class="icon-file-text2"></i> Data Transaksi
                                </h6>
                            </div>
                            <div class="panel-body">
                                <ul class="media-list">
                                    <li class="media">
                                        <div class="media-left">
                                            <a class="btn border-success text-success btn-flat btn-icon btn-rounded btn-sm">
                                                <i class="icon-calendar2"></i>
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            <div class="media-heading text-semibold text-uppercase bold">Tanggal Transakasi</div>

                                            <div class="media-annotation">{{ detail_transaksi.tanggal_transaksi | d_m_Y_H_m_s }}</div>
                                        </div>
                                    </li>
                                    <li class="media">
                                        <div class="media-left">
                                            <a class="btn border-warning text-warning btn-flat btn-icon btn-rounded btn-sm">
                                                <i class="icon-calendar2"></i>
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            <div class="media-heading text-semibold text-uppercase bold">Tanggal Jatuh Tempo</div>

                                            <div class="media-annotation">{{ detail_transaksi.tgl_jatuh_tempo | d_m_Y }}</div>
                                        </div>
                                    </li>
                                    <li class="media">
                                        <div class="media-left">
                                            <a class="btn border-info text-info btn-flat btn-icon btn-rounded btn-sm change-sales">
                                                <i class="icon-user-check"></i>
                                            </a>
                                        </div>

                                        <div class="media-body">
                                            <div class="media-heading text-semibold text-uppercase bold">Sales</div>

                                            <div class="media-annotation" v-if="detail_member.sales" id="nama_sales">{{ detail_member.sales.nama }}</div>
                                            <input type="hidden" name="id_sales" id="id_sales">
                                        </div>
                                    </li>
                                    <li class="media">
                                        <div class="media-left">
                                            <a :class="`btn btn-flat btn-icon btn-rounded btn-sm ${detail_transaksi.status_lunas ? 'border-success text-success' : 'border-danger text-danger'}`">
                                                <i class="fa fa-check" v-if="detail_transaksi.status_lunas"></i>
                                                <i class="icon-close2" v-if="!detail_transaksi.status_lunas"></i>
                                            </a>
                                        </div>

                                        <div class="media-body">
                                            <div class="media-heading text-semibold text-uppercase bold">Status Lunas</div>

                                            <div class="media-annotation">{{ detail_transaksi.status_lunas ? 'LUNAS' : 'BELUM LUNAS' }}</div>
                                        </div>

                                        <div class="media-body">
                                            <!-- <div class="media-heading text-semibold text-uppercase bold">Cicilan Ke</div> -->

                                            <div class="media-annotation"></div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="panel panel-white">
                            <div class="panel-heading">
                                <h6 class="panel-title">
                                    <i class="icon-megaphone position-left"></i>
                                    Voucher
                                </h6>
                            </div>
                            <div class="panel-body">
                                <!-- <form class="form-horizontal">
                                    <div class="col-md-12">
                                        <div class="input-group">
                                            <input type="text" class="form-control" placeholder="Masukan Kode Voucher">
                                            <span class="input-group-btn">
                                                <button class="btn btn-default" type="button">Gunakan Voucher</button>
                                            </span>
                                        </div>
                                    </div>
                                </form> -->
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="panel panel-white">
                            <div class="panel-heading">
                                <h6 class="panel-title">
                                    <i class="icon-box position-left"></i>
                                    Detail Transaksi
                                   
                                </h6>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped">
                                        <thead class="align-center">
                                            <tr class="bg-slate">
                                                <th style="width: 32%">Nama Barang</th>
                                                <th style="width: 20%">Harga</th>
                                                <th>Qty</th>
                                                <th>(%)</th>
                                                <th style="width: 25%">Jumlah</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="(barang, i) of detail_pembelian">
                                                <td>{{barang.nama}}</td>
                                                <td>
                                                    <span class="pull-left" v-if="barang.is_bonus == 0">Rp.</span>
                                                    <span class="pull-right" v-if="barang.is_bonus == 0">
                                                        {{ parseFloat(barang.harga) | to_rupiah }}
                                                    </span>
                                                    <span class="pull-left text-size-mini text-danger" v-if="barang.is_bonus == 1">
                                                        Bonus dari Barang {{ JSON.parse(barang.is_bonus_detail).nama }}
                                                    </span>
                                                </td>
                                                <td>{{barang.qty}}</td>
                                                <td>{{barang.diskon | to_rupiah}} % + {{barang.diskon_plus | to_rupiah}} %</td>
                                                <td><span class="pull-left">Rp.</span><span class="pull-right">{{ parseFloat(barang.grand_total) | to_rupiah }}</span></td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="2"></td>
                                                <td colspan="2" class="uppercase bold">Total Harga Barang</td>
                                                <td colspan="2" class="uppercase bold"><span class="pull-left">Rp.</span><span class="pull-right">{{ total_harga_barang | to_rupiah }}</span></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2"></td>
                                                <td colspan="2" class="uppercase bold">Diskon</td>
                                                <td colspan="2" class="uppercase bold">
                                                    <span class="pull-left">{{ detail_transaksi.jenis_diskon != 'persen' ? 'Rp.' : ''}} </span><span class="pull-right">{{ detail_transaksi.diskon }} {{ detail_transaksi.jenis_diskon == 'persen' ? '%' : ''}}</span>
                                                </td>
                                            </tr>
                                            <tr v-if="detail_transaksi.ongkir != 0">
                                                <td colspan="2"></td>
                                                <td colspan="2" class="uppercase bold">Ongkos Kirim</td>
                                                <td colspan="2" class="uppercase bold">
                                                    <span class="pull-left">Rp.</span><span class="pull-right">{{ parseFloat(detail_transaksi.ongkir) | to_rupiah}}</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2"></td>
                                                <td colspan="2" class="uppercase bold">Grand Total</td>
                                                <td colspan="2" class="uppercase bold">
                                                    <span class="pull-left">Rp.</span><span class="pull-right">{{ grand_total | to_rupiah }}</span>
                                                </td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <div class="text-right">
                    <button type="submit" class="btn btn-success btn-labeled simpan" @click="onProcess()">
                        <b><i class="icon-floppy-disk"></i></b>
                        Simpan
                    </button>
                    <a href="<?php echo base_url('sales/pengalihan_invoice/daftar_invoice') ?>" class="btn btn-default cancel-button">
                        Batal
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="modal_sales" class="modal fade" tabindex="-1" aria-hidden="true" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h5 class="modal-title">Daftar Sales</h5>
            </div>

            <div class="modal-body">
                <table style="width: 100%" class="table table-bordered" id="table-sales">
                    <thead>
                        <tr class="bg-slate">
                            <td>Nama Sales</td>
                            <td>Kontak</td>
                            <td>Alamat</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="empty_state_item">
                            <td colspan="4" class="text-center">TIDAK ADA DATA</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var loadSales = "<?= base_url('api/master/sales/list_sales/:STATUS'); ?>";
</script>