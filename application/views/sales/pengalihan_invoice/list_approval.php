<!-- Content area -->
<div class="content content-custom" id="list_transaksi">
    <!-- Row Filter -->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-flat">
                <div class="panel-heading uppercase text-center">
                    Filter Tanggal         
                </div>
                <div class="panel-body" style="display: block;">
                    <div class="text-center">
                       <vue-datepicker-local v-model="filter_range" :local="setupDefault.datepicker.lang" range-separator="/" @confirm="drawFilter()" show-buttons></vue-datepicker-local>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Row Datatable -->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <ul class="nav nav-tabs nav-tabs nav-justified">
                        <li class="active">
                            <a href="#verifikasi_pos_manager_tab" table="#approval_pos_manager" data-toggle="tab" aria-expanded="true">
                                <i class="icon-menu7 position-left"></i> Daftar Approval 
                            </a>
                        </li>
                        <li>
                            <a href="#verifikasi_diskon_manager_tab" table="#approval_diskon_manager" data-toggle="tab" aria-expanded="true">
                                <i class="icon-menu7 position-left"></i> History Approval
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="panel-body">
                    <div class="tabbable">
                        <div class="tab-content">
                            <div class="tab-pane active" id="verifikasi_pos_manager_tab">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped" id="approval_pos_manager">
                                        <thead>
                                            <tr class="bg-slate">
                                                <th class="text-center" style="width: 10%">TANGGAL</th>
                                                <th class="text-center" style="width: 10%">NO INVOICE</th>
                                                <!-- <th class="text-center" style="width: 10%">NO MEMBER</th> -->
                                                <th class="text-center" style="width: 18%">SALES</th>
                                                <th class="text-center" style="width: 18%">SALES BARU</th>
                                                <th class="text-center" style="width: 10%">DOKTER</th>
                                                <th class="text-center" style="width: 10%">AKSI</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td colspan="5" class="text-center">TIDAK ADA DATA</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="tab-pane" id="verifikasi_diskon_manager_tab">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped" id="approval_diskon_manager">
                                        <thead>
                                            <tr class="bg-slate">
                                                <th class="text-center" style="width: 10%">TANGGAL</th>
                                                <th class="text-center" style="width: 10%">NO INVOICE</th>
                                                <!-- <th class="text-center" style="width: 10%">NO MEMBER</th> -->
                                                <th class="text-center" style="width: 18%">SALES LAMA</th>
                                                <th class="text-center" style="width: 18%">SALES BARU</th>
                                                <th class="text-center" style="width: 10%">DOKTER</th>
                                                <th class="text-center" style="width: 10%">AKSI</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td colspan="5" class="text-center">TIDAK ADA DATA</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="modal_reject" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h5 class="modal-title">Keterangan Reject</h5>
            </div>

            <div class="modal-body">
                <input type="hidden" id="uid">
                <input type="hidden" id="type">
                <textarea class="form-control" id="desc_reject"></textarea>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onclick="VM_LIST_APPROVAL.reject_pos()">Save</button>
            </div>
        </div>
    </div>
</div>

<div id="modal_status" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h5 class="modal-title">INFO APPROVAL</h5>
            </div>

            <div class="modal-body">
                <hr>
                <h6 class="text-semibold">Diskon Per Barang</h6>
                <table style="width: 100%" class="table table-bordered">
                    <thead>
                        <tr class="bg-slate">
                            <td>Nama Barang</td>
                            <td>Qty</td>
                            <td>Disc</td>
                            <td>Total</td>
                        </tr>
                    </thead>
                    <tbody id="detail-per_item">
                        <tr class="empty_state_item">
                            <td colspan="4" class="text-center">TIDAK ADA DATA</td>
                        </tr>
                    </tbody>
                </table>
                <hr>
                <h6 class="text-semibold">Diskon Keseluruhan</h6>
                <table style="width: 100%" class="table table-bordered">
                    <thead>
                        <tr class="bg-slate">
                            <td colspan="2">Keterangan</td>
                            <td>Disc</td>
                            <td>Total</td>
                        </tr>
                    </thead>
                    <tbody id="detail-semua">
                        <tr class="empty_state_all">
                            <td colspan="4" class="text-center">TIDAK ADA DATA</td>
                        </tr>
                    </tbody>
                </table>
                <hr>
                <h6 class="text-semibold">Status Approval</h6>
                <table style="width: 100%" id="main-approval">
                    
                </table>
                <!-- <h6 class="text-semibold">Another paragraph</h6>
                <p>Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.</p>
                <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p> -->
            </div>

            <!-- <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div> -->
        </div>
    </div>
</div>

