<!-- Content area -->
<div class="content content-custom" id="list_transaksi">
    <!-- Row Filter -->
    <div class="row">
        <div class="col-md-3">
            <div class="panel panel-flat">
                <div class="panel-heading uppercase text-center">
                    Filter Tanggal         
                </div>
                <div class="panel-body" style="display: block;">
                    <div class="text-center">
                       <vue-datepicker-local v-model="filter_range" :local="setupDefault.datepicker.lang" range-separator="/" @confirm="drawFilter()" show-buttons></vue-datepicker-local>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-flat">
                <div class="panel-heading uppercase text-center">
                    Status         
                </div>
                <div class="panel-body" style="display: block;">
                    <div class="text-center">
                        <div class="btn-group">
                            <button v-for="item of filter_status.all_status" type="button" 
                                @click="selected_status(item.status)"
                                :class="`btn btn-sm ${filter_status.selected_status == item.status ? item.btn_class : 'btn-default'}`" v-cloak>
                                {{item.status}}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="panel panel-flat">
                <div class="panel-heading uppercase text-center">
                    Status Lunas      
                </div>
                <div class="panel-body" style="display: block;">
                    <div class="text-center">
                        <div class="btn-group btn-gtoup-sm">
                            <button v-for="item of filter_status_lunas.all_status" type="button" 
                                @click="selected_status_lunas(item.status)"
                                :class="`btn btn-sm ${filter_status_lunas.selected_status == item.status ? item.btn_class : 'btn-default'}`" v-cloak>
                                {{item.status}}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="panel panel-flat">
                <div class="panel-heading uppercase text-center">
                    Reset Filter 
                </div>
                <div class="panel-body" style="display: block;">
                    <div class="text-center">
                        <button type="button" class="btn btn-sm btn-info" @click="resetFilter()"><i class="fa fa-refresh"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Row Datatable -->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-flat">
                <div class="panel-heading no-padding-bottom uppercase">
                  
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped" id="myTable">
                            <thead>
                                <tr class="bg-slate">
                                    <th class="text-center" style="width: 10%">TANGGAL</th>
                                    <th class="text-center" style="width: 10%">NO INVOICE</th>
                                    <th class="text-center" style="width: 20%">NO DOKTER</th>
                                    <th class="text-center" style="width: 20%">DOKTER</th>
                                    <th class="text-center" style="width: 8%">STATUS LUNAS</th>
                                    <th class="text-center" style="width: 1%"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr></tr>
                                    <td colspan="5" class="text-center">TIDAK ADA DATA</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>