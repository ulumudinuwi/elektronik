<?php echo messages(); ?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-flat">
            <div class="row">
                <div class="col-md-6">
                    <div class="panel-heading">
                        <h6 class="panel-title">
                            <i class="icon-user position-left"></i>Biodata Member
                        </h6>
                        <hr>
                    </div>
                    <div class="panel-body">
                        <table class="table borderless t_custom">
                            <tr>
                                <th>No.Member</th>
                                <td>JK-0005270</td>
                            </tr>
                            <tr>
                                <th>Nama</th>
                                <td>Lili Tatamas</td>
                            </tr>
                            <tr>
                                <th>Jenis Kelamin</th>
                                <td>Perempuan</td>
                            </tr>
                            <tr>
                                <th>Alamat</th>
                                <td>Villa Artha Gading F/36</td>
                            </tr>
                            <tr>
                                <th>Nomor HP</th>
                                <td>6281381766919</td>
                            </tr>
                            <tr>
                                <th>Point</th>
                                <td>200</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel-heading">
                        <h6 class="panel-title">
                            <i class=" icon-magazine position-left"></i>Data Transaksi
                        </h6>
                        <hr>
                    </div>
                    <div class="panel-body">
                        <table class="table borderless t_custom">
                            <tr>
                                <th>Tanggal</th>
                                <td><input class="form-control" value="<?php echo Carbon\Carbon::now()->format('d/m/Y'); ?>"></td>
                            </tr>
                            <tr>
                                <th>No Kwitansi</th>
                                <td>IRJ.190111.001</td>
                            </tr>
                            <tr>
                                <td>Pembayaran</td>
                                <td>Mobile Apps</td>
                            </tr>
                            <tr>
                                <td>Sales</td>
                                <td>Boby</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel-heading">
                        <h6 class="panel-title">
                            <i class="icon-price-tag position-left"></i>Rincian Biaya
                        </h6>
                        <hr>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead class="bg-slate align-center">
                                    <tr>
                                        <th>Uraian</th>
                                        <th>Harga</th>
                                        <th>Qty</th>
                                        <th>(%)</th>
                                        <th>Jumlah</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Injection - Botox(Unit) (II)</td>
                                        <td>50.000</td>
                                        <td>1,00</td>
                                        <td>100%</td>
                                        <td>0</td>  
                                    </tr>
                                    <tr>
                                        <td>Gentle Touch Facial Cleanser</td>
                                        <td>382.500</td>
                                        <td>1,00</td>
                                        <td>0%</td>
                                        <td>382.500</td>  
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="3">TOTAL</td>
                                        <td colspan="2">
                                            <span class="pull-left">Rp.</span><span class="pull-right">531.400</span>
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="panel-body">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label class="control-label col-md-3">Redeem Point</label>
                                <div class="col-md-8">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <!-- <div class="checkbox"> -->
                                            <input type="checkbox" class="control-default" name="use_point">
                                            <!-- </div> -->
                                        </span>
                                        <input type="text" class="form-control" placeholder="point">
                                    </div>
                                    <span class="pull-right text-muted point">Total point 0 (Rp.0)</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Voucher</label>
                                <div class="col-md-8">
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="" value="F1K0ZE9W">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default">Redeem</button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel-body">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label class="control-label col-md-3 uppercase bold">Total</label>
                                <div class="col-md-8">
                                    <div>
                                        <span class="pull-left">Rp.</span><span class="pull-right">531.400</span>
                                    </div>
                                    <br>
                                    <span class="text-success pull-right">(-100.000)</span>
                                </div>
                            </div>
                            <div class="form-group" style="margin-top: 22px;">
                                <label class="control-label col-md-3 uppercase bold">Discount</label>
                                <div class="col-md-8">
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="" value="0">
                                        <span class="input-group-addon">%</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group" style="margin-top: 22px;">
                                <label class="control-label col-md-3 uppercase bold">Grand Total</label>
                                <div class="col-md-8">
                                    <div class="input-group">
                                        <span class="pull-left">Rp.</span><span class="pull-right">431.400,00</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel-heading">
                        <h6 class="panel-title">
                            <i class="icon-credit-card position-left"></i>Metode Pembayaran
                        </h6>
                        <hr>
                    </div>
                    <div class="panel-body">
                        <div class="table table-responsive">
                            <table class="table table-striped table-bordered">
                                <thead class="bg-slate align-center">
                                    <tr>
                                        <th>Cara Bayar</th>
                                        <th>Receipt No</th>
                                        <th>Bank</th>
                                        <th>Mesin EDC</th>
                                        <th>Nominal</th>
                                        <th>Bukti Pembayaran</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Transfer</td>
                                        <td>-</td>
                                        <td>Bank Central Asia (BCA)</td>
                                        <td>-</td>
                                        <td>431.400</td>
                                        <td class="text-center"><button class="btn btn-default btn-labeled"><b><i class="icon-download"></i></b> Preview</button></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-md-offset-6">
                    <div class="panel-body">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label class="control-label col-md-3 bold">Uang Diterima</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" placeholder="" value="431.400">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-8 col-md-offset-3">
                                    <span class="text-muted pull-right point">Total Point yang didapatkan 108</span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>