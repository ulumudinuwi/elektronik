<link href="<?php echo base_url() ?>assets/css/pages/sales/pos/index.css" rel="stylesheet" media="screen">
<form id="form">
	<div class="row">
	    <div class="col-md-7">
	        <div class="panel panel-white">
	            <div class="panel-heading">
	                <h6 class="panel-title">
	                    <i class="icon-user-tie"></i> Detail Member   
	                </h6>
	            </div>
	            <div class="panel-body" style="display: block;" v-if="detail_member.member">
	                <!-- <pre>{{ member_bio }}</pre> -->
	                <div class="detail-group">
	                    <div class="detail-label">
	                        <label for="" class="text-bold">No Member</label>
	                    </div>
	                    <div class="detail-info">
	                        <span class="text-semibold" id="no_member"></span>
	                    </div>
	                </div>
	                <div class="detail-group">
	                    <div class="detail-label">
	                        <label for="" class="text-bold">Nama Member</label>
	                    </div>
	                    <div class="detail-info">
	                        <span class="text-semibold" id="nama_member"></span>
	                    </div>
	                </div>
	                <div class="detail-group">
	                    <div class="detail-label">
	                        <label for="" class="text-bold">Email Member</label>
	                    </div>
	                    <div class="detail-info">
	                        <span class="text-semibold" id="email_member"></span>
	                    </div>
	                </div>
	                <div class="detail-group">
	                    <div class="detail-label">
	                        <label for="" class="text-bold">No.Tlp Member</label>
	                    </div>
	                    <div class="detail-info">
	                        <span class="text-semibold" id="no_hp"></span>
	                    </div>
	                </div>
	                <div class="detail-group">
	                    <div class="detail-label">
	                        <label for="" class="text-bold">Point</label>
	                    </div>
	                    <div class="detail-info">
	                        <span class="text-semibold" id="point"></span>
	                    </div>
	                </div>
	                <div class="detail-group">
	                    <div class="detail-label">
	                        <label for="" class="text-bold">Plafon</label>
	                    </div>
	                    <div class="detail-info">
	                        <span class="text-semibold" id="plafon"></span>
	                    </div>
	                </div>
	                <div class="detail-group">
	                    <div class="detail-label">
	                        <label for="" class="text-bold">Alamat</label>
	                    </div>
	                    <div class="detail-info">
	                        <span class="text-semibold" id="alamat"></span>
	                    </div>
	                </div>
	                <div class="detail-group">
	                    <div class="detail-label">
	                        <label for="" class="text-bold">Nama Klinik</label>
	                    </div>
	                    <div class="detail-info">
	                        <span class="text-semibold" id="nama_klinik"></span>
	                    </div>
	                </div>
	                <div class="detail-group">
	                    <div class="detail-label">
	                        <label for="" class="text-bold">No.Tlp Klinik</label>
	                    </div>
	                    <div class="detail-info">
	                        <span class="text-semibold" id="no_tlp_klinik"></span>
	                    </div>
	                </div>
	                <div class="detail-group">
	                    <div class="detail-label">
	                        <label for="" class="text-bold">Alamat Klinik</label>
	                    </div>
	                    <div class="detail-info">
	                        <span class="text-semibold" id="alamat_klinik"></span>
	                    </div>
	                </div>
	            </div>
	        </div>   
	    </div>
	    <div class="col-md-5">
	        <div class="panel panel-white">
	            <div class="panel-heading">
	                <h6 class="panel-title">
	                    <i class="icon-file-text2"></i> Data Transaksi
	                </h6>
	            </div>
	            <div class="panel-body">
	                <ul class="media-list">
	                    <li class="media">
	                        <div class="media-left">
	                            <a class="btn border-success text-success btn-flat btn-icon btn-rounded btn-sm">
	                                <i class="icon-calendar2"></i>
	                            </a>
	                        </div>
	                        <div class="media-body">
	                            <div class="media-heading text-semibold bold">Tanggal Transakasi</div>

	                            <div class="media-annotation" id="disp_tanggal_transaksi"></div>
	                        </div>
	                        <div class="media-body">
	                            <div class="media-heading text-semibold bold">Tanggal Jatuh Tempo</div>

	                            <div class="media-annotation" id="disp_tgl_jatuh_tempo"></div>
	                        </div>
	                    </li>

	                    <li class="media">
	                        <div class="media-left">
	                            <a class="btn border-info text-info btn-flat btn-icon btn-rounded btn-sm change-sales">
	                                <i class="icon-user-check"></i>
	                            </a>
	                        </div>

	                        <div class="media-body">
	                            <div class="media-heading text-semibold bold">Sales</div>

	                            <div class="media-annotation" id="nama_sales"></div>
	                        </div>
	                    </li>
	                    <li class="media">
	                        <div class="media-left">
	                            <a class="btn btn-flat btn-icon btn-rounded btn-sm style_media">
	                                <i class="fa fa-check" id="status_lunas_cek_1"></i>
	                                <i class="icon-close2" id="status_lunas_cek_2"></i>
	                            </a>
	                        </div>

	                        <div class="media-body">
	                            <div class="media-heading text-semibold bold">Status Lunas</div>

	                            <div class="media-annotation" id="status_lunas"></div>
	                        </div>

	                        <div class="media-body">
	                            <div class="media-annotation"></div>
	                        </div>
	                    </li>
	                </ul>
	            </div>
	        </div>
	    </div>
	    <div class="col-md-5">
	        <div class="panel panel-white">
	            <div class="panel-heading">
	                <h6 class="panel-title">
	                    <i class="icon-megaphone position-left"></i>
	                    Voucher
	                </h6>
	            </div>
	            <div class="panel-body">
	                <!-- <form class="form-horizontal">
	                    <div class="col-md-12">
	                        <div class="input-group">
	                            <input type="text" class="form-control" placeholder="Masukan Kode Voucher">
	                            <span class="input-group-btn">
	                                <button class="btn btn-default" type="button">Gunakan Voucher</button>
	                            </span>
	                        </div>
	                    </div>
	                </form> -->
	            </div>
	        </div>
	    </div>
	    <div class="col-md-12">
	        <div class="panel panel-white">
	            <div class="panel-heading">
	                <h6 class="panel-title">
	                    <i class="icon-box position-left"></i>
	                    Detail Transaksi
	                   
	                </h6>
	            </div>
	            <div class="panel-body">
	                <div class="table-responsive">
	                    <table class="table table-bordered table-striped" id="table_transaksi">
	                        <thead class="align-center">
	                            <tr class="bg-slate">
	                                <th style="width: 32%">Nama Barang</th>
	                                <th style="width: 20%">Harga (Rp.)</th>
	                                <th style="width: 12%">Qty</th>
	                                <th style="width: 12%">(%)</th>
	                                <th style="width: 25%">Jumlah</th>
	                                <th>-</th>
	                            </tr>
	                        </thead>
	                        <tbody>
	                        </tbody>
	                        <tfoot>
	                            <tr>
	                                <td colspan="4" class="uppercase bold text-right">
								        <button type="button" class="btn btn-success btn-labeled btn-tambah">
								            <b><i class="icon-plus-circle2"></i></b>
								            Tambah Barang
								        </button>
								    </td>
	                                <td colspan="4" class="uppercase bold text-right"></td>
	                            </tr>
	                            <tr>
	                                <td colspan="4" class="uppercase bold text-right">Total Harga Barang</td>
	                                <td colspan="4" class="uppercase bold text-right">
	                                    <span class="pull-left">Rp.</span><span class="pull-right" id="disp_total_harga_barang"></span>
	                                	<input type="hidden" name="total_harga_barang" id="total_harga_barang">
	                                </td>
	                            </tr>
	                            <tr>
	                                <td colspan="4" class="uppercase bold text-right">Ongkos Kirim</td>
	                                <td colspan="4" class="uppercase bold text-right">
	                                    <span class="pull-left">Rp.</span><span class="pull-right" id="disp_ongkos_kirim"></span>
	                                	<input type="hidden" name="ongkos_kirim" id="ongkos_kirim">
	                                </td>
	                            </tr>
	                            <tr>
	                                <td colspan="4" class="uppercase bold text-right">Grand Total</td>
	                                <td colspan="4" class="uppercase bold text-right">
	                                    <span class="pull-left">Rp.</span><span class="pull-right" id="disp_grand_total"></span>
	                                    <input type="hidden" name="grand_total" id="grand_total">
	                                </td>
	                            </tr>
	                        </tfoot>
	                    </table>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
	<div class="panel-footer">
	    <div class="text-right">
	    	<input type="hidden" name="pos_id" id="pos_id">
	    	<input type="hidden" name="pos_uid" id="pos_uid">
	    	<input type="hidden" name="sales_id" id="sales_id">
	    	<input type="hidden" name="member_id" id="member_id">
	    	<input type="hidden" name="total_pembayaran" id="total_pembayaran">
	    	<input type="hidden" name="tgl_jatuh_tempo" id="tgl_jatuh_tempo">
	    	<input type="hidden" name="tanggal_transaksi" id="tanggal_transaksi">
	    	<input type="hidden" name="no_invoice" id="no_invoice">
	        <button type="submit" class="btn btn-success btn-labeled simpan">
	            <b><i class="icon-floppy-disk"></i></b>
	            Simpan
	        </button>
	    </div>
	</div>
</form>

<div id="list-barang-modal" class="modal fade" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-slate">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h5 class="modal-title">Daftar Barang</h5>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="table-responsive">
                            <table id="table-barang" class="table table-bordered table-striped">
                                <thead>
                                    <tr class="bg-slate">
                                        <th class="text-center">Kode</th>
                                        <th class="text-center">Nama</th>
                                        <th class="text-center">Stock</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><b>Tutup</b></button>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('sales/retur_penjualan/script-form.php'); ?>