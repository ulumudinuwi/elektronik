<style type="text/css">
.table > thead > tr > th, .table > tbody > tr > td {
    white-space: nowrap;
    vertical-align: middle;
}

#btn-group_float {
    position: fixed;
    bottom: 10%;
    right: 20px;
    z-index: 10000;
    text-align: right;
}
</style>
<div class="panel panel-flat">
    <div class="panel-body form-horizontal"> 
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-3" for="invoice">PILIH INVOICE</label>
                    <div class="col-md-9">
                        <div class="input-group" style="width: 100%";>
                            <select class="form-control" id="invoice">
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <!-- <div class="col-md-6">
                <div class="form-group text-left">
                    <div class="input-group">
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-info btn-icon">
                                <i class="icon-undo"></i>
                            </button>
                        </div>
                        <button type="button" class="btn btn-default btn-icon">History Retur Penjualan</button>
                    </div>
                </div>
            </div> -->
        </div>
    </div>
</div>
<div id="section-invoice" class="panel panel-flat" style="display: none;">
    <div class="panel-body form-horizontal">
        <div class="row">
            <div class="col-md-12">
                <div id="div-invoice"></div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        let searchInvoice = "<?php echo site_url('api/sales/retur_penjualan/search_invoice'); ?>";

        // Format displayed data
        function formatInvoice (data) {
            return `
                <div class="select2-result-repository clearfix">
                    <div class="select2-result-repository__meta" style="margin-left: auto;">
                        <div class="select2-result-repository__title">${data.no_invoice} | ${data.nama_dokter}</div>
                        <div class="select2-result-repository__description" style="font-size: 0.8em;">Tanggal transaksi : ${data.tanggal_transaksi}</div>
                        <div class="select2-result-repository__description" style="font-size: 0.8em;">Alamat: ${data.alamat}</div>
                        <div class="select2-result-repository__description" style="font-size: 0.8em;">Sales : ${data.nama_sales}</div>
                    </div>
                </div>
            `;
        }

        // Format selection
        function formatLayout (data) {
            return data.no_invoice;
        }

        $('#invoice').select2({
            ajax: {
                url: searchInvoice,
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term, // search term
                        page: params.page
                    };
                },
                processResults: function (data, params) {

                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    params.page = params.page || 1;

                    return {
                        results: data.items,
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
            minimumInputLength: 1,
            templateResult: formatInvoice, // omitted for brevity, see the source of this page
            templateSelection: formatLayout // omitted for brevity, see the source of this page
        });

        $("#invoice").on('change', function (e) {
            var result = $(this).select2('data');
            var data = result.pop();
            var uid = data.uid;

            $('#section-invoice').show();
            $('#div-invoice').html('<div class="text-center"><i class="fa fa-spin fa-spinner fa-2x"></i> Tunggu Sebentar ...</div>');
            
            if (uid !== '') {
                $('#btn-group_float').show('slow');
                $("#div-invoice").load("<?php echo site_url('sales/retur_penjualan/form/'); ?>" + uid);
            } else {
                $('#btn-group_float').hide();
                $('#div-invoice').html('');
                $('#section-invoice').hide();
            }
        });
    }); 
</script>