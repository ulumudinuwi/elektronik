<?php echo messages(); ?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-flat">
            <div class="panel-heading">
                <div class="text-right">
                    <button type="submit" class="btn btn-success btn-labeled">
                        <b><i class="icon-floppy-disk"></i></b>
                        Simpan
                    </button>
                    <button type="button" class="btn btn-default cancel-button">
                        Batal
                    </button>
                </div>
            </div>
            <div class="line"></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="profile-wrap">
                                <div class="row-cs">
                                    <div class="col-sm-3 col-md-3 col-lg-3" style="padding: 0px 5px !important">
                                        <div class="cover-wrapper">
                                        <img src="<?php echo base_url('assets/img/profile-laki.jpg');?>" alt="" class="profile-cover lazy">
                                        </div>
                                    </div>
                                    <div class="col-sm-9 col-md-9 col-lg-9">
                                        <div class="biodata-wrapper">
                                            <div class="biodata_header">
                                                <img src="<?php echo base_url('assets/img/polygon.png'); ?>" alt="" style="height:28px;">
                                                <span class="header_title">Biodata Member</span>
                                            </div>
                                            <h4 class="biodata_nama">Ari Ardiansyah</h4>
                                            <span class="biodata_title">Dokter</span>
                                            <hr style="margin: 20px 0px 10px 0px">
                                            <div class="info-container">
                                                <table class="info-wrapper">
                                                    <tr>
                                                        <td class="info-title">No Member</td>
                                                        <td class="info-content">JK.00999</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="info-title">Jenis Kelamin</td>
                                                        <td class="info-content">-</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="info-title">Alamat</td>
                                                        <td class="info-content">Jl Kp.Rancakasiat Desa Rancamulya Kec.Pameungpeuk Jawa Barat Indonesia</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="info-title">Nomor HP</td>
                                                        <td class="info-content">083822658411</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="info-title">Point</td>
                                                        <td class="info-content">100</td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="panel-heading">
                                <span class="title-heading">
                                    <i class="icon-file-text2"></i> Data Transaksi
                                </span>
                            </div>
                            <div class="panel-body">
                                <ul class="media-list">
                                    <li class="media">
                                        <div class="media-left">
                                            <a class="btn border-success text-success btn-flat btn-icon btn-rounded btn-sm">
                                                <i class="icon-calendar2"></i>
                                            </a>
                                        </div>
                                        
                                        <div class="media-body">
                                            <div class="media-heading text-semibold text-uppercase bold">Tanggal</div>
                                            <div class="col-sm-6" style="padding: 0px;">
                                                <input class="form-control" id="ex1" type="text">
                                            </div>
                                        </div>

                                        <!-- <div class="media-body">
                                            Less thus overhung during rabbit goose while amid ludicrously after terribly that opposite the amicable

                                            <div class="media-annotation">19 minutes ago</div>
                                        </div> -->
                                    </li>

                                    <li class="media">
                                        <div class="media-left">
                                            <a class="btn border-success text-success btn-flat btn-icon btn-rounded btn-sm">
                                                <i class="icon-menu6"></i>
                                            </a>
                                        </div>

                                        <div class="media-body">
                                            <div class="media-heading text-semibold text-uppercase bold">No. Kwitansi</div>

                                            <div class="media-annotation">_____</div>
                                        </div>
                                    </li>

                                    <li class="media">
                                        <div class="media-left">
                                            <a class="btn border-info text-info btn-flat btn-icon btn-rounded btn-sm">
                                                <i class="icon-user-check"></i>
                                            </a>
                                        </div>

                                        <div class="media-body">
                                            <div class="media-heading text-semibold text-uppercase bold">Sales</div>

                                            <div class="media-annotation">Boby</div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="panel-heading">
                        <h6 class="panel-title">
                            <i class="icon-cart position-left"></i>Katalog
                        </h6>
                        <hr>
                    </div>
                    <div class="panel-body">

                        <div class="form-group has-feedback" style="margin-bottom: 20px">
                            <input type="search" class="form-control" placeholder="Pencarian...">
                            <div class="form-control-feedback">
                                <i class="icon-search4 text-size-base text-muted"></i>
                            </div>
                        </div>

                        <div class="container-catalog scrollbar-inner">
                            <div class="wrapper-catalog">
                                <div class="item-catalog panel panel-flat">
                                    <div class="panel-body">
                                        <div class="img-container">
                                            <img src="<?php echo  assets_url('img/sample.jpg'); ?>" alt="" class="product-cover"> 
                                        </div>
                                        <h5 class="panel-title">Whitening Pixy</h5>
                                        <p>Rp. <span class="pull-right">50.000,00</span></p>
                                        <div class="input-group product-input" data-stock="20">
                                            <div class="input-group-btn btn-min">
                                                <button class="btn btn-default btn-sm" type="button">
                                                    <i class="icon-minus3"></i>
                                                </button>
                                            </div>
                                            <input type="text" class="input-sm form-control count-product" readonly value="0">
                                            <div class="input-group-btn btn-plus">
                                                <button class="btn btn-default btn-sm" type="button">
                                                    <i class="icon-plus3"></i>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="btn-cart">
                                            <button class="btn btn-primary btn-labeled btn-sm"><b><i class="icon-cart2"></i></b> Add To Cart</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel-heading">
                        <h6 class="panel-title">
                            <i class="icon-price-tag position-left"></i>Rincian Biaya
                        </h6>
                        <hr>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead class="bg-slate align-center">
                                    <tr>
                                        <th>Uraian</th>
                                        <th>Harga</th>
                                        <th>Qty</th>
                                        <th>(%)</th>
                                        <th>Jumlah</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        foreach([0,0,0,0,] as $i){
                                            $html = '<tr>'.
                                                '<td>-</td>'.
                                                '<td>-</td>'.
                                                '<td>-</td>'.
                                                '<td>-</td>'.
                                                '<td>-</td>'.
                                                '<td><button class="btn btn-danger"><i class="icon-trash"></i></button></td>'.
                                            '</tr>';

                                            echo $html;
                                        }
                                    ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="4">TOTAL</td>
                                        <td colspan="2"><span class="pull-left">Rp.</span><span class="pull-right">10.710.880,00</span></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="panel-body">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label class="control-label col-md-3">Redeem Point</label>
                                <div class="col-md-8">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <!-- <div class="checkbox"> -->
                                            <input type="checkbox" class="control-default" name="use_point">
                                            <!-- </div> -->
                                        </span>
                                        <input type="text" class="form-control" placeholder="point">
                                    </div>
                                    <span class="pull-right text-muted">Total point 30 (Rp.3000)</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Voucher</label>
                                <div class="col-md-8">
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default">Redeem</button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group" style="margin-top: 22px;">
                                <div class="col-md-3">
                                    <div class="checkbox checkbox-right uppercase">
                                        <label class="bold">
                                            <input class="control-default" type="checkbox" name="piutang">
                                            Piutang
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel-body">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label class="control-label col-md-3 uppercase bold">Total</label>
                                <div class="col-md-8">
                                    <div class="input-group">
                                        <span class="pull-left">Rp.</span><span class="pull-right">10.710.880,00</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group" style="margin-top: 22px;">
                                <label class="control-label col-md-3 uppercase bold">Discount</label>
                                <div class="col-md-8">
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="">
                                        <span class="input-group-addon">%</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group" style="margin-top: 22px;">
                                <label class="control-label col-md-3 uppercase bold">Grand Total</label>
                                <div class="col-md-8">
                                    <div class="input-group">
                                        <span class="pull-left">Rp.</span><span class="pull-right">10.710.880,00</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel-heading">
                        <h6 class="panel-title">
                            <i class="icon-credit-card position-left"></i>Metode Pembayaran
                        </h6>
                        <hr>
                    </div>
                    <div class="panel-body">
                        <div class="table table-responsive">
                            <table class="table table-striped table-bordered">
                                <thead class="bg-slate align-center">
                                    <tr>
                                        <th>Cara Bayar</th>
                                        <th>Receipt No</th>
                                        <th>Bank</th>
                                        <th>Mesin EDC</th>
                                        <th>Nominal</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <select class="form-control select">
                                                <option value="" selected disabled>Pilih Cara Bayar</option>
                                            </select>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control">
                                        </td>
                                        <td>
                                            <select class="form-control select">
                                                <option value="" selected disabled>Pilih Bank</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select class="form-control select">
                                                <option value="" selected disabled>Pilih Mesin EDC</option>
                                            </select>
                                        </td>
                                        <td id="check-form">
                                            
                                        </td>
                                        <td class="text-center">
                                            <button class="btn btn-success"><i class="icon-check"></i></button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Tunai</td>
                                        <td>0002</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td><span>Rp.10.710.880,00</span></td>
                                        <td class="text-center">
                                            <button class="btn btn-danger"><i class="icon-trash"></i></button>
                                        </td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="3"></td>
                                        <td class="uppercase bold">TOTAL</td>
                                        <td class="uppercase bold"><span class="pull-left">Rp.</span><span class="pull-right">10.710.880,00</span></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                       <td colspan="6">
                                           <button class="btn btn-labeled btn-primary btn-block"><b><i class=" icon-plus-circle2"></i></b>Tambah Metode Pembayaran</button>
                                       </td> 
                                       <!-- <td></td> -->
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <div class="text-right">
                    <button type="submit" class="btn btn-success btn-labeled">
                        <b><i class="icon-floppy-disk"></i></b>
                        Simpan
                    </button>
                    <button type="button" class="btn btn-default cancel-button">
                        Batal
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>


