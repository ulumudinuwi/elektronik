<style type="text/css">
.table > thead > tr > th, .table > tbody > tr > td {
    white-space: nowrap;
    vertical-align: middle;
}

#btn-group_float {
    position: fixed;
    bottom: 10%;
    right: 20px;
    z-index: 10000;
    text-align: right;
}
</style>
<div id="btn-group_float" style="display: none;">
    <p>
        <button type="button" id="btn-print-excel" class="btn bg-success btn-float btn-rounded" data-popup="popover" title="Export to Excel" data-trigger="hover" data-content="Klik tombol ini untuk mengexport laporan dalam bentuk Excel." data-placement="left" data-delay="600">
            <b><i class="icon-file-excel"></i></b>
        </button>
    </p>
    <p>
        <button type="button" id="btn-print-pdf" class="btn bg-orange btn-float btn-rounded" data-popup="popover" title="Print PDF" data-trigger="hover" data-content="Klik tombol ini untuk mencetak laporan dalam bentuk PDF." data-placement="left" data-delay="600">
            <b><i class="icon-file-pdf"></i></b>
        </button>
    </p>
</div>
<div class="panel panel-flat">
    <div class="panel-body form-horizontal"> 
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-3" for="laporan">Laporan</label>
                    <div class="col-md-9">
                        <div class="input-group" style="width: 100%";>
                            <select class="form-control select" id="laporan">
                                <option value="">- Pilih Laporan -</option>
                                <option value="laporan_001">Rekap Sales Per Bulan</option>
                                <?php if (is_admin()): ?>
                                    <option value="laporan_002">Laporan Piutang</option>
                                    <option value="laporan_003">Laporan Diskon MOU</option>
                                    <option value="laporan_004">Laporan Invoice Lunas</option>
                                    <option value="laporan_005">Laporan Retur Penjualan</option>
                                <?php endif ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="section-laporan" class="panel panel-flat" style="display: none;">
    <div class="panel-body form-horizontal">
        <div class="row">
            <div class="col-md-12">
                <div id="div-laporan"></div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-print" tabindex="-1" role="dialog" aria-labelledby="modal-printLabel">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <div class="close">
                    <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">Close</button>
                </div>
                <h4 class="modal-title">Print Preview</h4>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script>
function subsDate(range, tipe) {
    let date = range.substr(0, 10);
    if(tipe === "sampai") date = range.substr(13, 10);
    return getDate(date);
}

$(document).ready(function () {
    $('select').select2();
    $("#laporan").change(function () {
        let laporan = $(this).val();

        $('#section-laporan').show();
        $('#div-laporan').html('<div class="text-center"><i class="fa fa-spin fa-spinner fa-2x"></i> Tunggu Sebentar ...</div>');
        
        if (laporan !== '') {
            $('#btn-group_float').show('slow');
            $("#div-laporan").load("<?php echo site_url('sales/laporan/'); ?>" + laporan);
        } else {
            $('#btn-group_float').hide();
            $('#div-laporan').html('');
            $('#section-laporan').hide();
        }
    });
}); 
</script>