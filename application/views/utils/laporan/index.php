<?php echo messages(); ?>
<style type="text/css">
#table thead th {
    text-align: center;
    vertical-align: middle;
}
#btn-group-float {
    position: fixed;
    bottom: 10%;
    right: 20px;
    z-index: 10000;
    text-align: right;
}
</style>
<div class="panel panel-flat">
    <div class="panel-body form-horizontal">
        <div class="row">
            <div class="col-md-4 mt-10">
                <div class="form-group">
                    <label class="control-label col-md-3" for="tanggal">Tanggal</label>
                    <div class="col-md-9">
                        <div class="input-group" style="width: 100%";>
                            <input class="form-control" id="disp_tanggal" value="<?php echo date('d/m/Y') ?>">
                            <input type="hidden" id="tanggal" value="<?php echo date('Y-m-d H:i:s') ?>">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 mt-10">
                <a href="#" data-href="<?php echo site_url('api/utils/laporan/excel_rawat_jalan?tanggal=:TANGGAL'); ?>" class="cetak btn btn-success btn-labeled">
                    <b><i class="icon-file-excel"></i></b> 
                    Rawat Jalan
                </a>
            </div>
            <div class="col-md-3 mt-10">
                <a href="#" data-href="<?php echo site_url('api/utils/laporan/excel_igd?tanggal=:TANGGAL'); ?>" class="cetak btn btn-success btn-labeled">
                    <b><i class="icon-file-excel"></i></b> 
                    IGD
                </a>
            </div>
            <div class="col-md-3 mt-10">
                <a href="#" data-href="<?php echo site_url('api/utils/laporan/excel_odc_ods?tanggal=:TANGGAL'); ?>" class="cetak btn btn-success btn-labeled">
                    <b><i class="icon-file-excel"></i></b> 
                    ODC/ODS
                </a>
            </div>
            <div class="col-md-3 mt-10">
                <a href="#" data-href="<?php echo site_url('api/utils/laporan/excel_rawat_inap_rujukan_masuk?tanggal=:TANGGAL'); ?>" class="cetak btn btn-success btn-labeled">
                    <b><i class="icon-file-excel"></i></b> 
                    Rawat Inap (Rujukan &amp; Masuk)
                </a>
            </div>
            <div class="col-md-3 mt-10">
                <a href="#" data-href="<?php echo site_url('api/utils/laporan/excel_rawat_inap?tanggal=:TANGGAL'); ?>" class="cetak btn btn-success btn-labeled">
                    <b><i class="icon-file-excel"></i></b> 
                    Rawat Inap
                </a>
            </div>
            <div class="col-md-3 mt-10">
                <a href="#" data-href="<?php echo site_url('api/utils/laporan/excel_laboratorium?tanggal=:TANGGAL'); ?>" class="cetak btn btn-success btn-labeled">
                    <b><i class="icon-file-excel"></i></b> 
                    Laboratorium
                </a>
            </div>
            <div class="col-md-3 mt-10">
                <a href="#" data-href="<?php echo site_url('api/utils/laporan/excel_radiologi?tanggal=:TANGGAL'); ?>" class="cetak btn btn-success btn-labeled">
                    <b><i class="icon-file-excel"></i></b> 
                    Radiologi
                </a>
            </div>
            <div class="col-md-3 mt-10">
                <a href="#" data-href="<?php echo site_url('api/utils/laporan/excel_fisioterapi?tanggal=:TANGGAL'); ?>" class="cetak btn btn-success btn-labeled">
                    <b><i class="icon-file-excel"></i></b> 
                    Fisioterapi
                </a>
            </div>
            <div class="col-md-3 mt-10">
                <a href="#" data-href="<?php echo site_url('api/utils/laporan/excel_ctscan?tanggal=:TANGGAL'); ?>" class="cetak btn btn-success btn-labeled">
                    <b><i class="icon-file-excel"></i></b> 
                    CT Scan
                </a>
            </div>
            <div class="col-md-3 mt-10">
                <a href="#" data-href="<?php echo site_url('api/utils/laporan/excel_audiometri?tanggal=:TANGGAL'); ?>" class="cetak btn btn-success btn-labeled">
                    <b><i class="icon-file-excel"></i></b> 
                    Audiometri
                </a>
            </div>
            <div class="col-md-3 mt-10">
                <a href="#" data-href="<?php echo site_url('api/utils/laporan/excel_diagnostik_fungsional?tanggal=:TANGGAL'); ?>" class="cetak btn btn-success btn-labeled">
                    <b><i class="icon-file-excel"></i></b> 
                    Diagnostik Fungsional
                </a>
            </div>
            <div class="col-md-3 mt-10">
                <a href="#" data-href="<?php echo site_url('api/utils/laporan/excel_cathlab?tanggal=:TANGGAL'); ?>" class="cetak btn btn-success btn-labeled">
                    <b><i class="icon-file-excel"></i></b> 
                    Cathlab
                </a>
            </div>
            <div class="col-md-3 mt-10">
                <a href="#" data-href="<?php echo site_url('api/utils/laporan/excel_farmasi_rajal?tanggal=:TANGGAL'); ?>" class="cetak btn btn-success btn-labeled">
                    <b><i class="icon-file-excel"></i></b> 
                    FARMASI RAJAL
                </a>
            </div>
            <div class="col-md-3 mt-10">
                <a href="#" data-href="<?php echo site_url('api/utils/laporan/excel_farmasi_ranap?tanggal=:TANGGAL'); ?>" class="cetak btn btn-success btn-labeled">
                    <b><i class="icon-file-excel"></i></b> 
                    FARMASI RANAP
                </a>
            </div>
            <div class="col-md-3 mt-10">
                <a href="#" data-href="<?php echo site_url('api/utils/laporan/excel_farmasi_rajal_penjualan?tanggal=:TANGGAL'); ?>" class="cetak btn btn-success btn-labeled">
                    <b><i class="icon-file-excel"></i></b> 
                    Farmasi RAJAL Penjualan
                </a>
            </div>
            <div class="col-md-3 mt-10">
                <a href="#" data-href="<?php echo site_url('api/utils/laporan/excel_farmasi_ranap_penjualan?tanggal=:TANGGAL'); ?>" class="cetak btn btn-success btn-labeled">
                    <b><i class="icon-file-excel"></i></b> 
                    Farmasi RANAP Penjualan
                </a>
            </div>
            <div class="col-md-3 mt-10">
                <a href="#" data-href="<?php echo site_url('api/utils/laporan/excel_update_stock?tanggal=:TANGGAL'); ?>" class="cetak btn btn-success btn-labeled">
                    <b><i class="icon-file-excel"></i></b> 
                    Update Stock
                </a>
            </div>
            <div class="col-md-3 mt-10">
                <a href="#" data-href="<?php echo site_url('api/utils/laporan/excel_kasir_transaksi_masuk?tanggal=:TANGGAL'); ?>" class="cetak btn btn-success btn-labeled">
                    <b><i class="icon-file-excel"></i></b> 
                    Kasir (Transaksi Masuk)
                </a>
            </div>
            <div class="col-md-3 mt-10">
                <a href="#" data-href="<?php echo site_url('api/utils/laporan/excel_kasir_transaksi_selesai?tanggal=:TANGGAL'); ?>" class="cetak btn btn-success btn-labeled">
                    <b><i class="icon-file-excel"></i></b> 
                    Kasir (Transaksi Selesai)
                </a>
            </div>
            <div class="col-md-3 mt-10">
                <a href="#" data-href="<?php echo site_url('api/utils/laporan/excel_ok?tanggal=:TANGGAL'); ?>" class="cetak btn btn-success btn-labeled">
                    <b><i class="icon-file-excel"></i></b> 
                    OK
                </a>
            </div>
        </div>
    </div>
</div>

<script>
    $("select").select2();

    $("#disp_tanggal").datepicker( {
        format: " dd/mm/yyyy", // Notice the Extra space at the beginning
        autoclose: true
    }).on('changeDate', function(e) {
        var tanggal = moment(e.date).format('YYYY-MM-DD HH:mm');
        $("#tanggal").val(moment(e.date).format('YYYY-MM-DD HH:mm'));
    });

    $('.cetak').click(function (e) {
        e.preventDefault();
        var tanggal = $("#tanggal").val();
        var link = $(this).data('href').replace(':TANGGAL', tanggal);
        window.location.assign(link);
    });
</script>