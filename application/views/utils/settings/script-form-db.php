<script>
var UID = "<?php echo $uid; ?>";

var url = {
    index: "<?php echo site_url('utils/settings'); ?>",
    getData: "<?php echo site_url('api/utils/setting_database/get_data/:UID'); ?>",
    check: "<?php echo site_url(); ?>",
    save: "<?php echo site_url('api/utils/setting_database/save'); ?>"
};

var form = $("#form"),
    formDatabase = $("#database_form");

var modalDatabase = $("#modal_database");

function update() {
    //
}

function fillForm(uid) {
    $.getJSON(url.getData.replace(':UID', uid), function (data, status) {
        if (status === 'success') {
            data = data.data;

            $("#id").val(data.id);
            $("#uid").val(data.uid);

            $("#database-list").empty();
            $("#database-container").empty();
            if (data.value && data.value.length > 0) {
                for (var i = 0; i < data.value.length; i++) {
                    addDatabase(data.value[i]);
                }
            } else {
                //
            }

            update();
        }
    });
}

function fillFormDatabase(data) {
    $("#database_driver").val(data.driver);
    $("#database_hostname").val(data.hostname);
    $("#database_port").val(data.port);
    $("#database_username").val(data.username);
    $("#database_password").val(data.password);
    $("#database_database").val(data.database);
    $("#database_id").val(data.id);

    $("#database_driver").trigger('change');

    modalDatabase.modal('show');
}

function addDatabase(data) {
    var id = makeid(10);
    var label = data.label ? data.label : 'Server';
    var driver = data.driver;
    var hostname = data.hostname;
    var port = data.port;
    var username = data.username;
    var password = data.password;
    var database = data.database;

    var list = TEMPLATE_DB_LIST
                    .replace(/\{\{ID\}\}/g, id)
                    .replace(/\{\{LABEL\}\}/g, label);

    var db = TEMPLATE_DB
                    .replace(/\{\{ID\}\}/g, id)
                    .replace(/\{\{LABEL\}\}/g, label)
                    .replace(/\{\{DRIVER\}\}/g, driver)
                    .replace(/\{\{HOSTNAME\}\}/g, hostname)
                    .replace(/\{\{PORT\}\}/g, port)
                    .replace(/\{\{USERNAME\}\}/g, username)
                    .replace(/\{\{PASSWORD\}\}/g, password)
                    .replace(/\{\{DATABASE\}\}/g, database);

    $("#database-list").append(list);
    $("#database-container").append(db);
}

form.validate({
    rules: {
        //
    },
    messages: {
        //
    },
    focusInvalid: true,
    errorPlacement: function(error, element) {
        var inputGroup = $(element).closest('.input-group');
        var checkbox = $(element).closest('.checkbox-inline');

        if (inputGroup.length) {
            error.insertAfter(inputGroup);
        } else if (checkbox.length) {
            checkbox.append(error);
        } else {
            $(element).closest("div").append(error);
        }
    },
    submitHandler: function (form) {
        blockPage();
        var postData = $(form).serialize();
        $.ajax({
            url: url.save,
            type: 'POST',
            dataType: "json",
            data: postData,
            success: function (data) {
                console.log(data);
                new PNotify({
                    title: 'Success',
                    text: 'Data berhasil disimpan.',
                    addclass: 'alert alert-success alert-styled-right',
                    type: 'success'
                });

                $('[type=submit]').prop('disabled', true);
                $('input, select, textarea, [type=submit]').prop('disabled', true);
                setTimeout(function () {
                    window.location.assign(url.index);
                }, 3000);
            },
            error: function () {
                $.unblockUI();
                new PNotify({
                    title: 'Error',
                    text: 'Terjadi kesalahan saat hendak menyimpan data.',
                    addclass: 'alert alert-danger alert-styled-right',
                    type: 'danger'
                });
            },
            complete: function () {
                $.unblockUI();
            }
        });
    }
});

formDatabase.validate({
    rules: {
        driver: { required: true },
        hostname: { required: true },
        port: { required: true },
        username: { required: true },
        password: { required: true },
        database: { required: true }
    },
    messages: {
        driver: "Driver Diperlukan.",
        hostname: "Hostname Diperlukan.",
        port: "Port Diperlukan.",
        username: "Username Diperlukan.",
        password: "Password Diperlukan.",
        database: "Database Diperlukan."
    },
    focusInvalid: true,
    errorPlacement: function(error, element) {
        var inputGroup = $(element).closest('.input-group');
        var checkbox = $(element).closest('.checkbox-inline');

        if (inputGroup.length) {
            error.insertAfter(inputGroup);
        } else if (checkbox.length) {
            checkbox.append(error);
        } else {
            $(element).closest("div").append(error);
        }
    },
    submitHandler: function (form) {
        var postData = $(form).serializeObject();
        console.log(postData);
    }
});

$('.submit-button').click(function () {
    form.submit();
});

$('.add-button').click(function () {
    fillFormDatabase({});
});

$('.cancel-button').click(function () {
    window.location.assign(url.index);
});


// List Database
$("#database-list").sortable();
$("#database-list").disableSelection();

// Handler
$("#database-container").on('click', 'a.check', function (e) {
    e.preventDefault();
    var id = $(this).data('id');
    var element = $("#database-" + id);
    //
});

$("#database-container").on('click', 'a.edit', function (e) {
    e.preventDefault();
    var id = $(this).data('id');
    var element = $("#database-" + id);
    fillFormDatabase({
        id: element.find('[name="database_id[]"]').val(),
        label: element.find('[name="database_label[]"]').val(),
        driver: element.find('[name="database_driver[]"]').val(),
        hostname: element.find('[name="database_hostname[]"]').val(),
        port: element.find('[name="database_port[]"]').val(),
        username: element.find('[name="database_username[]"]').val(),
        password: element.find('[name="database_password[]"]').val(),
        database: element.find('[name="database_database[]"]').val()
    });
});

$("#database-container").on('click', 'a.delete', function (e) {
    e.preventDefault();
    var id = $(this).data('id');
    var element = $("#database-" + id);
    //
});

fillForm(UID);
</script>
<script>
var TEMPLATE_DB_LIST = '<li data-id="{{ID}}"><i class="icon-database pull-left"></i> {{LABEL}}</li>';

var TEMPLATE_DB = 
'<div id="database-{{ID}}" class="col-lg-4 col-md-6">' + 
    '<div class="panel panel-body">' + 
        '<div class="media">' + 
            '<div class="media-left">' + 
                '<a href="#" class="database text-muted">' + 
                    '<i class="icon-database" style="width: 70px;height: 70px;font-size: 64px;"></i>' + 
                '</a>' + 
            '</div>' +
            '<div class="media-body">' +
                '<h6 class="media-heading">{{LABEL}}</h6>' + 
                '<p class="text-muted no-margin">{{HOSTNAME}}</p>' + 
                '<p class="text-muted no-margin">{{PORT}}</p>' + 
                '<p class="text-muted">{{DATABASE}}</p>' + 
                '<ul class="icons-list">' + 
                    '<li><a class="check" data-id="{{ID}}" href="#" data-popup="tooltip" title="Check" data-container="body"><i class="icon-database-check"></i></a></li>' +
                    '<li><a class="edit" data-id="{{ID}}" href="#" data-popup="tooltip" title="Edit" data-container="body"><i class="icon-database-edit2"></i></a></li>' +
                    '<li><a class="delete" data-id="{{ID}}" href="#" data-popup="tooltip" title="Hapus" data-container="body"><i class="icon-database-remove"></i></a></li>' +
                '</ul>' +
            '</div>' +
        '</div>' + 
    '</div>' + 
    '<input type="hidden" name="database_id[]" value="{{ID}}" />' +
    '<input type="hidden" name="database_label[]" value="{{LABEL}}" />' +
    '<input type="hidden" name="database_driver[]" value="{{DRIVER}}" />' +
    '<input type="hidden" name="database_hostname[]" value="{{HOSTNAME}}" />' +
    '<input type="hidden" name="database_port[]" value="{{PORT}}" />' +
    '<input type="hidden" name="database_username[]" value="{{USERNAME}}" />' +
    '<input type="hidden" name="database_password[]" value="{{PASSWORD}}" />' +
    '<input type="hidden" name="database_database[]" value="{{DATABASE}}" />' +
'</div>';
</script>