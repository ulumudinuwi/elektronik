<style>
.form-control {
    text-transform: none;
}
</style>
<form class="form-horizontal" method="post" id="form">
    <div class="row">
        <div class="col-md-3">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h6 class="panel-title text-semibold">Databases</h6>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                            <li><a data-action="reload"></a></li>
                            <li><a data-action="close"></a></li>
                        </ul>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="text-center">
                        <ul class="selectable-demo-list" id="database-list">
                            <li><i class="icon-database pull-left"></i> Database</li>
                            <li><i class="icon-database pull-left"></i> Database Backup</li>
                            <li><i class="icon-database pull-left"></i> Item 3</li>
                            <li><i class="icon-database pull-left"></i> Item 4</li>
                            <li><i class="icon-database pull-left"></i> Item 5</li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-md-9">
            <div class="row" id="database-container">
                <?php for ($i = 0; $i < 10; $i++): ?>
                <div class="col-lg-4 col-md-6">
                    <div class="panel panel-body">
                        <div class="media">
                            <div class="media-left">
                                <a href="#" class="database text-muted">
                                    <i class="icon-database" style="width: 70px;height: 70px;font-size: 64px;"></i>
                                </a>
                            </div>
                            <div class="media-body">
                                <h6 class="media-heading">Server PTRSBT</h6>
                                <p class="text-muted no-margin">192.168.122.2</p>
                                <p class="text-muted no-margin">3306</p>
                                <p class="text-muted no-margin">imedis</p>

                                <ul class="icons-list">
                                    <li><a href="#" data-popup="tooltip" title="Check" data-container="body"><i class="icon-database-check"></i></a></li>
                                    <li><a href="#" data-popup="tooltip" title="Edit" data-container="body"><i class="icon-database-edit2"></i></a></li>
                                    <li><a href="#" data-popup="tooltip" title="Hapus" data-container="body"><i class="icon-database-remove"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endfor; ?>
            </div>
        </div>
    </div>

    <input type="hidden" id="id" name="id" value="0" />
    <input type="hidden" id="uid" name="uid" value="0" />
</form>

<!-- Modal Database -->
<div id="modal_database" class="modal fade" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form id="database_form" class="form-horizontal">
                <div class="modal-header bg-slate">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h5 class="modal-title">Detail Koneksi</h5>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <!-- Detail Koneksi -->
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <fieldset>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label" for="database_driver">Driver</label>
                                            <div class="col-lg-8">
                                                <select id="database_driver" name="driver" class="form-control">
                                                    <option value="0">- Pilih Driver -</option>
                                                    <option value="mysqli">MySql</option>
                                                    <!-- TODO -->
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label" for="database_hostname">Hostname</label>
                                            <div class="col-lg-5">
                                                <input type="text" class="form-control" name="hostname" id="database_hostname" placeholder="Hostname">
                                            </div>
                                            <div class="col-lg-3">
                                                <input type="text" class="form-control" name="port" id="database_port" placeholder="Port">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label" for="database_username">Username</label>
                                            <div class="col-lg-8">
                                                <input type="text" class="form-control" name="username" id="database_username" placeholder="Username">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label" for="database_password">Password</label>
                                            <div class="col-lg-8">
                                                <input type="password" class="form-control" name="password" id="database_password" placeholder="Password">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label" for="database_database">Database</label>
                                            <div class="col-lg-8">
                                                <input type="text" class="form-control" name="database" id="database_database" placeholder="Database">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="id" id="database_id" value="0" />
                    <button type="submit" class="btn btn-success btn-labeled">
                        <b><i class="icon-floppy-disk"></i></b>
                        Simpan
                    </button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        <b>Tutup</b>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Modal Database -->

<script>
$('select').select2();
$(".switch").bootstrapSwitch();
$(".styled, .multiselect-container input").uniform({
    radioClass: 'choice'
});
$('[data-popup=tooltip]').tooltip();

$(function() {
    // Default initialization
    $('.wysihtml5-default').wysihtml5({
        parserRules:  wysihtml5ParserRules,
        stylesheets: ["assets/css/components.css"]
    });


    // Simple toolbar
    $('.wysihtml5-min').wysihtml5({
        parserRules:  wysihtml5ParserRules,
        stylesheets: ["assets/css/components.css"],
        "font-styles": true, // Font styling, e.g. h1, h2, etc. Default true
        "emphasis": true, // Italics, bold, etc. Default true
        "lists": true, // (Un)ordered lists, e.g. Bullets, Numbers. Default true
        "html": false, // Button which allows you to edit the generated HTML. Default false
        "link": true, // Button to insert a link. Default true
        "image": false, // Button to insert an image. Default true,
        "action": false, // Undo / Redo buttons
        "color": false
    });


    // Editor events
    $('.wysihtml5-init').on('click', function() {
        $(this).off('click').addClass('disabled');
        $('.wysihtml5-events').wysihtml5({
            parserRules:  wysihtml5ParserRules,
            stylesheets: ["assets/css/components.css"],
            events: {
                load: function() { 
                    $.jGrowl('Editor has been loaded.', { theme: 'bg-slate-700', header: 'WYSIHTML5 loaded' });
                },
                change_view: function() {
                    $.jGrowl('Editor view mode has been changed.', { theme: 'bg-slate-700', header: 'View mode' });
                }
            }
        });
    });

    // Style form components
    $('.styled').uniform();
});
</script>