<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php echo $template['metas']; ?>

    <title><?php echo $template['title']; ?></title>

    <link rel="icon" href="<?php echo assets_url('img/logo/favicon.png') ?>" type="image/png">
    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet"
          type="text/css">
    <link href="<?php echo css_url('icons/icomoon/styles.css') ?>" rel="stylesheet">
    <link href="<?php echo css_url('bootstrap.css') ?>" rel="stylesheet">
    <link href="<?php echo css_url('core.css') ?>" rel="stylesheet">
    <link href="<?php echo css_url('components.css') ?>" rel="stylesheet">
    <link href="<?php echo css_url('colors.css') ?>" rel="stylesheet">
    <link href="<?php echo css_url('vmt.custom.css') ?>" rel="stylesheet">
    <!-- /global stylesheets -->

    <!-- Core JS files -->

    <script type="text/javascript" src="<?php echo js_url('core/libraries/jquery.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo js_url('core/libraries/bootstrap.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo js_url('plugins/loaders/blockui.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo js_url('plugins/forms/selects/select2.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo js_url('plugins/forms/styling/uniform.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo js_url('plugins/forms/styling/switchery.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo js_url('plugins/forms/styling/switch.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo bower_url('moment/min/moment.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo bower_url('numeraljs/min/numeral.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo bower_url('lodash/dist/lodash.min.js') ?>"></script>
    <script type="text/javascript">
        var base_url = '<?php echo site_url() ?>';
        var site_url = '<?php echo base_url() ?>';
        var img_url = '<?php echo image_url()?>';
    </script>
    <!-- /core JS files -->

    <?php echo $template['js_header']; ?>
    <!-- /theme JS files -->

</head>

<body style="background: transparent;">
<div class="">
    <?php echo $template['content']; ?>
</div>
<?php echo $template['js_footer']; ?>
<?php
if (isset($template['script']) && $template['script'] != '')
    $this->load->view($template['script']);
?>
</body>
</html>
