<html>
<body>
<style>
body {
    line-height: 1.2em;
    font-size: 12px;
    font-family: Arial, sans-serif;
}
h1, h2, h3, h4, h5, h6 {
    font-family: inherit;
    font-weight: 400;
    line-height: 1.5384616;
    color: inherit;
    margin-top: 0;
    margin-bottom: 5px;
}
h1 {
    font-size: 26px;
}
h2 {
    font-size: 20px;
}
h3 {
    font-size: 18px;
}
h4 {
    font-size: 16px;
}
h5 {
    font-size: 14px;
}
h6 {
    font-size: 12px;
}
table {
    border-collapse: collapse;
    font-size: 12px;
}
table th,
table td {
    vertical-align: top;
    padding: 3px 10px;
    line-height: 1.5384616;
}
.table thead th {
    color: #fff;
    background-color: #607D8B;
    font-weight: bold;
    text-align: center;
}
.text-left {
    text-align: left;
}
.text-center {
    text-align: center;
}
.text-right {
    text-align: right;
}
.text-bold {
  font-weight: bold;
}
.uppercase {
    text-transform: uppercase !important;
}
.text-size-mini {
    font-size: 8px;
}
.text-muted {
    color: #999999;
}
.footer_current_date_user {
    color: #d10404;
    vertical-align: top;
    margin-top: 10px;
}
</style>
<h4 class="text-center text-bold no-margin no-padding">PURCHASE ORDER</h4>
<h6 class="text-center text-bold no-margin no-padding">Nomor Invoice : <?php echo $obj->kode; ?></h6>
<table style="width: 100%;" style="font-size: 11px;">
    <tr>
        <td style="width: 50%;">
            <table>
                <tr>
                    <td class="text-bold">Tanggal</td>
                    <td>:</td>
                    <td><?php echo strtoupper($obj->indo_tanggal); ?></td>
                </tr>
                <tr>
                    <td class="text-bold">Supplier</td>
                    <td>:</td>
                    <td><?php echo $obj->pabrik; ?></td>
                </tr>
            </table>
        </td>
        <td style="width: 50%;">
            <table>
                <tr>
                    <td class="text-bold">Total Pembelian</td>
                    <td>:</td>
                    <td><?php echo number_format($obj->total, 2, ",", "."); ?></td>
                </tr>
                <tr>
                    <td class="text-bold">Status</td>
                    <td>:</td>
                    <td><?php echo $obj->status == 2 ? "Sudah Diterima" : "Belum Diterima"; ?></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<br/>
<table class="table" border="1" style="font-size: 11px; width: 100%;">
    <thead>
        <tr class="bg-slate">
            <th style="width: 8%;">NO.</th>
            <th style="width: 25%;">BARANG</th>
            <th style="width: 15%;">SATUAN</th>
            <th style="width: 15%;">HARGA</th>
            <th style="width: 12%;">QTY</th>
            <th style="width: 12%;">TOTAL HARGA</th>
        </tr>
    </thead>
    <?php 
        if(count($obj->details) > 0):
            $no = 1;
            $grandtotalharga = 0;
            foreach ($obj->details as $i => $row): 
            $totalharga = $row->harga * $row->qty;
            $grandtotalharga += $totalharga;
    ?>
    <tbody>
        <tr>
            <td class="text-center"><?php echo $no; ?></td>
            <td><?php echo $row->barang; ?></td>
            <td><?php echo $row->satuan_po; ?></td>
            <td><?php echo $row->harga; ?></td>
            <td class="text-right"><?php echo number_format($row->qty, 2, ",", "."); ?></td>
            <td><?php echo $totalharga; ?></td>
        </tr>
    <?php 
            $no++;
        endforeach; 
    ?>
    <tr>
        <td class="text-right" colspan="5">TOTAL</td>
        <td><?php echo $grandtotalharga; ?></td>
    </tr>
    </tbody>
    <?php else: ?>
    <tbody>
        <tr>
            <td style="font-weight: bold;text-align: center;" colspan="4">TIDAK ADA DATA</td>
        </tr>
    </tbody>
    <?php endif; ?>
</table>
<br />
<table style="width: 100%; font-size: 10px;">
    <tr>
        <td style="width: 30%;"></td>
        <td style="width: 40%;"></td>
        <td style="width: 30%;"></td>
    </tr>
    <tr>
        <td class="text-center">PENANGGUNG JAWAB</td>
        <td>&nbsp;</td>
        <td class="text-center">Supplier</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td class="text-bold text-center">( <?php echo $obj->pengajuan_by; ?> )</td>
        <td>&nbsp;</td>
        <td class="text-bold text-center">( <?php echo $obj->pabrik; ?> )</td>
    </tr>
</table>
<div class="footer_current_date_user text-size-mini">
    <span><?php echo $current_date.", ".$current_user; ?></span>
</div>
</body>
</html>