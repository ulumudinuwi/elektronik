<script type="text/javascript">
	var tanggalDari = "<?php echo date('Y-m-01'); ?>", tanggalSampai = "<?php echo date('Y-m-d'); ?>";
	var table;
	var tableDetail = $('#table-detail');
	var detailModal = $('#detail-modal');
	var url = {
		loadData : "<?php echo site_url('api/gudang_farmasi/afkir/load_data'); ?>",
		getData : "<?php echo site_url('api/gudang_farmasi/afkir/get_data/:UID'); ?>",
		listen: "<?php echo site_url('api/logistik/stock/listen?uid=:UID&modul=gudang_farmasi'); ?>",
	};

	function subsDate(range, tipe) {
		let date = range.substr(0, 10);
		if(tipe === "sampai") date = range.substr(13, 10);
		return getDate(date);
	}

	function listen(q) {
		eventSource = new EventSource(url.listen.replace(':UID', q));
		eventSource.addEventListener('gudang_farmasi-' + q, function(e) {
			table.draw(false);
		});
	}

	function showDetail(uid) {
		$.getJSON(url.getData.replace(':UID', uid), function (data, status) {
			if (status === 'success') {
				data = data.data;

				$("#detail_kode").html(data.kode);
				$("#detail_tanggal").html(moment(data.tanggal).format('DD/MM/YYYY HH:mm'));
				$("#detail_keterangan").html(data.keterangan);

				var isDTable = $.fn.dataTable.isDataTable(tableDetail);
				if(isDTable === true) tableDetail.DataTable().destroy();

				tableDetail.DataTable({
					"ordering": false,
					"processing": true,
					"aaData": data.details,
					"columns": [
						{ 
							"data": "barang",
							"render": function (data, type, row, meta) {
									let tmp = data;
									tmp += `<br/><span class="text-slate-300 text-bold text-xs no-padding-left">${row.kode_barang}</span><br/>`;
									return tmp;
							},
						},
						{ "data": "satuan" },
						{ 
							"data": "qty",
							"render": function (data, type, row, meta) {
									return `<span class="label-qty">${numeral(data).format('0.0,')}</span>`;
							},
							"className": "text-right"
						},
						{ 
							"data": "expired_date",
							"render": function (data, type, row, meta) {
									return data ? moment(data).format('DD/MM/YYYY') : "&mdash;";
							},
							"className": "text-center"
						},
					],
				});
				detailModal.modal('show');
			}
		});
	}

	function handleLoadTable() {
		table = $("#table").DataTable({
			"processing": true,
			"serverSide": true,
			"ajax": {
					"url": url.loadData,
					"type": "POST",
					"data": function(p) {
						p.tanggal_dari = subsDate($("#search_range_tanggal").val(), 'dari');
						p.tanggal_sampai = subsDate($("#search_range_tanggal").val(), 'sampai');
					}
			},
			"order": [1, "desc"],
			"columns": [
				{ 
					"data": "kode",
					"render": function (data, type, row, meta) {
							let tmp = '<a class="show-row" data-uid="' + row.uid + '" data-toggle="tooltip" data-title="Lihat Detail" data-placement="top">' + data + '</a>';
							return tmp;
					},
				},
				{
					"data": "tanggal",
					"render": function (data, type, row, meta) {
						let tmp = moment(data).format('DD-MM-YYYY HH:mm');
						tmp += `<br/><span class="text-size-mini text-info"><b>Afkir Oleh:</b><br/> ${row.created_by}</span>`;
						return tmp;
					},
					"searchable": false,
				},
				{ 
					"data": "keterangan",
					"orderable": false,
					"searchable": false,
				},
			],
			"fnDrawCallback": function (oSettings) {
				$('[data-toggle=tooltip]').tooltip();
			},
		});
	}

	$(window).ready(function() {
		handleLoadTable();

		$(".rangetanggal-form").daterangepicker({
			autoApply: true,
			locale: {
					format: "DD/MM/YYYY",
			},
			startDate: moment(tanggalDari),
			endDate: moment(tanggalSampai),
		});

		$("#search_range_tanggal").on('apply.daterangepicker', function (ev, picker) {
			table.draw();
		});

		$("#btn_search_tanggal").click(function () {
			$("#search_range_tanggal").data('daterangepicker').toggle();
		});

		$("#search_range_tanggal").on('change', function() {
			table.draw();
		});

		$("#table").on("click", ".show-row", function () {
			let uid = $(this).data('uid');
			showDetail(uid);
		});

		listen("<?php echo $tabUid; ?>");
	});
</script>