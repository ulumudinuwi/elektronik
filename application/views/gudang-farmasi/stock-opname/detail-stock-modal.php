<div id="detail-stock-modal" class="modal fade" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form class="form-horizontal">
                <div class="modal-header bg-slate">
                    <h4>Stock Detail</h4>
                </div>
                <div class="modal-body bg-light lt">
                    <div class="row m-t-md">
                        <div class="col-sm-12">
                            <div class="row form-horizontal">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Barang</label>
                                        <div class="col-md-8">
                                            <div class="form-control-static text-bold" id="label-data-barang"></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Satuan</label>
                                        <div class="col-md-8">
                                            <div class="form-control-static text-bold" id="label-data-satuan"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr class="no-margin-top">
                            <div class="table-responsive">
                                <table id="table-detail-stock" class="table table-striped table-bordered">
                                    <thead>
                                        <tr class="bg-slate">
                                            <th>No. Batch</th>
                                            <th>Exp. Date</th>
                                            <th>Qty</th>
                                            <th style="width: 10%;"></th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                    <tfoot>
                                        <tr>
                                            <tr>
                                                <td>
                                                    <button type="button" id="btn-detail-stock-tambah" class="btn btn-primary btn-labeled">
                                                        <b><i class="icon-plus-circle2"></i></b>Tambah
                                                    </button>
                                                </td>
                                                <td class="text-right text-bold">Total</td>
                                                <td class="text-right text-bold" id="label-stock-fisik">0</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="btn-detail-stock-simpan" class="btn btn-primary">Simpan</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                </div>
                <div>
                    <input type="hidden" id="tr_index" value="0" />
                </div>
            </form>
        </div>
    </div>
</div>