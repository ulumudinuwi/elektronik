<script type="text/javascript">
	var tanggalDari = "<?php echo date('Y-m-01'); ?>", tanggalSampai = "<?php echo date('Y-m-d'); ?>";
	let table,
			tableDouble,
			tableHistory,
			tableDetail,
			detailStockModal = $('#detail-stock-modal'),
			detailModal = $('#detail-modal');
	
	let selectedObat = [];

	var url = {
		loadData: "<?php echo site_url('api/gudang_farmasi/so/load_data?browse=:BROWSE'); ?>",
		loadDataBarang: "<?php echo site_url('api/gudang_farmasi/so/load_data_barang'); ?>",
		getData: "<?php echo site_url('api/gudang_farmasi/so/get_data/:UID'); ?>",
		save: "<?php echo site_url('api/gudang_farmasi/so/save'); ?>",
		doubleCheck: "<?php echo site_url('gudang_farmasi/so/double_check/:UID'); ?>",
		listen: "<?php echo site_url('api/logistik/stock/listen?uid=:UID&modul=gudang_farmasi'); ?>",
	};

	function subsDate(range, tipe) {
		let date = range.substr(0, 10);
		if(tipe === "sampai") date = range.substr(13, 10);
		return getDate(date);
	}

	function listen(q, browse) {
		eventSource = new EventSource(url.listen.replace(':UID', q));
		eventSource.addEventListener('gudang_farmasi-' + q, function(e) {
			switch(browse) {
				default:
					tableDouble.draw(false);
					tableHistory.draw(false);
					break;
			}
		}, false);
	}

	tableDetail = detailModal.find('.table-detail').DataTable({
		"ordering": false,
		"processing": true,
		"columns": [
			{ 
				"data": "barang",
				"render": function (data, type, row, meta) {
						let tmp = data;
						tmp += `<br/><span class="text-slate-300 text-bold text-xs no-padding-left">${row.kode_barang}</span>`;
						return tmp;
				},
			},
			{ "data": "satuan" },
			{ 
				"data": "stock_sistem",
				"render": function (data, type, row, meta) {
						return numeral(data).format('0.0,');
				},
				"className": "text-right"
			},
			{ 
				"data": "pengecekan1_stock_fisik",
				"render": function (data, type, row, meta) {
						return numeral(data).format('0.0,');
				},
				"className": "text-right"
			},
			{ 
				"data": "pengecekan1_selisih",
				"render": function (data, type, row, meta) {
						return numeral(data).format('0.0,');
				},
				"className": "text-right"
			},
			{ 
				"render": function (data, type, row, meta) {
					return genStockDetail(row, 'no_batch', 1);
				},
				"className": "text-center"
			},
			{ 
				"render": function (data, type, row, meta) {
					return genStockDetail(row, 'expired_date', 1);
				},
			},
			{ 
				"render": function (data, type, row, meta) {
					return genStockDetail(row, 'qty', 1);
				},
			},
			{ 
				"data": "pengecekan2_stock_fisik",
				"render": function (data, type, row, meta) {
						return numeral(data).format('0.0,');
				},
				"className": "text-right"
			},
			{ 
				"data": "pengecekan2_selisih",
				"render": function (data, type, row, meta) {
						return numeral(data).format('0.0,');
				},
				"className": "text-right"
			},
			{ 
				"render": function (data, type, row, meta) {
					return genStockDetail(row, 'qty', 2);
				},
			},
		],
		fnDrawCallback: function (oSettings) {
			let tr = detailModal.find('.table-detail').find("tbody tr");
			tr.each(function() { 
				$(this).find('td:eq(3), td:eq(4), td:eq(5), td:eq(6), td:eq(7)').css('background', '#DDDDDD');
			});
		},
	});

	function showDetail(uid) {
		$.getJSON(url.getData.replace(':UID', uid), function (data, status) {
			if (status === 'success') {
				data = data.data;

				detailModal.find(".detail_kode").html(data.kode);
				detailModal.find(".detail_tanggal").html(moment(data.tanggal).format('DD/MM/YYYY HH:mm'));
				detailModal.find(".detail_pengecekan1_by").html(data.pengecekan1_by);
				detailModal.find(".detail_pengecekan2_by").html(data.pengecekan2_by);
				
				tableDetail.clear().draw();
				tableDetail.rows.add(data.details).draw();

				detailModal.modal('show');
			}
		});
	}

	function handleLoadTable(browse) {
		switch(browse) {
			case 'table_barang':
				table = $("#table").DataTable({
					"ordering": false,
					"processing": true,
					"serverSide": true,
					"ajax": {
						"url": url.loadDataBarang,
						"type": "POST"
					},
					"columns": getColumnTable(0),
					drawCallback: function (oSettings) {
						$('[data-toggle=tooltip]').tooltip();

						let tr = $('#table').find("tbody tr");
						tr.each(function(i) { 
							let data = table.row(i).data();
							if(data) {
								for (var val in selectedObat) {
									if(data.barang_id === val) {
										table.row(i).data(selectedObat[val]);
										
										let inputCheck = parseInt($(this).find('.input-check').val());
										if(inputCheck === 1) {
											let cbEl = $(this).find('.disp-check');
											cbEl.prop('checked', true);
										}
									}
								}
							}
						});

						$('.disp-check').uniform();
					},
				});

				var isDTable = $.fn.dataTable.isDataTable($('#table-double'));
				if(isDTable === false) handleLoadTable('table_double');
				break;
			case 'table_double':
				tableDouble = $("#table-double").DataTable({
					"processing": true,
					"serverSide": true,
					"ajax": {
							"url": url.loadData.replace(':BROWSE', 1),
							"type": "POST",
					},
					"order": [1, "asc"],
					"columns": getColumnTable(1),
					"fnDrawCallback": function (oSettings) {
						$('[data-toggle=tooltip]').tooltip();
					},
				});

				var isDTable = $.fn.dataTable.isDataTable($('#table-history'));
				if(isDTable === false) handleLoadTable('table_history');
				break;
			default:
				tableHistory = $("#table-history").DataTable({
					"processing": true,
					"serverSide": true,
					"ajax": {
							"url": url.loadData.replace(':BROWSE', 2),
							"type": "POST",
							"data": function(p) {
									p.tanggal_dari = subsDate($("#search_history_range_tanggal").val(), 'dari');
									p.tanggal_sampai = subsDate($("#search_history_range_tanggal").val(), 'sampai');
							}
					},
					"order": [1, "desc"],
					"columns": getColumnTable(2),
					"fnDrawCallback": function (oSettings) {
						$('[data-toggle=tooltip]').tooltip();
					},
				});
				break;
		}
	}

	function getColumnTable(browse) {
		let column = [];
		if(browse == 0) {
			column = [
					{ 
						"render": function (data, type, row, meta) {
							return `<input type="checkbox" class="disp-check" disabled>`;
						},
					},
					{ 
						"render": function (data, type, row, meta) {
							return `<span class="label-barang">${row.barang}</span>` +
											`<br/><span class="text-slate-300 text-bold text-size-mini label-kode-barang mt-10">${row.kode_barang}</span>`;
						},
					},
					{ 
						"render": function (data, type, row, meta) {
							return `<label class="label-satuan">${row.satuan}</label>`;
						},
					},
					{ 
						"render": function (data, type, row, meta) {
							return `<span class="label-stock_sistem">${numeral(row.stock_sistem).format('0.0,')}</span>`;
						},
					},
					{ 
						"render": function (data, type, row, meta) {
							return `<input type="hidden" class="input-stock_id" name="stock_id[]" value="${row.stock_id}">` +
							`<input type="hidden" class="input-barang_id" name="barang_id[]" value="${row.barang_id}">` +
							`<input type="hidden" class="input-satuan_id" name="satuan_id[]" value="${row.satuan_id}">` +
							`<input type="hidden" class="input-isi_satuan" name="isi_satuan[]" value="1">` +
							`<input type="hidden" class="input-stock_sistem" name="stock_sistem[]" value="${row.stock_sistem}">` +
							`<input type="hidden" class="input-selisih" name="selisih[]" value="${row.selisih}">` +
							`<input type="hidden" class="input-stock_fisik" name="stock_fisik[]" value="${row.stock_fisik}">` +
							`<input type="hidden" class="input-check" value="${row.input_check}">` +
							`<span class="label-stock_fisik">${numeral(row.stock_fisik).format('0.0,')}</span>`;
						},
					},
					{ 
						"render": function (data, type, row, meta) {
							return `<span class="label-selisih">${numeral(row.selisih).format('0.0,')}</span>`;
						},
					},
					{ 
						"render": function (data, type, row, meta) {
							return genStockDetail(row, 'id', 0) + '' + genStockDetail(row, 'no_batch', 0);
						},
						"className": "text-center"
					},
					{ 
						"render": function (data, type, row, meta) {
							return `<button type="button" class="btn btn-link btn-xs edit-row"><i class="fa fa-edit"></i></button>`;
						},
					},
				];
		} else {
			column = [
					{ 
						"data": "kode",
						"render": function (data, type, row, meta) {
							let title = "Lihat Detail";
							if(browse == 1) title = "Pemeriksaan Kedua";

							let tmp = `<a class="show-row" data-uid="${row.uid}" data-toggle="tooltip" data-title="${title}" data-placement="top">${data}</a>`;
							return tmp;
						},
					},
					{
						"data": "tanggal",
						"searchable": false,
						"render": function (data, type, row, meta) {
							return moment(data).format('DD/MM/YYYY HH:mm');
						},
					},
				];

			switch(parseInt(browse)) {
				case 2:
					column.push(
						{ 
							"data": "pengecekan1_by",
							"orderable": false,
							"searchable": false,
							"render": function (data, type, row, meta) {
								let tmp = `${row.pengecekan1_by}<br/><span class="text-size-mini text-info">${moment(row.pengecekan1_at).format('DD/MM/YYYY HH:mm')}</span>`;
								return tmp;
							},
						},
						{ 
							"data": "pengecekan2_by",
							"orderable": false,
							"searchable": false,
							"render": function (data, type, row, meta) {
								let tmp = `${row.pengecekan2_by}<br/><span class="text-size-mini text-info">${moment(row.pengecekan2_at).format('DD/MM/YYYY HH:mm')}</span>`;
								return tmp;
							},
						}
					); 
					break;
				default:
					column.push({ 
						"data": "pengecekan1_by",
						"orderable": false,
						"searchable": false,
						"render": function (data, type, row, meta) {
							let tmp = `<b>Pemeriksaan pertama oleh:</b><br/><span class="text-size-mini text-info">${row.pengecekan1_by}</span><br/><span class="text-size-mini text-danger">${moment(row.pengecekan1_at).format('DD/MM/YYYY HH:mm')}</span>`;
							return tmp;
						},
					}); 
					break;
			}
		}
		return column;
	}

	function genStockDetail(data, key, mode) {
		let res = [];
		
		let tmp = `<span class="label-${key}">:LABEL_VAL</span>`;
		let details = null;
		switch(parseInt(mode)) {
			case 1:
				details = data.pengecekan1_detail_barang;
				break;
			case 2:
				details = data.pengecekan2_detail_barang;
				break;
			default:
				tmp = `<input type="hidden" class="input-${key}" name="${key}_${data.barang_id}[]" value=":VAL">` +
							`<span class="label-${key}">:LABEL_VAL</span>`;
				details = data.details;
				break;
		}

		for (i = 0; i < details.length; i++) {
			res.push(details[i][key]);
		}

		switch(key) {
			case 'expired_date':
				res = res.map((item) => item ? tmp.replace(':LABEL_VAL', moment(item).format('DD/MM/YYYY')).replace(':VAL', item) : tmp.replace(':LABEL_VAL', "&mdash;").replace(':VAL', ""));
				break;
			case 'qty':
				res = res.map((item) => tmp.replace(':LABEL_VAL', numeral(item).format('0.0,')).replace(':VAL', item));
				break;
			case 'id':
				tmp = `<input type="hidden" class="input-${key}" name="${key}_${data.barang_id}[]" value=":VAL">`;
				res = res.map((item) => tmp.replace(':VAL', item));
				break;
			default:
				res = res.map((item) => item ? tmp.replace(':LABEL_VAL', item).replace(':VAL', item) : tmp.replace(':LABEL_VAL', "&mdash;").replace(':VAL', item));
				break;
		}
		return key != "id" ? res.join('<br/>') : res.join('');
	}

	function addRowDetailStock(data) {
		let tbody = $("#table-detail-stock tbody");
		let mode = (typeof data.mode === "undefined" || data.mode === "edit" ? "edit" : "add");
		let inputType = 'hidden';
		if(mode === "add") inputType = 'text';

		let tr = $("<tr/>")
				.appendTo(tbody);

		let tdNoBatch = $("<td/>")
			.addClass('text-center')
			.appendTo(tr);
		let inputId = $("<input/>")
				.prop('type', 'hidden')
				.prop('name', 'detail_id[]')
				.addClass('input-detail_id')
				.val(data.id)
				.appendTo(tdNoBatch);
		let inputMode = $("<input/>")
			.prop('type', 'hidden')
			.addClass('input-detail_mode')
			.val(mode)
			.appendTo(tdNoBatch);
		let inputNoBatch = $("<input/>")
				.prop('type', inputType)
				.prop('name', 'no_batch[]')
				.addClass('input-detail_no_batch form-control')
				.prop('placeholder', 'No. Batch')
				.val(data.no_batch ? data.no_batch : "")
				.appendTo(tdNoBatch);
		if(mode !== "add") {
			let labelNoBatch = $("<label/>")
				.html(data.no_batch ? data.no_batch : "&mdash;")
				.appendTo(tdNoBatch);
		}

		let tdExpDate = $("<td/>")
				.addClass('text-center')
				.appendTo(tr);
		let inputExpDate = $("<input/>")
				.prop('type', inputType)
				.prop('name', 'expired_date[]')
				.addClass('input-detail_expired_date form-control')
				.prop('placeholder', 'DD/MM/YYYY')
				.val(data.expired_date ? moment(data.expired_date).format('DD/MM/YYYY') : '')
				.formatter({ pattern: '{{99}}/{{99}}/{{9999}}' })
				.appendTo(tdExpDate);
		if(mode !== "add") {
			let labelExpDate = $("<label/>")
				.html(data.expired_date ? moment(data.expired_date).format('DD/MM/YYYY') : "&mdash;")
				.appendTo(tdExpDate);
		}

		let tdQty = $("<td/>")
				.appendTo(tr);
		let inputQty = $("<input/>")
				.prop('type', 'text')
				.prop('name', 'detail_qty[]')
				.addClass('form-control text-right input-detail_qty')
				.autoNumeric('init', {aSep: '.', aDec: ',', mDec: '2'})
				.appendTo(tdQty);
		inputQty.autoNumeric('set',data.qty);
		inputQty.on('keyup change', function() {
			updateStockDetailTotal();      
		});

		let tdAction = $("<td/>")
				.addClass('text-center')
				.appendTo(tr);
		if(mode === "add") {
			let btnDel = $("<button/>")
					.prop('type', 'button')
					.addClass('btn btn-danger btn-xs remove-row')
					.html('<i class="fa fa-trash-o"></i>')
					.appendTo(tdAction);

			btnDel.click(function () {
					tr.remove();
					updateStockDetailTotal();
			});
		} else tdAction.append("&mdash;");

		updateStockDetailTotal();
	}

	function updateStockDetailTotal() {
		let total = 0;
		let length = $("#table-detail-stock tbody").find('tr').length;
		$("#table-detail-stock tbody").find('tr').each((i, el) => {
				let qty = isNaN(parseFloat($(el).find('input[name="detail_qty[]"]').autoNumeric('get'))) ? 0 : parseFloat($(el).find('input[name="detail_qty[]"]').autoNumeric('get'));
				total += qty;

				$(el).find('.remove-row').show();
				if(parseInt(length) <= 1) $(el).find('.remove-row').hide();
		});
		$("#label-stock-fisik").html(numeral(total).format('0.0,'));
	}

	$(window).ready(function() {

		$(".rangetanggal-form").daterangepicker({
			autoApply: true,
			locale: {
					format: "DD/MM/YYYY",
			},
			startDate: moment(tanggalDari),
			endDate: moment(tanggalSampai),
		});
		handleLoadTable('table_barang');

		$('a[data-toggle="tab"]').click(function (e) {
			switch($(this).attr('href')) {
				case '#tab-2':
					tableDouble.draw(false)
					break;
				case '#tab-3':
					tableHistory.draw(false);
					break;
			}
		});

		// TAB 1
		$(".disp_tanggal").daterangepicker({
			singleDatePicker: true,
			startDate: moment("<?php echo date('Y-m-d'); ?>"),
			endDate: moment("<?php echo date('Y-m-d'); ?>"),
			applyClass: "bg-slate-600",
			cancelClass: "btn-default",
			opens: "center",
			autoApply: true,
			locale: {
				format: "DD/MM/YYYY"
			}
		});

		$('#table').on('click', '.edit-row', function (e) {
			let tr = $(this).closest('tr');
			let data = table.row(tr).data();
			detailStockModal.find('#tr_index').val(tr[0]._DT_RowIndex);
			detailStockModal.find('#label-data-barang').html(`${data.barang}<br/><span class="text-slate-300 text-bold text-xs">${data.kode_barang}</span>`);
			detailStockModal.find('#label-data-satuan').html(data.satuan);

			$("#table-detail-stock tbody").empty();
			for (var i = 0; i < data.details.length; i++) {
				addRowDetailStock(data.details[i]);
			}
			detailStockModal.modal('show');
		});

		$('#table-detail-stock').on('keyup', '.input-detail_expired_date', function() {
			let td = $(this).parent();
			
			let value = $(this).val();
			let day = value.substr(0, 2);
			day = isNaN(day) ? 0 : day;
			let month = value.substr(3, 2);
			month = isNaN(month) ? 0 : month;
			let year = value.substr(6, 4);
			year = isNaN(year) ? 0 : year;

			td.find('.span-exdate-error').remove();
			$("#btn-detail-stock-simpan").prop('disabled', false);
			if(value != "") {
				if(parseInt(day) > 31 || parseInt(month) > 12 || year.length < 4) {
					$("#btn-detail-stock-simpan").prop('disabled', true);
					if(td.find('.span-exdate-error').length <= 0) 
						td.append('<span class="label label-block label-danger span-exdate-error">Format Tanggal Salah</span>');
				}
			}
		});

		$('#btn-detail-stock-tambah').click(function() {
			addRowDetailStock({
				id: 0,
				no_batch: "",
				expired_date: "",
				qty: 0,
				mode: 'add',
			});
		});

		$('#btn-detail-stock-simpan').click(function() {
			let trIndex = $('#tr_index').val();
			let trData = table.row(trIndex).data();
			for (var val in selectedObat) {
				if(trData.barang_id === val) {
					trData = selectedObat[val];
				}
			}

			trData['input_check'] = 1;
			trData['stock_fisik'] = numeral($('#label-stock-fisik').html())._value;
			trData['selisih'] = trData['stock_sistem'] - trData['stock_fisik'];
			//if(trData['selisih'] != 0) trData['input_check'] = 1;

			let details = [];
			let trs = $("#table-detail-stock tbody").find('tr');
			trs.each(function() {
				let id = $(this).find('.input-detail_id').val();
				let no_batch = $(this).find('.input-detail_no_batch').val();
				let expired_date = $(this).find('.input-detail_expired_date').val();
				let qty = $(this).find('.input-detail_qty').autoNumeric('get') ? $(this).find('.input-detail_qty').autoNumeric('get') : 0;
				let mode = $(this).find('.input-detail_mode').val();

				details.push({
					id: id,
					no_batch: no_batch,
					expired_date: expired_date ? getDate(expired_date) : "",
					qty: qty,
					mode: mode,
				});
			});
			trData['details'] = details;

			selectedObat[trData.barang_id] = trData;
			table.row(trIndex).data(trData).draw(false);
			detailStockModal.modal('hide');
		});

		$('#form').validate({
			rules: {
				tanggal: { required: true },
			},
			focusInvalid: true,
			errorPlacement: function(error, element) {
					var placement = $(element).closest('.input-group');
					if (placement.length > 0) {
							error.insertAfter(placement);
					} else {
							error.insertAfter($(element));
					}
			},
			submitHandler: function (form) {
				var formData = new FormData();
				formData.append('tanggal', $('#tanggal').val());

				for (var dt_index in selectedObat) {
					let trData = selectedObat[dt_index];
					
					let check = parseInt(trData.input_check);
					if(check == 1) {
						let stock_id = trData.stock_id;
						let barang_id = trData.barang_id;
						let satuan_id = trData.satuan_id;
						let isi_satuan = 1;
						let stock_sistem = trData.stock_sistem;
						let selisih = trData.selisih;
						let stock_fisik = trData.stock_fisik;

						formData.append('stock_id[]', stock_id);
						formData.append('barang_id[]', barang_id);
						formData.append('satuan_id[]', satuan_id);
						formData.append('isi_satuan[]', isi_satuan);
						formData.append('stock_sistem[]', stock_sistem);
						formData.append('selisih[]', selisih);
						formData.append('stock_fisik[]', stock_fisik);

						for (let i = 0; i < trData.details.length; i++) {
							let detailData = trData.details[i];
							formData.append(`id_${trData.barang_id}[]`, detailData.id);
							formData.append(`no_batch_${trData.barang_id}[]`, detailData.no_batch);
							formData.append(`expired_date_${trData.barang_id}[]`, detailData.expired_date);
							formData.append(`qty_${trData.barang_id}[]`, detailData.qty);
						}
					}
				}

				swal({
					title: "Konfirmasi?",
					type: "warning",
					text: "Apakah pemeriksaan pertama yang dimasukan benar ??",
					showCancelButton: true,
					confirmButtonText: "Ya",
					confirmButtonColor: "#2196F3",
					cancelButtonText: "Batal",
					cancelButtonColor: "#FAFAFA",
					closeOnConfirm: true,
					showLoaderOnConfirm: true,
				},
				function() {
					blockPage('Sedang diproses ...');
					$.ajax({
						data: formData,
						type: 'POST',
						dataType: 'JSON', 
						processData: false,
						contentType: false,
						url: url.save,
						success: function(data){
								$.unblockUI();
								swal('Berhasil', 'Pemeriksaan pertama berhasil disimpan.', 'info');
								setTimeout(function() { 
									window.location.reload();
								}, 1500);
						},
						error: function(data){
								$.unblockUI();
								errorMessage('Peringatan', "Terjadi kesalahan saat memproses data.");
						}
					});
					return false;
				});
			}
		});

		// TAB 2
		$("#table-double").on("click", ".show-row", function() {
			let uid = $(this).data('uid');
			blockPage('Form pemeriksaan kedua sedang diproses ...');
			setTimeout(function() { 
				window.location.assign(url.doubleCheck.replace(':UID', uid));
			}, 1000);
		});

		// TAB 3
		$("#search_history_range_tanggal").on('apply.daterangepicker', function (ev, picker) {
			tableHistory.draw();
		});

		$("#btn_search_history_tanggal").click(function () {
			$("#search_history_range_tanggal").data('daterangepicker').toggle();
		});

		$("#search_history_range_tanggal").on('change', function() {
			tableHistory.draw();
		});

		$("#table-history").on("click", ".show-row", function () {
			let uid = $(this).data('uid');
			showDetail(uid);
		});

		detailModal.on('shown.bs.modal', function (e) {
			tableDetail.draw(false);
		});

		listen("<?php echo $tabUid; ?>", 1);
	});
</script>