<script type="text/javascript">
	var tanggalDari = "<?php echo date('Y-m-01'); ?>", tanggalSampai = "<?php echo date('Y-m-d'); ?>";
	var table;
	var tableDetail = $('#table-detail');
	var detailModal = $('#detail-modal');
	var url = {
		loadData : "<?php echo site_url('api/pesan/load_data'); ?>",
		getData : "<?php echo site_url('api/pesan/get_data/:UID'); ?>",
		read : "<?php echo site_url('api/pesan/read'); ?>",
	};

	function subsDate(range, tipe) {
		let date = range.substr(0, 10);
		if(tipe === "sampai") date = range.substr(13, 10);
		return getDate(date);
	}

	function showDetail(uid) {
		$.getJSON(url.getData.replace(':UID', uid), function (data, status) {
			if (status === 'success') {
				data = data.data;

				$("#detail_name").html(data.name);
				$("#detail_email").html(data.email);
				$("#detail_subject").html(data.subject);
				$("#detail_notelp").html(data.phone_number);
				$("#detail_deskripsi").html(data.description);

				detailModal.modal('show');

				readMessage(data.uid);
			}
		});
	}

	function readMessage(uid){
		let data = {
			uid: uid,
		};

		$.ajax({
			data: data,
			type: 'POST',
			dataType: 'JSON', 
			url: url.read,
			success: function(data){
				table.draw();
			},
			error: function(data){
				errorMessage('Peringatan', "Terjadi kesalahan saat memproses data.");
			}
		});

		table.draw();
	}

	function handleLoadTable() {
		table = $("#table").DataTable({
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": url.loadData,
				"type": "POST",
				"data": function(p) {
					p.tanggal_dari = subsDate($("#search_range_tanggal").val(), 'dari');
					p.tanggal_sampai = subsDate($("#search_range_tanggal").val(), 'sampai');
				}
			},
			"order": [0, "asc"],
			"columns": [
			{ 
				"data": "uid",
				'className': 'text-center',
				"render": function (data, type, row, meta) {
					let title = "Baca Pesan .",
					btnClass = "btn-info";
					if (row.status == 2) {
						title = "Pesan Sudah dibaca .";
						btnClass = "btn-success";
					}

					let tmp = '<button class="show-row btn '+btnClass+'" data-uid="' + row.uid + '" data-toggle="tooltip" data-title="'+title+'" data-placement="top"><b><i class="fa fa-eye"></i></b></button>';
					return tmp;
				},
			},
			{ 
				"data": "name",
				"orderable": false,
				"searchable": false,
				"render": function (data, type, row, meta) {
					return data;
				},
			},
			{ 
				"data": "email",
				"orderable": false,
				"searchable": false,
			},
			{ 
				"data": "subject",
				"orderable": false,
				"searchable": false,
			},
			],
			"fnDrawCallback": function (oSettings) {
				$('[data-toggle=tooltip]').tooltip();
			},
		});
	}

	$(window).ready(function() {
		$(".rangetanggal-form").daterangepicker({
			autoApply: true,
			locale: {
				format: "DD/MM/YYYY",
			},
			startDate: moment(tanggalDari),
			endDate: moment(tanggalSampai),
		});

		$("#search_range_tanggal").on('apply.daterangepicker', function (ev, picker) {
			table.draw();
		});

		$("#btn_search_tanggal").click(function () {
			$("#search_range_tanggal").data('daterangepicker').toggle();
		});

		$("#search_range_tanggal").on('change', function() {
			table.draw();
		});

		$("#table").on("click", ".show-row", function () {
			let uid = $(this).data('uid');
			showDetail(uid);
		});

		handleLoadTable();
	});
</script>