<div id="detail-modal" class="modal fade" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header bg-primary">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h6 class="modal-title">Detail pesan</h6>
			</div>
			<div class="modal-body form-horizontal">
				<div class="row mb-20">
					<fieldset>
						<div class="col-md-12">
							<legend class="text-bold"><i class="icon-magazine position-left"></i> Data pesan</legend>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-lg-3 control-label">Name</label>
								<div class="col-lg-6">
									<div class="form-control-static text-bold" id="detail_name"></div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-3 control-label">Email</label>
								<div class="col-lg-6">
									<div class="form-control-static text-bold" id="detail_email"></div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label col-md-3">Subject</label>
								<div class="col-md-6">
									<div class="form-control-static text-bold" id="detail_subject"></div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-3 control-label">No. Telp</label>
								<div class="col-lg-6">
									<div class="form-control-static text-bold" id="detail_notelp"></div>
								</div>
							</div>
						</div>
					</fieldset>
					<fieldset>
						<div class="col-md-12">
							<legend class="text-bold"><i class="icon-magazine position-left"></i> Deskripsi</legend>
							<div class="row">
								<div class="col-sm-12">
									<div class="form-control-static">
										<p id="detail_deskripsi"></p>
									</div>
								</div>
							</div>
						</div>
					</fieldset>
				</div>
			</div>
			<div class="modal-footer m-t-none">
				<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
			</div>
		</div>
	</div>
</div>