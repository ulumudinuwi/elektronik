<script>
var uid = "<?php echo $uid; ?>",
form = '#form';
$('select').select2();
var url = {
  index: "<?php echo site_url('master/kelompok_barang'); ?>",
  save: "<?php echo site_url('api/master/kelompok_barang/save'); ?>",
  getData: "<?php echo site_url('api/master/kelompok_barang/get_data/:UID'); ?>",
}

function fillForm(uid) {
  blockElement(form);
  $.getJSON(url.getData.replace(':UID', uid), function(data, status) {
    if (status === 'success') {
      data = data.data;

      $("#uid").val(data.uid);
      $("#nama").val(data.nama);
      $("#deskripsi").val(data.deskripsi);
      $("#old_foto_id").val(data.foto_id);
      $("#bpom").val(data.bpom).trigger('change');
      $("#priviewImg").prop('src', data.url_foto_id);

      $('.el-hidden').show('slow');
      $(form).unblock();
    }
  });
}

$(form).validate({
  rules: {
    nama: { required: true },
    //jenis_barang_id: { required: true },
  },
  messages: {
    nama: "Nama Diperlukan",
    //jenis_barang_id: "Jenis Diperlukan",
  },
  errorPlacement: function(error, element) {
    var placement = $(element).closest('.input-group');
    if (placement.length > 0) {
        error.insertAfter(placement);
    } else {
        error.insertAfter($(element));
    }
  },
  focusInvalid: true,
  submitHandler: function (form) {
    $('input, textarea, select').prop('disabled', false);
    
    blockElement($(form));
    var postData = $(form).serializeArray();
    var formData = new FormData($(form)[0]);

    for (var i = 0; i < postData.length; i++) {
        if (postData[i].name != 'foto_id' && postData[i].name.search(/\[\]/) === -1) {
            formData.delete(postData[i].name);
            formData.append(postData[i].name, postData[i].value);
        }
    }

    $.ajax({
      data: formData,
      processData: false,
      contentType: false,
      type: 'POST',
      dataType: 'JSON', 
      url: url.save,
      success: function(data){
          $(form).unblock();
          successMessage('Berhasil', "Kelompok barang berhasil disimpan.");
          window.location.assign(url.index);
      },
      error: function(data){
          $(form).unblock();
          errorMessage('Peringatan', "Terjadi kesalahan saat memproses data.");
      }
    });
    return false;
  }
});

$(".cancel-button").click(function () {
  window.location.assign(url.index);
});

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            //$('#priviewImg').css('background-image', 'url('+e.target.result +')');
            $('#priviewImg').prop('src', e.target.result);
            $('#priviewImg').hide();
            $('#priviewImg').fadeIn(650);
        }
        reader.readAsDataURL(input.files[0]);
    }
}

$("#foto_id").change(function() {
    readURL(this);
});

if(uid !== "") fillForm(uid);
</script>