<style>

</style>
<form class="form-horizontal" method="post" id="form">
  <div class="panel panel-flat">
    <div class="panel-body">
      <div class="text-right">
        <button type="submit" class="btn btn-success btn-labeled">
          <b><i class="icon-floppy-disk"></i></b>
          Simpan
        </button>
        <button type="button" class="btn btn-default cancel-button">Kembali</button>
      </div>
      <legend class="text-bold" style="margin-top: -20px"> &nbsp;</legend>

      <div class="row">
        <div class="col-sm-12 col-md-6 col-lg-6">
          <fieldset>
            <legend class="text-bold">
              <i class="fa fa-list-alt position-left"></i>
              <strong>Data <?php echo $this->lang->line('vendor_label'); ?></strong>
            </legend>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group el-hidden">
                  <label class="col-lg-4 control-label input-required">Kode</label>
                  <div class="col-lg-8">
                    <input type="text" class="form-control" name="kode" id="kode" placeholder="Kode ..." disabled="disabled">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-lg-4 control-label input-required">Nama</label>
                  <div class="col-lg-8">
                    <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama ...">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-lg-4 control-label">Alamat</label>
                  <div class="col-lg-8">
                    <textarea class="form-control wysihtml5-min" id="alamat" name="alamat"></textarea>
                  </div>
                </div>
              </div>
            </div>
          </fieldset>
        </div>

        <div class="col-sm-12 col-md-6 col-lg-6">
          <fieldset>
            <legend class="text-bold">
              <i class="fa fa-list-alt position-left"></i>
              <strong>Kontak</strong>
            </legend>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="col-lg-4 control-label">Penanggung Jawab</label>
                  <div class="col-lg-8">
                    <input type="text" class="form-control" name="penanggung_jawab" id="penanggung_jawab" >
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-lg-4 control-label">Jabatan</label>
                  <div class="col-lg-8">
                    <input type="text" class="form-control" name="jabatan" id="jabatan" >
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-lg-4 control-label">Telepon</label>
                  <div class="col-lg-8">
                    <input type="text" class="form-control" name="telepon" id="telepon" >
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-lg-4 control-label">Fax</label>
                  <div class="col-lg-8">
                    <input type="text" class="form-control" name="fax" id="fax">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-lg-4 control-label">Email</label>
                  <div class="col-lg-8">
                    <input type="text" class="form-control" name="email" id="email" >
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-lg-4 control-label">Website</label>
                  <div class="col-lg-8">
                    <input type="text" class="form-control" name="website" id="website" >
                  </div>
                </div>
              </div>
            </div>
          </fieldset>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-12 col-md-6 col-lg-6">
          <fieldset>
            <legend class="text-bold">
              <i class="fa fa-list-alt position-left"></i>
              <strong>Data Pembayaran</strong>
            </legend>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="col-lg-4 control-label">NPWP</label>
                  <div class="col-lg-8">
                    <input type="text" class="form-control" name="npwp" id="npwp" >
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-lg-4 control-label">Bank</label>
                  <div class="col-lg-8">
                    <input type="text" class="form-control" name="bank" id="bank" >
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-lg-4 control-label">No. Rekening</label>
                  <div class="col-lg-8">
                    <input type="text" class="form-control" name="no_rekening" id="no_rekening" >
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-lg-4 control-label">Atas Nama</label>
                  <div class="col-lg-8">
                    <input type="text" class="form-control" name="no_rekening_atas_nama" id="no_rekening_atas_nama" >
                  </div>
                </div>
              </div>
            </div>
          </fieldset>
        </div>
        <!-- Data Lain-lain -->
        <div class="col-sm-12 col-md-6 col-lg-6">
          <fieldset>
            <legend class="text-bold">
              <i class="fa fa-list-alt position-left"></i>
              <strong>Data Lain-Lain</strong>
            </legend>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="col-lg-4 control-label">Deskripsi</label>
                  <div class="col-lg-8">
                    <textarea id="deskripsi" name="deskripsi" class="form-control wysihtml5-min"></textarea>
                  </div>
                </div>
              </div>
            </div>
          </fieldset>
        </div>
      </div>
  
      <!-- Input Hidden -->
      <input type="hidden" name="uid" id="uid" value="" />

      <legend class="text-semibold"> &nbsp;</legend>
      <div class="text-right">
        <button type="submit" class="btn btn-success btn-labeled">
          <b><i class="icon-floppy-disk"></i></b>
          Simpan
        </button>
        <button type="button" class="btn btn-default cancel-button">Kembali</button>
      </div>
    </div>
  </div>
</form>
