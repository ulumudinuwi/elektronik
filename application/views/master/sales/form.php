<?php echo messages(); ?>
<form class="form-horizontal" method="POST" id="myForm">
    <input type="hidden" name="uid" value="<?php echo isset($sales) ? $sales->uid : ''; ?>">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-white">
                <div class="panel-heading">
                    <div class="text-right">
                        <button type="submit" class="btn btn-success btn-labeled">
                            <b><i class="icon-floppy-disk"></i></b>
                            Simpan
                        </button>
                        <a href="javascript:window.history.go(-1);" class="btn btn-default cancel-button">
                            Batal
                        </a>
                    </div>
                </div>
                <div class="line"></div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="panel panel-white no-border">
                                <div class="panel-heading">
                                    <h6 class="panel-title">
                                        <i class="icon-users position-left"></i>
                                        FORM SALES
                                    </h6>
                                </div>
                                
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label class="col-lg-4 control-label">
                                            Nama
                                        <span class="text-danger">*</span></label>
                                        <div class="col-lg-8">
                                            <input type="text" class="form-control" name="nama" value="<?php echo isset($sales) ? $sales->nama : '' ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-4 control-label">
                                            Tempat Lahir
                                        <span class="text-danger">*</span></label>
                                        <div class="col-lg-8">
                                            <input type="text" class="form-control" name="tempat_lahir" value="<?php echo isset($sales) ? $sales->tempat_lahir : '' ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-4 control-label">
                                            Tanggal Lahir
                                        <span class="text-danger">*</span></label>
                                        <div class="col-lg-8">
                                            <input type="text" class="form-control date_mask" name="tgl_lahir" value="<?php echo isset($sales) ? date_to_view($sales->tgl_lahir) : '' ?>" onkeyup="global.method.checkDate(this)">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-4 control-label input-required" for="jenis_kelamin">
                                            Jenis Kelamin
                                        </label>
                                        <div class="col-lg-6 parent_styled">
                                            <div class="col-lg-6">
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" name="jenis_kelamin" class="styled" value="1" <?php if(isset($sales) && $sales->jenis_kelamin == 1) echo 'checked'; ?>> 
                                                        Laki-Laki 
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" name="jenis_kelamin" class="styled" value="0" <?php if(isset($sales) && $sales->jenis_kelamin == 0) echo 'checked'; ?>> 
                                                        Perempuan
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-4 control-label">
                                            No.Hp
                                        <span class="text-danger">*</span></label>
                                        <div class="col-lg-8">
                                            <input type="text" class="form-control" name="no_hp" value="<?php echo isset($sales) ? $sales->no_hp : '' ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-4 control-label">
                                            Kode Promo
                                        <span class="text-danger">*</span></label>
                                        <div class="col-lg-8">
                                            <input type="text" class="form-control" name="kode_promo" value="<?php echo isset($sales) ? $sales->kode_promo : '' ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-4 control-label">
                                            Alamat
                                        <span class="text-danger">*</span></label>
                                        <div class="col-lg-8">
                                            <textarea name="alamat" class="form-control" rows="3"><?php echo isset($sales) ? $sales->alamat : '' ?></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-4 control-label">
                                            No.Ktp
                                        <span class="text-danger">*</span></label>
                                        <div class="col-lg-8">
                                            <input type="text" class="form-control" name="no_ktp" value="<?php echo isset($sales) ? $sales->no_ktp : '' ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-4 control-label">
                                            Tgl.Bergabung
                                        <span class="text-danger">*</span></label>
                                        <div class="col-lg-8">
                                            <input type="text" class="form-control date_mask" name="tgl_bergabung" value="<?php echo isset($sales) ? date_to_view($sales->tgl_bergabung) : '' ?>" onkeyup="global.method.checkDate(this)">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="panel panel-white no-border">
                                <div class="panel-heading">
                                    <h6 class="panel-title">
                                        <i class="icon-user position-left"></i>
                                        AKUN USER
                                    </h6>
                                </div>
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label class="col-lg-4 control-label">
                                            Username
                                        <span class="text-danger">*</span></label>
                                        <div class="col-lg-8">
                                            <input type="hidden" class="form-control" name="username_default" value="<?php echo isset($akun_user) ? $akun_user->username : ''; ?>">
                                            <input type="text" class="form-control" name="username" value="<?php echo isset($akun_user) ? $akun_user->username : ''; ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-4 control-label">
                                            Email
                                        <span class="text-danger">*</span></label>
                                        <div class="col-lg-8">
                                            <input type="hidden" class="form-control" name="email_default" value="<?php echo isset($akun_user) ? $akun_user->email : ''; ?>">
                                            <input type="text" class="form-control" name="email" value="<?php echo isset($akun_user) ? $akun_user->email : ''; ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-4 control-label">
                                            Password
                                        <span class="text-danger">*</span></label>
                                        <div class="col-lg-8">
                                            <input type="password" class="form-control" name="password">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-4 control-label">
                                            Konfirmasi Password
                                        <span class="text-danger">*</span></label>
                                        <div class="col-lg-8">
                                            <input type="password" class="form-control" name="password_confirmation">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <div class="text-right">
                        <button type="submit" class="btn btn-success btn-labeled">
                            <b><i class="icon-floppy-disk"></i></b>
                            Simpan
                        </button>
                        <a href="javascript:window.history.go(-1);" class="btn btn-default cancel-button">
                            Batal
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>


