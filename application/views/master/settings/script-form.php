<script>
let form = '#form',
	url = {
		index: "<?php echo site_url('master/settings'); ?>", 
  	save: "<?php echo site_url('api/master/options/save'); ?>",
    loadDataLookup: "<?php echo site_url('api/master/options/load_data_lookup'); ?>",
	};

let table = $("#table_lookup"),
    tableDt,
    tableTimer;


$(document).ready(function() {
  $(".cancel-button").click(function () {
    window.location.assign(url.index);
  });

  $('#btn-table_lookup').on('click', function () {
      tableDt.fnFilter($("#option_table").val(), 0);
      tableDt.fnDraw();
      $("#modal_lookup").modal('show');
  });

  tableDt = table.dataTable({
    "sPaginationType": "full_numbers",
    "bProcessing": true,
    "bServerSide": true,
    "sAjaxSource": url.loadDataLookup,
    "columns": [
        {
            "data": "kode",
            "render": function (data, type, row, meta) {
                return '<a href="#" class="click" data-id="' + row.id + '" data-nama="' + row.nama + '">' + data + '</a>';
            },
            "className": ""
        },
        {
            "data": "nama",
            "render": function (data, type, row, meta) {
                return data;
            },
            "className": ""
        }
    ],
    "order": [ [0, "asc"] ],
    "fnServerData": function ( sSource, aoData, fnCallback ) {
        if (tableTimer) clearTimeout(tableTimer);
        tableTimer = setTimeout(function () {
            blockElement(table.selector);
            $.getJSON( sSource, aoData, function (json) {
                fnCallback(json);
            });
        }, 500);
    },
    "fnDrawCallback": function (oSettings) {
      var n = oSettings._iRecordsTotal;
      
      table.unblock();

      table.find('[data-popup=tooltip]').tooltip();
    }
  });

  table.on('click', '.click', function (e) {
      e.preventDefault();
      let id = $(this).data('id');
      let nama = $(this).data('nama');
      $("#option_value").html(id).val(id);
      $("#label-option_value").html(nama);
      $("#modal_lookup").modal('hide');
  });

	$(form).validate({
    	rules: {
      		option_value: { required: true },
    	},
    	messages: {},
    	focusInvalid: true,
    	submitHandler: function (form) {
    		blockElement($(form));
      		
      		let formData = $(form).serialize();
      		$.ajax({
        		data: formData,
        		type: 'POST',
        		dataType: 'JSON', 
        		url: url.save,
        		success: function(data){
            		$(form).unblock();
            		successMessage('Berhasil', "Data berhasil disimpan.");
            		window.location.assign(url.index);
        		},
        		error: function(data){
            		$(form).unblock();
            		errorMessage('Peringatan', "Terjadi kesalahan saat memproses data.");

            		$('.input-decimal').each(function() {
						$(this).autoNumeric('set', $(this).val());
					});
        		}
      		});
      		return false;
    	}
  	});
});
</script>