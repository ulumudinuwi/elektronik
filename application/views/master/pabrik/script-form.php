<script>
var uid = "<?php echo $uid; ?>",
form = '#form';
var url = {
	index: "<?php echo site_url('master/principal'); ?>",
	save: "<?php echo site_url('api/master/pabrik/save'); ?>",
	getData: "<?php echo site_url('api/master/pabrik/get_data/:UID'); ?>",
	getKurs: "<?php echo site_url('api/master/kurs/get_all'); ?>",
	getVendor: "<?php echo site_url('api/master/vendor/get_all'); ?>",
}

var tableDetail = $('#table-detail');

function fillElement(obj, element) {
	let parent = element.parent();
	parent.find('.loading-select').show();
	$.getJSON(obj.url, function(data, status) {
		if (status === 'success') {
			var option = '';
			option += '<option value="" selected="selected">- Pilih -</option>';
			for (var i = 0; i < data.list.length; i++) {
				let selected = ""
				if (parseInt(obj.value) === parseInt(data.list[i].id)) selected = 'selected="selected"';

				let value_name = data.list[i].nama; 
				let data_kode = "";
				if(element.selector == '#kurs_id') {
					value_name = data.list[i].country_id + ' - ' + data.list[i].currency_name;
				} else {
					if (data.list[i].kode) {
						data_kode = `data-kode="${data.list[i].kode}"`;
						value_name = data.list[i].kode + ' - ' + data.list[i].nama;
					} 
				}
				option += '<option value="' + data.list[i].id + '" ' + data_kode + ' ' + selected + '>' + value_name + '</option>';
			}
			element.html(option).trigger("change");
		}
		parent.find('.loading-select').hide();
	});
}

function fillForm(uid) {
	if(uid === "") {
		fillElement({
			value: '',
			url: url.getKurs
		}, $('#kurs_id'));
		/*addVendor({
			id: 0,
			vendor_id: '',
			is_default: 1,
		});*/
		return;
	}

	blockElement(form);
	$.getJSON(url.getData.replace(':UID', uid), function(data, status) {
		if (status === 'success') {
			data = data.data;
			$('.el-hidden').show('slow');
			$("#id").val(data.id);
			$("#uid").val(data.uid);
			$("#kode").val(data.kode);
			$("#nama").val(data.nama);
			$("#alamat").html(data.alamat).data("wysihtml5").editor.setValue(data.alamat);
			fillElement({
				value: data.kurs_id,
				url: url.getKurs
			}, $('#kurs_id'));

			/*tableDetail.find('tbody').empty();
			if(data.details.length <= 0) {
				addVendor({
					id: 0,
					vendor_id: '',
					is_default: 1,
				});
			} else {
				for (var i = 0, n = data.details.length; i < n; i++) {
					addVendor(data.details[i]);
				}
			}*/
			$(form).unblock();
		}
	});
}

/*function addVendor(data) {
	data = data || {};

	var tbody = tableDetail.find('tbody');

	var count = $('[name="vendor_id[]"]').length;

	var ID = (count + 1);

	var tr = $("<tr/>")
	.prop('id', 'row-vd-' + ID)
	.appendTo(tbody);

	var tdVendor = $("<td/>")
	.appendTo(tr);
	var inputId = $("<input/>")
	.addClass('vd-input_id')
	.prop('type', 'hidden')
	.prop('name', 'detail_id[]')
	.val(data.id)
	.appendTo(tdVendor);
	var inputIsDefault = $("<input/>")
	.addClass('vd-is_default')
	.prop('type', 'hidden')
	.prop('name', 'is_default[]')
	.val(data.is_default)
	.appendTo(tdVendor);
	var inputVendor = $("<select/>")
	.prop('name', 'vendor_id[]')
	.prop('type', 'text')
	.addClass('form-control vd-input_vendor')
	.appendTo(tdVendor);

	var obj = {
		value: data.vendor_id,
		url: url.getVendor
	};
	fillElement(obj, inputVendor);

	var tdIsDefault = $("<td/>")
	.addClass('text-center')
	.appendTo(tr);

	var btnDefault = $("<button/>")
				.addClass('btn btn-xs')
				.prop('type', 'button')
				.data('is_default', data.is_default)
				.appendTo(tdIsDefault);

	switch(parseInt(data.is_default)) {
		case 1:
			btnDefault.addClass('is_default btn-warning')
				.prop('title', 'Default')
				.html('<i class="fa fa-star"></i>');
			break;  
		default:
			btnDefault.addClass('is_not_default btn-default')
				.prop('type', 'button')
				.prop('title', 'Set Default')
				.html('Set Default');
			break;
	}

	var tdAction = $("<td/>")
	.addClass('text-center')
	.appendTo(tr);
	var btnDel = $("<a/>")
	.addClass('delete-row')
	.html('<b><i class="fa fa-trash-o"></i></b>')
	.appendTo(tdAction);
	if (tr[0].sectionRowIndex === tbody.find('tr:first')[0].sectionRowIndex) btnDel.hide();

	$('#btn-tambah-vd, .btn-save').attr('disabled', true);

	inputVendor.select2();
	btnDefault.tooltip();

	inputVendor.change(function(e) {
		e.preventDefault();
		$('#btn-tambah-vd, .btn-save').attr('disabled', true);
		let curEl = $(this);
		let curVal = $(this).val();
		let curTr = $(this).parent().parent();
		let tr = tbody.find('tr');
		tr.each(function() {
			if(curTr[0].sectionRowIndex !== $(this)[0].sectionRowIndex) {
				val = $(this).find('[name="vendor_id[]"]').val();
				if(parseInt(curVal) === parseInt(val)) {
					curEl.val("").trigger("change");
					errorMessage('Peringatan !', 'Distributor telah dipilih sebelumnya. Silahkan pilih kembali !');
					return;
				}
			}
		});

		if (curEl.val() !== "") $('#btn-tambah-vd, .btn-save').attr('disabled', false);
	});

	btnDefault.click(function(e) {
		e.preventDefault();
		let isDefault = $(this).data('is_default');
		let trFirst = tbody.find('tr:first');

		if(parseInt(isDefault) === 0) {
			trFirst.before(tr);
			trFirst.find('[name="is_default[]"]').val(0);
			trFirst.find('.delete-row').show();
			trFirst.find('.is_default')
					.removeClass('is_default btn-warning')
					.addClass('is_not_default btn-default ')
					.prop('title', 'Set Default')
					.data('is_default', 0)
					.html('Set Default');

			inputIsDefault.val(1);
			btnDel.hide();
			btnDefault.removeClass('is_not_default')
					.addClass('is_default btn-warning')
					.prop('title', 'Default')
					.data('is_default', 1)
					.html('<i class="fa fa-star"></i>');
		}
	});

	btnDel.on('click', function () {
		tr.remove();
		$('#btn-tambah-vd, .btn-save').attr('disabled', false);
	});
}*/

$(document).ready(function() {
	$(".wysihtml5-min").wysihtml5({
		parserRules:  wysihtml5ParserRules
	});
	$(".wysihtml5-toolbar").remove();

	/*$("#btn-tambah-vd").click(function () {
		$('#btn-tambah-vd, .btn-save').attr('disabled', true);
		addVendor({
			id: 0,
			vendor_id: '',
			is_default: 0,
		});
	});*/

	$(form).validate({
		rules: {
			nama: { required: true },
		},
		messages: {
			nama: "Nama Diperlukan",
		},
		focusInvalid: true,
		submitHandler: function (form) {
			$('input, textarea, select').prop('disabled', false);
			blockElement($(form));
			var formData = $(form).serialize();
			$.ajax({
				data: formData,
				type: 'POST',
				dataType: 'JSON', 
				url: url.save,
				success: function(data){
						$(form).unblock();
						successMessage('Berhasil', "Data berhasil disimpan.");
						window.location.assign(url.index);
				},
				error: function(data){
						$(form).unblock();
						errorMessage('Peringatan', "Terjadi kesalahan saat memproses data.");
				}
			});
			return false;
		}
	});

	$(".cancel-button").click(function () {
		window.location.assign(url.index);
	});

	fillForm(uid);
});
</script>