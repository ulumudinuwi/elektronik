<style>
	table > thead > tr > th, table > tbody > tr > td  {
		white-space: nowrap;
	}
</style>
<form class="form-horizontal" method="post" id="form">
	<div class="panel panel-flat">
		<div class="panel-body">
			<div class="text-right">
				<button type="submit" class="btn btn-success btn-labeled btn-save">
					<b><i class="icon-floppy-disk"></i></b>
					Simpan
				</button>
				<button type="button" class="btn btn-default cancel-button">Kembali</button>
			</div>
			<legend class="text-bold" style="margin-top: -20px"> &nbsp;</legend>

			<div class="row">
				<!-- Data -->
				<div class="col-sm-12">
					<fieldset>
						<legend class="text-bold">
							<i class="fa fa-list-alt position-left"></i>
							<strong>Data <?php echo $this->lang->line('pabrik_label'); ?></strong>
						</legend>
						<div class="row mb-20">
							<div class="col-md-6">
								<div class="form-group el-hidden">
									<label class="col-lg-4 control-label input-required">Kode</label>
									<div class="col-lg-8">
										<input type="text" class="form-control" name="kode" id="kode" placeholder="Kode ..." disabled="disabled">
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-4 control-label input-required">Nama</label>
									<div class="col-lg-8">
										<input type="text" class="form-control" name="nama" id="nama" placeholder="Nama ...">
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-4 control-label">Kurs</label>
									<div class="col-lg-8">
										<div class="input-group">
											<select class="form-control" id="kurs_id" name="kurs_id"></select>
											<span class="input-group-addon loading-select"><i class="icon-spinner2 spinner"></i></span>
										</div>
										<p class="text-info text-size-mini"><i class="fa fa-info-circle"></i> Jika Kurs tidak diisi, maka akan menggunakan kurs rupiah</p>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="col-lg-4 control-label input-required">Alamat</label>
									<div class="col-lg-8">
										<textarea class="form-control wysihtml5-min" id="alamat" name="alamat" rows="5"></textarea>
									</div>
								</div>
							</div>
						</div>
						<!-- <div class="row">
							<div class="col-md-12">
								<fieldset>
									<legend class="text-bold">
										<i class="icon-list"></i> <strong>Daftar Distributor</strong>
									</legend>
									<div class="row">
										<div class="col-sm-12">
											<div class="table-responsive">
												<table id="table-detail" class="table table-bordered">
													<thead>
														<tr class="bg-slate">
															<th>Nama</th>
															<th class="text-center" style="width: 8%;">Status</th>
															<th class="text-center" style="width: 5%;"></th>
														</tr>
													</thead>
													<tbody></tbody>
													<tfoot>
														<tr>
															<td colspan="4">
																<button type="button" class="btn btn-xs btn-primary btn-labeled pull-left" id="btn-tambah-vd">
																	<b><i class="icon-plus-circle2"></i></b>
																	Tambah
																</button>
															</td>
														</tr>
													</tfoot>
												</table>
											</div>
										</div>
									</div>
								</fieldset>
							</div>
						</div> -->
					</fieldset>
				</div>
			</div>
	
			<!-- Input Hidden -->
			<input type="hidden" name="id" id="id" value="0" />
			<input type="hidden" name="uid" id="uid" value="" />

			<legend class="text-semibold"> &nbsp;</legend>
			<div class="text-right">
				<button type="submit" class="btn btn-success btn-labeled btn-save">
					<b><i class="icon-floppy-disk"></i></b>
					Simpan
				</button>
				<button type="button" class="btn btn-default cancel-button">Kembali</button>
			</div>
		</div>
	</div>
</form>
