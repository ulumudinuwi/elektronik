<div id="detail-modal" class="modal fade" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-primary">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h6 class="modal-title">Detail Pabrik</h6>
      </div>
      <div class="modal-body form-horizontal">
        <div class="row mb-20">
          <div class="col-md-12">
            <fieldset>
              <legend class="text-bold"><i class="icon-magazine position-left"></i> Data Pabrik</legend>
            </fieldset>
          </div>
          <div class="col-sm-12 col-md-6 col-lg-6">
            <div class="form-group">
              <label class="control-label col-md-3">Kode</label>
              <div class="col-md-7">
                <div class="form-control-static" id="detail_kode"></div>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">Nama</label>
              <div class="col-md-7">
                <div class="form-control-static" id="detail_nama"></div>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">Alamat</label>
              <div class="col-md-7">
                <div class="form-control-static" id="detail_alamat"></div>
              </div>
            </div>
          </div>
        </div>
        <!-- <div class="row">
          <div class="col-sm-12">
            <fieldset>
              <legend class="text-bold">
                <i class="icon-list"></i> <strong>Daftar Distributor</strong>
              </legend>
              <div class="row">
                <div class="col-sm-12">
                  <div class="table-responsive">
                    <table id="table-detail" class="table table-bordered">
                      <thead>
                        <tr class="bg-slate">
                          <th>Nama</th>
                          <th class="text-center" style="width: 8%;">Status</th>
                        </tr>
                      </thead>
                      <tbody></tbody>
                    </table>
                  </div>
                </div>
              </div>
            </fieldset>
          </div>
        </div> -->
      </div>
      <div class="modal-footer m-t-none">
          <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        </div>
    </div>
  </div>
</div>