<script>
  $('select').select2();

  var uid = "<?php echo $uid; ?>",
  form = '#form';
  var url = {
    index: "<?php echo site_url('master/promo'); ?>",
    save: "<?php echo site_url('api/master/promo/save'); ?>",
    getData: "<?php echo site_url('api/master/promo/getData/:UID'); ?>",
    loadDataBarang: "<?php echo site_url('api/gudang_farmasi/stock/load_data_modal?mode=po'); ?>",
  }

  /* variabel di modal */
  var tableBarang = $('#table-barang'),
  tableBarangDt,
  modalBarang = $('#list-barang-modal');

  function fillForm(uid) {
    blockElement(form);
    $.getJSON(url.getData.replace(':UID', uid), function(data, status) {
      if (status === 'success') {
        data = data.data;

        $("#uid").val(data.uid);
        $("#barang_id").val(data.barang_id);
        $(".disp_kode_barang").val(data.kode_barang +' - '+ data.nama_barang);
        $(`[name=cek_promo][value=${data.cek_promo}]`).click().prop('checked', true);
        $("#qty_promo").val(data.qty_promo);
        $("#harga_jual").autoNumeric('set', data.harga_jual);
        $("#deskripsi").val(data.description);
        $("#tanggal").val(moment(data.expired_at).format('DD/MM/YYYY'));      
        $("#old_banner_id").val(data.banner_id);
        $("#type").val(data.type).trigger('change');
        $("#priviewImg").prop('src', data.url_banner_id);
        $("#long_description").data("wysihtml5").editor.setValue(data.long_description);


        $('.el-hidden').show('slow');
        $(form).unblock();
      }
    });
  }

  $(form).validate({
    rules: {
      harga_jual: { required: true },
    },
    messages: {
      harga_jual: "Harga Jual Diperlukan",
    },
    errorPlacement: function(error, element) {
      var placement = $(element).closest('.input-group');
      if (placement.length > 0) {
        error.insertAfter(placement);
      } else {
        error.insertAfter($(element));
      }
    },
    focusInvalid: true,
    submitHandler: function (form) {
      $('input, textarea, select').prop('disabled', false);

      $('.input-decimal').each(function() {
        $(this).val($(this).autoNumeric('get'));
      });

      blockElement($(form));
      var postData = $(form).serializeArray();
      var formData = new FormData($(form)[0]);

      for (var i = 0; i < postData.length; i++) {
          if (postData[i].name != 'foto_id' && postData[i].name.search(/\[\]/) === -1) {
              formData.delete(postData[i].name);
              formData.append(postData[i].name, postData[i].value);
          }
      }
      $.ajax({
        data: formData,
        processData: false,
        contentType: false,
        type: 'POST',
        dataType: 'JSON', 
        url: url.save,
        success: function(data){
          $(form).unblock();
          successMessage('Berhasil', "Promo barang berhasil disimpan.");
          window.location.assign(url.index);
        },
        error: function(data){
          $(form).unblock();
          errorMessage('Peringatan', "Terjadi kesalahan saat memproses data.");
        }
      });
      return false;
    }
  });

  $(".cancel-button").click(function () {
    window.location.assign(url.index);
  });

  /* EVENTS TABLE DETAIL */
  $('body').on('click', '.cari-barang', function() {
    curRowBarang = $(this).data('id');
    tableBarangDt.draw(false);
    modalBarang.modal('show');
  });

  tableBarangDt = tableBarang.DataTable({
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": url.loadDataBarang,
      "type": "POST",
      "data": function(p) {
        p.pabrik_id = $('#pabrik_id').val();
      }
    },
    "columns": [
    { 
      "data": "kode",
      "render": function (data, type, row, meta) {
        var template = `<span class="text-slate-300 text-bold text-size-mini mt-10"><?php echo $this->lang->line('pabrik_label'); ?>: ${row.pabrik}</span><br/>` + 
        '<a href="#" class="add-barang" data-id="{{ID}}" data-kode="{{KODE}}" data-nama="{{NAMA}}" data-satuan_id="{{SATUAN_ID}}" data-satuan_po="{{SATUAN}}" data-isi_satuan="{{ISI}}" data-maximum="{{MAX}}" data-stock="{{STOCK}}" data-minimum="{{MIN}}" data-harga="{{HARGA}}">{{KODE}}</a>';
        return template
        .replace(/\{\{ID\}\}/g, row.id)
        .replace(/\{\{KODE\}\}/g, row.kode)
        .replace(/\{\{NAMA\}\}/g, row.nama)
        .replace(/\{\{SATUAN_ID\}\}/g, row.satuan_id)
        .replace(/\{\{SATUAN\}\}/g, row.satuan)
        .replace(/\{\{ISI\}\}/g, row.isi_satuan)
        .replace(/\{\{MAX\}\}/g, row.maximum)
        .replace(/\{\{STOCK\}\}/g, row.stock)
        .replace(/\{\{MIN\}\}/g, row.minimum)
        .replace(/\{\{HARGA\}\}/g, row.harga);
      },
    },
    { "data": "nama" },
    ]
  });

  tableBarang.on('click', '.add-barang', function (e) {
    e.preventDefault();

    let id = $(this).data('id');
    let kode = $(this).data('kode');
    let nama = $(this).data('nama');

    $('body').prop('id', "row-barang-" + id);
    $('body').find('.input-barang_id').val(id);
    $('body').find('.disp_kode_barang').val(kode+' - '+nama);

    modalBarang.modal('hide');
  });

  $(document).ready(function() {
    $(".styled").uniform();
    $(".input-decimal").autoNumeric('init', {aSep: '.', aDec: ',', mDec: '2'});
    $(".input-number").autoNumeric('init', {aSep: ''});

    $(".wysihtml5-min").wysihtml5({
        "action": false,
        "color": false,
        "html": false,
        "image": false,
        "link": false,
    });

    $('.btn-save').on('click', function(e) {
      e.preventDefault();
      $(form).submit();
    });

    $(".disp_tanggal").daterangepicker({
      singleDatePicker: true,
      startDate: moment("<?php echo date('Y-m-d'); ?>"),
      endDate: moment("<?php echo date('Y-m-d'); ?>"),
      applyClass: "bg-slate-600",
      cancelClass: "btn-default",
      opens: "center",
      autoApply: true,
      locale: {
        format: "DD/MM/YYYY"
      }
    });

    $(".btn-batal").click(function() {
      window.location.assign(url.index);
    });
    if(uid !== "") fillForm(uid);
  });

  $('body').on('click', '#cek_promo', function() {
    if ($(this).prop('checked') === true) {
      $('#qty_promo').prop('disabled', false);
      $('#qty_promo').prop('required', true);
    }else{
      $('#qty_promo').prop('disabled', true);
      $('#qty_promo').prop('required', false);
    }
  });

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            //$('#priviewImg').css('background-image', 'url('+e.target.result +')');
            $('#priviewImg').prop('src', e.target.result);
            $('#priviewImg').hide();
            $('#priviewImg').fadeIn(650);
        }
        reader.readAsDataURL(input.files[0]);
    }
}

$("#banner_id").change(function() {
    readURL(this);
});
</script>