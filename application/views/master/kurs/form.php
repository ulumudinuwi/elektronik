<form class="form-horizontal" method="post" id="form">
	<div class="panel panel-flat">
		<div class="panel-body">
			<div class="text-right">
				<button type="submit" class="btn btn-success btn-labeled btn-save">
					<b><i class="icon-floppy-disk"></i></b>
					Simpan
				</button>
				<button type="button" class="btn btn-default cancel-button">Kembali</button>
			</div>
			<legend class="text-bold" style="margin-top: -20px"> &nbsp;</legend>

			<div class="row">
				<div class="col-sm-12">
					<fieldset>
						<legend class="text-bold">
							<i class="fa fa-list-alt position-left"></i>
							<strong>Data <?php echo lang('label_kurs'); ?> <span class="text-info text-size-mini">*) 1 Mata Uang Negara Berapa Rupiah</span></strong>
						</legend>
						<div class="row mb-20">
							<div class="col-md-6">
								<div class="form-group">
									<label class="col-lg-3 control-label input-required"><?php echo lang('label_negara'); ?></label>
									<div class="col-lg-6">
										<div class="input-group">
											<select class="form-control" name="country_id" id="country_id"></select>
											<span class="input-group-addon loading-select"><i class="icon-spinner2 spinner"></i></span>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-3 control-label input-required"><?php echo lang('label_kurs'); ?></label>
									<div class="col-lg-8">
										<div class="input-group">
											<span class="input-group-addon span-label_currency">&mdash;</span>
											<input type="text" name="currency" id="currency" class="form-control" value="0">
											<span class="input-group-addon">RUPIAH</span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</fieldset>
				</div>
			</div>
	
			<!-- Input Hidden -->
			<input type="hidden" name="id" id="id" value="0" />
			<input type="hidden" name="uid" id="uid" value="" />
			<input type="hidden" name="country" id="country" value="" />
			<input type="hidden" name="currency_name" id="currency_name" value="" />

			<legend class="text-semibold"> &nbsp;</legend>
			<div class="text-right">
				<button type="submit" class="btn btn-success btn-labeled btn-save">
					<b><i class="icon-floppy-disk"></i></b>
					Simpan
				</button>
				<button type="button" class="btn btn-default cancel-button">Kembali</button>
			</div>
		</div>
	</div>
</form>
