<script>
var uid = "<?php echo $uid; ?>",
	form = '#form';
var url = {
	index: "<?php echo site_url('master/kurs'); ?>",
	save: "<?php echo site_url('api/master/kurs/save'); ?>",
	getData: "<?php echo site_url('api/master/kurs/get_data/:UID'); ?>",
	getCountry: "https://restcountries.eu/rest/v2/all",
	getCurrencyByCountryId: `https://free.currconv.com/api/v7/convert?q=:COUNTRY-ID_IDR&compact=ultra&apiKey=9568862c52a93ad0f1ad`,
}

function fillCountry(country_id, el) {
	let parent = $(`#${el}`).parent();
	parent.find('.loading-select').show();
	$.getJSON(url.getCountry, function(data, status) {
		if (status === 'success') {
			var option = '<option value="" selected="selected">- Pilih -</option>';
			for (var i = 0; i < data.length; i++) {
				let countryId = data[i].alpha2Code;
				let currencyCode = data[i].currencies[0].code;
				if(currencyCode) {
					currencyCode = currencyCode.split('');
					currencyCode = countryId + currencyCode[currencyCode.length - 1];
					let currencyName = data[i].currencies[0].name;
					currencyName = currencyName.split(' ');
					currencyName = currencyName[currencyName.length - 1].toUpperCase();

					option += `<option value="${currencyCode}" data-currency_name="${currencyName}">${data[i].name}</option>`;
				}
			}
			$(`#${el}`).html(option);
			$(`#${el}`).val(country_id).trigger("change");
		}
		parent.find('.loading-select').hide();
	});
}

function getCurrencyByCountryId(country_id, el) {
	$.getJSON(url.getCurrencyByCountryId.replace(':COUNTRY-ID', country_id), function(data, status) {
		if (status === 'success') {
			$(`#${el}`).val(data[Object.keys(data)[0]]);
		}
	});
}

function fillForm(uid) {
	blockElement(form);
	$.getJSON(url.getData.replace(':UID', uid), function(data, status) {
		if (status === 'success') {
			data = data.data;
			$("#id").val(data.id);
			$("#uid").val(data.uid);
			$("#country").val(data.country);
			$("#currency").val(data.currency);
			$("#currency_name").val(data.currency_name);

			fillCountry(data.country_id, "country_id");
			$(form).unblock();
		}
	});
}

$(document).ready(function() {
	let currencyName = '';
	let country = '';
	$('#country_id').change(function() {
		if($(this).val() != "") {
			currencyName = $(this).find('option:selected').data('currency_name');
			country = $(this).find('option:selected').text();
			//getCurrencyByCountryId($(this).val(), 'currency');
		}
		$('#country').val(country);
		$('#currency_name').val(currencyName);
		$('.span-label_currency').html(currencyName ? `1 ${currencyName}` : '&mdash;');
	});

	$(form).validate({
		rules: {
			nama: { required: true },
		},
		messages: {
			nama: "Nama Diperlukan",
		},
		focusInvalid: true,
		submitHandler: function (form) {
			$('input, textarea, select').prop('disabled', false);
			blockElement($(form));
			var formData = $(form).serialize();
			$.ajax({
				data: formData,
				type: 'POST',
				dataType: 'JSON', 
				url: url.save,
				success: function(data){
					$(form).unblock();
					successMessage('Berhasil', "Data berhasil disimpan.");
					window.location.assign(url.index);
				},
				error: function(data){
					$(form).unblock();
					errorMessage('Peringatan', "Terjadi kesalahan saat memproses data.");
				}
			});
			return false;
		}
	});

	$(".cancel-button").click(function () {
		window.location.assign(url.index);
	});

	fillForm(uid);
});
</script>