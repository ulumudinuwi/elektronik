<script>
var uid = "<?php echo $uid; ?>",
form = '#form';
var url = {
  index: "<?php echo site_url('master/area'); ?>",
  save: "<?php echo site_url('api/master/area/save'); ?>",
  getData: "<?php echo site_url('api/master/area/get_data/:UID'); ?>",
}

function fillForm(uid) {
  blockElement(form);
  $.getJSON(url.getData.replace(':UID', uid), function(data, status) {
    if (status === 'success') {
      data = data.data;

      $("#nama").val(data.nama);
      $("#uid").val(data.uid);
      $(form).unblock();
    }
  });
}

$(form).validate({
  rules: {
    nama: { required: true },
  },
  messages: {
    nama: "Nama Diperlukan",
  },
  focusInvalid: true,
  submitHandler: function (form) {
    blockElement($(form));
    var formData = $(form).serialize();
    $.ajax({
      data: formData,
      type: 'POST',
      dataType: 'JSON', 
      url: url.save,
      success: function(data){
          $(form).unblock();
          successMessage('Berhasil', "Area berhasil disimpan.");
          window.location.assign(url.index);
      },
      error: function(data){
          $(form).unblock();
          errorMessage('Peringatan', "Terjadi kesalahan saat memproses data.");
      }
    });
    return false;
  }
});

$(".cancel-button").click(function () {
  window.location.assign(url.index);
});

if(uid !== "") fillForm(uid);
</script>