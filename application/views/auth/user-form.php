<?php echo messages(); ?>
<div class="row">
	<div class="col-md-12">
		<form id="user-form" class="form-horizontal" method="post">
			<div class="panel panel-white">
				<div class="panel-heading">
					<h3 class="panel-title"><?php echo lang('account'); ?></h3>
				</div>
				<div class="panel-body">
					<?php echo $form->fields(); ?>
				</div>
				<div class="panel-footer">
					<div class="heading-elements">
						<div class="heading-btn pull-right">
							<button type="submit" name="save-button" value="Simpan" id="save-button" class="btn-success btn-labeled btn">
								<b><i class="icon-floppy-disk"></i></b>
								Simpan
							</button>
							<input type="submit" name="cancel-button" value="Batal" id="cancel-button" class="btn btn-default">
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<script>
    $(document).ready(function() {
    	$('select').select2();
        $("#first_name").autocomplete({
            minLength: 3,
            source: "<?php echo site_url('auth/user/pegawai_autocomplete_data') ?>",
            search: function () {
                $(this).parent().addClass('ui-autocomplete-processing');
            },
            open: function () {
                $(this).parent().removeClass('ui-autocomplete-processing');
            },
            select: function (event, ui) {
                var pegawaiID = event.target.id;
                $("input[name=pegawai_id]").val(ui.item.id);
            }
        });
    });
        
	$(window).load(function() {
        var typeahead_elem = $("#first_name");
            
        $('input[type=text], input[type=email]').css("text-transform", "none");
		$('#top-save-btn').on('click', function(e) {
			e.preventDefault();
			$('#save-button').click();
		});

		$("#unit_usaha_id").change(function () {
			var unit_usaha_id = $(this).val();
			$("#unit_kerja_id").empty();
			$.getJSON("<?php echo site_url('api/auth/user/load_unit_kerja'); ?>" + "?unit_usaha_id=" + unit_usaha_id, function (data, status) {
				
				if (status === 'success') {
					for (var id in data) {
						$("#unit_kerja_id").append('<option value="' + id + '">' + data[id] + '</option>');
					}
				}
			});
		});

        $("#roles_id").change(function (e) {
            var role_id = $("#role_id").val();
            var options = [];
            $(this).find(':selected').each(function (i, el) {
                options.push({ id: $(el).val(), text: $(el).text() });
            });

            // Delete Options in role_id
            $("#role_id option").each(function (i, el) {
                if (i > 0) {
                    $(el).remove();
                }
            });

            for (var i = 0; i < options.length; i++) {
                $("#role_id").append('<option value="' + options[i].id + '">' + options[i].text + '</option>');
            }

            if (role_id > 0 && $("#role_id option[value=" + role_id + "]").length > 0) {
                $("#role_id").val(role_id);
            } else {
                role_id = options.length > 0 ? options[0].id : 0;
                $("#role_id").val(role_id);
            }

            $("#role_id").trigger('change');
        });
	});
</script>