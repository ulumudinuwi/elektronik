<script>
var UID = "<?php echo $uid; ?>",
	form = '#form';

var url = {
	index: "<?php echo site_url('gudang_farmasi/afkir'); ?>",
	save: "<?php echo site_url('api/gudang_farmasi/afkir/save'); ?>",
	getData: "<?php echo site_url('api/gudang_farmasi/afkir/get_data/:UID'); ?>",
	loadDataBarang: "<?php echo site_url('api/gudang_farmasi/stock/load_data_modal'); ?>",
	getHistoryBarang: "<?php echo site_url('api/gudang_farmasi/history/load_data?uid=:UID'); ?>",
	loadDetailStock: "<?php echo site_url('api/gudang_farmasi/stock/load_detail_stock?uid=:UID'); ?>",
};

let tableDetail = $('#table_detail'),
	tableDetailDt,
	curRowBarang = null;

/* variabel di modal */
var tableBarang = $('#table-barang'),
	tableBarangDt,
	modalBarang = $('#list-barang-modal');

function fillForm(uid, data) {
	tableDetailDt = tableDetail.DataTable({
		"info": false,
		"ordering": false,
		"paginate": false,
		"drawCallback": function (settings) {
			let tr = tableDetail.find("tbody tr");
			tr.each(function() {
				let id = $(this).find('.input-barang_id').val();
				let stock = parseFloat(numeral($(this).find('.label-disp_stock').html())._value);
				let td = $(this).children().eq(2);
				td.removeClass('bg-danger');
				if(stock <= 0) td.addClass('bg-danger');
				$(this).find('.disp_qty').autoNumeric('init', {aSep: '.', aDec: ',', mDec: '2', vMax: stock, vMin: 0});
				$(this).find('.input-expired_date').formatter({ pattern: '{{99}}/{{99}}/{{9999}}' });
				$(this).prop('id', "row-barang-" + id);
			});
		},
	});

	if(parseInt(UID) === 0) return;

	blockElement($(form));
	$.getJSON(url.getData.replace(':UID', uid), function(data, status) {
		if (status === 'success') {
			data = data.data;
			$("#id").val(data.id);
			$("#uid").val(data.uid);
			$("#kode").val(data.kode);
			$("#keterangan").data("wysihtml5").editor.setValue(data.keterangan);
			$("#tanggal").data("daterangepicker").setStartDate(moment(data.tanggal).isValid() ? moment(data.tanggal) : moment())
			$("#tanggal").data("daterangepicker").setEndDate(moment(data.tanggal).isValid() ? moment(data.tanggal) : moment());

			for (var i = 0; i < data.details.length; i++) {
				columnBarang(data.details[i]);
			}
			$(form).unblock();
		}
	});
}

function columnBarang(data) {
	tableDetailDt.row.add([
		// Col 1
		`<input type="hidden" class="input-detail_id" name="detail_id[]" value="${data.id}">` +
		`<input type="hidden" class="input-barang_id" name="barang_id[]" value="${data.barang_id}">` +
		`<div class="input-group">` +
			`<input class="form-control disp_kode_barang" readonly="readonly" placeholder="Cari Barang" value="${data.kode_barang}">` +
			`<div class="input-group-btn">` +
				`<button type="button" class="btn btn-primary cari-barang" data-id="row-barang-${data.barang_id}"><i class="fa fa-search"></i></button>` +
			`</div>` +
		`</div>` +
		`<span class="text-slate-300 text-bold text-size-mini disp_barang mt-10">${data.barang}</span>` +
		`<br/><a class="label label-info history-row" data-barang_id="${btoa(JSON.stringify(data.barang_id))}">History</a>`,
		// Col 2
		`<input type="hidden" class="input-satuan_id" name="satuan_id[]" value="${data.satuan_id}">` +
		`<input type="hidden" class="input-isi_satuan" name="isi_satuan[]" value="${data.isi_satuan}">` +
		`${data.satuan}` +
		`<br/><span class="text-info-300 text-size-mini mt-10">Sisa Stock: <span class="text-bold label-disp_stock">${numeral(data.stock).format('0.0,')}</span></span>` + 
		`<br/><a class="label label-info detail_stock-row" data-barang_id="${btoa(JSON.stringify({ id: data.barang_id, search_by: "barang_id" }))}" data-barang="${data.kode_barang} - ${data.barang}" data-satuan="${data.satuan}">Detail Stock</a>`,
		// Col 3
		`<input type="hidden" class="input-qty" name="qty[]" value="${data.qty}">` +
		`<input class="form-control disp_qty text-right" value="${data.qty}">`,
		// Col 4
		`<input class="form-control input-expired_date" name="expired_date[]" placeholder="DD/MM/YYYY" value="${data.expired_date}">`,
		// Col 5
		`<button type="button" class="btn btn-xs btn-danger remove-row"><i class="fa fa-trash"></i></button>`,
	]).draw(false);
}

$(document).ready(function() {
	$(".input-decimal").autoNumeric('init', {aSep: '.', aDec: ',', mDec: '2'});
	$(".input-bulat").autoNumeric('init', {aSep: '.', aDec: ',', mDec: '0'});
	$(".wysihtml5-min").wysihtml5({
		parserRules:  wysihtml5ParserRules
	});
	$(".wysihtml5-toolbar").remove();

	$('.btn-save').on('click', function(e) {
		e.preventDefault();
		$(form).submit();
	});

	$(".btn-batal").click(function() {
		window.location.assign(url.index);
	});

	$(".disp_tanggal").daterangepicker({
		singleDatePicker: true,
		startDate: moment("<?php echo date('Y-m-d'); ?>"),
		endDate: moment("<?php echo date('Y-m-d'); ?>"),
		applyClass: "bg-slate-600",
		cancelClass: "btn-default",
		opens: "center",
		autoApply: true,
		locale: {
			format: "DD/MM/YYYY"
		}
	});

	$("#btn_tanggal").click(function () {
		let parent = $(this).parent();
		parent.find('input').data("daterangepicker").toggle();
	});

	tableBarangDt = tableBarang.DataTable({
		"processing": true,
		"serverSide": true,
		"ajax": {
				"url": url.loadDataBarang,
				"type": "POST"
		},
		"columns": [
			{ 
				"data": "kode",
				"render": function (data, type, row, meta) {
						var template = '<a href="#" class="add-barang" data-id="{{ID}}" data-kode="{{KODE}}" data-nama="{{NAMA}}" data-satuan_id="{{SATUAN_ID}}" data-satuan="{{SATUAN}}" data-isi_satuan="{{ISI}}" data-stock="{{STOCK}}">{{KODE}}</a>';
						return template
								.replace(/\{\{ID\}\}/g, row.id)
								.replace(/\{\{KODE\}\}/g, row.kode)
								.replace(/\{\{NAMA\}\}/g, row.nama)
								.replace(/\{\{SATUAN_ID\}\}/g, row.satuan_id)
								.replace(/\{\{SATUAN\}\}/g, row.satuan)
								.replace(/\{\{ISI\}\}/g, row.isi_satuan)
								.replace(/\{\{STOCK\}\}/g, row.stock);
				},
				"className": "text-center"
			},
			{ "data": "nama" },
			{ 
				"data": "satuan",
				"orderable": false,
				"searchable": false
			},
			{ 
				"data": "stock",
				"render": function (data, type, row, meta) {
						return numeral(data).format('0.0,');
				},
				"orderable": false,
				"searchable": false
			}
		]
	});

	tableBarang.on('click', '.add-barang', function (e) {
		e.preventDefault();
		let id = $(this).data('id');
		let kode = $(this).data('kode');
		let nama = $(this).data('nama');
		let satuan_id = $(this).data('satuan_id');
		let satuan = $(this).data('satuan');
		let isi_satuan = parseFloat($(this).data('isi_satuan'));
		let stock = parseFloat($(this).data('stock'));

		if(parseInt(stock) <= 0) {
			errorMessage('Peringatan !', 'Stock Barang tersebut tidak tersedia. Silahkan pilih kembali.');
			return;
		}

		if (curRowBarang != null) { // Row Exists
			let tr = $("#" + curRowBarang);
			let qty = tr.find('.input-qty').val();

			let uidHistory = btoa(JSON.stringify(id));
			let uidDetail = btoa(JSON.stringify({ id: id, search_by: "barang_id" }));

			tr.prop('id', "row-barang-" + id);
			tr.find('.cari-barang').data('id', "row-barang-" + id).attr('data-id', "row-barang-" + id);
			tr.find('.history-row').data('barang_id', uidHistory).attr('data-barang_id', uidHistory);
			tr.find('.detail_stock-row').data('barang_id', uidDetail).attr('data-barang_id', uidDetail);
			tr.find('.input-barang_id').val(id);
			tr.find('.input-satuan_id').val(satuan_id);
			tr.find('.input-isi_satuan').val(isi_satuan);
			tr.find('.disp_kode_barang').val(kode);
			tr.find('.disp_barang').html(nama);
			tr.find('.label-satuan').html(satuan);
			tr.find('.label-disp_stock').html(numeral(stock).format('0.0,'));
			tr.find('.disp_qty').focus().select();
		} else {
			columnBarang({
				id: 0,
				barang_id: id,
				kode_barang: kode,
				barang: nama,
				satuan_id: satuan_id,
				isi_satuan: isi_satuan,
				satuan: satuan,
				stock: stock,
				expired_date: "",
				qty: 1,
			});
		}
		modalBarang.modal('hide');
	});

	$("#btn-tambah").click(function (e) {
		curRowBarang = null;
		tableBarangDt.draw(false);
		modalBarang.modal('show');
	});

	/* EVENTS TABLE DETAIL */
	tableDetail.on('click', '.cari-barang', function() {
		curRowBarang = $(this).data('id');
		tableBarangDt.draw(false);
		modalBarang.modal('show');
	});

	tableDetail.on('click', '.history-row', function() {
		let barang_id = $(this).data('barang_id');
		showHistoryBarang(url.getHistoryBarang.replace(':UID', barang_id), null);
	});

	tableDetail.on("click", ".detail_stock-row", function () {
		let obj = {
			url: url.loadDetailStock.replace(':UID', $(this).data('barang_id')),
			barang: $(this).data('barang'),
			satuan: $(this).data('satuan'),
		};
		showDetailStock(obj);
 });

	tableDetail.on('change blur keyup', '.disp_qty', function() {
		let tr = $(this).parent().parent();
		let td = $(this).parent();
		let val = isNaN(parseFloat($(this).autoNumeric('get'))) ? 0 : parseFloat($(this).autoNumeric('get'));
		
		tr.find('.input-qty').val(val);
	});

	tableDetail.on('keyup', '.input-expired_date', function() {
		let td = $(this).parent();
		
		let value = $(this).val();
		let day = value.substr(0, 2);
		day = isNaN(day) ? 0 : day;
		let month = value.substr(3, 2);
		month = isNaN(month) ? 0 : month;
		let year = value.substr(6, 4);
		year = isNaN(year) ? 0 : year;

		$(".btn-save").prop('disabled', false);
		td.find('.span-exdate-error').remove();
		if(value != "") {
			if(parseInt(day) > 31 || parseInt(month) > 12 || year.length < 4) {
				$(".btn-save").prop('disabled', true);
				if(td.find('.span-exdate-error').length <= 0) 
					td.append('<span class="label label-block label-danger span-exdate-error">Format Tanggal Salah</span>');
			}
		}
	});

	tableDetail.on('click', '.remove-row', function() {
		tableDetailDt
				.row($(this).parents('tr'))
				.remove()
				.draw();
	});

	$(form).validate({
		rules: {
			kode: { required: true },
			tanggal: { required: true },
			pabrik_id: { required: true },
		},
		focusInvalid: true,
		errorPlacement: function(error, element) {
				var placement = $(element).closest('.input-group');
				if (placement.length > 0) {
						error.insertAfter(placement);
				} else {
						error.insertAfter($(element));
				}
		},
		submitHandler: function (form) {
			tableDetailDt.search('').draw(false);

			let input = $('input[name="barang_id[]"]');
			if (input.length <= 0) {
				swal({
						title: "Peringatan!",
						text: "Pilih Barang terlebih dahulu.",
						html: true,
						type: "warning",
						confirmButtonColor: "#2196F3"
				});
				return;
			}

			swal({
				title: "Konfirmasi?",
				type: "warning",
				text: "Apakah data yang dimasukan benar??",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#2196F3",
				cancelButtonText: "Batal",
				cancelButtonColor: "#FAFAFA",
				closeOnConfirm: true,
				showLoaderOnConfirm: true,
			},
			function() {
				$('.input-decimal').each(function() {
					$(this).val($(this).autoNumeric('get'));
				});

				$('.input-bulat').each(function() {
					$(this).val($(this).autoNumeric('get'));
				});

				$('input, textarea, select').prop('disabled', false);

				blockPage('Sedang diproses ...');
				var formData = $(form).serialize();
				$.ajax({
					data: formData,
					type: 'POST',
					dataType: 'JSON', 
					url: url.save,
					success: function(data){
						$.unblockUI();
						successMessage('Berhasil', "Afkir Barang berhasil disimpan.");
						window.location.assign(url.index);
					},
					error: function(data){
						$.unblockUI();
						errorMessage('Peringatan', "Terjadi kesalahan saat memproses data.");
					}
				});
				return false;
			});
		}
	});

	fillForm(UID);
});
</script>