<script>
var UID = "<?php echo $uid; ?>",
    form = '#form';

var url = {
  index: "<?php echo site_url('gudang_farmasi/so'); ?>",
  save: "<?php echo site_url('api/gudang_farmasi/so/save'); ?>",
  getData: "<?php echo site_url('api/gudang_farmasi/so/get_data/:UID?mode=pengecekan'); ?>",
};

var tableDetail = $('#table_detail'),
    tableDetailDt,
    detailStockModal = $('#detail-stock-modal');

function fillForm(uid) {
  tableDetailDt = tableDetail.DataTable({
    data: [],
    columns: getColumnTable(),
    ordering: false,
    scrollX: true,
    scrollCollapse: true,
    fixedColumns: {
      leftColumns: 4,
    },
    fnDrawCallback: function (oSettings) {
      $('[data-toggle=tooltip]').tooltip();
      $('.disp-check').uniform();
      $('.checkbox-inline')
              .css('top', '-15px')
              .css('left', '8px');

      let tr = tableDetail.find("tbody tr");
      tr.each(function() { 
        $(this).find('td:eq(4), td:eq(5), td:eq(6), td:eq(7), td:eq(8)').css('background', '#DDDDDD');
        
        let checkParent = $(this).find('.disp-check').parent();
        checkParent.removeClass('checked');
        let check = parseInt($(this).find('.input-check').val());
        if(check === 1) checkParent.addClass('checked');
      });
    },
  });

  blockElement($(form));
  $.getJSON(url.getData.replace(':UID', uid), function(data, status) {
    if (status === 'success') {
      data = data.data;

      $("#detail_kode").html(data.kode);
      $("#detail_tanggal").html(moment(data.tanggal).format('DD/MM/YYYY HH:mm'));
      $("#detail_pengecekan1_by").html(data.pengecekan1_by);
      $("#id").val(data.id);
      $("#uid").val(data.uid);
      $("#kode").val(data.kode);
      $("#tanggal").val(data.tanggal);

      tableDetailDt.clear().draw();
      tableDetailDt.rows.add(data.details).draw();

      $(form).unblock();
    }
  });
}

function getColumnTable() {
  let columns = [
        { 
          "render": function (data, type, row, meta) {
            return `<label class="checkbox-inline">` +
                    `<input type="checkbox" class="disp-check" disabled>` +
                    `</label>`;
          },
        },
        { 
          "render": function (data, type, row, meta) {
            return `<span class="label-barang">${row.barang}</span>` +
                    `<br/><span class="text-slate-300 text-bold text-size-mini label-kode-barang mt-10">${row.kode_barang}</span>`;
          },
        },
        { 
          "render": function (data, type, row, meta) {
            return `<label class="label-satuan">${row.satuan}</label>`;
          },
        },
        { 
          "render": function (data, type, row, meta) {
            return `<span class="label-stock_sistem">${numeral(row.stock_sistem).format('0.0,')}</span>`;
          },
        },
        { 
          "render": function (data, type, row, meta) {
            return `<span class="label-pengecekan1_stock_fisik">${numeral(row.pengecekan1_stock_fisik).format('0.0,')}</span>`;
          },
        },
        { 
          "render": function (data, type, row, meta) {
            return `<span class="label-pengecekan1_selisih">${numeral(row.pengecekan1_selisih).format('0.0,')}</span>`;
          },
        },
        { 
          "render": function (data, type, row, meta) {
            //return genStockDetail(row, 'id', 1) + '' + genStockDetail(row, 'no_batch', 1);
            return genStockDetail(row, 'no_batch', 1);
          },
          "className": "text-center"
        },
        { 
          "render": function (data, type, row, meta) {
            return genStockDetail(row, 'expired_date', 1);
          },
        },
        { 
          "render": function (data, type, row, meta) {
            return genStockDetail(row, 'qty', 1);
          },
        },
        { 
          "render": function (data, type, row, meta) {
            return `<input type="hidden" class="input-detail_id" name="detail_id[]" value="${row.id}">` +
            `<input type="hidden" class="input-stock_id" name="stock_id[]" value="${row.stock_id}">` +
            `<input type="hidden" class="input-barang_id" name="barang_id[]" value="${row.barang_id}">` +
            `<input type="hidden" class="input-satuan_id" name="satuan_id[]" value="${row.satuan_id}">` +
            `<input type="hidden" class="input-isi_satuan" name="isi_satuan[]" value="${row.isi_satuan}">` +
            `<input type="hidden" class="input-stock_sistem" name="stock_sistem[]" value="${row.stock_sistem}">` +
            `<input type="hidden" class="input-pengecekan1_stock_fisik" name="pengecekan1_stock_fisik[]" value="${row.pengecekan1_stock_fisik}">` +
            `<input type="hidden" class="input-pengecekan1_selisih" name="pengecekan1_selisih[]" value="${row.pengecekan1_selisih}">` +
            `<input type="hidden" class="input-stock_fisik" name="stock_fisik[]" value="${row.pengecekan2_stock_fisik}">` +
            `<input type="hidden" class="input-selisih" name="selisih[]" value="${row.pengecekan2_selisih}">` +
            `<input type="hidden" class="input-check" value="${row.input_check ? row.input_check : 0}">` +
            `<span class="label-stock_fisik">${numeral(row.pengecekan2_stock_fisik).format('0.0,')}</span>`;
          },
        },
        { 
          "render": function (data, type, row, meta) {
            return `<span class="label-selisih">${numeral(row.pengecekan2_selisih).format('0.0,')}</span>`;
          },
        },
        { 
          "render": function (data, type, row, meta) {
            return genStockDetail(row, 'id', 2) + '' + genStockDetail(row, 'no_batch', 2);
          },
          "className": "text-center"
        },
        { 
          "render": function (data, type, row, meta) {
            return genStockDetail(row, 'expired_date', 2);
          },
        },
        { 
          "render": function (data, type, row, meta) {
            return genStockDetail(row, 'qty', 2);
          },
        },
        { 
          "render": function (data, type, row, meta) {
            return `<button type="button" class="btn btn-link btn-xs edit-row"><i class="fa fa-edit"></i></button>`;
          },
        },
      ];
  return columns;
}

function genStockDetail(data, key, mode) {
  let res = [];

  let tmp = '';
  let details = null;
  switch(parseInt(mode)) {
    case 1:
      tmp = `<span class="label-${key}_${mode}">:LABEL_VAL</span>`;
      details = data.pengecekan1_detail_barang;
      break;
    default:
      tmp = `<input type="hidden" class="input-${key}_${mode}" name="${key}_${data.barang_id}[]" value=":VAL">` +
            `<span class="label-${key}_${mode}">:LABEL_VAL</span>`;
      details = data.pengecekan2_detail_barang ? data.pengecekan2_detail_barang : data.pengecekan1_detail_barang;
      break;
  }

  for (i = 0; i < details.length; i++) {
    res.push(details[i][key]);
  }

  switch(key) {
    case 'expired_date':
      res = res.map((item) => item ? tmp.replace(':LABEL_VAL', moment(item).format('DD/MM/YYYY')).replace(':VAL', item) : tmp.replace(':LABEL_VAL', "&mdash;").replace(':VAL', ""));
      break;
    case 'qty':
      res = res.map((item) => tmp.replace(':LABEL_VAL', numeral(item).format('0.0,')).replace(':VAL', item));
      break;
    case 'id':
      if(mode === 2) {
        tmp = `<input type="hidden" class="input-${key}_${mode}" name="${key}_${data.barang_id}[]" value=":VAL">`;
        res = res.map((item) => tmp.replace(':VAL', item));
      }
      break;
    default:
      res = res.map((item) => item ? tmp.replace(':LABEL_VAL', item).replace(':VAL', item) : tmp.replace(':LABEL_VAL', "&mdash;").replace(':VAL', item));
      break;
  }
  return key != "id" ? res.join('<br/>') : res.join('');
}

function addRowDetailStock(data) {
  let tbody = $("#table-detail-stock tbody");
  let mode = (typeof data.mode === "undefined" || data.mode === "edit" ? "edit" : "add");
  let inputType = 'hidden';
  if(mode === "add") inputType = 'text';

  let tr = $("<tr/>")
      .appendTo(tbody);

  let tdNoBatch = $("<td/>")
    .addClass('text-center')
    .appendTo(tr);
  let inputId = $("<input/>")
      .prop('type', 'hidden')
      .prop('name', 'detail_id[]')
      .addClass('input-detail_id')
      .val(data.id)
      .appendTo(tdNoBatch);
  let inputMode = $("<input/>")
      .prop('type', 'hidden')
      .addClass('input-detail_mode')
      .val(mode)
      .appendTo(tdNoBatch);
  let inputNoBatch = $("<input/>")
      .prop('type', inputType)
      .prop('name', 'no_batch[]')
      .addClass('input-detail_no_batch form-control')
      .prop('placeholder', 'No. Batch')
      .val(data.no_batch ? data.no_batch : "")
      .appendTo(tdNoBatch);
  if(mode !== "add") {
    let labelNoBatch = $("<label/>")
      .html(data.no_batch ? data.no_batch : "&mdash;")
      .appendTo(tdNoBatch);
  }

  let tdExpDate = $("<td/>")
      .addClass('text-center')
      .appendTo(tr);
  let inputExpDate = $("<input/>")
      .prop('type', inputType)
      .prop('name', 'expired_date[]')
      .addClass('input-detail_expired_date form-control')
      .prop('placeholder', 'DD/MM/YYYY')
      .val(data.expired_date ? moment(data.expired_date).format('DD/MM/YYYY') : '')
      .formatter({ pattern: '{{99}}/{{99}}/{{9999}}' })
      .appendTo(tdExpDate);
  if(mode !== "add") {
    let labelExpDate = $("<label/>")
      .html(data.expired_date ? moment(data.expired_date).format('DD/MM/YYYY') : "&mdash;")
      .appendTo(tdExpDate);
  }

  let tdQty = $("<td/>")
      .appendTo(tr);
  let inputQty = $("<input/>")
      .prop('type', 'text')
      .prop('name', 'detail_qty[]')
      .addClass('form-control text-right input-detail_qty')
      .autoNumeric('init', {aSep: '.', aDec: ',', mDec: '2'})
      .appendTo(tdQty);
  inputQty.autoNumeric('set',data.qty);
  inputQty.on('keyup change', function() {
    updateStockDetailTotal();      
  });

    let tdAction = $("<td/>")
        .addClass('text-center')
        .appendTo(tr);
  if(mode === "add") {
    let btnDel = $("<button/>")
        .prop('type', 'button')
        .addClass('btn btn-danger btn-xs remove-row')
        .html('<i class="fa fa-trash-o"></i>')
        .appendTo(tdAction);

    btnDel.click(function () {
        tr.remove();
        updateStockDetailTotal();
    });
  } else tdAction.append("&mdash;");

  updateStockDetailTotal();
}

function updateStockDetailTotal() {
  let total = 0;
  let length = $("#table-detail-stock tbody").find('tr').length;
  $("#table-detail-stock tbody").find('tr').each((i, el) => {
      let qty = isNaN(parseFloat($(el).find('input[name="detail_qty[]"]').autoNumeric('get'))) ? 0 : parseFloat($(el).find('input[name="detail_qty[]"]').autoNumeric('get'));
      total += qty;

      $(el).find('.remove-row').show();
      if(parseInt(length) <= 1) $(el).find('.remove-row').hide();
  });
  $("#label-stock-fisik").html(numeral(total).format('0.0,'));
}

$(document).ready(function() {
  $(".wysihtml5-min").wysihtml5({
    parserRules:  wysihtml5ParserRules
  });
  $(".wysihtml5-toolbar").remove();
  
  $('.btn-save').on('click', function(e) {
    e.preventDefault();
    $(form).submit();
  });

  $(".btn-batal").click(function() {
    window.location.assign(url.index);
  });

  tableDetail.on('click', '.edit-row', function (e) {
    let tr = $(this).closest('tr');
    let data = tableDetailDt.row(tr).data();
    detailStockModal.find('#tr_index').val(tr[0]._DT_RowIndex);
    detailStockModal.find('#label-data-barang').html(`${data.barang}<br/><span class="text-slate-300 text-bold text-xs">${data.kode_barang}</span>`);
    detailStockModal.find('#label-data-satuan').html(data.satuan);

    $("#table-detail-stock tbody").empty();
    for (var i = 0; i < data.pengecekan2_detail_barang.length; i++) {
      addRowDetailStock(data.pengecekan2_detail_barang[i]);
    }
    detailStockModal.modal('show');
  });

  $('#table-detail-stock').on('keyup', '.input-detail_expired_date', function() {
    let td = $(this).parent();
    
    let value = $(this).val();
    let day = value.substr(0, 2);
    day = isNaN(day) ? 0 : day;
    let month = value.substr(3, 2);
    month = isNaN(month) ? 0 : month;
    let year = value.substr(6, 4);
    year = isNaN(year) ? 0 : year;

    $("#btn-detail-stock-simpan").prop('disabled', false);
    td.find('.span-exdate-error').remove();
    if(value != "") {
      if(parseInt(day) > 31 || parseInt(month) > 12 || year.length < 4) {
        $("#btn-detail-stock-simpan").prop('disabled', true);
        if(td.find('.span-exdate-error').length <= 0) 
          td.append('<span class="label label-block label-danger span-exdate-error">Format Tanggal Salah</span>');
      }
    }
  });

  $('#btn-detail-stock-tambah').click(function() {
    addRowDetailStock({
      id: 0,
      no_batch: "",
      expired_date: "",
      qty: 0,
      mode: 'add',
    });
  });

  $('#btn-detail-stock-simpan').click(function() {
    let trIndex = $('#tr_index').val();
    let trData = tableDetailDt.row(trIndex).data();
    trData['input_check'] = 1;
    trData['pengecekan2_stock_fisik'] = numeral($('#label-stock-fisik').html())._value;
    trData['pengecekan2_selisih'] = trData['stock_sistem'] - trData['pengecekan2_stock_fisik'];
    //if(trData['pengecekan2_selisih'] != 0) trData['input_check'] = 1;

    let details = [];
    let trs = $("#table-detail-stock tbody").find('tr');
    trs.each(function() {
      let id = $(this).find('.input-detail_id').val();
      let no_batch = $(this).find('.input-detail_no_batch').val();
      let expired_date = $(this).find('.input-detail_expired_date').val();
      let qty = $(this).find('.input-detail_qty').autoNumeric('get') ? $(this).find('.input-detail_qty').autoNumeric('get') : 0;
      let mode = $(this).find('.input-detail_mode').val();

      details.push({
        id: id,
        no_batch: no_batch,
        expired_date: expired_date ? getDate(expired_date) : "",
        qty: qty,
        mode: mode,
      });
    });
    trData['pengecekan2_detail_barang'] = details;

    tableDetailDt.row(trIndex).data(trData).draw(false);
    detailStockModal.modal('hide');
  });

  $(form).validate({
    rules: {
      //
    },
    focusInvalid: true,
    errorPlacement: function(error, element) {
        var placement = $(element).closest('.input-group');
        if (placement.length > 0) {
            error.insertAfter(placement);
        } else {
            error.insertAfter($(element));
        }
    },
    submitHandler: function (form) {
      var formData = new FormData();
      formData.append('id', $('#id').val());
      formData.append('uid', $('#uid').val());
      formData.append('kode', $('#kode').val());
      formData.append('tanggal', $('#tanggal').val());

      tableDetailDt.rows().nodes().each(function (i, dt_index) {
        let trData = tableDetailDt.row(dt_index).data();
        let check = parseInt(trData.input_check);
        let detail_id = trData.id;
        let stock_id = trData.stock_id;
        let barang_id = trData.barang_id;
        let satuan_id = trData.satuan_id;
        let isi_satuan = trData.isi_satuan;
        let stock_sistem = trData.stock_sistem;
        let selisih = trData.pengecekan2_selisih;
        let stock_fisik = trData.pengecekan2_stock_fisik;

        formData.append('detail_id[]', detail_id);
        formData.append('stock_id[]', stock_id);
        formData.append('barang_id[]', barang_id);
        formData.append('satuan_id[]', satuan_id);
        formData.append('isi_satuan[]', isi_satuan);
        formData.append('stock_sistem[]', stock_sistem);
        formData.append('selisih[]', selisih);
        formData.append('stock_fisik[]', stock_fisik);

        for (let i = 0; i < trData.pengecekan2_detail_barang.length; i++) {
          let detailData = trData.pengecekan2_detail_barang[i];
          detailData.expired_date = (detailData.expired_date != null ? detailData.expired_date : "");

          formData.append(`id_${trData.barang_id}[]`, detailData.id);
          formData.append(`no_batch_${trData.barang_id}[]`, detailData.no_batch);
          formData.append(`expired_date_${trData.barang_id}[]`, detailData.expired_date);
          formData.append(`qty_${trData.barang_id}[]`, detailData.qty);
        }
      });

      swal({
        title: "Konfirmasi?",
        type: "warning",
        text: "Apakah pemeriksaan kedua yang dimasukan benar ??",
        showCancelButton: true,
        confirmButtonText: "Ya",
        confirmButtonColor: "#2196F3",
        cancelButtonText: "Batal",
        cancelButtonColor: "#FAFAFA",
        closeOnConfirm: true,
        showLoaderOnConfirm: true,
      },
      function() {
        blockPage('Sedang diproses ...');
        $.ajax({
          data: formData,
          type: 'POST',
          dataType: 'JSON', 
          processData: false,
          contentType: false,
          url: url.save,
          success: function(data){
              $.unblockUI();
              successMessage('Berhasil', "Pemeriksaan pertama berhasil disimpan.");
              window.location.assign(url.index);
          },
          error: function(data){
              $.unblockUI();
              errorMessage('Peringatan', "Terjadi kesalahan saat memproses data.");
          }
        });
        return false;
      });
    }
  });

  fillForm(UID);
});
</script>