<div class="row panel-body form-horizontal no-padding-top no-padding-bottom">
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label col-md-4"><?php echo lang('pabrik_label'); ?></label>
			<div class="col-md-8">    
                <select class="form-control input-search" id="search-pabrik">
                	<option value="" selected="selected"><?php echo lang('field_all'); ?></option>
                    <?php 
                      	foreach($pabrik as $row) {
                        	echo "<option value='{$row->id}'>{$row->nama}</option>";
                      	}
                    ?>
                </select>
            </div>
		</div>
	</div>
</div>
<hr>
<div class="table-responsive">
	<table id="table" class="table table-bordered table-striped">
		<thead class="bg-slate">
			<tr>
				<th>KODE</th>
				<th>NAMA BARANG</th>
				<th>SATUAN</th>
				<th>STOCK</th>
				<th>NILAI RP</th>
				<th>SUB TOTAL</th>
				<th>EXP. DATE</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="text-center" colspan="7">Tidak Ada Data</td>
			</tr>
		</tbody>
	</table>
</div>

<script>
$('select').select2();
(function () {
	var table = $("#table").DataTable({
		"processing": true,
		"serverSide": true,
		"ordering": false,
		"ajax": {
			"url": "<?php echo site_url('api/gudang_farmasi/laporan/laporan_001'); ?>",
			"type": "POST",
            "data": function(p) {
            	p.pabrik_id = $('#search-pabrik').val();
            }
		},
		 "columns": [
	      	{ "data": "kode" },
	      	{ "data": "nama" },
	      	{ "data": "satuan" },
	      	{ 
	      		"data": "stock",
	      		"searchable": false,
	      		"render": function (data, type, row, meta) {
	      			return numeral(data).format('0.0,');
		        }
	      	},
	      	{ 
	      		"data": "harga_penjualan",
	      		"searchable": false,
	      		"render": function (data, type, row, meta) {
	      			return 'Rp. ' + numeral(data).format('0.0,');
		        }
	      	},
	      	{ 
	      		"data": "sub_total",
	      		"searchable": false,
	      		"render": function (data, type, row, meta) {
	      			return 'Rp. ' + numeral(data).format('0.0,');
		        }
	      	},
	      	{ 
	      		"data": "expired_date",
	      		"searchable": false,
	      		"render": function (data, type, row, meta) {
	      			return data ? data : '&mdash;';
		        }
	      	},
	    ],
	});

	$(".input-search").on('change', function() {
      	table.draw();
    });

    $("#btn-print-excel").click(function () {
    	let pabrik_id = $('#search-pabrik').val();
    	let param = `?d=excel&pabrik_id=${pabrik_id}`;
		window.location.assign(`<?php echo site_url('api/gudang_farmasi/laporan/print_001'); ?>${param}`);
	});

	$("#btn-print-pdf").click(function () {
		let iframeHeight = $(window).height() - 220;
		let pabrik_id = $('#search-pabrik').val();
		let param = `?d=pdf&pabrik_id=${pabrik_id}`;
		$('#modal-print .modal-body').html(`<iframe id="modal-iframe_print" src="<?php echo site_url('api/gudang_farmasi/laporan/print_001'); ?>${param}" style="width: 100%; height: ${iframeHeight}px; border: 1px solid #e5e5e5;background-image: url(<?php echo image_url('spinner.gif') ?>); background-repeat: no-repeat; background-position: 50% 50%;"></iframe>`);
		$('#modal-print').modal('show');
	});
})();
</script>