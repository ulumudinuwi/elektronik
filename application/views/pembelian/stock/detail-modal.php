<div id="detail-stock-modal" class="modal fade" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-slate">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h6 class="modal-title"></h6>
      </div>
      <div class="modal-body form-horizontal">
        <div class="row mb-20">
          <div class="col-sm-12">
            <div class="table-responsive">
              <table id="table_detail_stock" class="table table-bordered">
                <thead>
                  <tr class="bg-slate">
                    <th style="width: 20%;">No. Batch</th>
                    <th style="width: 20%;">Expired Date</th>
                    <th style="width: 20%;">HNA</th>
                    <th style="width: 15%;">Quantity</th>
                  </tr>
                </thead>
                <tbody></tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer m-t-none">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>