<?php echo messages(); ?>
<style type="text/css">
	.table thead tr th, .table tbody tr td {
		white-space: nowrap;
		vertical-align: top;
	}

	.detail-td-footer {
		border-top: none !important;
	}
</style>
<div class="panel panel flat">
	<div class="tabbale">
		<ul class="nav nav-tabs nav-tabs-highlight">
			<li class="active">
				<a href="#tab-2" data-toggle="tab">Daftar Pembelian</a>
			</li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane active" id="tab-2">
				<div class="panel-body form-horizontal">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label col-md-3" for="search_tanggal">Tanggal</label>
								<div class="col-md-8">
									<div class="input-group">
										<span class="input-group-addon cursor-pointer" id="btn_search_tanggal">
											<i class="icon-calendar22"></i>
										</span>
										<input type="text" id="search_range_tanggal" class="form-control rangetanggal-form">
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label col-md-3">Supplier</label>
								<div class="col-md-8">
									<select class="form-control" id="search_pabrik">
										<option value="" selected="selected">- Pilih -</option>
										<?php 
											foreach($pabrik as $row) {
												echo "<option value='".base64_encode($row->id)."'>{$row->kode} - {$row->nama}</option>";
											}
										?>
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label col-md-3">Status</label>
								<div class="col-md-8">
									<select class="form-control" id="search_status">
										<option value="" selected="selected">- Pilih -</option>
										<?php 
											foreach($status as $key => $val) {
												echo "<option value='{$key}'>{$val}</option>";
											}
										?>
									</select>
								</div>
							</div>
						</div>
					</div>
				</div>
				<hr class="no-margin-bottom no-margin-top">
				<div class="panel-body no-padding-top">
					<div class="table-responsive">
						<table id="table" class="table">
							<thead>
								<tr class="bg-slate">
									<th>Nomor Invoice</th>
									<th>Tanggal</th>
									<th>Supplier</th>
									<th>Total Pembelian</th>
									<th>Status</th>
									<th class="text-center" style="width: 10%;"></th>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view('gudang-farmasi/po/detail-modal'); ?>