<style>
	.table > thead > tr > th,
	.table > tbody > tr > td, 
	.table > tfoot > tr > td {
        vertical-align: top;
		white-space: nowrap;
	}
</style>
<div class="row">
	<div class="col-md-12">
		<form id="form" class="form-horizontal" method="post">
			<div class="panel-group panel-group-control panel-group-control-right mb-10" id="accordion1">
				<div class="panel panel-white">
					<div class="panel-heading">
						<h6 class="panel-title text-bold">
							<a data-toggle="collapse" data-parent="#accordion1" class="collapsed" href="#accordion-group1">
								Data PO 
							</a>
						</h6>
					</div>
					<div id="accordion-group1" class="panel-collapse collapse">
						<div class="panel-body">
							<div class="row mb-20">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-3">Nomor</label>
										<div class="col-md-6">
											<div class="form-control-static text-bold" id="detail_kode"></div>
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-3 control-label">Tanggal</label>
										<div class="col-lg-6">
											<div class="form-control-static text-bold" id="detail_tanggal"></div>
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-3 control-label">Sifat</label>
										<div class="col-lg-6">
											<div class="form-control-static text-bold" id="detail_sifat"></div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-lg-3 control-label"><?php echo $this->lang->line('pabrik_label'); ?></label>
										<div class="col-lg-6">
											<div class="form-control-static text-bold" id="detail_pabrik"></div>
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-3 control-label">Status</label>
										<div class="col-lg-6">
											<div class="form-control-static text-bold" id="detail_status"></div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
				                <div class="col-sm-12">
				                    <fieldset>
				                        <legend class="text-bold">
				                            <i class="icon-list position-left"></i> <strong>Daftar Barang</strong>
				                        </legend>
				                        <div class="row">
				                            <div class="col-sm-12">
				                                <div class="table-responsive">
				                                    <table id="table_detail_po" class="table table-bordered">
				                                        <thead>
				                                            <tr class="bg-slate">
				                                                <th>Nama</th>
				                                                <th>Satuan</th>
				                                                <th>Qty</th>
				                                                <th>Harga</th>
				                                                <th>Sub Total</th>
				                                                <th>Diterima</th>
				                                            </tr>
				                                        </thead>
				                                        <tbody></tbody>
				                                        <tfoot>
				                                            <tr>
				                                                <td colspan="4" class="text-right text-bold">TOTAL</td>
				                                                <td colspan="2" class="text-right text-bold" id="detail_total">Rp.0</td>
				                                            </tr>
				                                            <tr>
				                                                <td colspan="4" class="text-right text-bold">TOTAL PPN</td>
				                                                <td colspan="2" class="text-right text-bold" id="detail_total_ppn">Rp.0</td>
				                                            </tr>
				                                            <tr>
				                                                <td colspan="4" class="text-right text-bold">GRAND TOTAL</td>
				                                                <td colspan="2" class="text-right text-bold" id="detail_grand_total">Rp.0</td>
				                                            </tr>
				                                        </tfoot>
				                                    </table>
				                                </div>
				                            </div>
				                        </div>
				                    </fieldset>
				                </div>
				            </div>
						</div>
					</div>
				</div>	
			</div>
			<div class="panel panel-white">
				<div class="panel-body">
					<div class="row mb-20">
						<fieldset>
							<div class="col-md-12">
								<legend class="text-bold"><i class="icon-magazine position-left"></i> Data Pengalihan PO</legend>
							</div>
							<div class="col-md-6">
								<div class="form-group">
                                    <label class="col-lg-4 control-label input-required">Tanggal</label>
                                    <div class="col-lg-6">
                                        <div class="input-group">
                                            <span class="input-group-addon cursor-pointer" id="btn_tanggal">
                                                <i class="icon-calendar22"></i>
                                            </span>
                                            <input type="text" id="tanggal" name="tanggal" class="form-control disp_tanggal" disabled="true">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
									<label class="control-label col-md-4 input-required">Sifat</label>
									<div class="col-md-6">
										<div class="input-group">
											<select class="form-control" id="sifat" name="sifat" disabled="disabled">
							                    <option value="" selected="selected">- Pilih -</option>
							                    <?php 
							                      foreach($sifat as $key => $val) {
							                        echo "<option value='{$key}'>{$val}</option>";
							                      }
							                    ?>
							                </select>
							            </div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label col-md-3 input-required"><?php echo $this->lang->line('pabrik_label'); ?></label>
									<div class="col-md-8">
										<div class="input-group">
											<select class="form-control" id="pabrik_id" name="pabrik_id">
												<option value="" selected="selected">- Pilih -</option>
												<?php 
												foreach($pabrik as $row) {
													echo "<option value='{$row->id}'>{$row->kode} - {$row->nama}</option>";
												}
												?>
											</select>
										</div>
									</div>
								</div>
							</div>
						</fieldset>
					</div>
		            <div class="row">
		                <div class="col-sm-12">
		                    <fieldset>
		                        <legend class="text-bold">
		                            <i class="icon-list position-left"></i> <strong>Daftar Barang</strong>
		                        </legend>
		                        <div class="row">
		                            <div class="col-sm-12">
		                                <div class="table-responsive">
		                                    <table id="table_detail" class="table table-bordered">
		                                        <thead>
		                                            <tr class="bg-slate">
		                                                <th>Nama</th>
		                                                <th>Satuan</th>
		                                                <th>Min</th>
		                                                <th>Stock</th>
		                                                <th>Max</th>
		                                                <th>Qty</th>
		                                                <th>Harga</th>
		                                                <th>Sub Total</th>
		                                            </tr>
		                                        </thead>
		                                        <tbody></tbody>
		                                        <tfoot>
		                                            <tr>
		                                                <td colspan="7" class="text-right text-bold">TOTAL</td>
		                                                <td class="text-right text-bold" id="disp_total">Rp.0</td>
		                                            </tr>
		                                            <tr>
		                                                <td colspan="7" class="text-right text-bold">TOTAL PPN</td>
		                                                <td class="text-right text-bold" id="disp_total_ppn">Rp.0</td>
		                                            </tr>
		                                            <tr>
		                                                <td colspan="7" class="text-right text-bold">GRAND TOTAL</td>
		                                                <td class="text-right text-bold" id="disp_grand_total">Rp.0</td>
		                                            </tr>
		                                        </tfoot>
		                                    </table>
		                                </div>
		                            </div>
		                        </div>
		                    </fieldset>
		                </div>
		            </div>
					<div class="row">
						<div class="col-md-8">
							<label>Alasan Pengalihan</label>
							<div>
								<textarea class="form-control wysihtml5-min" id="keterangan" name="keterangan"></textarea>
							</div>
						</div>
					</div>
					<div>
						<input type="hidden" id="po_id" name="po_id" value="0">
						<input type="hidden" id="po_uid" name="po_uid" value="">
						<input type="hidden" id="po_kode" name="po_kode" value="">
						<input type="hidden" id="total" name="total" value="0">
						<input type="hidden" id="total_ppn" name="total_ppn" value="0">
					</div>
				</div>
				<div class="panel-footer">
					<div class="heading-elements">
						<div class="heading-btn pull-right">
							<button type="submit" class="btn btn-success btn-labeled btn-save">
								<b><i class="icon-floppy-disk"></i></b>
								Simpan
							</button>
							<button type="button" class="btn btn-default btn-batal">Kembali</button>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<?php $this->load->view('logistik/list-barang-modal'); ?>