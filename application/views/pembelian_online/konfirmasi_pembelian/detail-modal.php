<div id="detail-modal" class="modal fade" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header bg-primary">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h6 class="modal-title">Detail Jurnal Umum</h6>
			</div>
			<div class="modal-body form-horizontal">
				<div class="row mb-20">
					<fieldset>
						<div class="col-md-12">
							<legend class="text-bold"><i class="icon-magazine position-left"></i> Detail Jurnal Umum</legend>
							<div class="row">
								<div class="col-sm-12">
									<div class="table-responsive">
										<table id="table-detail" class="table table-bordered">
											<thead>
												<tr class="bg-slate">
													<th>Nama Perkiraan</th>
													<th class="text-center" style="width: 15%;">Debit (Rp.)</th>
													<th class="text-center" style="width: 15%;">Kredit (Rp.)</th>
												</tr>
											</thead>
											<tbody></tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</fieldset>
				</div>
			</div>
			<div class="modal-footer m-t-none">
				<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
			</div>
		</div>
	</div>
</div>