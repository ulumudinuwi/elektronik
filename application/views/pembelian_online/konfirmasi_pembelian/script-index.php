<script type="text/javascript">
	var tanggalDari = "<?php echo date('Y-m-01'); ?>", tanggalSampai = "<?php echo date('Y-m-d'); ?>";
	var table;
	var tableDetail = $('#table-detail');
	var detailModal = $('#detail-modal');	
	$('#ongkos_kirim').autoNumeric('init', {aSep: '.', aDec: ',', mDec: '0'});
	$('#total_harga').autoNumeric('init', {aSep: '.', aDec: ',', mDec: '0'});
	$('#total_pembayaran').autoNumeric('init', {aSep: '.', aDec: ',', mDec: '0'});

	var url = {
		loadData : "<?php echo site_url('api/konfirmasi_pembelian/konfirmasi_pembelian/load_data'); ?>",
		loadDataHistory : "<?php echo site_url('api/konfirmasi_pembelian/konfirmasi_pembelian/load_data_history'); ?>",
		getData : "<?php echo site_url('api/konfirmasi_pembelian/konfirmasi_pembelian/get_data?uid=:UID'); ?>",
		CancelData : "<?php echo site_url('api/konfirmasi_pembelian/konfirmasi_pembelian/cancel_data'); ?>",
		KonfirmasiData : "<?php echo site_url('api/konfirmasi_pembelian/konfirmasi_pembelian/confirm_data'); ?>",
	};

	function subsDate(range, tipe) {
		let date = range.substr(0, 10);
		if(tipe === "sampai") date = range.substr(13, 10);
		return getDate(date);
	}

	$(window).ready(function() {
		$(".rangetanggal-form").daterangepicker({
			autoApply: true,
			locale: {
					format: "DD/MM/YYYY",
			},
			startDate: moment(tanggalDari),
			endDate: moment(tanggalSampai),
		});

		$("#search_range_tanggal").on('apply.daterangepicker', function (ev, picker) {
			table.draw();
		});

		$("#btn_search_tanggal").click(function () {
			$("#search_range_tanggal").data('daterangepicker').toggle();
		});

		$("#search_range_tanggal").on('change', function() {
			table.draw();
		});

		$("body").on("click", ".show-row", function () {
			let uid = $(this).data('uid');
			showDetail(uid, 'detail');
		});

		$("body").on("click", ".confirm-row", function () {
			let uid = $(this).data('uid');
			showDetail(uid, 'confirm');
		});

		$("body").on("click", ".cancel-row", function () {
			let uid = $(this).data('uid');
			showDetail(uid, 'cancel');
		});

		$("body").on("click", ".cancel-detail-row", function () {
			let uid = $(this).data('uid');
			showDetail(uid, 'cancel-detail');
		});

		$("body").on("click", ".lihat-row", function () {
			let uid = $(this).data('uid');
			showBuktiPembayaran(uid);
		});

		$("body").on("click", ".cetak-row", function () {
			let uid = $(this).data('uid');
			showCetak(uid);
		});

		$("#tab-1").on("click", function () {
			$("#table").DataTable().destroy();

			handleLoadTable($("#table"), url.loadData);
		});

		$("#tab-2").on("click", function () {
			$("#tableHistory").DataTable().destroy();
			$("#verifikasi_pos_manager_tab").removeClass('active');
			handleLoadTable($("#tableHistory"), url.loadDataHistory);
		});

		detailModal.on('click', '.btn-delete', function(){
			let uid = detailModal.find('#uid').val(),
				desc = detailModal.find('#penolakan_desc').val();

			if (desc == '') {
				swal("Alasan dikembalikan harus diisi !!!")
			}else{
				swal({
				  title: "Peringatan !!!",
				  text: "Apakan benar anda ingin mengembalikan pesanan ini ??",
				  type: "warning",
				  showCancelButton: true,
				  confirmButtonClass: "btn-danger",
				  confirmButtonText: "Ya, Kembalikan!",
				  closeOnConfirm: false
				},
				function(){
					$.ajax({
						data: {
							uid : uid,
							desc : desc,
						},
						type: 'POST',
						dataType: 'JSON', 
						url: url.CancelData,
						success: function(data){
				    		detailModal.modal('hide');
				    		swal("Pengembalian!", "Pengembalian sedang diproses.", "warning");
							setTimeout(function () {
					    		swal("Pengembalian!", "Pengembalian Berhasil !!!.", "success");
								table.draw();
							}, 3000);
						},
						error: function(data){
				    		swal("Pengembalian!", "Pengembalian Data Gagal.", "warning");
						}
					});
				});
			}
		});

		detailModal.on('click', '.btn-confirm', function(){
			let uid = detailModal.find('#uid').val();

			swal({
			  title: "Peringatan !!!",
			  text: "Konfirmasi pesanan ini ??",
			  type: "warning",
			  showCancelButton: true,
			  confirmButtonClass: "btn-danger",
			  confirmButtonText: "Konfirmasi.",
			  closeOnConfirm: false
			},
			function(){
				$.ajax({
					data: {
						uid : uid,
					},
					type: 'POST',
					dataType: 'JSON', 
					url: url.KonfirmasiData,
					success: function(data){
			    		detailModal.modal('hide');
			    		swal("Konfirmasi!", "Konfirmasi sedang diproses.", "warning");
						setTimeout(function () {
				    		swal("Konfirmasi!", "Konfirmasi Berhasil !!!.", "success");
							table.draw();
						}, 3000);
					},
					error: function(data){
			    		swal("Konfirmasi!", "Konfirmasi Data Gagal.", "warning");
					}
				});
			});
		});

		handleLoadTable($("#table"), url.loadData);
	});

	function showDetail(uid, param) {
		$('#uid').val(uid);

		$.getJSON(url.getData.replace(':UID', uid), function (data, status) {
			if (status === 'success') {
				data = data;

				total_pembayaran = parseInt(data.total_harga) + parseInt(data.ongkir);

				$('#no_invoice').html(data.no_invoice);
				$('#tanggal_transaksi').html(data.tanggal_transaksi);
				$('#status_pembayaran').html(data.status_label);
				$('#ekspedisi').html(data.ekspedisi ? data.ekspedisi : '-');
				$('#ongkos_kirim').autoNumeric('set', data.ongkir == 0 ? '-' : data.ongkir);
				$('#total_harga').autoNumeric('set', parseInt(data.total_harga));
				$('#total_pembayaran').autoNumeric('set', total_pembayaran);
				$('#penolakan_desc').html(data.alasan_penolakan);

				//detail
				tableDetail.find('tbody').empty();
				if (data.dataDetail.length > 0) {
					for (var i = 0, n = data.dataDetail.length; i < n; i++) {
						addDetail(data.dataDetail[i]);
					}
				}else{
					tableDetail.find('tbody').empty();
					tableDetail.find('tbody').append('<tr><td colspan="4" class="text-center">Tidak Ada Data</td></tr>');
				}

				detailModal.modal('show');
			}
		});

		switch(param) {
		  case 'detail':
				$('.btn-delete').hide();
				$('.btn-confirm').hide();
				$('.penolakanData').hide();
		    break;
		  case 'confirm':
				$('.btn-confirm').show();
				$('.btn-delete').hide();
				$('.penolakanData').hide();
		    break;
		  case 'cancel':
				$('.penolakanData').show();
				$('.btn-delete').show();
				$('.btn-confirm').hide();
				$('#penolakan_desc').html('a');
				$('#penolakan_desc').prop('disabled', false);
		    break;
		  case 'cancel-detail':
				$('.penolakanData').show();
				$('.btn-delete').hide();
				$('.btn-confirm').hide();
				$('#penolakan_desc').prop('disabled', true);
		    break;
		}
	}

	function showBuktiPembayaran(uid) {
		$('.btn-delete').hide();
		$('.btn-confirm').hide();
		$.getJSON(url.getData.replace(':UID', uid), function (data, status) {
			if (status === 'success') {
				data = data;

				$('#detail-modal-lihat').find('.modal-dialog').removeClass('modal-lg');
				$('#detail-modal-lihat').find('.modal-body').empty();
				$('#detail-modal-lihat').find('.modal-title').html('Bukti Pembayaran');

				let base_url = '<?php echo base_url() ?>';
				let img = '<div class="text-center"><img src="'+base_url+data.bukti_pembayaran+'" width="400px" height="400px"></div>';
				$('#detail-modal-lihat').find('.modal-body').append(img);

				$('#detail-modal-lihat').modal('show');
			}
		});
	}

	function showCetak(uid) {
		let base_url = '<?php echo base_url() ?>';
		let url = base_url+'api/v1/transaksi/checkout/print_invoice_online?uid='+uid;
		
		blockPage('Loading ...');
        
		$('#PrintModal').find('#modalIframe').prop('src', url);
		setTimeout(()=>{
	      	$.unblockUI();
	        $('#PrintModal').modal('show');
	        $('#PrintModal').find('#PrintModalLabel').html('Cetak faktur');
	    },1000);
	}

	function addDetail(data) {
		data = data || {};
		let hargaDiskon = (parseFloat(data.harga) * (parseFloat(data.diskon) / 100));
		let harga = parseFloat(data.harga) - hargaDiskon;

		var tbody = tableDetail.find('tbody');

		var tr = $("<tr/>")
		.appendTo(tbody);

		var tdPerkiraan = $("<td/>")
		.appendTo(tr);
		var inputPerkiraan = $("<label/>")
		.html(data.nama)
		.appendTo(tdPerkiraan);

		var tdQty = $("<td/>")
		.addClass('text-center')
		.appendTo(tr);
		var inputQty = $("<label/>")
		.html(parseInt(data.qty))
		.appendTo(tdQty);

		var tdPrice = $("<td/>")
		.addClass('text-right')
		.appendTo(tr);
		var inputPrice = $("<label/>")
		.html(parseInt(harga))
		.appendTo(tdPrice);

		let total_harga = parseInt(data.qty) * parseInt(harga);

		var tdTotalPrice = $("<td/>")
		.addClass('text-right')
		.appendTo(tr);
		var inputTotalPrice = $("<label/>")
		.html(parseInt(total_harga))
		.appendTo(tdTotalPrice);

		tdPrice.autoNumeric('init', {aSep: '.', aDec: ',', mDec: '0'});
		tdTotalPrice.autoNumeric('init', {aSep: '.', aDec: ',', mDec: '0'});
	}

	function handleLoadTable(tablles, uri) {
		table = tablles.DataTable({
			"processing": true,
			"serverSide": true,
			"ordering": false,
			"ajax": {
					"url": uri,
					"type": "POST",
					"data": function(p) {
						p.tanggal_dari = subsDate($("#search_range_tanggal").val(), 'dari');
						p.tanggal_sampai = subsDate($("#search_range_tanggal").val(), 'sampai');
					}
			},
			"columns": [
				{ 
					"data": "tanggal_transaksi",
					"render": function (data, type, row, meta) {
							let tmp = '<a class="show-row btn btn-success" data-uid="' + row.uid + '" data-modul="' + row.modul + '" data-total="' + row.total + '" data-transaksi_id="' + row.transaksi_id + '" data-toggle="tooltip" data-title="Lihat Detail" data-placement="top">' + moment(data).format('DD-MM-YYYY HH:mm'); + '</a>';
							return tmp;
					},
				},
				{ 
					"data": "no_invoice",
					"orderable": false,
					"searchable": false,
				},
				{ 
					"data": "no_member",
					"render": function (data, type, row, meta) {
							//let tmp = numeral(data).format();
							return data;
					},
				},
				{ 
					"data": "nama",
					"render": function (data, type, row, meta) {
							//let tmp = numeral(data).format();
							return data;
					},
				},
				{ 
					"data": "status",
					"class": "text-center",
					"render": function (data, type, row, meta) {
						let tmp = '';

							switch(parseInt(data)) {
							  case 1:
							  	tmp = '<button class="btn btn-danger">Belum Bayar</button>';
							    break;
							  case 2:
							  	tmp = '<button class="btn btn-info">Konfirmasi / Dikemas</button>';
							    break;
							  case 3:
							  	tmp = '<button class="btn btn-warning">Dikirim</button>';
							    break;
							  case 4:
							  	tmp = '<button class="btn btn-success">Selesai</button>';
							    break;
							  case 5:
							  	tmp = '<button class="btn btn-success">Dibatalkan</button>';
							    break;
							  case 6:
							  	tmp = '<button class="btn btn-danger">Dikembalikan</button>';
							    break;
							}
							if (row.status_pembayaran == 2) {
							  	tmp = '<button class="btn btn-info">Konfirmasi</button>';
							}
						return tmp;
					},
				},
				{ 
					"data": "uid",
					"class": "text-center",
					"render": function (data, type, row, meta) {
							let tmp = '';
							if (row.status >= 2 && row.status <= 5 && row.status_pembayaran == 1) {
								tmp = '<a class="confirm-row btn btn-success" data-uid="' + row.uid + '" data-toggle="tooltip" data-title="Konfirmasi" data-placement="top"><i class=" icon-checkmark"></i></a> <a class="cancel-row btn btn-danger" data-uid="' + row.uid + '" data-toggle="tooltip" data-title="Pengembalian Pesanan" data-placement="top"><i class="icon-cancel-square2"></i></a> <a class="lihat-row btn btn-info" data-uid="' + row.uid + '" data-toggle="tooltip" data-title="Lihat Bukti Pembayaran" data-placement="top"><i class="icon-eye"></i></a> <a class="cetak-row btn btn-info" data-uid="' + row.uid + '" data-toggle="tooltip" data-title="Cetak Faktur" data-placement="top"><i class="icon-printer"></i></a>';
							}else if (row.status >= 2 && row.status <= 5 && row.status_pembayaran == 2) {
								tmp = ' <a class="lihat-row btn btn-info" data-uid="' + row.uid + '" data-toggle="tooltip" data-title="Lihat Bukti Pembayaran" data-placement="top"><i class="icon-eye"></i></a> <a class="cetak-row btn btn-success" data-uid="' + row.uid + '" data-toggle="tooltip" data-title="Cetak Faktur" data-placement="top"><i class="icon-printer"></i></a>';
							}else if(row.status == 6){
								tmp = '<a class="cancel-detail-row btn btn-warning" data-uid="' + row.uid + '" data-toggle="tooltip" data-title="Lihat Alasan Pengembalian" data-placement="top"><i class="icon-eye"></i></a> <a class="lihat-row btn btn-info" data-uid="' + row.uid + '" data-toggle="tooltip" data-title="Lihat Bukti Pembayaran" data-placement="top"><i class="icon-eye"></i></a>';
							}else{
								tmp = '-';
							}

							return tmp;
					},
				},
			],
			"fnDrawCallback": function (oSettings) {
				$('[data-toggle=tooltip]').tooltip();
			},
		});
	}
</script>