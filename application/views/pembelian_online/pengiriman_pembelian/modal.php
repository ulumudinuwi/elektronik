<div id="detail-modal" class="modal fade" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header bg-primary">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h6 class="modal-title">Pengiriman Pesanan Online</h6>
			</div>
			<div class="modal-body form-horizontal">
				<div class="row mb-20">
					<fieldset>
						<div class="col-md-12">
							<legend class="text-bold"><i class="icon-magazine position-left"></i> Data Pengiriman Pesanan</legend>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group row">
										<label for="noInvoice" class="col-md-6 col-form-label">No Invoice</label>
										<div class="col-md-6">
											<label type="text" readonly="" class="col-form-label text-bold" id="no_invoice">-</label>
										</div>
									</div>
									<div class="form-group row">
										<label for="noInvoice" class="col-md-6 col-form-label">Tanggal Tansaksi</label>
										<div class="col-md-6">
											<label type="text" readonly="" class="col-form-label text-bold" id="tanggal_transaksi">-</label>
										</div>
									</div>
									<div class="form-group row">
										<label for="noInvoice" class="col-md-6 col-form-label">Status</label>
										<div class="col-md-6">
											<label type="text" readonly="" class="col-form-label text-bold" id="status_pembayaran">-</label>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group row">
										<label for="noInvoice" class="col-md-6 col-form-label">Ekspedisi</label>
										<div class="col-md-6">
											<label type="text" readonly="" class="col-form-label text-bold" id="ekspedisi">-</label>
										</div>
									</div>
									<div class="form-group row">
										<label for="noInvoice" class="col-md-6 col-form-label">Ongkos Kirim</label>
										<div class="col-md-6">
											<label type="text" readonly="" class="col-form-label text-bold" id="ongkos_kirim">-</label>
										</div>
									</div>
									<div class="form-group row">
										<label for="noInvoice" class="col-md-6 col-form-label">Total Harga (Rp)</label>
										<div class="col-md-6">
											<label type="text" readonly="" class="col-form-label text-bold" id="total_harga">-</label>
										</div>
									</div>
									<div class="form-group row">
										<label for="noInvoice" class="col-md-6 col-form-label">Total Pembayaran (Rp)</label>
										<div class="col-md-6">
											<label type="text" readonly="" class="col-form-label text-bold" id="total_pembayaran">-</label>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-12">
							<legend class="text-bold"><i class="icon-magazine position-left"></i> Detail Pesanan</legend>
							<div class="row">
								<div class="col-sm-12">
									<div class="table-responsive">
										<table id="table-detail" class="table table-bordered">
											<thead>
												<tr class="bg-slate">
													<th>Product</th>
													<th class="text-center" style="width: 20%;">Quantity</th>
													<th class="text-center" style="width: 20%;">Price (Rp)</th>
													<th class="text-center" style="width: 20%;">Total Price (Rp.)</th>
												</tr>
											</thead>
											<tbody></tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</fieldset>
				</div>

				<div class="row penolakanData">
					<fieldset>
						<div class="col-md-12">
							<legend class="text-bold"><i class="icon-magazine position-left"></i>Pengiriman Pesanan</legend>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group row">
										<label for="no_resi" class="col-md-3 col-form-label">Nomor Resi Pengiriman</label>
										<div class="col-md-4">
											<input type="text" id="no_resi" name="no_resi" class="form-control"></textarea>
										</div>
									</div>
									<div class="form-group row">
										<label for="ekspedisival" class="col-md-3 col-form-label">Ekspedisi</label>
										<div class="col-md-4">
											<select class="form-control" name="ekspedisival" id="ekspedisival">
												<option value="">- Pilih Ekspedisi -</option>
												<option value="jne">JNE</option>
												<option value="tiki">TIKI</option>
											</select>
										</div>
									</div>
								</div>
							</div>
						</div>
					</fieldset>
				</div>
			</div>
			<div class="modal-footer m-t-none">
				<input type="hidden" name="uid" id="uid">
				<button type="button" class="btn btn-success btn-confirm" >Konfirmasi</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
			</div>
		</div>
	</div>
</div>

<div id="detail-modal-lihat" class="modal fade" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header bg-primary">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h6 class="modal-title">Pengiriman Barang</h6>
			</div>
			<div class="modal-body form-horizontal">
				<div class="row mb-20">
					<fieldset>
						<div class="col-md-12">
							<legend class="text-bold"><i class="icon-magazine position-left"></i> Data Pengiriman Pesanan</legend>
							<div class="row">
								<div class="col-md-5">
									<div class="form-group row">
										<label for="no_resi" class="col-md-6 col-form-label">Nomor Resi</label>
										<div class="col-md-6">
											<label type="text" readonly="" class="col-form-label text-bold" id="no_resi_p">-</label>
										</div>
									</div>
									<div class="form-group row">
										<label for="tanggal_pengiriman" class="col-md-6 col-form-label">Tanggal Pengiriman</label>
										<div class="col-md-6">
											<label type="text" readonly="" class="col-form-label text-bold" id="tanggal_pengiriman">-</label>
										</div>
									</div>
									<div class="form-group row">
										<label for="service" class="col-md-6 col-form-label">Service</label>
										<div class="col-md-6">
											<label type="text" readonly="" class="col-form-label text-bold" id="service">-</label>
										</div>
									</div>
								</div>
								<div class="col-md-7">
									<div class="form-group row">
										<label for="dikirim_oleh" class="col-md-3 col-form-label">Dikirim Oleh</label>
										<div class="col-md-9">
											<label type="text" readonly="" class="col-form-label text-bold" id="dikirim_oleh">-</label>
										</div>
									</div>
									<div class="form-group row">
										<label for="dikirim_ke" class="col-md-3 col-form-label">Dikirim Ke</label>
										<div class="col-md-9">
											<label type="text" readonly="" class="col-form-label text-bold" id="dikirim_ke">-</label>
										</div>
									</div>
									<div class="form-group row">
										<label for="status_pengiriman" class="col-md-3 col-form-label">Status Pengiriman</label>
										<div class="col-md-9">
											<label type="text" readonly="" class="col-form-label text-bold" id="status_pengiriman">-</label>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-12">
							<legend class="text-bold"><i class="icon-magazine position-left"></i> Proses Pengiriman</legend>
							<div class="row">
								<div class="col-sm-12">
									<div class="table-responsive">
										<table id="table-pengiriman" class="table table-bordered">
											<thead>
												<tr class="bg-slate">
													<th class="text-center" style="width: 30%;">Tanggal</th>
													<th>Keterangan</th>
												</tr>
											</thead>
											<tbody></tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</fieldset>
				</div>
			</div>
			<div class="modal-footer m-t-none">
				<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="PrintModal" tabindex="-1" role="dialog" aria-labelledby="PrintModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="PrintModalLabel">Cetak Faktur</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<iframe src="" height="450" width="100%" id="modalIframe"></iframe>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-xs btn-primary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>