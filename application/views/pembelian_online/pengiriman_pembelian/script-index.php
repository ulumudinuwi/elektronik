<script type="text/javascript">
	$('select').select2();
	var site_url = '<?php echo site_url() ?>';
	var tanggalDari = "<?php echo date('Y-m-01'); ?>", tanggalSampai = "<?php echo date('Y-m-d'); ?>";
	var table;
	var tableDetail = $('#table-detail');
	var tablePengiriman = $('#table-pengiriman');
	var detailModal = $('#detail-modal');	
	$('#ongkos_kirim').autoNumeric('init', {aSep: '.', aDec: ',', mDec: '0'});
	$('#total_harga').autoNumeric('init', {aSep: '.', aDec: ',', mDec: '0'});
	$('#total_pembayaran').autoNumeric('init', {aSep: '.', aDec: ',', mDec: '0'});

	var url = {
		loadData : "<?php echo site_url('api/konfirmasi_pembelian/pengiriman_pembelian/load_data'); ?>",
		loadDataHistory : "<?php echo site_url('api/konfirmasi_pembelian/pengiriman_pembelian/load_data_history'); ?>",
		getData : "<?php echo site_url('api/konfirmasi_pembelian/pengiriman_pembelian/get_data?uid=:UID'); ?>",
		InputResi : "<?php echo site_url('api/konfirmasi_pembelian/pengiriman_pembelian/input_resi'); ?>",
	};

	function subsDate(range, tipe) {
		let date = range.substr(0, 10);
		if(tipe === "sampai") date = range.substr(13, 10);
		return getDate(date);
	}

	$(window).ready(function() {
		$(".rangetanggal-form").daterangepicker({
			autoApply: true,
			locale: {
					format: "DD/MM/YYYY",
			},
			startDate: moment(tanggalDari),
			endDate: moment(tanggalSampai),
		});

		$("#search_range_tanggal").on('apply.daterangepicker', function (ev, picker) {
			table.draw();
		});

		$("#btn_search_tanggal").click(function () {
			$("#search_range_tanggal").data('daterangepicker').toggle();
		});

		$("#search_range_tanggal").on('change', function() {
			table.draw();
		});

		$("body").on("click", ".show-row", function () {
			let uid = $(this).data('uid');
			showDetail(uid, 'detail');
		});

		$("body").on("click", ".confirm-row", function () {
			let uid = $(this).data('uid');
			detailModal.find('#no_resi').val('');
			detailModal.find('#ekspedisival').val('');
			showDetail(uid, 'confirm');
		});

		$("body").on("click", ".pengiriman-row", function () {
			let uid = $(this).data('uid');
			showEkspedisi(uid);
		});

		$("body").on("click", ".cetak-row", function () {
			let uid = $(this).data('uid');
			showCetak(uid);
		});

		$("#tab-1").on("click", function () {
			$("#table").DataTable().destroy();

			handleLoadTable($("#table"), url.loadData);
		});

		$("#tab-2").on("click", function () {
			$("#tableHistory").DataTable().destroy();
			$("#verifikasi_pos_manager_tab").removeClass('active');
			handleLoadTable($("#tableHistory"), url.loadDataHistory);
		});

		detailModal.on('click', '.btn-confirm', function(){
			let uid = detailModal.find('#uid').val(),
				no_resi = detailModal.find('#no_resi').val();
				ekspedisi = detailModal.find('#ekspedisival').val();

			if (no_resi == '') {
				swal("Nomor resi harus diisi !!!")
			}else{
				swal({
				  title: "Peringatan !!!",
				  text: "Apakan Nomor Resi sudah benar ??",
				  type: "warning",
				  showCancelButton: true,
				  confirmButtonClass: "btn-danger",
				  confirmButtonText: "Ya, Lanjutkan!",
				  closeOnConfirm: false
				},
				function(){
					$.ajax({
						data: {
							uid : uid,
							no_resi : no_resi,
							ekspedisi : ekspedisi,
						},
						type: 'POST',
						dataType: 'JSON', 
						url: url.InputResi,
						success: function(data){
				    		detailModal.modal('hide');
				    		swal("Pengiriman!", "Pengiriman sedang diproses.", "warning");
							setTimeout(function () {
					    		swal("Pengiriman!", "Pengiriman Berhasil !!!.", "success");
								table.draw();
							}, 3000);
						},
						error: function(data){
				    		swal("Pengiriman!", "Pengiriman Data Gagal.", "warning");
						}
					});
				});
			}
		});

		handleLoadTable($("#table"), url.loadData);
	});

	function showDetail(uid, param) {
		$('#uid').val(uid);

		$.getJSON(url.getData.replace(':UID', uid), function (data, status) {
			if (status === 'success') {
				data = data;

				total_pembayaran = parseInt(data.total_harga) + parseInt(data.ongkir);

				$('#no_invoice').html(data.no_invoice);
				$('#tanggal_transaksi').html(data.tanggal_transaksi);
				$('#status_pembayaran').html(data.status_label);
				$('#ekspedisi').html(data.ekspedisi ? data.ekspedisi : '-');
				$('#ongkos_kirim').autoNumeric('set', data.ongkir == 0 ? '-' : data.ongkir);
				$('#total_harga').autoNumeric('set', parseInt(data.total_harga));
				$('#total_pembayaran').autoNumeric('set', total_pembayaran);
				$('#penolakan_desc').html(data.alasan_penolakan);

				//detail
				tableDetail.find('tbody').empty();
				if (data.dataDetail.length > 0) {
					for (var i = 0, n = data.dataDetail.length; i < n; i++) {
						addDetail(data.dataDetail[i]);
					}
				}else{
					tableDetail.find('tbody').empty();
					tableDetail.find('tbody').append('<tr><td colspan="4" class="text-center">Tidak Ada Data</td></tr>');
				}

				detailModal.modal('show');
			}
		});

		switch(param) {
		  case 'confirm':
				$('.btn-confirm').show();
				$('.btn-delete').hide();
				$('.penolakanData').show();
		    break;
		}
	}

	function showEkspedisi(uid) {
		$('.btn-delete').hide();
		$('.btn-confirm').hide();
		
		blockPage('Loading ...');

		$.getJSON(url.getData.replace(':UID', uid), function (data, status) {
			if (status === 'success') {
				results = data;

				data2 = { 
			    		  courier : results.ekspedisi,
			    		  no_resi : results.no_resi,
			    		};

				$.ajax({
			        type : 'POST',
			        url : site_url+'/pembelian_online/pengiriman_pembelian/getWaybill',
			        data : data2,
			        dataType : 'json',
			        success: function (data) {
			          	let resultD = data.rajaongkir.result;

			          	$('#no_resi_p').html(resultD.details.waybill_number);
						$('#tanggal_pengiriman').html(moment(resultD.details.waybill_date).format('MMM D, YYYY')+' '+ resultD.details.waybill_time);
						$('#service').html(resultD.summary.service_code);
						$('#dikirim_oleh').html(resultD.details.shippper_name+' '+resultD.details.origin);
						$('#dikirim_ke').html(resultD.details.receiver_name+' '+resultD.details.receiver_address1+' '+resultD.details.receiver_address2+' '+resultD.details.receiver_address3+' '+resultD.details.receiver_city);
						$('#status_pengiriman').html(resultD.summary.status);
						
						tablePengiriman.find('tbody').empty();

						for (var i = 0, n = resultD.manifest.length; i < n; i++) {
							prosesPengirirman(resultD.manifest[i]);
						}
			        }
			    });
				setTimeout(()=>{
	      			$.unblockUI();
					$('#detail-modal-lihat').modal('show');
			    },1000);

			}
		});
	}

	function prosesPengirirman(data){
		data = data || {};

		var tbody = tablePengiriman.find('tbody');

		var tr = $("<tr/>")
		.appendTo(tbody);

		var tdTanggal = $("<td/>")
		.appendTo(tr);
		var inputTanggal = $("<label/>")
		.html(moment(data.manifest_date).format('MMM D, YYYY')+' '+ data.manifest_time)
		.appendTo(tdTanggal);

		var tdKetrangan = $("<td/>")
		.appendTo(tr);
		var inputKeterangan = $("<label/>")
		.html(data.manifest_description)
		.appendTo(tdKetrangan);
	}
	
	function showCetak(uid) {
		let base_url = '<?php echo base_url() ?>';
		let url = base_url+'api/v1/transaksi/checkout/print_invoice_online?uid='+uid;
		
		blockPage('Loading ...');
        
		$('#PrintModal').find('#modalIframe').prop('src', url);
		setTimeout(()=>{
	      	$.unblockUI();
	        $('#PrintModal').modal('show');
	        $('#PrintModal').find('#PrintModalLabel').html('Cetak faktur');
	    },1000);
	}

	function addDetail(data) {
		data = data || {};
		let hargaDiskon = (parseFloat(data.harga) * (parseFloat(data.diskon) / 100));
		let harga = parseFloat(data.harga) - hargaDiskon;

		var tbody = tableDetail.find('tbody');

		var tr = $("<tr/>")
		.appendTo(tbody);

		var tdPerkiraan = $("<td/>")
		.appendTo(tr);
		var inputPerkiraan = $("<label/>")
		.html(data.nama)
		.appendTo(tdPerkiraan);

		var tdQty = $("<td/>")
		.addClass('text-center')
		.appendTo(tr);
		var inputQty = $("<label/>")
		.html(parseInt(data.qty))
		.appendTo(tdQty);

		var tdPrice = $("<td/>")
		.addClass('text-right')
		.appendTo(tr);
		var inputPrice = $("<label/>")
		.html(parseInt(harga))
		.appendTo(tdPrice);

		let total_harga = parseInt(data.qty) * parseInt(harga);

		var tdTotalPrice = $("<td/>")
		.addClass('text-right')
		.appendTo(tr);
		var inputTotalPrice = $("<label/>")
		.html(parseInt(total_harga))
		.appendTo(tdTotalPrice);

		tdPrice.autoNumeric('init', {aSep: '.', aDec: ',', mDec: '0'});
		tdTotalPrice.autoNumeric('init', {aSep: '.', aDec: ',', mDec: '0'});
	}

	function handleLoadTable(tablles, uri) {
		table = tablles.DataTable({
			"processing": true,
			"serverSide": true,
			"ordering": false,
			"ajax": {
					"url": uri,
					"type": "POST",
					"data": function(p) {
						p.tanggal_dari = subsDate($("#search_range_tanggal").val(), 'dari');
						p.tanggal_sampai = subsDate($("#search_range_tanggal").val(), 'sampai');
					}
			},
			"columns": [
				{ 
					"data": "tanggal_transaksi",
					"render": function (data, type, row, meta) {
							let tmp = '<a class="show-row btn btn-success" data-uid="' + row.uid + '" data-modul="' + row.modul + '" data-total="' + row.total + '" data-transaksi_id="' + row.transaksi_id + '" data-toggle="tooltip" data-title="Lihat Detail" data-placement="top">' + moment(data).format('DD-MM-YYYY HH:mm'); + '</a>';
							return tmp;
					},
				},
				{ 
					"data": "no_invoice",
					"orderable": false,
					"searchable": false,
				},
				{ 
					"data": "no_member",
					"render": function (data, type, row, meta) {
							//let tmp = numeral(data).format();
							return data;
					},
				},
				{ 
					"data": "nama",
					"render": function (data, type, row, meta) {
							//let tmp = numeral(data).format();
							return data;
					},
				},
				{ 
					"data": "status",
					"class": "text-center",
					"render": function (data, type, row, meta) {
						let tmp = '';
							if (row.status_pembayaran == 2) {
							  	tmp = '<button class="btn btn-warning">Konfirmasi / Dikemas</button>';
							}else {
							  	tmp = '<button class="btn btn-success">Dikirim</button>';
							}
						return tmp;
					},
				},
				{ 
					"data": "uid",
					"class": "text-center",
					"render": function (data, type, row, meta) {
							let tmp = '';
							if (row.status_pembayaran == 2) {
								tmp = '<a class="confirm-row btn btn-success" data-uid="' + row.uid + '" data-toggle="tooltip" data-title="Input No. Resi" data-placement="top"><i class=" icon-pencil"></i></a> <a class="cetak-row btn btn-info" data-uid="' + row.uid + '" data-toggle="tooltip" data-title="Cetak Faktur" data-placement="top"><i class="icon-printer"></i></a>';
							}else{
								tmp = '<a class="pengiriman-row btn btn-warning" data-uid="' + row.uid + '" data-toggle="tooltip" data-title="Lihat Pengiriman" data-placement="top"><i class="icon-truck"></i></a> <a class="cetak-row btn btn-info" data-uid="' + row.uid + '" data-toggle="tooltip" data-title="Cetak Faktur" data-placement="top"><i class="icon-printer"></i></a>';
							}

							return tmp;
					},
				},
			],
			"fnDrawCallback": function (oSettings) {
				$('[data-toggle=tooltip]').tooltip();
			},
		});
	}
</script>