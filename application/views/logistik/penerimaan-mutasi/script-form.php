<script>
var UID = "<?php echo $uid; ?>",
    form = '#form';

var url = {
  index: "<?php echo site_url('logistik/penerimaan_mutasi'); ?>",
  save: "<?php echo site_url('api/logistik/penerimaan_mutasi/save'); ?>",
  getDataMutasi: "<?php echo site_url('api/request/unit_kerja/mutasi/get_data/:UID'); ?>",
};

var tableDetail = $('#table_detail'),
    tableDetailDt;

function fillForm(uid) {
  tableDetailDt = tableDetail.DataTable({
    "info": false,
    "ordering": false,
    "paginate": false,
  });

  blockElement($(form));
  $.getJSON(url.getDataMutasi.replace(':UID', uid), function(data, status) {
    if (status === 'success') {
      data = data.data;
      
      $("#mutasi_id").val(data.id);
      $("#pemohon").val(data.pemohon);
      $("#unitkerja_id").val(data.unitkerja_id).trigger('change');
      $("#label_kode_mutasi").html(data.kode);
      $("#label_pemohon").html(data.pemohon);
      $("#label_keterangan").html(data.keterangan);
      
      for (var i = 0; i < data.details.length; i++) {
        columnBarang(data.details[i]);
      }
      $(form).unblock();
    }
  });
}

function columnBarang(data) {
  tableDetailDt.row.add([
    // Col 1
    `<input type="hidden" class="input-detail_id" name="detail_id[]" value="${data.id}">` +
    `<input type="hidden" class="input-barang_id" name="barang_id[]" value="${data.barang_id}">` +
    `${data.kode_barang}` +
    `<br/><span class="text-slate-300 text-bold text-size-mini label-disp_barang mt-10">${data.barang}</span>`,
    // Col 2
    `<input type="hidden" class="input-satuan_id" name="satuan_id[]" value="${data.satuan_id}">` +
    `<input type="hidden" class="input-isi_satuan" name="isi_satuan[]" value="${data.isi_satuan}">` +
    `<label class="label-satuan">` +
    `${data.satuan}` +
    `</label>`,
    // Col 3
    `<input type="hidden" class="input-qty" name="qty[]" value="${data.qty}">` +
    `<label class="label-qty">${numeral(data.qty).format('0.0,')}</label>`,
  ]).draw(false);
}

$(document).ready(function() {
  $(".input-decimal").autoNumeric('init', {aSep: '.', aDec: ',', mDec: '2'});
  $(".input-bulat").autoNumeric('init', {aSep: '.', aDec: ',', mDec: '0'});

  $(".wysihtml5-min").wysihtml5({
    parserRules:  wysihtml5ParserRules
  });
  $(".wysihtml5-toolbar").remove();

  $('.btn-save').on('click', function(e) {
    e.preventDefault();
    $(form).submit();
  });

  $(".btn-batal").click(function() {
    window.location.assign(url.index);
  });

  $(".disp_tanggal").daterangepicker({
    singleDatePicker: true,
    startDate: moment("<?php echo date('Y-m-d'); ?>"),
    endDate: moment("<?php echo date('Y-m-d'); ?>"),
    applyClass: "bg-slate-600",
    cancelClass: "btn-default",
    opens: "center",
    autoApply: true,
    locale: {
      format: "DD/MM/YYYY"
    }
  });

  $("#btn_tanggal").click(function () {
    let parent = $(this).parent();
    parent.find('input').data("daterangepicker").toggle();
  });
  
  $(form).validate({
    rules: {
      tanggal: { required: true },
      unitkerja_id: { required: true },
    },
    focusInvalid: true,
    errorPlacement: function(error, element) {
        var placement = $(element).closest('.input-group');
        if (placement.length > 0) {
            error.insertAfter(placement);
        } else {
            error.insertAfter($(element));
        }
    },
    submitHandler: function (form) {
      tableDetailDt.search('').draw(false);
      swal({
        title: "Konfirmasi?",
        type: "warning",
        text: "Apakah data yang dimasukan benar??",
        showCancelButton: true,
        confirmButtonText: "Ya",
        confirmButtonColor: "#2196F3",
        cancelButtonText: "Batal",
        cancelButtonColor: "#FAFAFA",
        closeOnConfirm: true,
        showLoaderOnConfirm: true,
      },
      function() {
        $('.input-decimal').each(function() {
          $(this).val($(this).autoNumeric('get'));
        });

        $('.input-bulat').each(function() {
          $(this).val($(this).autoNumeric('get'));
        });

        $('input, textarea, select').prop('disabled', false);

        blockPage('Sedang diproses ...');
        var formData = $(form).serialize();
        $.ajax({
          data: formData,
          type: 'POST',
          dataType: 'JSON', 
          url: url.save,
          success: function(data){
              $.unblockUI();
              successMessage('Berhasil', "Penerimaan Mutasi berhasil disimpan.");
              window.location.assign(url.index);
          },
          error: function(data){
              $.unblockUI();
              errorMessage('Peringatan', "Terjadi kesalahan saat memproses data.");
          }
        });
        return false;
      });
    }
  });

  fillForm(UID);
});
</script>