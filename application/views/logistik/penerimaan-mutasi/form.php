<style>
	.table > thead > tr > th,
	.table > tbody > tr > td {
        vertical-align: top;
		white-space: nowrap;
	}
</style>
<div class="row">
	<div class="col-md-12">
		<form id="form" class="form-horizontal" method="post">
			<div class="panel panel-white">
				<div class="panel-body">
					<div class="row mb-20">
						<fieldset>
							<div class="col-md-12">
								<legend class="text-bold"><i class="icon-magazine position-left"></i> Data Pengeluaran</legend>
							</div>
							<div class="col-md-6">
								<div class="form-group el-hidden">
									<label class="control-label col-md-4 input-required">Nomor</label>
									<div class="col-md-6">
										<input type="text" class="form-control" id="kode" name="kode" placeholder="Nomor ..." disabled="true">
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4 input-required">No. Mutasi</label>
									<div class="col-md-6">
										<div class="form-control-static text-bold" id="label_kode_mutasi">&mdash;</div>
									</div>
								</div>
								<div class="form-group">
                                    <label class="col-lg-4 control-label input-required">Tanggal</label>
                                    <div class="col-lg-6">
                                        <div class="input-group">
                                            <span class="input-group-addon cursor-pointer" id="btn_tanggal">
                                                <i class="icon-calendar22"></i>
                                            </span>
                                            <input type="text" id="tanggal" name="tanggal" class="form-control disp_tanggal" disabled="true">
                                        </div>
                                    </div>
                                </div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
                                    <label class="col-lg-4 control-label input-required">Pemohon</label>
                                    <div class="col-lg-6">
                                		<div class="form-control-static text-bold" id="label_pemohon">&mdash;</div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-4 control-label input-required">Unit</label>
                                    <div class="col-lg-6">
                                    	<div class="input-group">
	                                		<select class="form-control" id="unitkerja_id" name="unitkerja_id" disabled="disabled">
	                                			<option value="" selected="selected">- Pilih -</option>
							                    <?php 
							                      	foreach($unit as $row) {
								                        echo "<option value='{$row->id}'>{$row->nama}</option>";
							                      	}
							                    ?>
	                                		</select>
	                                	</div>
                                    </div>
                                </div>
							</div>
						</fieldset>
					</div>
		            <div class="row mb-20">
		                <div class="col-sm-12">
		                    <fieldset>
		                        <legend class="text-bold">
		                            <i class="icon-list"></i> Daftar Barang
		                        </legend>
		                        <div class="row">
		                            <div class="col-sm-12">
		                                <div class="table-responsive">
		                                    <table id="table_detail" class="table table-bordered">
		                                        <thead>
		                                            <tr class="bg-slate">
		                                                <th>Nama</th>
		                                                <th class="text-center" width="15%">Satuan</th>
		                                                <th class="text-center" width="12%">Qty</th>
		                                            </tr>
		                                        </thead>
		                                        <tbody></tbody>
		                                    </table>
		                                </div>
		                                <div class="row mt-10">
											<div class="col-md-8">
												<label>Keterangan</label>
												<div>
							                        <div class="form-control-static text-bold" id="label_keterangan">&mdash;</div>
							                    </div>
											</div>
										</div>
		                            </div>
		                        </div>
		                    </fieldset>
		                </div>
		            </div>
					<div>
						<input type="hidden" id="id" name="id" value="0">
						<input type="hidden" id="uid" name="uid" value="">
						<input type="hidden" id="pemohon" name="pemohon" value="">
						<input type="hidden" id="mutasi_id" name="mutasi_id" value="">
					</div>
				</div>
				<div class="panel-footer">
					<div class="heading-elements">
						<div class="heading-btn pull-right">
							<button type="submit" class="btn btn-success btn-labeled btn-save">
								<b><i class="icon-floppy-disk"></i></b>
								Simpan
							</button>
							<button type="button" class="btn btn-default btn-batal">Kembali</button>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>