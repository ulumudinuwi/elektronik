<script>
var UID = "<?php echo $uid; ?>",
    form = '#form';

var url = {
  index: "<?php echo site_url('logistik/so'); ?>",
  save: "<?php echo site_url('api/logistik/so/save'); ?>",
  getData: "<?php echo site_url('api/logistik/so/get_data/:UID'); ?>",
};

var tableDetail = $('#table_detail'),
    tableDetailDt;

function fillForm(uid) {
  tableDetailDt = tableDetail.DataTable({
    "ordering": false,
    "fnDrawCallback": function (oSettings) {
      $('[data-toggle=tooltip]').tooltip();
      $('.input-stock_fisik').autoNumeric('init', {aSep: '.', aDec: ',', mDec: '2', vMin: 0});
      $('.disp-check').uniform();
      $('.checkbox-inline')
              .css('top', '-15px')
              .css('left', '8px');
    },
  });

  blockElement($(form));
  $.getJSON(url.getData.replace(':UID', uid), function(data, status) {
    if (status === 'success') {
      data = data.data;

      $("#detail_kode").html(data.kode);
      $("#detail_tanggal").html(moment(data.tanggal).format('DD/MM/YYYY HH:mm'));
      $("#detail_pengecekan1_by").html(data.pengecekan1_by);
      $("#id").val(data.id);
      $("#uid").val(data.uid);
      $("#kode").val(data.kode);
      $("#tanggal").val(data.tanggal);

      for (var i = 0; i < data.details.length; i++) {
        columnBarang(data.details[i]);
      }
      $(form).unblock();
    }
  });
}

function columnBarang(data) {
  tableDetailDt.row.add([
    // Col 1
    `<input type="hidden" class="input-detail_id" name="detail_id[]" value="${data.id}">` +
    `<input type="hidden" class="input-stock_id" name="stock_id[]" value="${data.stock_id}">` +
    `${data.barang}` +
    `<br/><span class="text-slate-300 text-bold text-size-mini disp_barang mt-10">${data.kode_barang}</span>`,
    // Col 2
    `<label class="label-satuan">${data.satuan}</label>`,
    // Col 3
    `<label>${numeral(data.stock_sistem).format('0.0,')}</label>`,
    // Col 4
    `<label>${numeral(data.pengecekan1_stock_fisik).format('0.0,')}</label>`,
    // Col 5
    `<label>${numeral(data.pengecekan1_selisih).format('0.0,')}</label>`,
    // Col 6
    `<input type="hidden" class="input-stock_sistem" name="stock_sistem[]" value="${data.stock_sistem}">` +
    `<input type="hidden" class="input-pengecekan1_stock_fisik" name="pengecekan1_stock_fisik[]" value="${data.pengecekan1_stock_fisik}">` +
    `<input type="hidden" class="input-pengecekan1_selisih" name="pengecekan1_selisih[]" value="${data.pengecekan1_selisih}">` +
    `<input type="hidden" class="input-selisih" name="selisih[]" value="0">` +
    `<input type="hidden" class="input-check" value="0">` +
    `<input type="text" class="input-stock_fisik form-control text-right input-decimal" name="stock_fisik[]" value="">`,
    // Col 7
    `<span class="label-selisih">${numeral(0).format('0.0,')}</span>`,
    // Col 8
    `<label class="checkbox-inline">` +
    `<input type="checkbox" class="disp-check" disabled>` +
    `</label>`,
  ]).draw(false);
}

$(document).ready(function() {
  $(".wysihtml5-min").wysihtml5({
    parserRules:  wysihtml5ParserRules
  });
  $(".wysihtml5-toolbar").remove();
  
  $('.btn-save').on('click', function(e) {
    e.preventDefault();
    $(form).submit();
  });

  $(".btn-batal").click(function() {
    window.location.assign(url.index);
  });

  tableDetail.on('keyup', '.input-stock_fisik', function() {
    let tr = $(this).parents('tr');
    let val = isNaN(parseFloat($(this).autoNumeric('get'))) ? "" : parseFloat($(this).autoNumeric('get'));
    let selisih = 0;
    let check = 0;
    if(val !== "") {
      let stockSistem = parseFloat(tr.find('.input-stock_sistem').val());
      selisih = stockSistem - val;
      check = 1;
    }
    tr.find('.input-selisih').val(selisih);
    tr.find('.label-selisih').html(numeral(selisih).format('0.0,'));
    tr.find('.input-check').val(check);

    let parent = tr.find('.disp-check').parent();
    parent.removeClass('checked');
    if(check === 1) parent.addClass('checked');
  });

  $(form).validate({
    rules: {
      //
    },
    focusInvalid: true,
    errorPlacement: function(error, element) {
        var placement = $(element).closest('.input-group');
        if (placement.length > 0) {
            error.insertAfter(placement);
        } else {
            error.insertAfter($(element));
        }
    },
    submitHandler: function (form) {
      var formData = new FormData($('#form'));
      formData.append('id', $('#id').val());
      formData.append('uid', $('#uid').val());
      formData.append('kode', $('#kode').val());
      formData.append('tanggal', $('#tanggal').val());

      tableDetailDt.rows().nodes().each(function (i, dt_index) {
        let check = parseInt(tableDetailDt.row(dt_index).nodes().to$().find('.input-check').val());
        //if(check == 1) {
          let detail_id = tableDetailDt.row(dt_index).nodes().to$().find('.input-detail_id').val();
          let stock_id = tableDetailDt.row(dt_index).nodes().to$().find('.input-stock_id').val();
          let pengecekan1_stock_fisik = tableDetailDt.row(dt_index).nodes().to$().find('.input-pengecekan1_stock_fisik').val();
          let pengecekan1_selisih = tableDetailDt.row(dt_index).nodes().to$().find('.input-pengecekan1_selisih').val();
          let stock_fisik = tableDetailDt.row(dt_index).nodes().to$().find('.input-stock_fisik').autoNumeric('get');
          let selisih = tableDetailDt.row(dt_index).nodes().to$().find('.input-selisih').val();
          
          formData.append('detail_id[]', detail_id);
          formData.append('stock_id[]', stock_id);
          formData.append('pengecekan1_stock_fisik[]', pengecekan1_stock_fisik);
          formData.append('pengecekan1_selisih[]', pengecekan1_selisih);
          formData.append('stock_fisik[]', stock_fisik);
          formData.append('selisih[]', selisih);
        //}
      });

      swal({
        title: "Konfirmasi?",
        type: "warning",
        text: "Apakah pemeriksaan kedua yang dimasukan benar ??",
        showCancelButton: true,
        confirmButtonText: "Ya",
        confirmButtonColor: "#2196F3",
        cancelButtonText: "Batal",
        cancelButtonColor: "#FAFAFA",
        closeOnConfirm: true,
        showLoaderOnConfirm: true,
      },
      function() {
        blockPage('Sedang diproses ...');
        $.ajax({
          data: formData,
          type: 'POST',
          dataType: 'JSON', 
          processData: false,
          contentType: false,
          url: url.save,
          success: function(data){
              $.unblockUI();
              successMessage('Berhasil', "Pemeriksaan pertama berhasil disimpan.");
              window.location.assign(url.index);
          },
          error: function(data){
              $.unblockUI();
              errorMessage('Peringatan', "Terjadi kesalahan saat memproses data.");
          }
        });
        return false;
      });
    }
  });

  fillForm(UID);
});
</script>