<?php
require (APPPATH.'libraries/dbuger.php');
use Carbon\Carbon;
use Ryuna\Auth;
use Illuminate\Database\Capsule\Manager as DB;

if (!function_exists('abort')) {
    function abort($status = 404, $message = ''){
        
        $CI = &get_instance();
        $CI->lang->load('abort');
        $data = [];
        
        $message = $message ? $message : '';
        switch ($status) {
            case 404:
                $data['status'] = $status;
                $data['message'] = $message ? $message : lang('page_not_found');
            break;
            case 400:
                $data['status'] = $status;
                $data['message'] = $message ? $message : lang('page_not_found');
            break;
            
            default: return; break;
        }

       

        $CI->load->view('errors/abort', $data);
        echo $CI->load->view('errors/abort', $data, TRUE);

        die();

    }
}

if (!function_exists('cek_hela')) {
    function cek_hela($arg){
        new dbuger($arg);
        exit();
    }
}

if (!function_exists('cek_dulu')) {
    function cek_dulu($arg){
        dump($arg);
        exit();
    }
}

if (!function_exists('uuid')) {
    function uuid(){
        return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            // 32 bits for "time_low"
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),

            // 16 bits for "time_mid"
            mt_rand( 0, 0xffff ),

            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            mt_rand( 0, 0x0fff ) | 0x4000,

            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            mt_rand( 0, 0x3fff ) | 0x8000,

            // 48 bits for "node"
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
        );
    }
}

if (!function_exists('date_to_db')) {
    function date_to_db($date){
        if($date) {
            return Carbon::createFromFormat('d/m/Y', $date)->format('Y-m-d');
        }
        return '';
    }
}

if (!function_exists('date_to_view')) {
    function date_to_view($date){
        if($date) {
            return Carbon::createFromFormat('Y-m-d', $date)->format('d/m/Y');
        }
        return '';
    }
}

if (!function_exists('harga_sebelum_diskon')) {
    function harga_sebelum_diskon($harga_sesudah_diskon, $diskon){
        $diskon = (Int) $diskon;
        $harga_ = $harga_sesudah_diskon * 100;
        $diskon_ = 100 - $diskon;
        $sebelum_diskon = $harga_/$diskon_;
        return $sebelum_diskon; 
    }
}

if (!function_exists('get_request')) {
    function get_request(){
        $request = $_POST + $_GET + $_FILES;
        return (Object) $request;
    }
}

if (!function_exists('timestamp')) {
    function timestamp(){
        return Carbon::now()->format('Y-m-d H:m:s');
    }
}

if (!function_exists('auth_id')) {
    function auth_id(){
        return Auth::user()->id;
    }
}

if (!function_exists('toNumber')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function toNumber($rupiah)
    {
        $clean = explode(',',$rupiah);
        $clean = preg_replace('/\D/','',$clean[0]);
        return $clean;
    }
}

if (!function_exists('toRupiah')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function toRupiah($number)
    {
        $number = (Float) $number;
        $hasil_rupiah = number_format($number,2,',','.');
        return $hasil_rupiah;
    }
}

if (!function_exists('request_handler')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function request_handler()
    {
        $request = $_REQUEST;
        if(!isset($request)){
            abort(400,'Bad Request'); exit();
        }
        return (Object) $request;
    }
}
