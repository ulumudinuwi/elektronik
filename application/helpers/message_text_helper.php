<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

define("MESSAGE_TEXT_TABLE", "t_message_text");

function send_mesage($module, $action, $message) {
	$CI =& get_instance();
	$CI->load->database();
	
	$data = array(
		'module'	=> $module,
		'action'	=> $action,
		'message'	=> $message
	);
	$CI->db->set('uid', 'UUID()', FALSE);
	$CI->db->insert(MESSAGE_TEXT_TABLE, $data);
}

function get_message($module, $action) {
	$CI =& get_instance();
	$CI->load->database();
	
	$query = $CI->db->select('*')
					->from(MESSAGE_TEXT_TABLE)
					->where('module', $module)
					->where('action', $action)
					->where('status', 0)
					->get();
	
	if ($query->num_rows() < 1)
		return false;
	
	return $query->result();
}

function update_message($module, $action) {
	$CI =& get_instance();
	$CI->load->database();
	
	$CI->db->set("status", 1);
	$CI->db->where("status", 0);
	$CI->db->where("module", $module);
	$CI->db->where("action", $action);
	$CI->db->update(MESSAGE_TEXT_TABLE);
}

function clear_message() {
	$CI =& get_instance();
	$CI->load->database();
	
	return $CI->db->delete(MESSAGE_TEXT_TABLE, array('option_name' => $name));
}