<?php

/**
 * Untuk Menarik notifikasi untuk user tertentu
 */
if (! function_exists('notif_msg_fetch')) {
    function notif_msg_fetch ($member_id, $offset, $limit) {
        $CI =& get_instance();

        $messages = $CI->db->where('member_id', $member_id)
            ->order_by('created_at', 'desc')
            ->limit($limit, $offset)
            ->get('t_notifikasi_ecommerce')->result();

        return $messages;
    }
}

/**
 * Untuk menandai bahwa notifikasi sudah di read
 */
if (! function_exists('notif_msg_read')) {
    function notif_msg_read ($id) {
        $CI =& get_instance();

        $notif = $CI->db->where('id', $id)
            ->get('t_notifikasi_ecommerce')->row();

        if ($notif) {
            $CI->db->where('id', $id);
            $CI->db->update('t_notifikasi_ecommerce', [
                'read_flag' => 1,
                'read_at' => date('Y-m-d H:i:s'),
            ]);

            return ['message' => 'Notifikasi telah dibaca'];
        } else {
            return ['message' => 'Data tersebut tidak tersedia'];
        }
    };
}

/**
 * Untuk menandai bahwa notifikasi sudah dibaca semua berdasarkan member id
 */
if (! function_exists('notif_msg_read_all_by_member_id')) {
    function notif_msg_read_all_by_member_id($member_id) {
        $CI =& get_instance();

        $CI->db->where('member_id', $member_id);
        $CI->db->where('read_flag', 0);
        $CI->db->update('t_notifikasi_ecommerce', [
            'read_flag' => 1,
            'read_at' => date('Y-m-d H:i:s'),
        ]);
    }
}

/**
 * Untuk Membuat Notifikasi Untuk Member Tertentu
 */
if (! function_exists('notif_msg_member')) {
    function notif_msg_member($member_id, $title, $description, $link = '#') {
        $CI =& get_instance();

        $CI->db->insert('t_notifikasi_ecommerce', [
            'member_id' => $member_id,
            'title' => $title,
            'description' => $description,
            'link' => $link,
            'created_by' => $CI->session->userdata('auth_user'),
            'created_at' => date('Y-m-d H:i:s'),
        ]);
    }
}