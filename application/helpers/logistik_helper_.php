<?php
if(! function_exists('procStockMasuk')) {
	function procStockMasuk($obj, $detail, $tipe) {
		$CI =& get_instance();
		$table_def_stock = 't_logistik_stock';
		$table_def_stock_detail = 't_logistik_stock_detail';
		$table_def_kartu_stock = 't_logistik_kartu_stock';
		$table_def_barang = 'm_barang';

		$checkExists = $CI->db->select('id') // CHECK STOCK BARANG
							->where('barang_id', $detail->barang_id)
							->get($table_def_stock);

		if($tipe == $CI->config->item('tipe_kartu_stock_penerimaan')) {	
			if($checkExists->num_rows() > 0) {
				$stock_id = $checkExists->row()->id;
				$CI->db->set('qty', 'qty + '.$detail->qty, FALSE)
						->set('dipesan', $detail->dipesan)
						->set('update_at', date('Y-m-d H:i:s'))
						->set('update_by', $CI->auth->userid())
						->where('id', $stock_id)
						->update($table_def_stock);
			} else {
				$objNew = array(
					'barang_id' => $detail->barang_id,
					'qty' => $detail->qty,
					'created_at' => date('Y-m-d H:i:s'),
					'created_by' => $CI->auth->userid(),
				);
				$CI->db->insert($table_def_stock, $objNew);
				$stock_id = $CI->db->insert_id();
			}
			# GET STOCK DETAIL
			$checkExists = $CI->db->select('id')
									->where('stock_id', $stock_id)
									->where('harga', $detail->harga)
									->get($table_def_stock_detail);
		} else { // else - KONDISI TIPE KARTU STOCK
			$stock_id = 0;
			if($checkExists->num_rows() > 0) $stock_id = $checkExists->row()->id;
			# GET STOCK DETAIL
			$checkExists = $CI->db->select('id, qty')
									->where('stock_id', $stock_id)
									->where('qty < qty_awal')
									->order_by('id', 'DESC')
									->limit(1)
									->get($table_def_stock_detail);
		}

		# UPDATE STOCK DETAIL
		if($checkExists->num_rows() > 0) {
			$stock_detail_id = $checkExists->row()->id;
			if($tipe == $CI->config->item('tipe_kartu_stock_penerimaan')) {
				$CI->db->set('qty_awal', 'qty_awal + '.$detail->qty, FALSE)
					->set('qty', 'qty + '.$detail->qty, FALSE)
					->set('update_at', date('Y-m-d H:i:s'))
				   	->set('update_by', $CI->auth->userid())
					->where('id', $stock_detail_id)
					->update($table_def_stock_detail);

				$objStock = $CI->db->select('qty')
						->where('id', $stock_id)
						->get($table_def_stock)->row();

				$objStockDetail = $CI->db->select('qty')
									->where('id', $stock_detail_id)
									->get($table_def_stock_detail)->row();

				# KARTU STOCK
				$kartu = new stdClass();
				$kartu->tipe    = $tipe;
				$kartu->tanggal = $obj->tanggal;
				$kartu->kode = $obj->kode;
				$kartu->transaksi_id = $obj->id;
				$kartu->transaksi_detail_id = $detail->id;
				$kartu->barang_id = $detail->barang_id;
				$kartu->stock_detail_id = $stock_detail_id;
				$kartu->masuk 	  = $detail->qty;
				$kartu->sisa 	  = $objStockDetail->qty;
				$kartu->sisa_sum  = $objStock->qty;
				$kartu->keterangan = isset($obj->keterangan) ? $obj->keterangan : '';
				$kartu->created_by = $CI->auth->userid();
		        $kartu->created_at = date('Y-m-d H:i:s');
		        $CI->db->insert($table_def_kartu_stock, $kartu);
			} else { // else - KONDISI TIPE KARTU STOCK
				$tmpQty = $detail->qty;
				do {
					$objStockDetail = $CI->db->select('id, qty, qty_awal')
											->where('stock_id', $stock_id)
											->where('qty < qty_awal')
											->order_by('id', 'DESC')
											->limit(1)
											->get($table_def_stock_detail)->row();

		            if($objStockDetail) {
		            	$jumlahMasuk = $tmpQty;
		            	$tmpQtyAwal = $objStockDetail->qty_awal;
		            	$tmpStock = $tmpQty + $objStockDetail->qty;
		            	if($tmpQtyAwal < $tmpStock) {
		            		$jumlahMasuk = $tmpStock - $tmpQtyAwal;
		            		$tmpStock = $tmpQtyAwal;
		            	}
		            	$tmpQty -= $jumlahMasuk;

		                $CI->db->set('qty', 'qty + '.$jumlahMasuk, FALSE)
		                		->where('id', $objStockDetail->id)
		                		->update($table_def_stock_detail);

		                $CI->db->set('qty', 'qty + '.$jumlahMasuk, FALSE)
								->set('update_at', date('Y-m-d H:i:s'))
								->set('update_by', $CI->auth->userid())
								->where('id', $stock_id)
								->update($table_def_stock);

						$stock = $CI->db->select('qty')
										->where('id', $stock_id)
										->get($table_def_stock)->row()->qty;

		                # KARTU STOCK
		                $kartu = new stdClass();
						$kartu->tipe    = $tipe;
						$kartu->tanggal = $obj->tanggal;
						$kartu->kode = $obj->kode;
						$kartu->transaksi_id = $obj->id;
						$kartu->transaksi_detail_id = $detail->id;
						$kartu->barang_id = $detail->barang_id;
						$kartu->stock_detail_id = $objStockDetail->id;
						$kartu->masuk 	  = $jumlahMasuk;
						$kartu->sisa 	  = $tmpStock;
						$kartu->sisa_sum  = $stock;
						$kartu->keterangan = $obj->keterangan ? $obj->keterangan : '&mdash;';
						$kartu->created_by = $CI->auth->userid();
				        $kartu->created_at = date('Y-m-d H:i:s');
				        $CI->db->insert($table_def_kartu_stock, $kartu);
		            } else break;
				} while ($tmpQty > 0);					
			}
		} else {
			$harga = isset($detail->harga) ? $detail->harga : 0;
			if($tipe != $CI->config->item('tipe_kartu_stock_penerimaan')) {
				$objObat = $this->db->query("SELECT harga_pembelian, isi_satuan_penggunaan FROM {$table_def_barang} WHERE id = {$detail->barang_id}")->row();
				if($objObat) $harga = $objObat->harga_pembelian / $obj->isi_satuan_penggunaan;
			}

			$objNew = array(
				'stock_id' => $stock_id,
				'harga' => $harga,
				'qty' => $detail->qty,
				'qty_awal' => $detail->qty,
				'created_at' => date('Y-m-d H:i:s'),
				'created_by' => $CI->auth->userid(),
			);
			$CI->db->insert($table_def_stock_detail, $objNew);
			$stock_detail_id = $CI->db->insert_id();

			$objStock = $CI->db->select('qty')
							->where('id', $stock_id)
							->get($table_def_stock)->row();

			$objStockDetail = $CI->db->select('qty')
								->where('id', $stock_detail_id)
								->get($table_def_stock_detail)->row();

			# KARTU STOCK
			$kartu = new stdClass();
			$kartu->tipe    = $tipe;
			$kartu->tanggal = $obj->tanggal;
			$kartu->kode = $obj->kode;
			$kartu->transaksi_id = $obj->id;
			$kartu->transaksi_detail_id = $detail->id;
			$kartu->barang_id = $detail->barang_id;
			$kartu->stock_detail_id = $stock_detail_id;
			$kartu->masuk 	  = $detail->qty;
			$kartu->sisa 	  = $objStockDetail->qty;
			$kartu->sisa_sum  = $objStock->qty;
			$kartu->keterangan = isset($obj->keterangan) ? $obj->keterangan : '';
			$kartu->created_by = $CI->auth->userid();
	        $kartu->created_at = date('Y-m-d H:i:s');
	        $CI->db->insert($table_def_kartu_stock, $kartu);
		}
	}
}

if(! function_exists('procStockKeluar')) {
	function procStockKeluar($obj, $detail, $tipe) {
		$CI =& get_instance();
		$table_def_stock = 't_logistik_stock';
		$table_def_stock_detail = 't_logistik_stock_detail';
		$table_def_kartu_stock = 't_logistik_kartu_stock';

		$objStock = $CI->db->select('id')
							->where('barang_id', $detail->barang_id)
							->get($table_def_stock)->row();

		$tmpQty = $detail->qty;
		do {
			$objStockDetail = $CI->db->select('id, qty')
									->where('stock_id', $objStock->id)
									->order_by('id', 'ASC')
									->limit(1)
									->get($table_def_stock_detail)->row();

            if($objStockDetail) {
                $tmpStockSisa = $tmpQty - $objStockDetail->qty;
                $stock_sisa = $tmpQty - $objStockDetail->qty;
                if($stock_sisa > 0) {
                    $stock_sisa = 0;
                    $sendJumlah = $objStockDetail->qty;
                } else {
                    $stock_sisa = $objStockDetail->qty - $tmpQty;
                    $sendJumlah = $tmpQty;
                }
                $tmpQty = $tmpStockSisa;
                $CI->db->set('qty', $stock_sisa)
                		->where('id', $objStockDetail->id)
                		->update($table_def_stock_detail);

                $CI->db->set('qty', 'qty - '.$sendJumlah, FALSE)
						->set('update_at', date('Y-m-d H:i:s'))
						->set('update_by', $CI->auth->userid())
						->where('id', $objStock->id)
						->update($table_def_stock);

				$stock = $CI->db->select('qty')
								->where('barang_id', $detail->barang_id)
								->get($table_def_stock)->row()->qty;

                # KARTU STOCK
                $kartu = new stdClass();
				$kartu->tipe    = $tipe;
				$kartu->tanggal = $obj->tanggal;
				$kartu->kode = $obj->kode;
				$kartu->transaksi_id = $obj->id;
				$kartu->transaksi_detail_id = $detail->id;
				$kartu->barang_id = $detail->barang_id;
				$kartu->stock_detail_id = $objStockDetail->id;
				$kartu->keluar 	  = $sendJumlah;
				$kartu->sisa 	  = $stock_sisa;
				$kartu->sisa_sum  = $stock;
				$kartu->keterangan = $obj->keterangan;
				$kartu->created_by = $CI->auth->userid();
		        $kartu->created_at = date('Y-m-d H:i:s');
		        $CI->db->insert($table_def_kartu_stock, $kartu);
            } else break;
		} while ($tmpQty > 0);					
	}
}

if(! function_exists('procStockSO')) {
	function procStockSO($obj, $detail, $tipe) {
		$CI =& get_instance();
		$table_def_stock = 't_logistik_stock';
		$table_def_stock_detail = 't_logistik_stock_detail';
		$table_def_kartu_stock = 't_logistik_kartu_stock';
		$table_def_barang = 'm_barang';

		$stock_id = $detail->stock_id;
		$objStock = $CI->db->select('id, barang_id, qty')
						->where('id', $stock_id)
						->get($table_def_stock)->row();
		
		$selisih = $detail->pengecekan2_selisih;
		$fisik = $detail->pengecekan2_stock_fisik;
		if($selisih < 0) {
			$stock = 0;
			do {
				$checkExists = $CI->db->select('id, qty, qty_awal')
										->where('stock_id', $stock_id)
										->where('qty > ', 0)
										->where('qty < qty_awal')
										->order_by('id', 'DESC')
										->limit(1)
										->get($table_def_stock_detail);
				if($checkExists->num_rows() <= 0) {
					$checkExists = $CI->db->select('id, qty, qty_awal')
										->where('stock_id', $stock_id)
										->where('qty <= qty_awal')
										->order_by('id', 'DESC')
										->limit(1)
										->get($table_def_stock_detail);
				}

				$objStockDetail = $checkExists->row();
	            if($objStockDetail) {
	            	$stock_detail_id = $objStockDetail->id;

	            	$jumlahMasuk = $fisik;
	            	$tmpStock = $jumlahMasuk;
	            	$tmpQtyAwal = $objStockDetail->qty_awal;
	            	if($tmpQtyAwal == $objStockDetail->qty) {
	            		$tmpQtyAwal = $tmpStock;
	            	} else {
	            		if($tmpQtyAwal < $tmpStock) {
		            		$jumlahMasuk = $tmpQtyAwal;
		            		$tmpStock = $tmpQtyAwal;
		            	}
	            	}
	            	
	                $CI->db->set('qty_awal', $tmpQtyAwal, FALSE)
	                		->set('qty', $jumlahMasuk, FALSE)
	                		->where('id', $stock_detail_id)
	                		->update($table_def_stock_detail);
	            } else {
	            	$harga = 0;
					$objBarang = $CI->db->query("SELECT harga_pembelian, isi_satuan_penggunaan FROM {$table_def_barang} WHERE id = {$objStock->barang_id}")->row();
					if($objBarang) $harga = $objBarang->harga_pembelian / $objBarang->isi_satuan_penggunaan;

					$objNew = array(
						'stock_id' => $stock_id,
						'harga' => $harga,
						'qty' => $fisik,
						'qty_awal' => $fisik,
						'created_at' => date('Y-m-d H:i:s'),
						'created_by' => $CI->auth->userid(),
					);
					$CI->db->insert($table_def_stock_detail, $objNew);
					$stock_detail_id = $CI->db->insert_id();

					$jumlahMasuk = $fisik;
					$tmpStock = $fisik;

					$CI->db->set('qty', $tmpStock, FALSE)
	                		->set('status', 1)
							->set('update_at', date('Y-m-d H:i:s'))
							->set('update_by', $CI->auth->userid())
							->where('id', $stock_id)
							->update($table_def_stock);
	            }
				$fisik -= $jumlahMasuk;

				$CI->db->set('qty', $stock + $jumlahMasuk, FALSE)
	                		->set('status', 1)
							->set('update_at', date('Y-m-d H:i:s'))
							->set('update_by', $CI->auth->userid())
							->where('id', $stock_id)
							->update($table_def_stock);
				$stock += $jumlahMasuk;

                # KARTU STOCK
                $kartu = new stdClass();
				$kartu->tipe    = $tipe;
				$kartu->tanggal = $obj->tanggal;
				$kartu->kode = $obj->kode;
				$kartu->transaksi_id = $obj->id;
				$kartu->transaksi_detail_id = $detail->id;
				$kartu->barang_id = $objStock->barang_id;
				$kartu->stock_detail_id = $stock_detail_id;
				$kartu->masuk 	  = $tmpStock;
				$kartu->sisa 	  = $tmpStock;
				$kartu->sisa_sum  = $stock;
				$kartu->keterangan = 'Stock Opname Barang';
				$kartu->created_by = $CI->auth->userid();
		        $kartu->created_at = date('Y-m-d H:i:s');
		        $CI->db->insert($table_def_kartu_stock, $kartu);
			} while ($fisik > 0);					
		} else {
			$CI->db->set('qty', $fisik, FALSE)
            		->set('status', 1)
					->set('update_at', date('Y-m-d H:i:s'))
					->set('update_by', $CI->auth->userid())
					->where('id', $stock_id)
					->update($table_def_stock);

			$stock = 0;
			do {
				$checkExists = $CI->db->select('id, qty, qty_awal')
										->where('stock_id', $stock_id)
										->where('qty >', 0)
										->where('qty <= qty_awal')
										->order_by('id', 'ASC')
										->limit(1)
										->get($table_def_stock_detail);

				$objStockDetail = $checkExists->row();
	            if($objStockDetail) {
	            	$jumlahKeluar = $fisik;
	            	$tmpQty = $objStockDetail->qty;
	            	$tmpStock = $fisik - $tmpQty;
	            	if($tmpStock > 0) {
	            		$jumlahKeluar = $objStockDetail->qty;
	            		$tmpStock = 0;
	            	}
	            	$fisik -= $jumlahKeluar;
	            	$stock += $jumlahKeluar;

	                $CI->db->set('qty_awal', $jumlahKeluar, FALSE)
	                		->set('qty', $jumlahKeluar, FALSE)
	                		->where('id', $objStockDetail->id)
	                		->update($table_def_stock_detail);

	                # KARTU STOCK
	                $kartu = new stdClass();
					$kartu->tipe    = $tipe;
					$kartu->tanggal = $obj->tanggal;
					$kartu->kode = $obj->kode;
					$kartu->transaksi_id = $obj->id;
					$kartu->transaksi_detail_id = $detail->id;
					$kartu->barang_id = $objStock->barang_id;
					$kartu->stock_detail_id = $objStockDetail->id;
					$kartu->keluar 	  = $jumlahKeluar;
					$kartu->sisa 	  = $jumlahKeluar;
					$kartu->sisa_sum  = $stock;
					$kartu->keterangan = 'Stock Opname Barang';
					$kartu->created_by = $CI->auth->userid();
			        $kartu->created_at = date('Y-m-d H:i:s');
			        $CI->db->insert($table_def_kartu_stock, $kartu);
	            } else break;
			} while ($fisik > 0);								
		}
	}
}