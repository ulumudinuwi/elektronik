<?php
use Illuminate\Database\Capsule\Manager as DB;
use Carbon\Carbon;
use Hhxsv5\SSE\SSE;
use Hhxsv5\SSE\Update;

class EventProcess {

    private $table = '';
    public $last_checksum = '';
    //Action method in the controller

    public function __construct($uid,$event_name){
        $this->table = base64_decode($uid);
        $this->event_name = $event_name;
        $this->last_checksum = DB::select('CHECKSUM TABLE '.$this->table)[0]['Checksum'];
        // $this->sendEvent();
    }

    public function start()
    {
        header('Content-Type: text/event-stream');
        header('Cache-Control: no-cache');
        header('Connection: keep-alive');
        header('X-Accel-Buffering: no');

        $sse = new SSE();
        $sse->start(new Update(function () {
            $new_checksum = DB::select('CHECKSUM TABLE '.$this->table)[0]['Checksum'];
            // return json_encode([$new_checksum,$this->last_checksum]);
            if($this->last_checksum != $new_checksum){
                $this->last_checksum = $new_checksum;
                $message = [
                    'last_checksum' => $this->last_checksum,
                    'new_checksum' => $new_checksum,
                    'times' => Carbon::now()->format('d-m-Y'),
                ];
            }

            if (!empty($message)) {
                return json_encode(['message' => $message]);
            }
            return false;
        }), $this->event_name);
    }

    public function update(){
        
    }
}