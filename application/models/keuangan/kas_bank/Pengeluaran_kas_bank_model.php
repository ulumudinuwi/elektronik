<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Pengeluaran_kas_bank_model extends CI_Model {
	protected $table_def = "t_kas_bank";
	protected $table_def_detail = "t_kas_bank_detail";
  	protected $table_def_user = "auth_users";

	public function __construct() {
        parent::__construct();
        $this->load->helper('kas_bank');
        $this->load->helper('laba_rugi');
    }
	
	private function _get_select($customSelect = "") {
		if($customSelect != "") {
			$select = $customSelect;
		} else {
			$select = array(
				"{$this->table_def}.*",
				"CONCAT(created_by.first_name, ' ',created_by.last_name) created_by",
			);
		}
		return 'SELECT '.implode(', ', $select).' ';
	}
	
	private function _get_from() {
		$from = "FROM {$this->table_def}";
		return $from;
	}

	private function _get_join() {
		$join = "LEFT JOIN {$this->table_def_user} created_by ON {$this->table_def}.created_by = created_by.id ";
		return $join;
	}
	
	public function get_by($sWhere = "", $customSelect = "") {
		$sql = $this->_get_select($customSelect)." ";
		$sql .= $this->_get_from()." ";
		$sql .= $this->_get_join();
		if (!empty($sWhere)) {
			$sql .= " ".$sWhere;
		}
        $query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			return $query->row();
		}
		else {
			return false;
		}
    }
	
	public function get_all($iLimit = 10, $iOffset = 0, $sWhere = "", $sOrder = "", $customSelect = "") {
		
		$data = array();
		$sql_count = "SELECT COUNT({$this->table_def}.id) AS numrows ";
		$sql_count .= $this->_get_from()." ";
		$sql_count .= $this->_get_join();
		if (!empty($sWhere)) {
			$sql_count .= " ".$sWhere." ";
		}
		$query = $this->db->query($sql_count);
		if ($query->num_rows() == 0) {
			$data['total_rows'] = 0;
		}
		else {
			$row = $query->row();
			$data['total_rows'] = (int) $row->numrows;
		}
		
		$select = $this->_get_select($customSelect);
		$from = $this->_get_from();
		$join = $this->_get_join();
		$sql = $select." ".$from." ".$join." ";
		if (!empty($sWhere)) {
			$sql .= $sWhere." ";
		}
		if (!empty($sOrder)) {
			$sql .= $sOrder." ";
		}
		if ($iLimit > 0) {
			$sql .= "LIMIT ".$iOffset.", ".$iLimit;
		}
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			$data['data'] = $query->result();
		}
		else {
			$data['data'] = array();
		}
		return $data;
	}

	public function create($obj) {
		$this->db->trans_start();

		$obj->no_pengeluaran = generateNomor($obj->kode_p, $this->table_def);

		$data = get_object_vars($obj);
		unset($data['details']);
		unset($data['uid']);
		unset($data['kode_p']);
		unset($data['jumlah']);
		unset($data['keterangan']);
		unset($data['modul']);
		unset($data['tipe']);
		$this->db->set('uid', 'UUID()', FALSE);;
		$data['created_by'] = $this->session->userdata('auth_user');
		$data['created_at'] = date('Y-m-d H:i:s');

		$this->db->insert($this->table_def, $data);
		$obj->id =  $this->db->insert_id();

        $dataLabaRugi = new stdClass();
        $dataLabaRugi->transaksi_id = $obj->id;
        $dataLabaRugi->modul = $obj->modul; //JENIS_MODUL_PENGELUARAN_KAS_BANK;
        $dataLabaRugi->total = $obj->jumlah;
        $dataLabaRugi->keterangan = $obj->keterangan; //"Pengeluaran Kas Bank senilai : ".$obj->jumlah;
        $dataLabaRugi->perkiraan_id = $data['perkiraan_id'];
        $dataLabaRugi->tipe = $obj->tipe; //JENIS_KREDIT;
        //insert
        insertLabarugi($dataLabaRugi);

		$this->_saveDetail($obj);

		$this->db->trans_complete();
		if ($this->db->trans_status() === TRUE) {
		  $this->db->trans_commit();
		  return TRUE;
		} else {
		  $this->db->trans_rollback();
		  return false;
		}
	}

	public function update($obj) {
		$this->db->trans_start();

		$data = get_object_vars($obj);
		unset($data['details']);
		unset($data['uid']);
		unset($data['kode_p']);
		unset($data['jumlah']);
		unset($data['keterangan']);
		unset($data['modul']);
		unset($data['tipe']);
		$data['update_by'] = $this->session->userdata('auth_user');
		$data['update_at'] = date('Y-m-d H:i:s');
		
		$this->db->where('uid', $obj->uid);
		$this->db->update($this->table_def, $data);

        $dataLabaRugi = new stdClass();
        $dataLabaRugi->transaksi_id = $obj->id;
        $dataLabaRugi->modul = $obj->modul; //JENIS_MODUL_PENGELUARAN_KAS_BANK;
        $dataLabaRugi->total = $obj->jumlah;
        $dataLabaRugi->keterangan = $obj->keterangan; //"Pengeluaran Kas Bank senilai : ".$obj->jumlah;
        $dataLabaRugi->bpom = $this->session->userdata('bpom');;
        $dataLabaRugi->perkiraan_id = $data['perkiraan_id'];
        $dataLabaRugi->tipe = $obj->tipe; //JENIS_KREDIT;
        //insert
        updateLabarugi($dataLabaRugi);

		$this->_saveDetail($obj);

		$this->db->trans_complete();
		if ($this->db->trans_status() === TRUE) {
		  $this->db->trans_commit();
		  return TRUE;
		} else {
		  $this->db->trans_rollback();
		  return false;
		}
	}

	private function _saveDetail($obj) {
		if (! property_exists($obj, 'details')) return;
		foreach ($obj->details as $detail) {
			$data = get_object_vars($detail);
			unset($data['id']);
			unset($data['data_mode']);
			unset($data['perkiraan']);
			unset($data['keterangan_detail']);
			unset($data['tipe']);
			$data['update_by'] = $this->session->userdata('auth_user');
        	$data['update_at'] = date('Y-m-d H:i:s');
            switch ($detail->data_mode) {
                case DATA_MODE_MODEL::DATA_MODE_ADD:
                	$data['kas_bank_id'] = $obj->id;
                	$data['perkiraan_id'] = $detail->perkiraan;
					$data['created_by'] = $this->session->userdata('auth_user');
	        		$data['created_at'] = date('Y-m-d H:i:s');
					$this->db->insert($this->table_def_detail, $data);

					$detail->id = $this->db->insert_id();

			        $dataLabaRugi = new stdClass();
			        $dataLabaRugi->transaksi_id = $obj->id;
			        $dataLabaRugi->transaksi_detail_id = $detail->id;
			        $dataLabaRugi->modul = $obj->modul; //JENIS_MODUL_PENGELUARAN_KAS_BANK;
        			$dataLabaRugi->bpom = $this->session->userdata('bpom');;
			        $dataLabaRugi->total = $detail->jumlah;
			        $dataLabaRugi->keterangan = $detail->keterangan_detail; //"Pengeluaran Kas Bank senilai : ".$obj->jumlah;
			        $dataLabaRugi->perkiraan_id = $detail->perkiraan;
			        $dataLabaRugi->tipe = $detail->tipe; //JENIS_KREDIT;
			        //insert
			        insertLabarugi($dataLabaRugi);
                    break;
                case DATA_MODE_MODEL::DATA_MODE_EDIT:
                    $this->db->where('id', $detail->id)
                    		->update($this->table_def_detail, $data);

					        $dataLabaRugi = new stdClass();
					        $dataLabaRugi->transaksi_id = $obj->id;
					        $dataLabaRugi->transaksi_detail_id = $detail->id;
					        $dataLabaRugi->modul = $obj->modul; //JENIS_MODUL_PENGELUARAN_KAS_BANK;
					        $dataLabaRugi->total = $detail->jumlah;
					        $dataLabaRugi->keterangan = $detail->keterangan_detail; //"Pengeluaran Kas Bank senilai : ".$obj->jumlah;
        					$dataLabaRugi->bpom = $this->session->userdata('bpom');;
					        $dataLabaRugi->perkiraan_id = $detail->perkiraan;
					        $dataLabaRugi->tipe = $detail->tipe; //JENIS_KREDIT;
					        //insert
					        updateLabarugi($dataLabaRugi);
                    break;
                case DATA_MODE_MODEL::DATA_MODE_DELETE:
                    $this->db->where('id', $detail->id)
                    		->delete($this->table_def_detail); 

					        $dataLabaRugi = new stdClass();
					        $dataLabaRugi->transaksi_id = $obj->id;
					        $dataLabaRugi->transaksi_detail_id = $detail->id;
					        $dataLabaRugi->modul = $obj->modul;
					        deleteLabarugi($dataLabaRugi);
                    break;
            }
        }		
	}

	public function delete_pengeluaran_kas_bank($uid, $modul) {
		$this->db->set('deleted', 1);
		$this->db->where('uid', $uid);
		$this->db->update($this->table_def);

		$data = $this->db->select('id')
	            ->where('uid', $uid)
	            ->get($this->table_def)
	            ->row();

        $dataLabaRugi = new stdClass();
        $dataLabaRugi->transaksi_id = $data->id;
        $dataLabaRugi->modul = $modul;
        deleteLabarugi($dataLabaRugi);

		return true;
	}
}

?>