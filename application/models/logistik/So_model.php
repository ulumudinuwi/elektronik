<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class So_model extends CI_Model {
	protected $table_def = "t_logistik_stock_opname";
  	protected $table_def_detail = "t_logistik_stock_opname_detail";
  	protected $table_def_stock = "t_logistik_stock";
	protected $table_def_user = "auth_users";

	public function __construct() {
        parent::__construct();
        $this->load->helper('logistik');
        $this->load->helper('gudang');
    }
	
	private function _get_select($customSelect = "") {
		if($customSelect != "") {
			$select = $customSelect;
		} else {
			$select = array(
				"{$this->table_def}.*",
				"CONCAT(pengecekan1_by.first_name, ' ',pengecekan1_by.last_name) pengecekan1_by",
				"CONCAT(pengecekan2_by.first_name, ' ',pengecekan2_by.last_name) pengecekan2_by",
			);
		}
		return 'SELECT '.implode(', ', $select).' ';
	}
	
	private function _get_from() {
		$from = "FROM {$this->table_def}";
		return $from;
	}

	private function _get_join() {
		$join = "LEFT JOIN {$this->table_def_user} pengecekan1_by ON {$this->table_def}.pengecekan1_by = pengecekan1_by.id ";
		$join .= "LEFT JOIN {$this->table_def_user} pengecekan2_by ON {$this->table_def}.pengecekan2_by = pengecekan2_by.id ";
		return $join;
	}
	
	public function get_by($sWhere = "", $customSelect = "") {
		$sql = $this->_get_select($customSelect)." ";
		$sql .= $this->_get_from()." ";
		$sql .= $this->_get_join();
		if (!empty($sWhere)) {
			$sql .= " ".$sWhere;
		}
        $query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			return $query->row();
		}
		else {
			return false;
		}
    }
	
	public function get_all($iLimit = 10, $iOffset = 0, $sWhere = "", $sOrder = "", $customSelect = "") {
		
		$data = array();
		$sql_count = "SELECT COUNT({$this->table_def}.id) AS numrows ";
		$sql_count .= $this->_get_from()." ";
		$sql_count .= $this->_get_join();
		if (!empty($sWhere)) {
			$sql_count .= " ".$sWhere." ";
		}
		$query = $this->db->query($sql_count);
		if ($query->num_rows() == 0) {
			$data['total_rows'] = 0;
		}
		else {
			$row = $query->row();
			$data['total_rows'] = (int) $row->numrows;
		}
		
		$select = $this->_get_select($customSelect);
		$from = $this->_get_from();
		$join = $this->_get_join();
		$sql = $select." ".$from." ".$join." ";
		if (!empty($sWhere)) {
			$sql .= $sWhere." ";
		}
		if (!empty($sOrder)) {
			$sql .= $sOrder." ";
		}
		if ($iLimit > 0) {
			$sql .= "LIMIT ".$iOffset.", ".$iLimit;
		}
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			$data['data'] = $query->result();
		}
		else {
			$data['data'] = array();
		}
		return $data;
	}

	public function saveFirstCheck($obj) {
		$this->db->trans_start();

		$data = get_object_vars($obj);
		unset($data['details']);
		$data['kode'] = generateKode("GLSO", $this->table_def);
		$this->db->set('uid', 'UUID()', FALSE);
		$this->db->insert($this->table_def, $data);
		$obj->id = $this->db->insert_id();

		$this->_saveDetail($obj);

		$this->db->trans_complete();
		if ($this->db->trans_status() === TRUE) {
		  $this->db->trans_commit();
		  return TRUE;
		} else {
		  $this->db->trans_rollback();
		  return false;
		}
	}

	public function saveSecondCheck($obj) {
		$this->db->trans_start();

		$data = get_object_vars($obj);
		unset($data['uid']);
		unset($data['details']);
		$this->db->where('uid', $obj->uid);
		$this->db->update($this->table_def, $data);

		$this->_saveDetail($obj);

		$this->db->trans_complete();
		if ($this->db->trans_status() === TRUE) {
		  $this->db->trans_commit();
		  return $obj->uid;
		} else {
		  $this->db->trans_rollback();
		  return false;
		}
	}

	public function _saveDetail($obj) {
		if (! property_exists($obj, 'details')) return;
		foreach ($obj->details as $detail) {
			$data = get_object_vars($detail);
			unset($data['id']);
			unset($data['data_mode']);
			switch ($detail->data_mode) {
                case Data_mode_model::DATA_MODE_ADD:
                	$data['so_id'] = $obj->id;
					$this->db->insert($this->table_def_detail, $data);

					$this->db->set('status', 2)
						->where('id', $detail->stock_id)
						->update($this->table_def_stock);
                    break;
                case Data_mode_model::DATA_MODE_EDIT:
                    $this->db->where('id', $detail->id)
                    		->update($this->table_def_detail, $data);

                    procStockSO($obj, $detail, $this->config->item('tipe_kartu_stock_stock_opname'));
                    break;
            }
        }		
	}
}

?>