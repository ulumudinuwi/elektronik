<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Pabrik_model extends CI_Model {

	protected $table_def = "m_pabrik";
	protected $table_def_kurs = "m_kurs";
	protected $table_def_detail_distributor = "m_pabrik_distributor";

	public function __construct() {
        parent::__construct();
    }
	
	private function _get_select() {
		$select = array(
			"{$this->table_def}.*",
			"{$this->table_def_kurs}.country_id kurs_code",
			"{$this->table_def_kurs}.currency_name kurs_name",
			"{$this->table_def_kurs}.currency kurs",
		);
		return "SELECT ".implode(', ', $select);
	}
	
	private function _get_from() {
		$from = "FROM ".$this->table_def;
		return $from;
	}

	private function _get_join() {
		$join = array(
			"LEFT JOIN {$this->table_def_kurs} ON {$this->table_def}.kurs_id = {$this->table_def_kurs}.id",
		);
		return implode(' ', $join);
	}
	
	public function get_by($sWhere = "") {
		$sql = $this->_get_select()." ";
		$sql .= $this->_get_from()." ";
		$sql .= $this->_get_join();
		if (!empty($sWhere)) {
			$sql .= " ".$sWhere;
		}
        $query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			return $query->row();
		}
		else {
			return false;
		}
    }
	
	public function get_all($iLimit = 10, $iOffset = 0, $sWhere = "", $sOrder = "") {
		
		$data = array();
		$sql_count = "SELECT COUNT({$this->table_def}.id) AS numrows ";
		$sql_count .= $this->_get_from()." ";
		$sql_count .= $this->_get_join();
		if (!empty($sWhere)) {
			$sql_count .= " ".$sWhere." ";
		}
		$query = $this->db->query($sql_count);
		if ($query->num_rows() == 0) {
			$data['total_rows'] = 0;
		}
		else {
			$row = $query->row();
			$data['total_rows'] = (int) $row->numrows;
		}
		
		$select = $this->_get_select();
		$from = $this->_get_from();
		$join = $this->_get_join();
		$sql = $select." ".$from." ".$join." ";
		if (!empty($sWhere)) {
			$sql .= $sWhere." ";
		}
		if (!empty($sOrder)) {
			$sql .= $sOrder." ";
		}
		if ($iLimit > 0) {
			$sql .= "LIMIT ".$iOffset.", ".$iLimit;
		}
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			$data['data'] = $query->result();
		}
		else {
			$data['data'] = array();
		}
		return $data;
	}

	public function create($obj) {
		$this->db->trans_start();

		$data = get_object_vars($obj);
		unset($data['uid']);
		unset($data['kode']);
		unset($data['details']);
		$this->db->set('kode', 'func_get_kode_pabrik()', FALSE);
		$this->db->set('uid', 'UUID()', FALSE);
		$data['created_by'] = $this->session->userdata('auth_user');
		$data['created_at'] = date('Y-m-d H:i:s');

		$this->db->insert($this->table_def, $data);
		$obj->id =  $this->db->insert_id();

		$this->_saveDetail($obj);

		$this->db->trans_complete();
		if ($this->db->trans_status() === TRUE) {
		  $this->db->trans_commit();
		  return TRUE;
		} else {
		  $this->db->trans_rollback();
		  return false;
		}
	}

	public function update($obj) {
		$this->db->trans_start();

		$data = get_object_vars($obj);
		unset($data['uid']);
		unset($data['details']);
		$data['update_by'] = $this->session->userdata('auth_user');
		$data['update_at'] = date('Y-m-d H:i:s');

		$this->db->where('uid', $obj->uid);
		$this->db->update($this->table_def, $data);

		$this->_saveDetail($obj);

		$this->db->trans_complete();
		if ($this->db->trans_status() === TRUE) {
		  $this->db->trans_commit();
		  return $obj->uid;
		} else {
		  $this->db->trans_rollback();
		  return false;
		}
	}

	public function update_status($uid, $status) {
		$this->db->trans_start();

		$data = array();
		$data['update_by'] = $this->session->userdata('auth_user');
		$data['update_at'] = date('Y-m-d H:i:s');
		$data['status'] = $status;

		$this->db->where('uid', $uid);
		$this->db->update($this->table_def, $data);

		$this->db->trans_complete();
		if ($this->db->trans_status() === TRUE) {
		  $this->db->trans_commit();
		  return $uid;
		} else {
		  $this->db->trans_rollback();
		  return false;
		}
	}

	private function _saveDetail($obj) {
		if (! property_exists($obj, 'details')) return;
		foreach ($obj->details as $detail) {
			$data = get_object_vars($detail);
			unset($data['id']);
			unset($data['data_mode']);
			$data['update_by'] = $this->session->userdata('auth_user');
        	$data['update_at'] = date('Y-m-d H:i:s');
            switch ($detail->data_mode) {
                case DATA_MODE_MODEL::DATA_MODE_ADD:
                	$data['pabrik_id'] = $obj->id;
					$data['created_by'] = $this->session->userdata('auth_user');
	        		$data['created_at'] = date('Y-m-d H:i:s');
					$this->db->insert($this->table_def_detail_distributor, $data);
                    break;
                case DATA_MODE_MODEL::DATA_MODE_EDIT:
                    $this->db->where('id', $detail->id)
                    		->update($this->table_def_detail_distributor, $data);
                    break;
                case DATA_MODE_MODEL::DATA_MODE_DELETE:
                    $this->db->where('id', $detail->id)
                    		->delete($this->table_def_detail_distributor); 
                    break;
            }
        }		
	}
}

?>