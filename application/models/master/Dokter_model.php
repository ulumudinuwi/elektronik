<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Model_Eloquent;
class Dokter_model extends Model_Eloquent
{
    protected $table = 'm_dokter_member';
    public $timestamps = true;

    public static function findByUID($uid){
        return self::where('uid', $uid)->first();
    }
}
