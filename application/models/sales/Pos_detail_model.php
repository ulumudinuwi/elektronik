<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Model_Eloquent;
class Pos_detail_model extends Model_Eloquent
{
    protected $table = 't_pos_detail';
    public $timestamps = true;
}
