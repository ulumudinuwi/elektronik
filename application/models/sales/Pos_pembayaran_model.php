<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Capsule\Manager as DB;
use Illuminate\Database\Eloquent\Model as Model_Eloquent;
use Carbon\Carbon;
use Ryuna\Auth;

class Pos_pembayaran_model extends Model_Eloquent
{
    public static $table_barang = 'm_barang';
    public static $table_stock_barang = 't_gudang_farmasi_stock';
    public static $table_dokter = 'm_dokter_member';
    public static $table_sales = 'm_sales';
    public static $table_pos = 't_pos';
    public static $table_pos_detail = 't_pos_detail';
    public static $table_pos_pembayaran = 't_pos_pembayaran';
    public static $table_pos_riwayat_approval = 't_pos_riwayat_approval';
    public static $table_pos_batal = 't_pos_batal';
    public static $table_pos_riwayat = 't_pos_riwayat';

    protected $table = 't_pos_pembayaran';
    public $timestamps = true;

    public static function pembayaran_datatables_template($field = [], $sWhere = []){
        if(empty($field)) return false;

        $query = DB::table(self::$table_pos_pembayaran);
        $query->select([
            self::$table_pos.'.id',
            self::$table_pos.'.uid as uid_pos',
            self::$table_pos.'.no_invoice',
            self::$table_pos.'.is_approved',
            self::$table_dokter.'.uid as uid_dokter',
            self::$table_dokter.'.nama as nama_dokter',
            self::$table_dokter.'.no_member as no_member',
            self::$table_sales.'.nama as nama_sales',
            self::$table_pos_pembayaran.'.uid as uid_pembayaran',
            self::$table_pos_pembayaran.'.no_kuitansi as no_kuitansi',
            self::$table_pos_pembayaran.'.created_at as tanggal',
        ]);
            
        $query->join(self::$table_pos,self::$table_pos.'.id','=',self::$table_pos_pembayaran.'.pos_id');
        $query->join(self::$table_dokter,self::$table_dokter.'.id','=',self::$table_pos_pembayaran.'.member_id');
        $query->join(self::$table_sales,self::$table_sales.'.id','=',self::$table_pos_pembayaran.'.sales_id');

        $filter_range = $_REQUEST['filter_range'];
        // dd($filter_range);
        $query->whereDate(self::$table_pos_pembayaran.'.created_at','>=',$filter_range[0]);
        $query->whereDate(self::$table_pos_pembayaran.'.created_at','<=',$filter_range[1]);
        if(!empty($sWhere)){
            foreach($sWhere as $_field => $_search){
                $query->where($_field, $_search);
            }
        }
        if (Auth::user()->bpom == 1) {
            $query->where(self::$table_pos.".bpom", Auth::user()->bpom);
        }
        
		$draw= $_REQUEST ? $_REQUEST['draw'] : 0;

		$length= $_REQUEST ? $_REQUEST['length'] : 10;

		$start= $_REQUEST ? $_REQUEST['start'] : 0;

		$search = $_REQUEST ? $_REQUEST['search']["value"] : '';
        
        $order = $_REQUEST && isset($_REQUEST['order']) ? $_REQUEST['order'][0] : null;
        
        $total= $query;
		$total= $total->count();

		$output = [];

		$output['draw'] = $draw;

		$output['recordsTotal'] = $output['recordsFiltered'] = $total;

		$output['data'] = [];

		if($search != ""){
            $sNewColumn = "";
            $new_column = [];
            for($i = 0; $i < count($_REQUEST['columns']); $i++){
                if($_REQUEST){
                    $is_searchable = $_REQUEST['columns'][$i]['searchable'];
                    if($is_searchable == "true"){
                        $new_column[] = $field[$i].' LIKE "%'.$search.'%"';
                    }
                }
            }
            
            if(count($new_column) > 0) $sNewColumn = implode(' OR ', $new_column);

            if($sNewColumn) $query->whereRaw('('.$sNewColumn.')');

            $countSearch = $query;
            $countSearch = $countSearch->count(); 
            $output['recordsTotal'] = $output['recordsFiltered'] = $countSearch;
        }

		$query->skip($start);
        $query->limit($length);

        for($i = 0; $i < count($field); $i++){
            if($_REQUEST){
                $is_orderable = $_REQUEST['columns'][$i]['orderable'];
                if($is_orderable){
                    if($order['column'] == $i){
                        $query->orderBy($field[$i],$order['dir']);
                    }
                }
            }
        }
		$results = $query->get();

        $output['data'] = $results;

		return $output;
    }

    public static function list_invoice_page(){
        $request = request_handler(); 
        $page = (!empty( (Array) $request) && $request->page) ? $request->page : 1;
        $resultCount = 10;

        $offset = ($page - 1) * $resultCount;

        $query = DB::table(self::$table_pos);
        $query->select([
            self::$table_pos.'.id as id',
            self::$table_pos.'.uid as uid_pos',
            self::$table_pos.'.no_invoice as text',
            self::$table_pos.'.no_invoice',
            self::$table_pos.'.is_approved',
            self::$table_pos.'.tanggal_transaksi',
            self::$table_dokter.'.uid as uid_dokter',
            self::$table_dokter.'.nama as nama_dokter',
            self::$table_dokter.'.no_member as no_member',
            self::$table_sales.'.uid as uid_sales',
            self::$table_sales.'.nama as nama_sales',
        ]);
        $query->join(self::$table_sales, self::$table_pos.'.sales_id', '=',self::$table_sales.'.id');
        $query->join(self::$table_dokter, self::$table_pos.'.member_id', '=',self::$table_dokter.'.id');

        $query->where(self::$table_pos.'.status_lunas','=', '0');
        $query->where(self::$table_pos.'.is_approved','=', '1');
        $query->where(self::$table_pos.'.status_pembayaran','=', '2');

        if (Auth::user()->bpom == 1) {
            $query->where(self::$table_pos.".bpom", Auth::user()->bpom);
        }

        if($request->term){
            $sCol = [
                self::$table_dokter.'.nama LIKE "%'.$request->term.'%"',
                self::$table_sales.'.nama LIKE "%'.$request->term.'%"',
                //self::$table_pos.'.tanggal_transaksi LIKE %'.$request->term.'%',
                self::$table_pos.'.no_invoice LIKE "%'.$request->term.'%"',
            ];
    
            $sNCol = implode(' OR ', $sCol);
            $query->whereRaw('('.$sNCol.')');
        }
        // $query->orWhere(self::$table_dokter.'.nama', 'LIKE','%'.$request->term.'%');
        // $query->orWhere(self::$table_sales.'.nama', 'LIKE','%'.$request->term.'%');
        // $query->orWhere(self::$table_pos.'.tanggal_transaksi', 'LIKE','%'.$request->term.'%');

        $query->orderBy(self::$table_pos.'.tanggal_transaksi', 'DESC');

        $total_count = $query->count();
        
        $query->skip($offset);
        $query->take($resultCount);

        $results = $query->get();
                
        $count = $total_count;
        $endCount = $offset + $resultCount;
        $morePages = $count > $endCount;

        $results = array(
          "results" => $results,
          "pagination" => array(
            "more" => $morePages
		  ),
		  "total_count" => $count,
        );
        return $results;
    }

    public static function pembayaran_cicilan($object){
        /**
         * Aturan
         */
        // $object = [
        //     'pos' => [ ],
        //     'pembayaran' => [ 
        //          'metode_pembayaran' => '',
        //          'sisa_pembayaran' => '',
        //          '' => '',
        //      ],
        //     'dokter' => [ ],
        //     'sales' => [ ],
        // ];
        $CI =& get_instance();
        $CI->load->helper('laba_rugi_helper');
        $total_pembayaran = $object->pembayaran->total_pembayaran;
        $sisa_pembayaran = $object->pembayaran->sisa_pembayaran;
        $pos_id = $object->pos->id;
        $get_cicilan = cicilan_ke($pos_id)['cicilan_ke'];

        $status_lunas = $sisa_pembayaran == 0 ? 1 : 0; 
        // dd($get_cicilan, $sisa_pembayaran);
        DB::beginTransaction();
        try {
            $pos = DB::table(self::$table_pos)->where('id', $object->pos->id)->update([
                'status_lunas' => $status_lunas,
                'total_pembayaran' => $total_pembayaran,
                'sisa_pembayaran' => $sisa_pembayaran,
                'piutang' => 0,
                'updated_at' => timestamp(),
                'updated_by' => auth_id(),
            ]);

            $is_cicilan = $sisa_pembayaran > 0 ? 1 : 0;
            if($get_cicilan == 1 && $status_lunas) {
                $is_cicilan = 0;
                $cicilan_ke = 0;
            }else if($get_cicilan > 1 && $status_lunas){
                $is_cicilan = 1;
                $cicilan_ke = $get_cicilan;
            }else{
                $is_cicilan = 1;
                $cicilan_ke = $get_cicilan;
            
            }
            // dd($is_cicilan, $cicilan_ke);
            $no_kuitansi = get_no_kuitansi();
            foreach($object->pembayaran->metode_pembayaran as $item){
                $pembayaran = DB::table(self::$table_pos_pembayaran)->insertGetId([
                    'uid' => uuid(),
                    'tanggal_bayar' => $item->tanggal_bayar,
                    'no_kuitansi' => $no_kuitansi,
                    'no_receipt' => $item->receipt_no,
                    'member_id' => $object->dokter->id,
                    'sales_id' => $object->sales->id,
                    'pos_id' => $object->pos->id,
                    'total_harus_dibayar' => $object->pos->grand_total,
                    'jenis_pembayaran' => $item->cara_bayar,
                    'nominal_pembayaran' => $item->nominal,
                    'sisa_pembayaran' => $sisa_pembayaran,
                    'bank_id' => $item->cara_bayar != 'tunai' ? $item->bank_id : 0,
                    'mesin_edc_id' => $item->cara_bayar != 'tunai' ? $item->mesin_edc_id : 0,
                    'is_cicilan' => $is_cicilan,
                    'cicilan_ke' => $cicilan_ke,
                    'created_at' => timestamp(),
                    'created_by' => auth_id(),
                    'updated_at' => timestamp(),
                    'updated_by' => auth_id(),
                ]);

                /*$dataLabaRugi = new stdClass();
                $dataLabaRugi->transaksi_id = $pembayaran;
                $dataLabaRugi->modul = JENIS_MODUL_POS;
                $dataLabaRugi->total = $item->nominal;
                $dataLabaRugi->keterangan = 'Pembayaran POS dengan metode pembayaran '.$item->cara_bayar.' <b>No Kwitansi : '.$no_kuitansi.'</b>';
                $dataLabaRugi->perkiraan_id = get_option('perkiraan_pos');
                $dataLabaRugi->tipe = JENIS_KREDIT;
                //insert
                insertLabarugi($dataLabaRugi);
                    
                $dataLabaRugi->perkiraan_id = get_option('perkiraan_kas');
                $dataLabaRugi->tipe = JENIS_DEBIT;
                insertLabarugi($dataLabaRugi);*/
            }
            DB::commit();
            // all good
            return true;
        } catch (\Exception $e) {
            DB::rollback();
            print_r($e->getMessage());
            return false;
            // something went wrong
        }


    }

}
