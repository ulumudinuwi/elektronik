<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Laporan_model extends CI_Model {
	protected $table_def = "t_pos";
	protected $table_def_detail = "t_pos_detail";
	protected $table_def_sales = "m_sales";
	protected $table_def_dokter = "m_dokter_member";
	protected $table_def_barang = "m_barang";
	protected $table_def_mou = "t_diskon_mou";
	protected $table_def_retur = "t_retur";
	
	public function __construct() {
        parent::__construct();
    }
	
	private function _get_select($customSelect = "") {
		if($customSelect != "") {
			$select = $customSelect;
		} else {
			$select = array(
				"{$this->table_def}.tanggal_transaksi",
				"{$this->table_def}.tgl_jatuh_tempo",
				"{$this->table_def}.sisa_pembayaran",
				"{$this->table_def}.no_invoice",
				"{$this->table_def_detail}.id",
				"{$this->table_def_detail}.no_fbp",
				"{$this->table_def_detail}.qty",
				"{$this->table_def_detail}.harga",
				"{$this->table_def_detail}.total as jumlah",
				"{$this->table_def_detail}.is_bonus",
				"{$this->table_def_detail}.diskon",
				"{$this->table_def_detail}.diskon_plus",
				"{$this->table_def_detail}.grand_total as total",
				"{$this->table_def}.status_lunas",
				"{$this->table_def_dokter}.nama as dokter",
				"{$this->table_def_sales}.nama as marketing",
				"{$this->table_def_barang}.nama as barang",
			);
		}
		return 'SELECT '.implode(', ', $select).' ';
	}
	
	private function _get_from() {
		$from = "FROM {$this->table_def_detail}";
		return $from;
	}

	private function _get_join($customSelect = "") {
		$join = array(
			"LEFT JOIN {$this->table_def_dokter} ON {$this->table_def_detail}.member_id = {$this->table_def_dokter}.id",
			"LEFT JOIN {$this->table_def} ON {$this->table_def_detail}.pos_id = {$this->table_def}.id",
			"LEFT JOIN {$this->table_def_sales} ON {$this->table_def_detail}.sales_id = {$this->table_def_sales}.id",
			"LEFT JOIN {$this->table_def_barang} ON {$this->table_def_detail}.barang_id = {$this->table_def_barang}.id",
		);

		return implode(' ', $join);
	}
	
	public function get_by($sWhere = "", $customSelect = "") {
		$sql = $this->_get_select($customSelect)." ";
		$sql .= $this->_get_from()." ";
		$sql .= $this->_get_join($customSelect);
		if (!empty($sWhere)) {
			$sql .= " ".$sWhere;
		}
        $query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			return $query->row();
		}
		else {
			return false;
		}
    }
	
	public function get_all($iLimit = 10, $iOffset = 0, $sWhere = "", $sOrder = "", $customSelect = "") {
		
		$data = array();
		$sql_count = "SELECT COUNT({$this->table_def_detail}.id) AS numrows ";
		$sql_count .= $this->_get_from()." ";
		$sql_count .= $this->_get_join($customSelect);
		if (!empty($sWhere)) {
			$sql_count .= " ".$sWhere." ";
		}
		$query = $this->db->query($sql_count);
		if ($query->num_rows() == 0) {
			$data['total_rows'] = 0;
		}
		else {
			$row = $query->row();
			$data['total_rows'] = (int) $row->numrows;
		}
		
		$select = $this->_get_select($customSelect);
		$from = $this->_get_from();
		$join = $this->_get_join($customSelect);
		$sql = $select." ".$from." ".$join." ";
		if (!empty($sWhere)) {
			$sql .= $sWhere." ";
		}
		if (!empty($sOrder)) {
			$sql .= $sOrder." ";
		}
		if ($iLimit > 0) {
			$sql .= "LIMIT ".$iOffset.", ".$iLimit;
		}
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			$data['data'] = $query->result();
		}
		else {
			$data['data'] = array();
		}
		return $data;
	}

	private function _get_select_mou($customSelect = "") {
		if($customSelect != "") {
			$select = $customSelect;
		} else {
			$select = array(
				"{$this->table_def}.no_invoice",
				"{$this->table_def}.tanggal_transaksi",
				"{$this->table_def_dokter}.nama as dokter",
				"{$this->table_def_barang}.nama as barang",
				"{$this->table_def_sales}.nama as marketing",
				"{$this->table_def_mou}.diskon",
				"{$this->table_def_mou}.total_diskon",
				"{$this->table_def_mou}.id",
			);
		}
		return 'SELECT '.implode(', ', $select).' ';
	}
	
	private function _get_from_mou() {
		$from = "FROM {$this->table_def_mou}";
		return $from;
	}

	private function _get_join_mou($customSelect = "") {
		$join = array(
			"LEFT JOIN {$this->table_def_dokter} ON {$this->table_def_mou}.member_id = {$this->table_def_dokter}.id",
			"LEFT JOIN {$this->table_def_detail} ON {$this->table_def_mou}.pos_detail_id = {$this->table_def_detail}.id",
			"LEFT JOIN {$this->table_def} ON {$this->table_def_detail}.pos_id = {$this->table_def}.id",
			"LEFT JOIN {$this->table_def_sales} ON {$this->table_def_mou}.sales_id = {$this->table_def_sales}.id",
			"LEFT JOIN {$this->table_def_barang} ON {$this->table_def_mou}.barang_id = {$this->table_def_barang}.id",
		);

		return implode(' ', $join);
	}
	
	public function get_by_mou($sWhere = "", $customSelect = "") {
		$sql = $this->_get_select_mou($customSelect)." ";
		$sql .= $this->_get_from_mou()." ";
		$sql .= $this->_get_join_mou($customSelect);
		if (!empty($sWhere)) {
			$sql .= " ".$sWhere;
		}
        $query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			return $query->row();
		}
		else {
			return false;
		}
    }
	
	public function get_allMou($iLimit = 10, $iOffset = 0, $sWhere = "", $sOrder = "", $customSelect = "") {
		
		$data = array();
		$sql_count = "SELECT COUNT({$this->table_def_mou}.id) AS numrows ";
		$sql_count .= $this->_get_from_mou()." ";
		$sql_count .= $this->_get_join_mou($customSelect);
		if (!empty($sWhere)) {
			$sql_count .= " ".$sWhere." ";
		}
		$query = $this->db->query($sql_count);
		if ($query->num_rows() == 0) {
			$data['total_rows'] = 0;
		}
		else {
			$row = $query->row();
			$data['total_rows'] = (int) $row->numrows;
		}
		
		$select = $this->_get_select_mou($customSelect);
		$from = $this->_get_from_mou();
		$join = $this->_get_join_mou($customSelect);
		$sql = $select." ".$from." ".$join." ";
		if (!empty($sWhere)) {
			$sql .= $sWhere." ";
		}
		if (!empty($sOrder)) {
			$sql .= $sOrder." ";
		}
		if ($iLimit > 0) {
			$sql .= "LIMIT ".$iOffset.", ".$iLimit;
		}
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			$data['data'] = $query->result();
		}
		else {
			$data['data'] = array();
		}
		return $data;
	}

	private function _get_select_retur($customSelect = "") {
		if($customSelect != "") {
			$select = $customSelect;
		} else {
			$select = array(
				"{$this->table_def}.id",
				"{$this->table_def}.no_invoice",
				"{$this->table_def}.tanggal_transaksi",
				"{$this->table_def_dokter}.nama as dokter",
				"{$this->table_def_sales}.nama as marketing",
			);
		}
		return 'SELECT '.implode(', ', $select).' ';
	}
	
	private function _get_from_retur() {
		$from = "FROM {$this->table_def_retur}";
		return $from;
	}

	private function _get_join_retur($customSelect = "") {
		$join = array(
			"LEFT JOIN {$this->table_def} ON {$this->table_def_retur}.pos_id = {$this->table_def}.id",
			"LEFT JOIN {$this->table_def_dokter} ON {$this->table_def}.member_id = {$this->table_def_dokter}.id",
			"LEFT JOIN {$this->table_def_sales} ON {$this->table_def}.sales_id = {$this->table_def_sales}.id",
		);

		return implode(' ', $join);
	}
	
	public function get_by_retur($sWhere = "", $customSelect = "") {
		$sql = $this->_get_select_retur($customSelect)." ";
		$sql .= $this->_get_from_retur()." ";
		$sql .= $this->_get_join_retur($customSelect);
		if (!empty($sWhere)) {
			$sql .= " ".$sWhere;
		}
        $query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			return $query->row();
		}
		else {
			return false;
		}
    }
	
	public function get_all_retur($iLimit = 10, $iOffset = 0, $sWhere = "", $sOrder = "", $customSelect = "") {
		
		$data = array();
		$sql_count = "SELECT COUNT({$this->table_def_retur}.id) AS numrows ";
		$sql_count .= $this->_get_from_retur()." ";
		$sql_count .= $this->_get_join_retur($customSelect);
		if (!empty($sWhere)) {
			$sql_count .= " ".$sWhere." ";
		}
		$query = $this->db->query($sql_count);
		if ($query->num_rows() == 0) {
			$data['total_rows'] = 0;
		}
		else {
			$row = $query->row();
			$data['total_rows'] = (int) $row->numrows;
		}
		
		$select = $this->_get_select_retur($customSelect);
		$from = $this->_get_from_retur();
		$join = $this->_get_join_retur($customSelect);
		$sql = $select." ".$from." ".$join." ";
		if (!empty($sWhere)) {
			$sql .= $sWhere." ";
		}
		if (!empty($sOrder)) {
			$sql .= $sOrder." ";
		}
		if ($iLimit > 0) {
			$sql .= "LIMIT ".$iOffset.", ".$iLimit;
		}
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			$data['data'] = $query->result();
		}
		else {
			$data['data'] = array();
		}
		return $data;
	}
}

?>