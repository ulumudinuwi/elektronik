<?php

/**
 * Created by PhpStorm.
 * User: agungrizkyana
 * Date: 8/31/16
 * Time: 06:53
 */
class Pdf
{


    function print_pdf($html, $filename = "", $stream = TRUE)
    {
        $dompdf = new \Dompdf\Dompdf();
        $dompdf->loadHtml($html);
        $dompdf->render();
        if($stream){
            $dompdf->stream($filename);
        }else{
            $dompdf->stream($filename, array("Attachment" => 0));
//            return $dompdf->output();
        }
    }

}