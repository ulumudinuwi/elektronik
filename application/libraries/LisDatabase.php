<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once dirname(__FILE__) . '/lis/LisAbstract.php';

class LisDatabase extends LisAbstract {

    public function __construct($config = array()) {
        $default = array(
            'version' => '1.0',
            'driver' => 'sqlsrv',
            'host' => '127.0.0.1',
            'port' => '1433',
            'database' => 'lis_integration',
            'schema' => 'dbo',
            'user' => 'root',
            'password' => '',
            'tables' => array(
                'orders' => 'LabOrders',
                'resultHeader' => 'LabResult_Header',
                'resultDetail' => 'LabResult_Details',
                'orders_2' => 'list_order',
                'results_2' => 'resultlab',
            ),
        );

        $params = ['version', 'driver', 'host', 'port', 'database', 'schema', 'user', 'password', 'tables'];
        foreach ($params as $param) {
            $this->config[$param] = array_key_exists($param, $config) ? $config[$param] : $default[$param];
        }

        $this->connect();
    }

    /** 
     * Send Data to LIS
     */
    public function send($data) {
        switch ($this->config['version']) {
            case '1.0':
                $table = $this->config['driver'] == 'mysql' ? $this->config['database'].".".$this->config['tables']['orders'] : $this->config['schema'].'.'.$this->config['tables']['orders'];
                $sql = "INSERT INTO {$table} (message_id, message_dt, Version, Order_control, Pid, Apid, Pname, Address, Ptype, Birth_dt, Sex, Ono, Lno, Request_dt, Source, Clinician, Room_no, Priority, P_Status, Comment, Visitno, Order_testid, status, Read_status, Order_status) VALUES (:message_id, :message_dt, :version, :order_control, :pid, :apid, :pname, :address, :ptype, :birth_dt, :sex, :ono, :lno, :request_dt, :source, :clinician, :room_no, :priority, :p_status, :comment, :visitno, :order_testid, :status, :read_status, :order_status)";

                try {
                    $this->conn->beginTransaction();

                    $stmt = $this->conn->prepare($sql);
                    $result = $stmt->execute($data);
                    $insertId = $this->conn->lastInsertId();

                    $this->conn->commit();
                    return true;
                } catch (PDOException $e) {
                    print_r($data);
                    echo $e->getMessage();
                    $this->logsError[] = "[".$e->getCode()."]: ".$e->getMessage();
                    $this->conn->rollback();
                    return false;
                }
                break;
            case '1.1':
                $table = $this->config['driver'] == 'mysql' ? $this->config['database'].".".$this->config['tables']['orders_2'] : $this->config['schema'].'.'.$this->config['tables']['orders_2'];
                $sql = "INSERT INTO {$table} (message_id, message_dt, order_control, pid, apid, pname, address, ptype, birt_dt, sex, ono, lno, request_dt, source, clinician, room_no, priority, p_status, comment, visitno, order_testid, insurer, rujukandr, status) VALUES (:message_id, :message_dt, :order_control, :pid, :apid, :pname, :address, :ptype, :birt_dt, :sex, :ono, :lno, :request_dt, :source, :clinician, :room_no, :priority, :p_status, :comment, :visitno, :order_testid, :insurer, :rujukandr, :status)";

                try {
                    $this->conn->beginTransaction();

                    $stmt = $this->conn->prepare($sql);
                    $result = $stmt->execute($data);
                    $insertId = $this->conn->lastInsertId();

                    $this->conn->commit();
                    return true;
                } catch (PDOException $e) {
                    print_r($data);
                    echo $e->getMessage();
                    $this->logsError[] = "[".$e->getCode()."]: ".$e->getMessage();
                    $this->conn->rollback();
                    return false;
                }
                break;
        }
    }

    /**
     * Get Data from LIS
     */
    public function getHeader($ono) {
        switch ($this->config['version']) {
            case '1.0':
                $table = $this->config['driver'] == 'mysql' ? $this->config['database'].".".$this->config['tables']['resultHeader'] : $this->config['schema'].'.'.$this->config['tables']['resultHeader'];
                if (!$this->conn) return null;
                $sql = "SELECT * FROM {$table} WHERE ono = :ono";

                try {
                    $stmt = $this->conn->prepare($sql);
                    $stmt->execute(['ono' => $ono]);
                    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

                    return $result;
                } catch (PDOException $e) {
                    $this->logsError[] = $e->getMessage();
                    return null;
                }
                break;
            case '1.1':
                $table = $this->config['driver'] == 'mysql' ? $this->config['database'].".".$this->config['tables']['results_2'] : $this->config['schema'].'.'.$this->config['tables']['results_2'];
                if (!$this->conn) return null;
                $sql = "SELECT * FROM {$table} WHERE ono = :ono";

                try {
                    $stmt = $this->conn->prepare($sql);
                    $stmt->execute(['ono' => $ono]);
                    $result = $this->_parseHeader($stmt->fetchAll(PDO::FETCH_ASSOC));

                    return $result;
                } catch (PDOException $e) {
                    $this->logsError[] = $e->getMessage();
                    return null;
                }
                break;
        }
    }

    public function getDetail($ono, $order_testid) {
        $obxFields = ['test_code', 'test_name', 'data_type', 'result_value', 'unit', 'flag', 'reference_range', 'status', 'test_comment', 'validated_by', 'validated_on'];
        switch ($this->config['version']) {
            case '1.0':
                $table = $this->config['driver'] == 'mysql' ? $this->config['database'].".".$this->config['tables']['resultDetail'] : $this->config['schema'].'.'.$this->config['tables']['resultDetail'];
                if (!$this->conn) return null;
                $sql = "SELECT * FROM {$table} WHERE ono = :ono AND order_testid LIKE '".$order_testid."^%'";

                try {
                    $collection = array();

                    $stmt = $this->conn->prepare($sql);
                    $stmt->execute(['ono' => $ono]);
                    $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
                    
                    foreach ($results as $result) {
                        $detail = new stdClass();
                        foreach ($obxFields as $field) {
                            $detail->$field = array_key_exists($field, $result) ? $result[$field] : '';
                        }
                        $collection[] = $detail;
                    }

                    return $collection;
                } catch (PDOException $e) {
                    $this->logsError[] = $e->getMessage();
                    return null;
                }
                break;
            case '1.1':
                $table = $this->config['driver'] == 'mysql' ? $this->config['database'].".".$this->config['tables']['results_2'] : $this->config['schema'].'.'.$this->config['tables']['results_2'];
                $sql = "SELECT * FROM {$table} WHERE ono = :ono AND ordertest LIKE '".$order_testid."^%'";

                try {
                    $collection = array();
                    
                    $stmt = $this->conn->prepare($sql);
                    $stmt->execute(['ono' => $ono]);

                    $row = $stmt->fetch(PDO::FETCH_ASSOC);

                    if ($row) {
                        $obxText = $row['result_value'];
                        $obxLines = explode("\n", (str_replace("\r", "", $obxText)));
                        foreach ($obxLines as $line) {
                            if (empty(trim($line))) continue;
                            $line = explode("=", $line)[1];
                            $detail = new stdClass();
                            $obx = explode("|", $line);
                            foreach ($obxFields as $i => $field) {
                                $detail->$field = array_key_exists($i, $obx) ? $obx[$i] : "";
                            }
                            $collection[] = $detail;
                        }
                    }

                    return $collection;
                } catch (PDOException $e) {
                    $this->logsError[] = $e->getMessage();
                    return null;
                }
                break;
        }
    }

    public function cancel($ono) {
        $table = $this->config['driver'] == 'mysql' ? $this->config['database'].".".$this->config['tables']['orders'] : $this->config['schema'].'.'.$this->config['tables']['orders'];
        $sql = "UPDATE {$table} SET order_control = 'CA' WHERE ono = ?";

        try {
          $this->conn->beginTransaction();

          $stmt = $this->conn->prepare($sql);

          $result = $stmt->execute([$ono]);

          $this->conn->commit();
        } catch (PDOException $e) {
          $this->conn->rollback();
          echo $e->getMessage();
        }
    }

    /**
     * Clean
     */
    public function clean($lis_id) {
        // TODO
    }

    public function connect() {
        $driver = $this->config['driver'];
        $serverName = "tcp:{$this->config['host']},{$this->config['port']}";
        $database = $this->config['database'];
        $uid = $this->config['user'];
        $pwd = $this->config['password'];

        $this->logsInfo[] = "Connecting to Server {$this->config['host']}:{$this->config['port']}.";

        try {
          //Establishes the connection
          $this->conn = new PDO( "$driver:server=$serverName ; Database = $database", $uid, $pwd);
          $this->conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
          $this->isConnected = true;
          $this->logsInfo[] = "Connection Established to Server {$this->config['host']}:{$this->config['port']}.";
          return true;
        } catch (PDOException $e) {
          $this->logsError[] = "Failed to get DB handle: " . $e->getMessage();
          $this->isConnected = false;
          return false;
        }
    }

    public function disconnect() {
        $this->logsInfo[] = "Disconnecting.";
        $this->isConnected = false;
        $this->conn = null;
    }

    public function checkConnection() {
        $this->logsInfo[] = "Checking Connection to Server.";
        exec(sprintf('ping -c 1 -W 5 %s', escapeshellarg($this->config['host'])), $res, $rval);
        return $rval === 0;
    }


    private function _isTableExists($table) {
        // Try a select statement against the table
        // Run it in try/catch in case PDO is in ERRMODE_EXCEPTION.
        try {
            $result = $this->conn->query("SELECT 1 FROM $table LIMIT 1");
        } catch (Exception $e) {
            // We got an exception == table not found
            return FALSE;
        }

        // Result is either boolean FALSE (no table found) or PDOStatement Object (table found)
        return $result !== FALSE;
    }

    private function _parseHeader($results) {
        foreach ($results as $result) {
            $header = array(
                'message_id' => $result['message_id'],
                'message_dt' => $result['message_dt'],
                'pid' => $result['pid'],
                'apid' => $result['apid'],
                'pname' => $result['pname'],
                'ono' => $result['ono'],
                'lno' => $result['lno'],
                'request_dt' => $result['request_dt'],
                'clinician' => $result['clinician'],
                'priority' => $result['priority'],
                'comments' => $result['comments'],
                'visitno' => $result['visitno'],
                // 'order_testid' => ,
                // 'result_status' => ,
            );
        }
    }

    public function parseData($data) {
        switch ($this->config['version']) {
            case '1.0':
                return parent::parseData($data);
                break;
            case '1.1':
                return $this->_parseData11($data);
        }
    }

    private function _parseData11($data) {
        $kodeChunk = explode('-', $data->kode_transaksi);
        $temp = [];
        foreach ($data->details as $detail) {
          $temp[] = (isset($this->settings['kode_lis']) && $this->settings['kode_lis']) ? $detail->kode_lis : $detail->kode;
        }
        $order_testid = implode('~', $temp);
        $data = array(
            'message_id' => 'O01', 
            'message_dt' => date('YmdHis'), 
            'order_control' => 'NW', 
            'pid' => $data->no_rekam_medis ? : "", 
            'apid' => '', 
            'pname' => $data->nama ? : "", 
            'address' => substr($data->alamat, 0, 200), 
            'ptype' => $data->layanan_jenis == 1 || $data->layanan_jenis == 2 ? 'OP' : 'IP', 
            'birt_dt' => date('YmdHis', strtotime($data->tgl_lahir)), 
            'sex' => $data->jenis_kelamin == 0 ? '2' : '1', 
            'ono' => $data->kode_transaksi ? : "", 
            'lno' => '', 
            'request_dt' => date('YmdHis'), 
            'source' => $data->layanan_jenis == 1 || $data->layanan_jenis == 2 ? 'OPD^Outpatient Department' : 'IPD^Inpatient Department', 
            'clinician' => '101^'.$data->dokter, 
            'room_no' => $data->bed ? : "", 
            'priority' => 'R', 
            'p_status' => '0', 
            'comment' => 'SIMRS iMedis', 
            'visitno' => '', 
            'order_testid' => $order_testid ? : "", 
            'insurer' => '', 
            'rujukandr' => '', 
            'status' => '0',
        );

        return $data;
    }
}