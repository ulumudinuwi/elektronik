<?php
namespace Ryuna;
require(APPPATH.'/libraries/Ryuna/Rules/UniqueRule.php');
require(APPPATH.'/libraries/Ryuna/Rules/MinimalUangRule.php');
use Rakit\Validation\Validator;

/**
 * Validasi Base from Rakit/validation
 * 
 * @package Ryuna
 * @author Ari Ardiansyah
 * @author Rakit/Validtaion Rakit Lab 
 */
class Validasi {
    
    /**
     * Validate input
     * 
     * @param array $input
     * @param array $input 
     */
    public static function buat($input, $rule){
        $input = (Array) $input;
        $CI =& get_instance();
        $config = $CI->db;
        $pdo =  new \PDO('mysql:host='.$config->hostname.';dbname='.$config->database.'', $config->username, $config->password);
        $validator = new Validator;
        // make it
        $validator->addValidator('unique', new \UniqueRule($pdo));
        $validator->addValidator('min_uang', new \MinimalUangRule());
        $validator->setMessages([
			'required' => ':attribute harus diisi.',
            'numeric' => ':attribute harus berupa angka.',
            'same' => ':attribute harus sama dengan :field',
            'unique' => ':attribute :value telah digunakan',
            'min_uang' => ':attribute minimal :nominal',
			// etc
		]);
        $validation = $validator->make($input, $rule);

        // then validate
        $validation->validate();

        if ($validation->fails()) {
            // handling errors
            $errors = $validation->errors();
            $validasi = new \stdClass;
            $validasi->status  = false;
            $validasi->message = $errors->firstOfAll(); 
            return $validasi;
        } else {
            // validate success
            $validasi = new \stdClass;
            $validasi->status = true;
            return $validasi;
        }
    }   
}