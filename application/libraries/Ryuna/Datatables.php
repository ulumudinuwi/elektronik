<?php
namespace Ryuna;

use Ryuna\CodeigniterAdapter;
use Ozdemir\Datatables\Datatables as Dt;

/**
 * Datatables Helper From Odzemir Awowokkowkwo
 * 
 * @package Ryuna
 * @author Ari Ardiansyah 
 */

class Datatables {

    protected static $CI;

    /**
     * Initialize Assign the CodeIgniter super-object
     */
    public static function init() {
        if(empty($CI)) self::$CI =& get_instance();
    }
    
    /**
     * Return Datatables
     * 
     * @param string $query
     */
    public static function of($query) {
        self::init();
        
        $datatables = new Dt(new CodeigniterAdapter);
        $datatables->query($query);
        return $datatables;
    }
}