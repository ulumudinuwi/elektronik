<?php
namespace Ryuna;

/**
 * Simple Response Library
 * 
 * @package Ryuna
 * @author Ari Ardiansyah 
 */

class Response {

    protected static $CI;

    /**
     * Initialize Assign the CodeIgniter super-object
     */
    public static function init() {
        if(empty($CI)) self::$CI =& get_instance();
    }
    
    /**
     * Return response Json
     * 
     * @param array $object
     * @param string $status
     */
    public static function json($object = [''],$status = 200) {
        self::init();
        return self::$CI->output
            ->set_content_type('application/json')
            ->set_output(json_encode($object))
            ->set_status_header($status);
    }

    public static function datatables($object = [''],$status = 200) {
        self::init();
        return self::$CI->output
            ->set_content_type('application/json')
            ->set_output($object)
            ->set_status_header($status);
    }
}