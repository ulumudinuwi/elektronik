<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once dirname(__FILE__) . '/lis/LisAbstract.php';

use phpseclib\Net\SFTP;

class LisFTP extends LisAbstract {

	public function __construct($config = array()) {
		$default = array(
			'host' => 'localhost',
			'port' => '22',
			'path' => '/',
			'user' => 'admin',
			'password' => 'admin password',
			'timeout' => 10,
		);

		$params = ['host', 'port', 'user', 'password', 'path', 'timeout'];
		foreach ($params as $param) {
			$this->config[$param] = array_key_exists($param, $config) ? $config[$param] : $default[$param];
		}

		$this->connect();
	}

	/** 
	 * Send Data to LIS
	 */
	public function send($data) {
		if (! $this->isConnected) return false;
		$file = $this->getRealFile($data['ono']);

		$data = $this->getText($data);

		if ($this->conn->put($file, $data)) {
			return true;
		} else {
			return false;
		}

	}

	/**
	 * Get Data from LIS
	 */
	public function getHeader($ono) {
		return null;
	}

	public function getDetail($ono, $test_code) {
		return null;
	}

	/**
	 * Clean
	 */
	public function clean($lis_id) {
		// TODO
	}

	public function cancel($ono) {
		// TODO
	}

	public function connect() {
		$this->logsInfo[] = "Connecting to Server {$this->config['host']}:{$this->config['port']}.";
	    $this->conn = new SFTP($this->config['host'], $this->config['port'], $this->config['timeout']);

	    if ($this->conn->login($this->config['user'], $this->config['password'])) {
	    	$this->isConnected = true;
	    	return true;
	    } else {
			$this->logsError[] = "Failed to connect to FTP server.";
			$this->isConnected = false;
			return false;
	    }
	}

	public function disconnect() {
		$this->conn = null;
		$this->isConnected = false;
	}

	public function checkConnection() {
		$this->logsInfo[] = "Checking Connection to Server.";
	    exec(sprintf('ping -c 1 -W 5 %s', escapeshellarg($this->config['host'])), $res, $rval);
	    return $rval === 0;
	}

	/** 
	 * Get send data as text
	 */
	private function getText($data) {
		$content = "";

		foreach ($data as $key => $val) {
			$content .= $key.": ".$val.PHP_EOL;
		}

		return $content;
	}

	private function getRealFile($filename) {
		return $this->config['path'].'/'.$filename;
	}
}